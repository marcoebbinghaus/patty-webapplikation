INSERT INTO
  benutzergruppe(id, gruppenname)VALUES (1, 'Guest');
INSERT INTO
  benutzergruppe(id, gruppenname)VALUES (2, 'User');
INSERT INTO
  benutzergruppe(id, gruppenname)VALUES (3, 'Admin');

INSERT INTO pk_gen(gen_key, gen_value) VALUES ('BenutzerGruppe_id', 4);


INSERT INTO
  safetyquestion(id, englishtext, germantext)VALUES (1, E'What is your first pets\' name?','Wie lautet der Name Ihres ersten Haustieres?');
INSERT INTO
  safetyquestion(id, englishtext, germantext)VALUES (2, 'Which is your favorite vacation resort?','Wie lautet der Name Ihres Lieblings-Urlaubsorts?');
INSERT INTO
  safetyquestion(id, englishtext, germantext)VALUES (3, E'In which year was your mother born? (yyyy)','In welchem Jahr wurde Ihre Mutter geboren? (jjjj)');

INSERT INTO pk_gen(gen_key, gen_value) VALUES ('SafetyQuestion_id', 4);


INSERT INTO
  benutzer(
    id, validated, email, failedlogins, hidden, lastlogin, loginname, password, validfrom, validuntil, benutzergruppe_id, safetyquestion_id, safetyanswer)
  VALUES (1, true , 'guest@patty.de', 0, false , '2012-09-11', 'guest', 'Rz+eCX7FxAKZCNbhi+tbsiEVCIcekgpxaVEOJ609368=', '2012-09-11', '2013-09-11', 1, 1, 'Blacky');

INSERT INTO
  benutzer(
    id, validated, email, failedlogins, hidden, lastlogin, loginname, password, validfrom, validuntil, benutzergruppe_id, safetyquestion_id, safetyanswer)
  VALUES (2, true , 'marco@patty.de', 0, false , '2012-09-11', 'marco', 'Rz+eCX7FxAKZCNbhi+tbsiEVCIcekgpxaVEOJ609368=', '2012-09-11', '2013-09-11', 2, 1, 'Blacky');

INSERT INTO
  benutzer(
    id, validated, email, failedlogins, hidden, lastlogin, loginname, password, validfrom, validuntil, benutzergruppe_id, safetyquestion_id, safetyanswer)
  VALUES (3, true , 'admin@patty.de', 0, false , '2012-09-11', 'admin', 'Rz+eCX7FxAKZCNbhi+tbsiEVCIcekgpxaVEOJ609368=','2012-09-11','2013-09-11', 3, 1, 'Blacky');

INSERT INTO pk_gen(gen_key, gen_value) VALUES ('Benutzer_id', 4);

INSERT INTO searchpattern(id, title, content, alltimevotecount, alltimescore, benutzer_id, description, creationdate, activated)
  VALUES (1, 'allgemein' , 'content:%INPUT% OR title:%INPUT% OR url:%INPUT%', 0, 0, 2, 'eine Beschreibung', '2013-07-20', true)
  , (2, 'b1' , 'content:%INPUT% OR title:%INPUT% OR url:%INPUT%', 0, 0, 2, 'eine Beschreibung', '2013-07-20', true)
  , (3, 'b2' , 'content:%INPUT% OR title:%INPUT% OR url:%INPUT%', 0, 0, 2, 'eine Beschreibung', '2013-07-20', true)
  , (4, 'b3' , 'content:%INPUT% OR title:%INPUT% OR url:%INPUT%', 0, 0, 2, 'eine Beschreibung', '2013-07-20', true)
  , (5, 'b4' , 'content:%INPUT% OR title:%INPUT% OR url:%INPUT%', 0, 0, 2, 'eine Beschreibung', '2013-07-20', true)
  , (6, 'b6' , 'content:%INPUT% OR title:%INPUT% OR url:%INPUT%', 0, 0, 2, 'eine Beschreibung', '2013-07-20', true)
  , (7, 'b5' , 'content:%INPUT% OR title:%INPUT% OR url:%INPUT%', 0, 0, 2, 'eine Beschreibung', '2013-07-20', true)
  , (8, 'bvcb' , 'content:%INPUT% OR title:%INPUT% OR url:%INPUT%', 0, 0, 2, 'eine Beschreibung', '2013-07-20', true)
  , (9, 'b7' , 'content:%INPUT% OR title:%INPUT% OR url:%INPUT%', 0, 0, 2, 'eine Beschreibung', '2013-07-20', false);

INSERT INTO pk_gen(gen_key, gen_value) VALUES ('SearchPattern_id', 10);