package de.patty.rmi.managers;

import de.patty.etc.Constants;
import de.patty.persistence.pojos.LogFile;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static de.patty.etc.Constants.DELIMITER;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/25/13
 * Time: 1:25 PM
 *
 * Gets all logfiles or a single logfile from the crawler system.
 */
public class LogfilesManager extends Manager {

    /**
     * Gets all logfiles (names and size) from the crawler system as a {@link LogFile} list.
     *
     * @return the logfiles
     * @throws NoConnectionException thrown if no connection to the crawler system is established.
     */
    public List<LogFile> getLogfiles() throws NoConnectionException {
        final String response = remoteCommunicator.sendRequestAndGetResponse(Constants.CRAWLER_COMMAND_GET_LOGFILES);
        if (response == null || response.equals("null")) {
            throw new NoConnectionException();
        }
        final List<LogFile> logFiles = new ArrayList<>();
        final String[] fileNamesAndSizes = response.split(DELIMITER);
        for (int i = 0; i < fileNamesAndSizes.length; i += 2) {
            final String fileName = fileNamesAndSizes[i];
            final Long fileSizeInKB = Long.valueOf(fileNamesAndSizes[i + 1]) / 1024;
            final LogFile logFile = new LogFile(fileName, fileSizeInKB);
            logFiles.add(logFile);
        }
        Collections.sort(logFiles);
        return logFiles;
    }

    /**
     * Gets the content of a single logfile.
     *
     * @param logFile the logfile whose content shall be returned.
     * @return the content of the logfile
     */
    public String getContentOfLogfile(final LogFile logFile) {
        StringBuilder commandStringBuilder = new StringBuilder();
        commandStringBuilder.append(Constants.CRAWLER_COMMAND_GET_LOGFILE_CONTENT + " ");
        commandStringBuilder.append(logFile.getFileName());
        return remoteCommunicator.sendRequestAndGetResponse(commandStringBuilder.toString());
    }
}
