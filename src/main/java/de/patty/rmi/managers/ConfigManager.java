package de.patty.rmi.managers;

import de.patty.webgui.ui.workingareas.administration.configuration.model.CrawlerParameter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static de.patty.etc.Constants.*;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 5/17/13
 * Time: 12:42 PM
 *
 * Communicates with the Crawler-System. Gets the configuration from the crawler system and sends the updated configuration.
 */
public class ConfigManager extends Manager {

    private StringBuilder stringBuilder;

    /**
     * Instantiates a new ConfigManager.
     */
    public ConfigManager() {
        stringBuilder = new StringBuilder();
    }

    /**
     * Sends a new configuration to the crawler system where it is saved / updated.
     *
     * @param crawlerParameters the crawler parameters (names and values)
     * @return true if the communication with the crawler system succeeded, false otherwise.
     */
    public Boolean setConfig(final List<CrawlerParameter> crawlerParameters) {
        stringBuilder.append(CRAWLER_COMMAND_SETCONFIG);
        for (final CrawlerParameter crawlerParameter : crawlerParameters) {
            if (crawlerParameter.getOverwrittenValue() != null) {
                stringBuilder.append(DELIMITER + crawlerParameter.getName() + DELIMITER + crawlerParameter.getOverwrittenValue());
            }
        }
        return Boolean.valueOf(remoteCommunicator.sendRequestAndGetResponse(stringBuilder.toString()));
    }

    /**
     * Gets the configuration from the nutch-site.xml on the crawler system as a string.
     *
     * @return the user config as string
     */
    public String getUserConfigAsString() {
        final String confString = remoteCommunicator.sendRequestAndGetResponse(CRAWLER_COMMAND_GET_CONF_USER);
        return confString;
    }

    /**
     * Gets the (default) configuration from the nutch-default.xml on the crawler system as a string.
     *
     * @return the default config as string
     */
    public String getDefaultConfigAsString() {
        final String confString = remoteCommunicator.sendRequestAndGetResponse(CRAWLER_COMMAND_GET_CONF_DEFAULT);
        return confString;
    }

    /**
     * Gets the configuration from the nutch-default.xml or nutch-site.xml on the crawler system as a {@link CrawlerParameter} List.
     *
     * @param onlyOverwrittenParameters boolean value if only overwritten parameters (parameters from nutch-site.xml)
     *                                  should be returned.
     * @return the configuration as {@link CrawlerParameter} list
     * @throws NoOverwrittenParametersException thrown if only overwritten parameters should be returned, but no
     * overwritten parameters exist.
     */
    public List<CrawlerParameter> getConfigAsParameterList(final boolean onlyOverwrittenParameters) throws NoOverwrittenParametersException {
        final List<CrawlerParameter> crawlerParameterList = new ArrayList<>();
        final String defaultConfString = remoteCommunicator.sendRequestAndGetResponse(CRAWLER_COMMAND_GET_CONF_DEFAULT);
        final String[] namesAndValues = defaultConfString.split(DELIMITER);
        for (int i = 0; i < namesAndValues.length; i += 3) {
            final String paramName = namesAndValues[i];
            final String paramDescription = namesAndValues[i + 1];
            final String paramValueString = namesAndValues[i + 2];
            final Object paramValueObject = parseParamValueString(paramValueString);
            final CrawlerParameter crawlerParameter = new CrawlerParameter();
            crawlerParameter.setName(paramName);
            crawlerParameter.setDescription(paramDescription);
            crawlerParameter.setDefaultValue(paramValueObject);
            crawlerParameterList.add(crawlerParameter);
        }
        final String userConfString = remoteCommunicator.sendRequestAndGetResponse(CRAWLER_COMMAND_GET_CONF_USER);
        final String[] namesAndValuesOfUserConf = userConfString.split(DELIMITER);
        try {
            for (int i = 0; i < namesAndValuesOfUserConf.length; i += 2) {
                final String paramName = namesAndValuesOfUserConf[i];
                final String paramValueString = namesAndValuesOfUserConf[i + 1];
                final Object paramValueObject = parseParamValueString(paramValueString);
                for (final CrawlerParameter crawlerParameter : crawlerParameterList) {
                    if (crawlerParameter.getName().equals(paramName)) {
                        crawlerParameter.setOverwrittenValue(paramValueObject);
                    }
                }
            }
        } catch (final ArrayIndexOutOfBoundsException e) {
            //do nothing
        }
        if (onlyOverwrittenParameters) {
            final Iterator<CrawlerParameter> iterator = crawlerParameterList.iterator();
            while (iterator.hasNext()) {
                final CrawlerParameter currentParameter = iterator.next();
                if (currentParameter.getOverwrittenValue() == null) {
                    iterator.remove();
                }
            }
        }
        if (crawlerParameterList.isEmpty()) {
            throw new NoOverwrittenParametersException();
        }
        return crawlerParameterList;
    }

    /**
     * Gets the crawler depth (count of IGFPU-runs) and the topN (count of urls that should be crawled per IGFPU-run)
     * from the crawler system.
     *
     * @return the depth and top n
     */
    public String getDepthAndTopN() {
        final String confString = remoteCommunicator.sendRequestAndGetResponse(CRAWLER_COMMAND_GET_PATTYRECRAWL_PARAMETERS);
        return confString;
    }

    /**
     * Sends the edited crawler depth (count of IGFPU-runs) and the topN (count of urls that should be crawled per IGFPU-run)
     * to the crawler system where it is saved.
     * @param depth the depth
     * @param topN the top n
     * @return true if the communication with the crawler system succeeded, false otherwise.
     */
    public Boolean setDepthAndTopN(final String depth, final String topN) {
        stringBuilder.append(CRAWLER_COMMAND_SET_PATTYRECRAWL_PARAMETERS + DELIMITER + depth + DELIMITER + topN);
        return Boolean.valueOf(remoteCommunicator.sendRequestAndGetResponse(stringBuilder.toString()));
    }

    private Object parseParamValueString(final String paramValueString) {
        Object paramValue = null;
        if (paramValueString.equals("")) {
            paramValue = paramValueString;
        } else {
            try {
                paramValue = Integer.valueOf(paramValueString);
            } catch (NumberFormatException e) {
                try {
                    paramValue = Double.valueOf(paramValueString);
                } catch (NumberFormatException e1) {
                    final String lowerCaseParamValue = paramValueString.toLowerCase();
                    switch (lowerCaseParamValue) {
                        case "true":
                            paramValue = true;
                            break;
                        case "false":
                            paramValue = false;
                            break;
                        default:
                            paramValue = paramValueString;
                            break;
                    }
                }
            }
        }
        return paramValue;
    }
}
