package de.patty.rmi.managers;

import de.patty.rmi.RemoteCommunicator;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/22/13
 * Time: 2:03 PM
 *
 * Abstract ManagerClass. Contains a {@link RemoteCommunicator} for communication with the crawler system.
 */
public abstract class Manager {

    /**
     * The Remote communicator.
     */
    protected RemoteCommunicator remoteCommunicator;

    /**
     * Instantiates a new Manager.
     */
    public Manager() {
        remoteCommunicator = new RemoteCommunicator();
    }
}
