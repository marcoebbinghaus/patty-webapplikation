package de.patty.rmi.managers;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/26/13
 * Time: 9:51 AM
 *
 * Is thrown if the communication with the crawler system failed because there was no connection established to it.
 */
public class NoConnectionException extends Throwable {
}
