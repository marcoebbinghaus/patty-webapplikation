package de.patty.rmi.managers;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static de.patty.etc.Constants.*;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 5/25/13
 * Time: 11:05 AM
 *
 * Communicates with the crawler system. Gets all urls from it and adds/removes urls.
 */
public class URLManager extends Manager {

    /**
     * Adds a list of URL strings on the crawler system (seeds.txt).
     *
     * @param urls the urls
     * @return true if the communication with the crawler system succeeded, false otherwise.
     */
    public Boolean addURLs(final List<String> urls) {
        final String urlsString = createFinalUrlsString(CRAWLER_COMMAND_ADDURLS, urls);
        return Boolean.valueOf(remoteCommunicator.sendRequestAndGetResponse(urlsString));
    }

    /**
     * Adds URLs to the crawler system (whitespace separated).
     *
     * @param urlsString the urls string
     * @return true if the communication with the crawler system succeeded, false otherwise.
     */
    public Boolean addURLs(final String urlsString) {
        return Boolean.valueOf(remoteCommunicator.sendRequestAndGetResponse(urlsString));
    }

    /**
     * Removes the given urls from the seeds.txt on the crawler system.
     *
     * @param urls the urls to remove (whitespace separated)
     * @return true if the communication with the crawler system succeeded, false otherwise.
     */
    public Boolean removeURLs(final List<String> urls) {
        final String urlsString = createFinalUrlsString(CRAWLER_COMMAND_DELETEURLS, urls);
        return Boolean.valueOf(remoteCommunicator.sendRequestAndGetResponse(urlsString));
    }

    /**
     * Removes the given urls fom the seeds.txt on the crawler system.
     *
     * @param urlsString the urls string
     * @return true if the communication with the crawler system succeeded, false otherwise.
     */
    public Boolean removeURLs(final String urlsString) {
        return Boolean.valueOf(remoteCommunicator.sendRequestAndGetResponse(urlsString));
    }

    /**
     * Gets a list of URLs to crawl (as strings) from the crawler system.
     *
     * @return the URLs which shall be crawled (which are listed in the seeds.txt on the crawler system)
     * @throws NullPointerException thrown if no URLs are in the seeds.txt
     */
    public List<String> getURLs() throws NullPointerException {
        final String[] urlArray = getURLsString().split(DELIMITER);
        final List<String> urlList = Arrays.asList(urlArray);
        Collections.sort(urlList);
        return urlList;
    }

    private String getURLsString() throws NullPointerException {
        final String urlStringFromCrawler = remoteCommunicator.sendRequestAndGetResponse(CRAWLER_COMMAND_GET_URLS);
        return urlStringFromCrawler;
    }

    private String createFinalUrlsString(final String command, final List<String> urls) {
        final StringBuilder sb = new StringBuilder();
        sb.append(command + " ");
        for (final String urlString : urls) {
            sb.append(urlString);
            sb.append(DELIMITER);
        }
        return sb.toString();
    }

}
