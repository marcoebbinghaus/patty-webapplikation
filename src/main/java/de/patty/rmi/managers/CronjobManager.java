package de.patty.rmi.managers;

import java.util.Arrays;
import java.util.List;

import static de.patty.etc.Constants.CRAWLER_COMMAND_GET_CRONJOB;
import static de.patty.etc.Constants.CRAWLER_COMMAND_SET_CRONJOB;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/12/13
 * Time: 10:06 AM
 * Communicates with the Crawler-System. Gets the cronjob interval from the crawler system and sends the updated interval.
 */
public class CronjobManager extends Manager {

    /**
     * Gets cronjob parts as string list.
     *
     * @return the cronjob parts as list
     */
    public List<String> getCronjobTimesAsList() {
        final String cronjobLine = remoteCommunicator.sendRequestAndGetResponse(CRAWLER_COMMAND_GET_CRONJOB);
        final String[] cronjobLineParts = cronjobLine.split("\\s");
        return Arrays.asList(cronjobLineParts);
    }

    /**
     * Gets the cronjob line from the crontab.
     *
     * @return the cronjob line
     */
    public String getCronjobLine() {
        return remoteCommunicator.sendRequestAndGetResponse(CRAWLER_COMMAND_GET_CRONJOB);
    }

    /**
     * Sends the cronjob to the crawler system as a stringlist.
     *
     * @param cronjobParts the cronjob parts
     * @return true if the communication with the crawler system succeeded, false otherwise.
     */
    public Boolean setCronjob(final List<String> cronjobParts) {
        StringBuilder cronjobLineStringBuilder = new StringBuilder();
        cronjobLineStringBuilder.append(CRAWLER_COMMAND_SET_CRONJOB + " ");
        for (final String cronjobPart : cronjobParts) {
            cronjobLineStringBuilder.append(cronjobPart + " ");
        }
        cronjobLineStringBuilder = cronjobLineStringBuilder.deleteCharAt(cronjobLineStringBuilder.length() - 1);
        return Boolean.valueOf(remoteCommunicator.sendRequestAndGetResponse(cronjobLineStringBuilder.toString()));
    }

    /**
     * Sends the cronjob line to the crawler system.
     *
     * @param cronjobLine the cronjob line
     * @return true if the communication with the crawler system succeeded, false otherwise.
     */
    public Boolean setCronjob(final String cronjobLine) {
        return Boolean.valueOf(remoteCommunicator.sendRequestAndGetResponse(CRAWLER_COMMAND_SET_CRONJOB + " " + cronjobLine));
    }
}
