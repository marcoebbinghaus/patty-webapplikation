package de.patty.rmi.managers;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/25/13
 * Time: 10:24 AM
 *
 * Thrown if all overwritten parameters were requested but none exist (if the nutch-site.xml is empty)
 */
public class NoOverwrittenParametersException extends Throwable {
}
