package de.patty.rmi.managers;

import de.patty.etc.Constants;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 18.05.13
 * Time: 10:52
 * Communicates with the Crawler-System. Controls the crawler (tells the crawler system to start/stop the crawler and
 * checks if the crawler is running)
 */
public class CrawlerManager extends Manager {

    /**
     * Sends the command to start the crawler to the crawler system.
     *
     * @return true if the communication with the crawler system succeeded, false otherwise.
     */
    public Boolean startCrawler() {
        return Boolean.valueOf(remoteCommunicator.sendRequestAndGetResponse(Constants.CRAWLER_COMMAND_START));
    }

    /**
     * Sends the command to stop the crawler to the crawler system.
     *
     * @return true if the communication with the crawler system succeeded, false otherwise.
     */
    public Boolean stopCrawler() {
        return Boolean.valueOf(remoteCommunicator.sendRequestAndGetResponse(Constants.CRAWLER_COMMAND_STOP));
    }

    /**
     * Asks the crawler system to check whether the crawler is running or not.
     *
     * @return null if there is no connection to the crawler system, or true if the crawler is running (false otherwise)
     */
    public Boolean checkRunning() {
        final String answer = remoteCommunicator.sendRequestAndGetResponse(Constants.CRAWLER_COMMAND_CHECKRUNNING);
        if (answer.equals("null")) {
            return null;
        } else {
            return Boolean.valueOf(answer);
        }
    }
}
