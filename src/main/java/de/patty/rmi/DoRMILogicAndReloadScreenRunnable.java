package de.patty.rmi;

import com.vaadin.ui.Notification;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.Screen;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/12/13
 * Time: 10:16 AM
 *
 * Executor class for communication with the crawler system where the screen is reloaded after the communication. Used
 * where the communication with the crawler system is done in an own thread.
 */
public abstract class DoRMILogicAndReloadScreenRunnable implements Runnable {

    private boolean success;
    private Screen screen;
    private MainPattyUI ui;
    private String errormessageName;
    private ResourceBundle messages;

    /**
     * Instantiates a new DoRMILogicAndReloadScreenRunnable.
     *
     * @param ui the ui
     * @param errormessageName the messagename (i18n) of the error which should be displayed if the communication
     *                         with the crawler system failed.
     */
    public DoRMILogicAndReloadScreenRunnable(final MainPattyUI ui, final String errormessageName) {
        this.screen = ui.getScreen();
        this.ui = ui;
        this.errormessageName = errormessageName;
        messages = ui.getResourceBundle();
    }

    @Override
    public void run() {
        ui.getSession().lock();
        try {
            success = doRMI();
            screen.reload();
            if (!success) {
                Notification.show(messages.getString(errormessageName));
            }
        } finally {
            ui.getSession().unlock();
        }

    }

    /**
     * The communication with the crawler system is done here.
     *
     * @return true if the communication with the crawler system succeeded, false otherwise.
     */
    public abstract boolean doRMI();
}
