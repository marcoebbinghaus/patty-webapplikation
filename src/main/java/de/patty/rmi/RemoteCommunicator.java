package de.patty.rmi;

import de.patty.etc.utilities.LoginDataSingleton;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 5/16/13
 * Time: 12:54 PM
 *
 * Does the plain RMI with the crawler system. Sends commands to the RemoteCommunication-Server program on the crawler
 * system which is waiting for commands.
 */
public class RemoteCommunicator {

    private static final Logger logger = Logger.getLogger(RemoteCommunicator.class);

    private Socket socket;

    /**
     * Sends a command and returns the response from the crawler system.
     *
     * @param command the command for the RemoteCommunication-Server program.
     * @return the answer from the crawler system which is true/false for "sender" methods or a corresponding response
     * (i.e. all URLs or the configuration) for getter methods.
     */
    public synchronized String sendRequestAndGetResponse(final String command) {
        try {
            socket = new Socket(LoginDataSingleton.getInstance().getString("crawler_system_ip"),
                    Integer.valueOf(LoginDataSingleton.getInstance().getString("crawler_system_port")));
            final OutputStream outputStream = socket.getOutputStream();
            outputStream.write(command.getBytes());
            outputStream.flush();
            socket.shutdownOutput();
            final String receivedMessage = receiveMessage();
            outputStream.close();
            socket.close();
            return receivedMessage;
        } catch (final IOException e) {
            logger.error("Fehler beim Senden einer Nachricht an den Crawler.");
            return null;
        }
    }


    private String receiveMessage() {
        try {
            final InputStream inputStream = socket.getInputStream();
            int c;
            final StringBuilder answer = new StringBuilder();
            while ((c = inputStream.read()) != -1) {
                answer.append((char) c);
            }
            inputStream.close();
            return answer.toString();
        } catch (final IOException e) {
            logger.error("IOException während receiveMessage");
            e.printStackTrace();
        }
        return null;
    }
}
