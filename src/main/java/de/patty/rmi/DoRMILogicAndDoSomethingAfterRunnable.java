package de.patty.rmi;

import de.patty.webgui.MainPattyUI;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/12/13
 * Time: 10:16 AM
 *
 * Executor class for communication with the crawler system and following actions after the communication. Used
 * where the communication with the crawler system is done in an own thread.
 */
public abstract class DoRMILogicAndDoSomethingAfterRunnable implements Runnable {

    /**
     * The Success.
     */
    protected boolean success;
    private MainPattyUI ui;
    private ResourceBundle messages;
    /**
     * The Exception.
     */
    protected Exception exception;

    /**
     * Instantiates a new DoRMILogicAndDoSomethingAfterRunnable.
     *
     * @param ui the ui
     */
    public DoRMILogicAndDoSomethingAfterRunnable(final MainPattyUI ui) {
        this.ui = ui;
        messages = ui.getResourceBundle();
    }

    @Override
    public void run() {
        ui.getSession().lock();
        try {
            success = doRMI();
            doSomethingAfterRMI();
        } finally {
            ui.getSession().unlock();
        }

    }

    /**
     * The communication with the crawler system is done here.
     *
     * @return true if the communication with the crawler system succeeded, false otherwise.
     */
    public abstract boolean doRMI();

    /**
     * All actions after the communication with the crawler system (i.e. closing windows, reload screen, ...) are done
     * here.
     */
    public abstract void doSomethingAfterRMI();
}
