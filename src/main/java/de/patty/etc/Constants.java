package de.patty.etc;

import java.util.Arrays;
import java.util.List;
/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/17/13
 * Time: 10:16 AM
 *
 * Contains all constants which are used in all other classes.
 */
public class Constants {

    public static final String DATE_FORMAT = "dd.MM.yyyy HH:mm";

    public static final String PATH_TO_DOWNLOADED_FILES = "/home/marco/Downloads";

    public static final String PATTY_LOGO = "images/patty.png";
    public static final String CRAWLER_RUNNING_IMAGE = "images/running.png";
    public static final String CRAWLER_NOT_RUNNING_IMAGE = "images/notrunning.png";
    public static final String CRAWLER_NOT_CONNECTED = "images/notconnected.png";

    public static final String ICON_RATE = "images/rate.png";
    public static final String ICON_DELETE = "images/delete.png";
    public static final String ICON_EDIT = "images/edit.png";
    public static final String ICON_ADD = "images/add.png";
    public static final String ICON_ACTIVATE = "images/unlock.png";
    public static final String ICON_INFO = "images/info.png";
    public static final String ICON_NOT_OK = "images/notok.png";
    public static final String ICON_OK = "images/ok.png";

    public static final String RATING_0_STARS = "images/0stars.png";
    public static final String RATING_1_STARS = "images/1stars.png";
    public static final String RATING_2_STARS = "images/2stars.png";
    public static final String RATING_3_STARS = "images/3stars.png";
    public static final String RATING_4_STARS = "images/4stars.png";
    public static final String RATING_5_STARS = "images/5stars.png";

    public static final String SOLR_URL = "http://localhost:8080/solr-patty/";
    public static final String MAILSERVER_URL = "smtp.gmail.com";
    public static final String NOREPLY_EMAIL_ADRESS = "noreply@patty-search.de";
    public static final String CONFIRMATION_CODE_PARAM_NAME = "confirm";


    public static final String MESSAGESBUNDLE = "MessagesBundle";

    public static final String TODO = " (TODO)";

    public static final String URL = "url";

    public static final int CHECKRUNNING_INTERVAL = 500;

    public static final String CRAWLER_COMMAND_ADDURLS = "addurls";
    public static final String CRAWLER_COMMAND_DELETEURLS = "deleteurls";
    public static final String CRAWLER_COMMAND_SETCONFIG = "setconf";
    public static final String CRAWLER_COMMAND_START = "start";
    public static final String CRAWLER_COMMAND_STOP = "stop";
    public static final String CRAWLER_COMMAND_CHECKRUNNING = "checkrunning";
    public static final String CRAWLER_COMMAND_GET_URLS = "geturls";
    public static final String CRAWLER_COMMAND_GET_CONF_DEFAULT = "getdefaultconf";
    public static final String CRAWLER_COMMAND_GET_CONF_USER = "getuserconf";
    public static final String CRAWLER_COMMAND_GET_CRONJOB = "getcronjobline";
    public static final String CRAWLER_COMMAND_SET_CRONJOB = "setcronjobline";
    public static final String CRAWLER_COMMAND_GET_PATTYRECRAWL_PARAMETERS = "getpattyrecrawlparams";
    public static final String CRAWLER_COMMAND_SET_PATTYRECRAWL_PARAMETERS = "setpattyrecrawlparams";
    public static final String CRAWLER_COMMAND_GET_LOGFILES = "getlogfiles";
    public static final String CRAWLER_COMMAND_GET_LOGFILE_CONTENT = "getlogfilecontent";

    public static final String DELIMITER = "----";

    public static final String PATTYSEARCH_INPUT_CONSTANT = "%INPUT%";

    public static final int HASH_COUNT = 5;
    public static final Integer PW_LENGTH = 8;
    public static final String ZEICHENSATZ = "UTF-8";
    public static final String GUESTACCOUNT = "guest";

    public static final String[] keyWords = new String[]{"content:", "title:", "url:", " AND ", " OR "};
    public static final List<String> SOLR_QUERY_KEYWORDS = Arrays.asList(keyWords);

    public static final String[] MINUTES = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12",
            "13", "14", "15", "16", "17", "19", "18", "20", "21", "22", "23"
            , "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35"
            , "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47"
            , "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59"};
    public static final String[] MONTH_DAYS = new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12",
            "13", "14", "15", "16", "17", "19", "18", "20", "21", "22", "23"
            , "24", "25", "26", "27", "28", "29", "30", "31"};

    public static String LOGIN_DATA = "LoginData";
}
