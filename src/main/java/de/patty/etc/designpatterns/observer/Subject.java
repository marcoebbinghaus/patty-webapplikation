package de.patty.etc.designpatterns.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 17.04.13
 * Time: 11:01
 *
 * Abstract Subject-class appropriate to the GOF Design Pattern Observer.
 */
public abstract class Subject {
    /**
     * The Observers.
     */
    protected List<Observer> observers;

    /**
     * Attaches an observer to the subject.
     *
     * @param observer the observer
     */
    public void attach(final Observer observer) {
        observers.add(observer);
    }

    /**
     * Detaches an observer from the subject.
     *
     * @param observer the observer
     */
    public void detach(final Observer observer) {
        observers.remove(observer);
    }

    /**
     * Calls the update()-method of every contained observer.
     */
    public void notifyObservers() {
        for (final Observer observer : observers) {
            observer.update();
        }
    }

    /**
     * Removes all contained observers.
     */
    public void removeAllObservers() {
        final List<Observer> observersCopy = new ArrayList<>(observers);
        for (final Observer observer : observersCopy) {
            detach(observer);
        }
    }
}
