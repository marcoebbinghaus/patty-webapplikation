package de.patty.etc.designpatterns.observer;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 17.04.13
 * Time: 11:05
 *
 * Observer-Interface appropriate to the GOF Design Pattern Observer.
 */
public interface Observer {

    /**
     * Method which is called on every observer that a subject contains, after the subject's state changed.
     */
    public void update();
}