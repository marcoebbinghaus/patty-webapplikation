package de.patty.etc.utilities;

import de.patty.etc.Constants;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/14/13
 * Time: 12:36 PM
 *
 * Encrypts a given password with the given salt.
 */
public class PasswordEncrypter {

    /**
     * Encrypts the password+salt in Base64.
     *
     * @param password the password to encrypt
     * @param theSalt the salt which is appended to the password before the encryption
     * @return the encrypted hash
     */
    public String encrypt(final String password, final String theSalt) {
        try {
            final MessageDigest digest = MessageDigest.getInstance(LoginDataSingleton.getInstance().getString("encryption_algorithm"));
            final BASE64Encoder encoder = new BASE64Encoder();
            final BASE64Decoder decoder = new BASE64Decoder();
            final byte[] salt = decoder.decodeBuffer(theSalt);
            digest.reset();
            digest.update(salt);
            byte[] btPass = digest.digest(password.getBytes(Constants.ZEICHENSATZ));
            for (int i = 0; i < Constants.HASH_COUNT; i++) {
                digest.reset();
                btPass = digest.digest(btPass);
            }
            final String encryptedPW = encoder.encode(btPass);
            return encryptedPW;
        } catch (final UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
