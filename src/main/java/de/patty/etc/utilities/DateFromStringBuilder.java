package de.patty.etc.utilities;

import java.util.Calendar;
import java.util.Date;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/25/13
 * Time: 1:36 PM
 *
 * Gets the Date from a string (Used to get the Date out of a logfile's name).
 */
public class DateFromStringBuilder {

    /**
     * Gets date from the logfile's name.
     *
     * @param logfileName the logfileName
     * @return the date from logfileName
     */
    public static Date getDateFromString(final String logfileName) {
        final String[] dateData = logfileName.split("_");
        final Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, Integer.valueOf(dateData[0]));
        calendar.set(Calendar.MONTH, Integer.valueOf(dateData[1]) - 1);
        calendar.set(Calendar.DAY_OF_MONTH, Integer.valueOf(dateData[2]));
        calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(dateData[3]));
        calendar.set(Calendar.MINUTE, Integer.valueOf(dateData[4]));
        return calendar.getTime();
    }
}
