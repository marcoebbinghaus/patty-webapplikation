package de.patty.etc.utilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/17/13
 * Time: 11:48 AM
 *
 * Creates a random password for a given length. It consists of lower- and uppercase letters and numbers.
 */
public class PasswordCreator {

    private Integer length;

    /**
     * Instantiates a new Password creator.
     *
     * @param length number of digits
     */
    public PasswordCreator(final Integer length) {
        this.length = length;
    }

    /**
     * Creates the password and returns it.
     *
     * @return the password
     */
    public String createPassword() {
        final List<Byte> byteList = new ArrayList<>();
        for (byte i = 48; i < 58; i++) {
            byteList.add(i);
        }
        for (byte i = 65; i < 90; i++) {
            byteList.add(i);
        }
        for (byte i = 97; i < 123; i++) {
            byteList.add(i);
        }
        final StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            Collections.shuffle(byteList);
            char c = (char) ((byte) byteList.get(0));
            sb.append(c);
        }
        return sb.toString();
    }
}
