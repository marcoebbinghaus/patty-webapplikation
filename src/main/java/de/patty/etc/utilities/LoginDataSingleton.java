package de.patty.etc.utilities;

import de.patty.etc.Constants;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/25/13
 * Time: 2:36 PM
 *
 * Returns an instance of a ResourceBundle which contains all security-related data like the email account
 * and password for the pattysearch@gmail.com email address.
 */

public class LoginDataSingleton {

    private static ResourceBundle loginData = ResourceBundle.getBundle(Constants.LOGIN_DATA);

    /**
     * Gets the ResourceBundle-Instance.
     *
     * @return the instance
     */
    public static ResourceBundle getInstance() {
        return loginData;
    }

    private LoginDataSingleton() {
    }
}