package de.patty.etc.utilities;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/17/13
 * Time: 10:16 AM
 *
 * Singleton which contains the state of a sent stop command (was a stop command sent or not?).
 */
public class StopCommandSentSingleton {
    private static volatile Boolean stopCommandSent;

    private StopCommandSentSingleton() {
    }

    /**
     * Returns true if a stop command was sent or false if not.
     *
     * @return boolean value if a stop command was sent
     */
    public static Boolean getStopCommandSent() {
        if (stopCommandSent == null) {
            synchronized (StopCommandSentSingleton.class) {
                if (stopCommandSent == null) {
                    stopCommandSent = new Boolean(Boolean.FALSE);
                }
            }
        }
        return stopCommandSent;
    }

    /**
     * Sets the boolean value of a sent stop command.
     *
     * @param stopCommandSent the boolean value
     */
    public static void setStopCommandSent(final Boolean stopCommandSent) {
        synchronized (StopCommandSentSingleton.class) {
            StopCommandSentSingleton.stopCommandSent = stopCommandSent;
        }
    }
}