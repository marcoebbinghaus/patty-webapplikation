package de.patty.etc.utilities;

import de.patty.etc.Constants;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/17/13
 * Time: 10:16 AM
 *
 * Offers methods to send a mail with a subject and a content to a receiver.
 */
public class MailSender {

    private Properties props = new Properties();
    private Session session;
    private Message message;

    /**
     * Instantiates a new Mail sender. Sets the needed properties.
     *
     * @throws MessagingException the messaging exception
     */
    public MailSender() throws MessagingException {
        props.put("mail.smtps.host", Constants.MAILSERVER_URL);
        props.put("mail.smtps.port", "465");
        props.put("mail.smtps.user", LoginDataSingleton.getInstance().getString("send_email_account"));
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.transport.protocol", "smtps");
        session = Session.getDefaultInstance(props, null);
        message = new MimeMessage(session);
        message.setFrom(new InternetAddress(Constants.NOREPLY_EMAIL_ADRESS));
    }

    /**
     * Tries to send the mail. Throws a MessaginException if the mail couldn't be sent.
     *
     * @param receiverAddress the receiver address
     * @param subject the email's subject
     * @param content the email's content
     * @throws MessagingException the messaging exception
     */
    public void sendMail(final String receiverAddress, final String subject, final String content) throws MessagingException {
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(receiverAddress));
        message.setSubject(subject);
        message.setContent(content, "text/html");
        message.saveChanges();
        final Transport transport = session.getTransport("smtp");
        transport.connect(Constants.MAILSERVER_URL, LoginDataSingleton.getInstance().getString("send_email_account"), LoginDataSingleton.getInstance().getString("send_email_password"));
        transport.sendMessage(message, message.getAllRecipients());
        transport.close();
    }
}
