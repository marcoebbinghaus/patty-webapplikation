package de.patty.etc.utilities;

import com.vaadin.server.FileDownloader;
import com.vaadin.server.StreamResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinResponse;

import java.io.IOException;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/17/13
 * Time: 04:16 AM
 *
 * Extends {@link FileDownloader} and is needed to offer a logfile as a download.
 */
public class OnDemandFileDownloader extends FileDownloader {

    /**
     * Provide both the  and the filename in an on-demand way.
     */
    public interface OnDemandStreamResource extends StreamResource.StreamSource {
        /**
         * Gets filename.
         *
         * @return the filename
         */
        String getFilename();
    }

    private static final long serialVersionUID = 1L;
    private final OnDemandStreamResource onDemandStreamResource;

    /**
     * Instantiates a new On demand file downloader.
     *
     * @param onDemandStreamResource the on demand stream resource
     */
    public OnDemandFileDownloader(OnDemandStreamResource onDemandStreamResource) {
        super(new StreamResource(onDemandStreamResource, ""));
        this.onDemandStreamResource = onDemandStreamResource;
    }

    @Override
    public boolean handleConnectorRequest(VaadinRequest request, VaadinResponse response, String path)
            throws IOException {
        getResource().setFilename(onDemandStreamResource.getFilename());
        return super.handleConnectorRequest(request, response, path);
    }

    private StreamResource getResource() {
        return (StreamResource) this.getResource("dl");
    }

}