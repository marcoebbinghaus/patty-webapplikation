package de.patty.etc.utilities;

import de.patty.etc.Constants;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/17/13
 * Time: 10:16 AM
 *
 * Singleton which returns an instance of the SolrServer.
 */
public class SolrServerSingleton {
    private static SolrServer solrServer = new HttpSolrServer(Constants.SOLR_URL);

    /**
     * Returns the SolrServer instance.
     *
     * @return the instance
     */
    public static SolrServer getInstance() {
        return solrServer;
    }

    private SolrServerSingleton() {
    }
}