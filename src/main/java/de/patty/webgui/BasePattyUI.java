package de.patty.webgui;

import com.github.wolfie.refresher.Refresher;
import com.vaadin.annotations.PreserveOnRefresh;
import com.vaadin.annotations.Theme;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.*;
import de.patty.etc.Constants;
import de.patty.etc.utilities.LoginDataSingleton;
import de.patty.etc.utilities.PasswordEncrypter;
import de.patty.persistence.dao.BenutzerDAO;
import de.patty.persistence.dao.DaoFactory;
import de.patty.persistence.dao.DaoFactorySingelton;
import de.patty.persistence.entities.Benutzer;
import de.patty.webgui.ui.RapidInfoWindow;
import de.patty.webgui.ui.RapidPanel;
import de.patty.webgui.ui.exceptions.AlreadyValidatedException;
import de.patty.webgui.ui.exceptions.UnknownValidationcodeException;
import de.patty.webgui.ui.exceptions.UserNotValidatedException;
import de.patty.webgui.ui.windows.LoginMask;
import de.patty.webgui.ui.windows.registration.logic.BenutzerManager;
import de.patty.webgui.ui.workingareas.Screen;
import de.patty.webgui.ui.workingareas.administration.crawlerstatus.logic.CheckRunningThread;
import de.patty.webgui.ui.workingareas.administration.crawlerstatus.logic.ThreadListener;
import de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles.panels.filterpanel.model.LogFileFilters;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.model.SearchPatternActions;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.model.FilterOperators;
import de.patty.webgui.ui.workingareas.administration.recrawling.model.enums.Days;
import de.patty.webgui.ui.workingareas.administration.recrawling.model.enums.Hours;
import de.patty.webgui.ui.workingareas.administration.recrawling.model.enums.Months;
import org.apache.log4j.Logger;

import java.util.Locale;
import java.util.ResourceBundle;

@SuppressWarnings("serial")
@Theme("patty")
@PreserveOnRefresh
public abstract class BasePattyUI extends UI {
    private static final Logger logger = Logger.getLogger(BasePattyUI.class);

    private final HorizontalLayout imageLayout = new HorizontalLayout();
    private final MenuBar menubar = new MenuBar();
    private final RapidPanel workingArea = new RapidPanel();
    private final VerticalLayout mainlayout = new VerticalLayout();

    protected Benutzer currentUser;
    protected Locale locale = new Locale("de", "DE");
    protected Refresher refresher;
    protected CheckRunningThread thread;
    protected ThreadListener threadListener;
    public static ResourceBundle messages;
    private Screen screen;

    @Override
    public void init(final VaadinRequest request) {
        this.setSizeFull();
        messages = ResourceBundle.getBundle(Constants.MESSAGESBUNDLE, locale);
        Hours.setResourceBundle(messages);
        Days.setResourceBundle(messages);
        Months.setResourceBundle(messages);
        SearchPatternActions.setResourceBundle(messages);
        FilterOperators.setResourceBundle(messages);
        LogFileFilters.setResourceBundle(messages);
        getPage().setTitle("P@ttySearch");
        final String confirmationCode = request.getParameter(Constants.CONFIRMATION_CODE_PARAM_NAME);
        if (confirmationCode != null) {
            tryToValidate(confirmationCode);
        }
        final VaadinSession session = getSession();
        if (session.getAttribute(Benutzer.class) == null) {
            buildLoginScreen();
        } else {
            currentUser = session.getAttribute(Benutzer.class);
            try {
                authentication(currentUser.getLoginname(), currentUser.getPassword());
            } catch (final UserNotValidatedException e) {
                addWindow(new RapidInfoWindow(this, "notyetvalidated_title", "notyetvalidated_content", true));
            } catch (final Exception e) {
                logger.error("Erneute Authentifizierung fehlgeschlagen", e.fillInStackTrace());
            }
        }
    }

    private void tryToValidate(String confirmationCode) {
        try {
            final BenutzerManager benutzerManager = new BenutzerManager(this);
            final Boolean success = benutzerManager.tryToConfirmAccount(confirmationCode);
            if (success) {
                addWindow(new RapidInfoWindow(this, "validationcodevalidatedtitle", "validationcodevalidatedcontent", true));
            } else {
                logger.warn("Fehler beim validieren eines Benutzers");
            }
        } catch (final UnknownValidationcodeException e) {
            Notification.show(messages.getString("error_unknownvalidationcode"));
        } catch (final AlreadyValidatedException e) {
            Notification.show(messages.getString("error_alreadyvalidatedcode"));
        }
    }


    public void authentication(final String enteredLogin, final String enteredPasswd) throws Exception, UserNotValidatedException {
        final DaoFactory daoFactory = DaoFactorySingelton.getInstance();
        final BenutzerDAO benutzerDAO = daoFactory.getBenutzerDAO();
        final Benutzer user = benutzerDAO.loadBenutzerForLogin(enteredLogin);
        final String enteredPasswdHashed = hash(enteredPasswd, LoginDataSingleton.getInstance().getString("encryption_salt"));
        final String userLogin = user.getLoginname();
        final String userPasswd = user.getPassword();
        final Boolean userValidated = user.getValidated();
        if (userLogin.equals(enteredLogin) && userPasswd.equals(enteredPasswdHashed)) {
            if (!userValidated) {
                throw new UserNotValidatedException();
            }
            currentUser = user;
            getSession().setAttribute(Benutzer.class, currentUser);
            setContent(null);
            refresher = new Refresher();
            refresher.setRefreshInterval(Constants.CHECKRUNNING_INTERVAL);
            thread = new CheckRunningThread();
            threadListener = new ThreadListener(thread);
            refresher.addListener(threadListener);
            addExtension(refresher);
            loadProtectedRessources();
            return;
        }
        throw new Exception("Login failed..");
    }

    private String hash(final String enteredPasswd, String passwordSalt) {
        final PasswordEncrypter encrypter = new PasswordEncrypter();
        return encrypter.encrypt(enteredPasswd, passwordSalt);
    }

    public void localization(final Object value) {
        switch (value.toString()) {
            case "GERMAN":
                locale = new Locale("de", "DE");
                setLocale(new Locale("de", "DE"));
                break;
            case "ENGLISH":
                locale = new Locale("en", "US");
                setLocale(new Locale("en", "US"));
                break;
        }
        messages = ResourceBundle.getBundle(Constants.MESSAGESBUNDLE, locale);
    }

    private void loadProtectedRessources() {
        thread.start();
        buildMainLayout();
    }

    public MenuBar getMenuBar() {
        return menubar;
    }


    public RapidPanel getWorkingAreaContainer() {
        return workingArea;
    }

    public void setWorkingArea(final Screen screen) {
        this.screen = screen;
        this.workingArea.removeAllComponents();
        this.workingArea.getContentLayout().setMargin(false);
        this.workingArea.addComponent(screen);
        this.workingArea.getContent().setSizeFull();
        this.workingArea.setSizeFull();
        thread.setWorkingArea(screen);
    }


    private void buildMainLayout() {
        mainlayout.setSizeFull();
        workingArea.setSizeFull();
        createImageLayout();

        initMenuBar(menubar);
        menubar.setSizeUndefined();


        imageLayout.setSizeUndefined();
        imageLayout.setWidth("100%");
        menubar.setWidth("100%");
        mainlayout.setSpacing(false);
        mainlayout.addComponent(menubar);
        mainlayout.addComponent(imageLayout);
        mainlayout.addComponent(workingArea);
        mainlayout.setExpandRatio(workingArea, 1);
        workingArea.setSizeFull();
        mainlayout.setSpacing(false);
        setContent(mainlayout);
    }

    private void createImageLayout() {
        final Image pattyImage = new Image("", new ThemeResource(Constants.PATTY_LOGO));
        imageLayout.setWidth("100%");
        imageLayout.addComponent(pattyImage);
        imageLayout.setComponentAlignment(pattyImage, Alignment.TOP_CENTER);
    }

    protected abstract void initMenuBar(MenuBar menuBar);


    private void buildLoginScreen() {
        final VerticalLayout loginLayout = new VerticalLayout();
        final LoginMask mask = new LoginMask(this);
        final Image pattyPicture = new Image("", new ThemeResource(Constants.PATTY_LOGO));
        loginLayout.addComponent(pattyPicture);
        loginLayout.addComponent(mask);
        loginLayout.setSizeFull();
        loginLayout.setComponentAlignment(pattyPicture, Alignment.BOTTOM_CENTER);
        loginLayout.setExpandRatio(pattyPicture, 0.3f);
        loginLayout.setComponentAlignment(mask, Alignment.TOP_CENTER);
        loginLayout.setExpandRatio(mask, 0.7f);
        setContent(null);
        setContent(loginLayout);
    }

    public ResourceBundle getResourceBundle() {
        return messages;
    }

    public Benutzer getCurrentUser() {
        return currentUser;
    }

    public Refresher getRefresher() {
        return refresher;
    }

    public Locale getTheLocale() {
        return locale;
    }

    public ThreadListener getThreadListener() {
        return threadListener;
    }

    public Screen getScreen() {
        return screen;
    }
}
