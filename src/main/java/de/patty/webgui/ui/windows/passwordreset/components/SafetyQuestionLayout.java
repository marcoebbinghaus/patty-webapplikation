package de.patty.webgui.ui.windows.passwordreset.components;

import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.*;
import de.patty.persistence.entities.Benutzer;
import de.patty.rmi.DoRMILogicAndDoSomethingAfterRunnable;
import de.patty.webgui.BasePattyUI;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.windows.passwordreset.ResetPasswordWindow;
import de.patty.webgui.ui.windows.registration.logic.BenutzerManager;
import de.patty.webgui.ui.workingareas.search.model.validators.NotEmptyValidator;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/17/13
 * Time: 11:15 AM
 *
 * The Layout which is shown in the {@link ResetPasswordWindow} after the user entered his loginname in the previous
 * layout {@link LoginnameLayout}. It shows the user's safetyquestion and asks for the corresponding answers.
 */
public class SafetyQuestionLayout extends FormLayout {

    private Label questionLabel;
    private TextField answerField;
    private Button sendButton;
    private ResourceBundle messages;
    private ProgressIndicator progressIndicator;

    /**
     * Instantiates a new Safety question layout.
     *
     * @param ui the ui
     * @param user the user
     * @param resetPasswordWindow the reset password window
     */
    public SafetyQuestionLayout(final BasePattyUI ui, final Benutzer user, final ResetPasswordWindow resetPasswordWindow) {
        messages = ui.getResourceBundle();
        setMargin(true);
        questionLabel = new Label(messages.getString("safetyQuestion"));
        answerField = new TextField(messages.getString("safetyAnswer"));
        if (ui.getTheLocale().equals(Locale.GERMANY)) {
            questionLabel.setValue(user.getSafetyQuestion().getGermantext());
        } else {
            questionLabel.setValue(user.getSafetyQuestion().getEnglishtext());
        }
        questionLabel.setReadOnly(true);
        answerField.setImmediate(true);
        answerField.setRequired(true);
        answerField.addValidator(new NotEmptyValidator(messages));
        progressIndicator = new ProgressIndicator();
        progressIndicator.setIndeterminate(true);
        progressIndicator.setEnabled(false);
        progressIndicator.setVisible(false);
        sendButton = new Button(messages.getString("send"));
        sendButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        sendButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                if (user.getSafetyAnswer().toLowerCase().equals(answerField.getValue().toLowerCase())) {
                    new Thread(new DoRMILogicAndDoSomethingAfterRunnable((MainPattyUI) ui) {
                        @Override
                        public boolean doRMI() {
                            final BenutzerManager benutzerManager = new BenutzerManager(ui);
                            return benutzerManager.resetPasswordForUser(user);
                        }

                        @Override
                        public void doSomethingAfterRMI() {
                            if (success) {
                                resetPasswordWindow.setContent(new Label(messages.getString("newpasswordsent")));
                            } else {
                                Notification.show("Mail konnte nicht versandt werden.");
                            }
                        }
                    }).start();
                    progressIndicator.setEnabled(true);
                    progressIndicator.setVisible(true);
                    sendButton.setEnabled(false);
                } else {
                    resetPasswordWindow.setContent(new Label(messages.getString("newpasswordsent")));
                }
                progressIndicator.setEnabled(true);
                progressIndicator.setVisible(true);
                sendButton.setEnabled(false);
            }
        });
        addComponent(questionLabel);
        addComponent(answerField);
        addComponent(sendButton);
        addComponent(progressIndicator);
    }
}
