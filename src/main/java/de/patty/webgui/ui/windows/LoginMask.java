package de.patty.webgui.ui.windows;

import com.vaadin.data.Property;
import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.*;
import de.patty.etc.Constants;
import de.patty.etc.utilities.LoginDataSingleton;
import de.patty.persistence.dao.DaoFactory;
import de.patty.persistence.dao.DaoFactorySingelton;
import de.patty.persistence.entities.Benutzer;
import de.patty.webgui.BasePattyUI;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.Languages;
import de.patty.webgui.ui.RapidInfoWindow;
import de.patty.webgui.ui.RapidPanel;
import de.patty.webgui.ui.exceptions.UserNotValidatedException;
import de.patty.webgui.ui.windows.passwordreset.ResetPasswordWindow;
import de.patty.webgui.ui.windows.registration.RegistrationWindow;
import de.patty.webgui.ui.workingareas.Internationalizationable;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/13/13
 * Time: 11:34 AM
 *
 * LoginMask which is displayed if a user enters the website (http://www.patty-search.de). Contains fields for the login
 * and buttons/links for registration / password recovery / impress / ...
 */
public class LoginMask extends VerticalLayout implements Internationalizationable {

    private static final Logger logger = Logger.getLogger(LoginMask.class);
    private BasePattyUI ui;
    private TextField usernameField;
    private PasswordField passwordField;
    private ComboBox languageBox;
    private RapidPanel frame;
    private Button imprintButton;
    private Button registerButton;
    private Button forgotPasswordButton;
    private Label orLabel;
    private Button guestButton;
    private HorizontalLayout loginRegisterLayout;

    private FormLayout loginLayout;
    private Button loginButton;
    private ResourceBundle messages;

    /**
     * Instantiates a new LoginMask.
     *
     * @param ui the ui
     */
    public LoginMask(final BasePattyUI ui) {
        this.ui = ui;
        messages = ui.getResourceBundle();
        frame = new RapidPanel();
        frame.setWidth("300px");
        registerButton = new Button();
        registerButton.setStyleName("link");
        registerButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                ui.addWindow(new RegistrationWindow(ui));
            }
        });
        forgotPasswordButton = new Button();
        forgotPasswordButton.setStyleName("link");
        forgotPasswordButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                ui.addWindow(new ResetPasswordWindow(ui));
            }
        });
        orLabel = new Label();
        guestButton = new Button();
        guestButton.setStyleName("link");
        guestButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                try {
                    final DaoFactory daoFactory = DaoFactorySingelton.getInstance();
                    final Benutzer guestUser = daoFactory.getBenutzerDAO().loadBenutzerForLogin(Constants.GUESTACCOUNT);
                    final String username = guestUser.getLoginname();
                    final String password = LoginDataSingleton.getInstance().getString("guestpassword");
                    ui.authentication(username, password);
                } catch (final UserNotValidatedException e) {
                    ui.addWindow(new RapidInfoWindow(ui, "notyetvalidated_title", "notyetvalidated_content", true));
                } catch (final Exception e) {
                    Notification.show(messages.getString("loginfailed"));
                }
            }
        });
        imprintButton = new Button();
        imprintButton.setStyleName("link");
        imprintButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                ui.addWindow(new ImpressumWindow((MainPattyUI) ui));
            }
        });
        loginLayout = new FormLayout();
        usernameField = new TextField();
        usernameField.setSizeFull();
        usernameField.focus();
        passwordField = new PasswordField();
        passwordField.setSizeFull();
        loginButton = new Button();
        loginButton.setStyleName("link");
        loginButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        languageBox = new ComboBox("", Arrays.asList(Languages.values()));
        loginButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(final Button.ClickEvent clickEvent) {
                try {
                    final String username = usernameField.getValue();
                    final String password = passwordField.getValue();
                    ui.authentication(username, password);
                } catch (final UserNotValidatedException e) {
                    ui.addWindow(new RapidInfoWindow(ui, "notyetvalidated_title", "notyetvalidated_content", true));
                } catch (final Exception e) {
                    Notification.show(messages.getString("loginfailed"));
                    e.printStackTrace();
                }
            }
        });
        loginRegisterLayout = new HorizontalLayout(loginButton, registerButton);
        loginRegisterLayout.setSizeUndefined();
        loginRegisterLayout.setSpacing(true);
        languageBox.setImmediate(true);
        languageBox.setValue(Languages.GERMAN);
        languageBox.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                ui.localization(languageBox.getValue());
                messages = ui.getResourceBundle();
                doInternationalization();
            }
        });
        languageBox.setNullSelectionAllowed(false);
        languageBox.setTextInputAllowed(false);
        languageBox.setSizeFull();

        loginLayout.setSizeFull();
        loginLayout.addComponent(usernameField);
        loginLayout.addComponent(passwordField);
        loginLayout.addComponent(loginRegisterLayout);

        frame.addComponent(loginLayout);
        frame.addComponent(orLabel);
        frame.addComponent(guestButton);
        frame.addComponent(forgotPasswordButton);
        frame.addComponent(languageBox);
        frame.addComponent(imprintButton);

        setComponents();
        doInternationalization();
    }

    /**
     * Sets the components.
     */
    public void setComponents() {
        addComponent(frame);
        setComponentAlignment(frame, Alignment.MIDDLE_CENTER);
    }

    @Override
    public void doInternationalization() {
        registerButton.setCaption(messages.getString("register"));
        orLabel.setCaption(messages.getString("or"));
        guestButton.setCaption(messages.getString("loginasguest"));
        forgotPasswordButton.setCaption(messages.getString("forgotpassword"));
        imprintButton.setCaption(messages.getString("imprint"));
        usernameField.setCaption(messages.getString("username"));
        passwordField.setCaption(messages.getString("password"));
        loginButton.setCaption(messages.getString("login"));
        languageBox.setCaption(messages.getString("language"));
        loginLayout.setCaption(messages.getString("enterlogin"));
    }
}
