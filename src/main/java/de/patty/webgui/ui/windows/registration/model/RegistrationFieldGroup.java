package de.patty.webgui.ui.windows.registration.model;

import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.ui.*;
import de.patty.persistence.dao.DaoFactory;
import de.patty.persistence.dao.DaoFactorySingelton;
import de.patty.persistence.entities.Benutzer;
import de.patty.persistence.entities.SafetyQuestion;
import de.patty.webgui.ui.windows.registration.validators.SameInputValidator;
import de.patty.webgui.ui.workingareas.search.model.validators.NotEmptyValidator;

import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/13/13
 * Time: 11:47 AM
 *
 * {@link FieldGroup} for the {@link de.patty.webgui.ui.windows.registration.RegistrationWindow}. Contains Fields for
 * all required informations.
 */
public class RegistrationFieldGroup extends FieldGroup {

    private ResourceBundle messages;
    private TextField emailVerifyField;
    private PasswordField passwordVerifyField;
    private ComboBox securityQuestionBox;
    private DaoFactory daoFactory;

    /**
     * Instantiates a new RegistrationFieldGroup.
     *
     * @param messages ResourceBundle (i18n)
     * @param newBenutzer the new benutzer
     */
    public RegistrationFieldGroup(final ResourceBundle messages, final Benutzer newBenutzer) {
        this.messages = messages;
        setItemDataSource(new BeanItem<>(newBenutzer));
        daoFactory = DaoFactorySingelton.getInstance();
        buildAndLocalizeAllFields();
    }

    private void buildAndLocalizeAllFields() {
        for (final Object propertyId : getUnboundPropertyIds()) {
            switch (propertyId.toString()) {

                case Benutzer.LOGIN:
                    final Field field = buildField(propertyId);
                    field.addValidator(new NotEmptyValidator(messages));
                    break;
                case Benutzer.EMAIL:
                    final Field field2 = buildField(propertyId);
                    field2.addValidator(new EmailValidator(messages.getString("error_emailvalidator")));
                    break;
                case Benutzer.PASSWD:
                    buildField(propertyId);
                    break;
                case Benutzer.SAFETY_ANSWER:
                    buildField(propertyId);
                    break;
            }
        }
        emailVerifyField = new TextField();
        emailVerifyField.setRequired(true);
        emailVerifyField.setCaption(messages.getString(Benutzer.EMAIL_VERIFY.toString()));
        emailVerifyField.setWidth("100%");
        emailVerifyField.setImmediate(true);
        emailVerifyField.addValidator(new SameInputValidator(messages, (AbstractField) getField(Benutzer.EMAIL)));
        passwordVerifyField = new PasswordField();
        passwordVerifyField.setRequired(true);
        passwordVerifyField.setCaption(messages.getString(Benutzer.PASSWORD_VERIFY.toString()));
        passwordVerifyField.setWidth("100%");
        passwordVerifyField.setImmediate(true);
        passwordVerifyField.addValidator(new SameInputValidator(messages, (AbstractField) getField(Benutzer.PASSWD)));
        final List<SafetyQuestion> questions = daoFactory.getSafetyQuestionDAO().loadAllEntities();
        final BeanItemContainer<SafetyQuestion> container = new BeanItemContainer<SafetyQuestion>(SafetyQuestion.class);
        container.addAll(questions);
        securityQuestionBox = new ComboBox(messages.getString("safetyQuestion"), container);
        securityQuestionBox.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        if (messages.getLocale().equals(Locale.GERMANY)) {
            securityQuestionBox.setItemCaptionPropertyId(SafetyQuestion.GERMAN_TEXT);
        } else {
            securityQuestionBox.setItemCaptionPropertyId(SafetyQuestion.ENGLISH_TEXT);
        }
        securityQuestionBox.setImmediate(true);
        securityQuestionBox.setTextInputAllowed(false);
        securityQuestionBox.setRequired(true);
        securityQuestionBox.setWidth("100%");
    }

    private Field buildField(Object propertyId) {
        final Field field;
        if (propertyId.equals(Benutzer.PASSWD)) {
            field = new PasswordField();
            bind(field, propertyId);
        } else {
            field = buildAndBind(propertyId);
        }
        field.setRequired(true);
        field.setCaption(messages.getString(propertyId.toString()));
        field.setWidth("100%");
        field.setValue("");
        ((AbstractField) field).setImmediate(true);
        return field;
    }

    /**
     * Gets emailVerifyField.
     *
     * @return the emailVerifyField
     */
    public TextField getEmailVerifyField() {
        return emailVerifyField;
    }

    /**
     * Gets passwordVerifyField.
     *
     * @return the passwordVerifyField
     */
    public PasswordField getPasswordVerifyField() {
        return passwordVerifyField;
    }

    /**
     * Gets the securityQuestionBox.
     *
     * @return the securityQuestionBox
     */
    public ComboBox getSecurityQuestionBox() {
        return securityQuestionBox;
    }
}
