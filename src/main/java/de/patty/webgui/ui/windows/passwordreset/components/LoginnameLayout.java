package de.patty.webgui.ui.windows.passwordreset.components;

import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import de.patty.persistence.dao.DaoFactory;
import de.patty.persistence.dao.DaoFactorySingelton;
import de.patty.persistence.entities.Benutzer;
import de.patty.webgui.BasePattyUI;
import de.patty.webgui.ui.windows.passwordreset.ResetPasswordWindow;
import de.patty.webgui.ui.workingareas.search.model.validators.NotEmptyValidator;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/17/13
 * Time: 11:14 AM
 *
 * Layout which is shown in the {@link ResetPasswordWindow} when the user clicks on the "forgot password" link in the
 * {@link de.patty.webgui.ui.windows.LoginMask}.
 */
public class LoginnameLayout extends FormLayout {

    private TextField loginnameField;
    private Button nextButton;
    private ResourceBundle messages;

    /**
     * Instantiates a new Loginname layout.
     *
     * @param ui the ui
     * @param resetPasswordWindow the reset password window
     */
    public LoginnameLayout(final BasePattyUI ui, final ResetPasswordWindow resetPasswordWindow) {
        messages = ui.getResourceBundle();
        setMargin(true);
        loginnameField = new TextField(messages.getString("loginname"));
        nextButton = new Button(messages.getString("next"));
        loginnameField.setImmediate(true);
        loginnameField.setRequired(true);
        loginnameField.addValidator(new NotEmptyValidator(messages));
        loginnameField.setWidth("100%");
        nextButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        nextButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                if (loginnameField.isValid()) {
                    final DaoFactory daoFactory = DaoFactorySingelton.getInstance();
                    final Benutzer user = daoFactory.getBenutzerDAO().loadBenutzerForLogin(loginnameField.getValue());
                    if (user == null) {
                        Notification.show(messages.getString("error_nosuchuser"));
                    } else {
                        resetPasswordWindow.setContent(new SafetyQuestionLayout(ui, user, resetPasswordWindow));
                    }
                } else {
                    Notification.show(messages.getString("invaliddata"));
                }

            }
        });
        addComponent(loginnameField);
        addComponent(nextButton);
    }
}
