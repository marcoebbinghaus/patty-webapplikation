package de.patty.webgui.ui.windows.passwordreset;

import com.vaadin.ui.Window;
import de.patty.webgui.BasePattyUI;
import de.patty.webgui.ui.windows.passwordreset.components.LoginnameLayout;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/17/13
 * Time: 10:57 AM
 *
 * The window that is displayed when the user clicks on the "forgot password" link in the {@link de.patty.webgui.ui.windows.LoginMask}
 */
public class ResetPasswordWindow extends Window {

    /**
     * Instantiates a new Reset password window.
     *
     * @param ui the ui
     */
    public ResetPasswordWindow(final BasePattyUI ui) {
        setWidth("400px");
        setModal(true);
        setCaption(ui.getResourceBundle().getString("resetpassword"));
        setContent(new LoginnameLayout(ui, this));
    }


}
