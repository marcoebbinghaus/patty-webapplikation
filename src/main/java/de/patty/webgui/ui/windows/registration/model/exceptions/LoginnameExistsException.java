package de.patty.webgui.ui.windows.registration.model.exceptions;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/13/13
 * Time: 4:42 PM
 *
 * Thrown if the user entered a loginname for the new Benutzer account that is already linked to an existing account.
 */
public class LoginnameExistsException extends Exception {
}
