package de.patty.webgui.ui.windows.registration.logic;

import de.patty.etc.Constants;
import de.patty.etc.utilities.LoginDataSingleton;
import de.patty.etc.utilities.MailSender;
import de.patty.etc.utilities.PasswordCreator;
import de.patty.etc.utilities.PasswordEncrypter;
import de.patty.persistence.dao.DaoFactory;
import de.patty.persistence.dao.DaoFactorySingelton;
import de.patty.persistence.entities.Benutzer;
import de.patty.persistence.entities.BenutzerGruppe;
import de.patty.webgui.BasePattyUI;
import de.patty.webgui.ui.exceptions.AlreadyValidatedException;
import de.patty.webgui.ui.exceptions.UnknownValidationcodeException;
import de.patty.webgui.ui.windows.registration.model.exceptions.EmailExistsException;
import de.patty.webgui.ui.windows.registration.model.exceptions.LoginnameExistsException;
import sun.misc.BASE64Encoder;

import javax.mail.MessagingException;
import javax.persistence.EntityManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/13/13
 * Time: 2:21 PM
 *
 * Communicates with the Database. Contains all methods for that.
 */
public class BenutzerManager {

    private DaoFactory daoFactory;
    private ResourceBundle messages;
    private BasePattyUI ui;

    /**
     * Instantiates a new BenutzerManager.
     *
     * @param ui the ui
     */
    public BenutzerManager(final BasePattyUI ui) {
        this.ui = ui;
        this.messages = ui.getResourceBundle();
        daoFactory = DaoFactorySingelton.getInstance();
    }

    /**
     * Registers a new Benutzer (encrypts the entered passwords, saves all informations in the database, ...)
     *
     * @param newBenutzer the new benutzer
     * @return true if the user was registered successful
     * @throws MessagingException thrown if the confirmation email couldn't be sent.
     * @throws EmailExistsException thrown if the user entered an email address which is already linked to an existing
     * Benutzer account.
     * @throws LoginnameExistsException thrown if the user chooses a loginname that is already linked to an existing
     * Benutzer account.
     */
    public boolean registerUser(final Benutzer newBenutzer) throws MessagingException, EmailExistsException, LoginnameExistsException {
        final List<Benutzer> benutzerList = daoFactory.getBenutzerDAO().loadAllEntities();
        for (final Benutzer benutzerFromDB : benutzerList) {
            if (benutzerFromDB.getLoginname().equals(newBenutzer.getLoginname())) {
                throw new LoginnameExistsException();
            }
            if (benutzerFromDB.getEmail().equals(newBenutzer.getEmail())) {
                throw new EmailExistsException();
            }
        }
        final PasswordEncrypter encrypter = new PasswordEncrypter();
        final String encryptedPW = encrypter.encrypt(newBenutzer.getPassword(), LoginDataSingleton.getInstance().getString("encryption_salt"));
        newBenutzer.setPassword(encryptedPW);
        newBenutzer.setValidated(false);
        newBenutzer.setValidationCode(generateValidationCode(newBenutzer.getEmail()));
        final BenutzerGruppe groupUsers = daoFactory.getBenutzerGruppeDAO().loadBenutzerGruppeByName(BenutzerGruppe.USER);
        newBenutzer.setBenutzerGruppe(groupUsers);
        daoFactory.new Transaction() {
            @Override
            public void doTask() {
                final EntityManager entityManager = daoFactory.getEntityManager();
                entityManager.persist(newBenutzer);
                entityManager.flush();
            }
        }.execute();
        sendMail(newBenutzer.getEmail(), newBenutzer.getValidationCode());
        return true;
    }

    private String generateValidationCode(final String email) {
        final Integer i = (int) (Math.random() * 100);
        final String stringToHash = email + i.toString();
        final String validationCode = new BASE64Encoder().encode(stringToHash.getBytes());
        return validationCode;
    }

    private void sendMail(final String email, final String validationCode) throws MessagingException {
        final String contentString = messages.getString("mail_content1") + "<a href=\"http://www.patty-search.de/?confirm=" +
                validationCode + "\">" + messages.getString("confirmation") + "</a>" + messages.getString("mail_footer");
        final MailSender mailSender = new MailSender();
        mailSender.sendMail(email, messages.getString("mail_subject"), contentString);

    }

    /**
     * Tries to confirm a Benutzer account (called when the user clicks on the confirmation link in the confirmation email).
     *
     * @param confirmationCode the confirmation code
     * @return true, if the confirmation was successful
     * @throws AlreadyValidatedException thrown if the user account is already confirmed
     * @throws UnknownValidationcodeException thrown if the appended validation code is unknown
     */
    public Boolean tryToConfirmAccount(final String confirmationCode) throws AlreadyValidatedException, UnknownValidationcodeException {
        final List<Benutzer> users = daoFactory.getBenutzerDAO().loadAllEntities();
        final Map<String, Boolean> validationCodesValidated = new HashMap<>();
        for (final Benutzer user : users) {
            validationCodesValidated.put(user.getValidationCode(), user.getValidated());
        }
        if (validationCodesValidated.containsKey(confirmationCode)) {
            if (validationCodesValidated.get(confirmationCode) == true) {
                throw new AlreadyValidatedException();
            } else {
                daoFactory.new Transaction() {
                    @Override
                    public void doTask() {
                        final EntityManager entityManager = daoFactory.getEntityManager();
                        final Benutzer benutzer = daoFactory.getBenutzerDAO().loadBenutzerByValidationcode(confirmationCode);
                        benutzer.setValidated(true);
                        entityManager.flush();
                    }
                }.execute();
            }
        } else {
            throw new UnknownValidationcodeException();
        }
        return true;
    }

    /**
     * Resets the password for the Benutzer (if he forgot his password).
     *
     * @param benutzer the benutzer
     * @return true if the password was reset successful, false otherwise.
     */
    public Boolean resetPasswordForUser(final Benutzer benutzer) {
        try {
            final PasswordCreator passwordCreator = new PasswordCreator(Constants.PW_LENGTH);
            final String generatedPassword = passwordCreator.createPassword();
            daoFactory.new Transaction() {
                @Override
                public void doTask() {
                    final EntityManager entityManager = daoFactory.getEntityManager();
                    final PasswordEncrypter passwordEncrypter = new PasswordEncrypter();
                    final String encryptedPassword = passwordEncrypter.encrypt(generatedPassword, LoginDataSingleton.getInstance().getString("encryption_salt"));
                    benutzer.setPassword(encryptedPassword);
                    entityManager.flush();
                }
            }.execute();
            final MailSender mailSender = new MailSender();
            final String contentString = messages.getString("mail_newpassword") + generatedPassword + messages.getString("mail_footer");
            mailSender.sendMail(benutzer.getEmail(), messages.getString("subject_passwordreset"), contentString);
        } catch (final Exception e) {
            return false;
        }
        return true;
    }


}
