package de.patty.webgui.ui.windows.registration;

import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import de.patty.persistence.entities.Benutzer;
import de.patty.persistence.entities.SafetyQuestion;
import de.patty.rmi.DoRMILogicAndDoSomethingAfterRunnable;
import de.patty.webgui.BasePattyUI;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.RapidWindow;
import de.patty.webgui.ui.SaveCancelButtonBar;
import de.patty.webgui.ui.windows.registration.logic.BenutzerManager;
import de.patty.webgui.ui.windows.registration.model.RegistrationFieldGroup;
import de.patty.webgui.ui.windows.registration.model.exceptions.EmailExistsException;
import de.patty.webgui.ui.windows.registration.model.exceptions.LoginnameExistsException;

import javax.mail.MessagingException;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/13/13
 * Time: 11:34 AM
 *
 * Window that is displayed if a user wants to register a new account (by clicking the "register" link in the
 * {@link de.patty.webgui.ui.windows.LoginMask}
 */
public class RegistrationWindow extends RapidWindow {

    private RegistrationFieldGroup registrationFieldGroup;
    private SaveCancelButtonBar saveCancelButtonBar;
    private ProgressIndicator progressIndicator;

    /**
     * Instantiates a new RegistrationWindow.
     *
     * @param ui the ui
     */
    public RegistrationWindow(final BasePattyUI ui) {
        super(ui);
        setWidth("500px");
        progressIndicator = new ProgressIndicator();
        progressIndicator.setIndeterminate(true);
        progressIndicator.setEnabled(false);
        progressIndicator.setVisible(false);
        registrationFieldGroup = new RegistrationFieldGroup(ui.getResourceBundle(), new Benutzer());
        saveCancelButtonBar = new SaveCancelButtonBar() {
            @Override
            public void doInternationalization() {
                saveButton.setCaption(messagesBundle.getString("register"));
                cancelButton.setCaption(messagesBundle.getString("cancel"));
            }

            @Override
            public void setSaveButtonListener() {
                saveButton.addClickListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {
                        if (registrationFieldGroup.isValid() && registrationFieldGroup.getEmailVerifyField().isValid() &&
                                registrationFieldGroup.getPasswordVerifyField().isValid() && registrationFieldGroup.getSecurityQuestionBox().isValid()) {
                            try {
                                registrationFieldGroup.commit();
                                registrationFieldGroup.getEmailVerifyField().commit();
                                registrationFieldGroup.getPasswordVerifyField().commit();
                                registrationFieldGroup.getSecurityQuestionBox().commit();
                                final Thread thread = new Thread(new DoRMILogicAndDoSomethingAfterRunnable((MainPattyUI) ui) {
                                    @Override
                                    public boolean doRMI() {
                                        try {
                                            final Benutzer newBenutzer = ((BeanItem<Benutzer>) registrationFieldGroup.getItemDataSource()).getBean();
                                            newBenutzer.setSafetyQuestion((SafetyQuestion) registrationFieldGroup.getSecurityQuestionBox().getValue());
                                            final BenutzerManager benutzerManager = new BenutzerManager(ui);
                                            return benutzerManager.registerUser(newBenutzer);
                                        } catch (final MessagingException e) {
                                            exception = e;
                                            return false;
                                        } catch (final LoginnameExistsException loginnameExistsException) {
                                            exception = loginnameExistsException;
                                            return false;
                                        } catch (final EmailExistsException emailExistsException) {
                                            exception = emailExistsException;
                                            return false;
                                        }
                                    }

                                    @Override
                                    public void doSomethingAfterRMI() {
                                        progressIndicator.setEnabled(false);
                                        progressIndicator.setVisible(false);
                                        if (success) {
                                            RegistrationWindow.this.getContentLayout().removeAllComponents();
                                            final Label infoLabel = new Label(messagesBundle.getString("checkemails"));
                                            infoLabel.setContentMode(ContentMode.HTML);
                                            RegistrationWindow.this.getContentLayout().addComponent(infoLabel);
                                        } else {
                                            saveButton.setEnabled(true);
                                            cancelButton.setEnabled(true);
                                            if (exception instanceof MessagingException) {
                                                Notification.show("Leider konnte keine Mail losgeschickt werden. Doof.");
                                                exception.printStackTrace();
                                            } else if (exception instanceof LoginnameExistsException) {
                                                Notification.show(messagesBundle.getString("error_loginnameexists"));
                                            } else if (exception instanceof EmailExistsException) {
                                                Notification.show(messagesBundle.getString("error_emailexists"));
                                            }
                                        }
                                    }
                                });
                                thread.start();
                                progressIndicator.setEnabled(true);
                                progressIndicator.setVisible(true);
                                saveButton.setEnabled(false);
                                cancelButton.setEnabled(false);
                            } catch (FieldGroup.CommitException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Notification.show(messagesBundle.getString("invaliddata"));
                        }
                    }
                });
            }

            @Override
            public void setCancelButtonListener() {
                cancelButton.addClickListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {
                        RegistrationWindow.this.close();
                    }
                });
            }
        };
        setComponents();
        doInternationalization();
    }

    @Override
    protected void setLayout() {
        setContentLayout(new FormLayout());
    }

    @Override
    public void setComponents() {
        addComponent(progressIndicator);
        addComponent(registrationFieldGroup.getField(Benutzer.LOGIN));
        addComponent(registrationFieldGroup.getField(Benutzer.EMAIL));
        addComponent(registrationFieldGroup.getEmailVerifyField());
        addComponent(registrationFieldGroup.getField(Benutzer.PASSWD));
        addComponent(registrationFieldGroup.getPasswordVerifyField());
        addComponent(registrationFieldGroup.getSecurityQuestionBox());
        addComponent(registrationFieldGroup.getField(Benutzer.SAFETY_ANSWER));
        addComponent(saveCancelButtonBar);
    }

    @Override
    public void doInternationalization() {
        setCaption(messagesBundle.getString("registration"));
    }
}
