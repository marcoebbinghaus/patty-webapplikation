package de.patty.webgui.ui.windows.registration.validators;

import com.vaadin.data.Validator;
import com.vaadin.ui.AbstractField;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/13/13
 * Time: 1:51 PM
 *
 * {@link Validator} which validates that the input of an element was the same as the input of an other element (used
 * for the email+email verification field and the password + password verification field in the
 * {@link de.patty.webgui.ui.windows.registration.model.RegistrationFieldGroup}
 */
public class SameInputValidator implements Validator {

    private ResourceBundle messages;
    private AbstractField originalField;

    /**
     * Instantiates a new SameInputValidator.
     *
     * @param messages the ResourceBundle (i18n)
     * @param originalField the field with the original value
     */
    public SameInputValidator(final ResourceBundle messages, final AbstractField originalField) {
        this.messages = messages;
        this.originalField = originalField;
    }

    @Override
    public void validate(Object o) throws InvalidValueException {
        if (!o.toString().equals(originalField.getValue().toString())) {
            throw new InvalidValueException(messages.getString("error_notsameinput"));
        }
    }
}
