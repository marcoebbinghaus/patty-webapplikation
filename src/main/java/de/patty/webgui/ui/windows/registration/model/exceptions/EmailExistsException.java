package de.patty.webgui.ui.windows.registration.model.exceptions;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/13/13
 * Time: 4:42 PM
 *
 * Thrown if a user tries to register a new account with an email address that is already linked to an existing account.
 */
public class EmailExistsException extends Exception {
}
