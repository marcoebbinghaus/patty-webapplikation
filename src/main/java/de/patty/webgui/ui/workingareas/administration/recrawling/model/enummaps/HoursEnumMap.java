package de.patty.webgui.ui.workingareas.administration.recrawling.model.enummaps;

import de.patty.webgui.ui.workingareas.administration.recrawling.model.enums.Hours;

import java.util.EnumMap;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/9/13
 * Time: 3:42 PM
 *
 * Map to link the different hours from the {@link Hours} enum to integer values.
 */
public class HoursEnumMap extends EnumMap {

    /**
     * Instantiates a new HoursEnumMap.
     *
     * @param map the map
     */
    public HoursEnumMap(EnumMap<Hours, Integer> map) {
        super(map);
        put(Hours.ONE_AM, 1);
        put(Hours.TWO_AM, 2);
        put(Hours.THREE_AM, 3);
        put(Hours.FOUR_AM, 4);
        put(Hours.FIVE_AM, 5);
        put(Hours.SIX_AM, 6);
        put(Hours.SEVEN_AM, 7);
        put(Hours.EIGHT_AM, 8);
        put(Hours.NINE_AM, 9);
        put(Hours.TEN_AM, 10);
        put(Hours.ELEVEN_AM, 11);
        put(Hours.TWELVE_NOON, 12);
        put(Hours.ONE_PM, 13);
        put(Hours.TWO_PM, 14);
        put(Hours.THREE_PM, 15);
        put(Hours.FOUR_PM, 16);
        put(Hours.FIVE_PM, 17);
        put(Hours.SIX_PM, 18);
        put(Hours.SEVEN_PM, 19);
        put(Hours.EIGHT_PM, 20);
        put(Hours.NINE_PM, 21);
        put(Hours.TEN_PM, 22);
        put(Hours.ELEVEN_PM, 23);
        put(Hours.TWELVE_MIDNIGHT, 0);
    }
}
