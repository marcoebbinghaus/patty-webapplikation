package de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.searchpatterns.allpatterns.logic.instances;

import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.ListSelect;
import de.patty.persistence.entities.SearchPattern;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.SearchPatternPanel;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.display.model.SortModes;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.searchpatterns.allpatterns.logic.AllSearchPatternsBuilder;

import java.util.Collections;
import java.util.List;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/6/13
 * Time: 10:05 AM
 *
 */
public class AlphabeticalListBuilder extends AllSearchPatternsBuilder {

    private ListSelect listSelect;
    private SortModes sortMode;

    public AlphabeticalListBuilder(final List<SearchPattern> searchPatterns, final SearchPatternPanel searchPatternPanel,
                                   final SortModes sortMode) {
        super(searchPatterns, searchPatternPanel);
        this.sortMode = sortMode;
    }

    @Override
    protected void buildSpecificDisplayComponent() {
        Collections.sort(searchPatterns);
        if (sortMode == SortModes.DESCENDING) {
            Collections.reverse(searchPatterns);
        }
        final BeanItemContainer<SearchPattern> beanItemContainer = new BeanItemContainer<SearchPattern>(SearchPattern.class);
        beanItemContainer.addAll(searchPatterns);
        listSelect = new ListSelect("", beanItemContainer);
        listSelect.setSizeFull();
        listSelect.setNullSelectionAllowed(true);
        listSelect.setImmediate(true);
        listSelect.setMultiSelect(false);
        listSelect.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        listSelect.setItemCaptionPropertyId(SearchPattern.TITLE);
        listSelect.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                final SearchPattern selectedSearchPattern = (SearchPattern) listSelect.getValue();
                searchPatternPanel.setSelectedSearchPattern(selectedSearchPattern);
                searchPatternPanel.getButtonLeiste().reload(selectedSearchPattern);
            }
        });
        displayComponent = listSelect;
    }
}
