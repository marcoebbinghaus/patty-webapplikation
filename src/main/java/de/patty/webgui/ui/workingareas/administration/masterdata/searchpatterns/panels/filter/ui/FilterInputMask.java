package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.ui;

import com.vaadin.data.Property;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.Componentssetable;
import de.patty.webgui.ui.workingareas.Internationalizationable;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.model.FilterOperators;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.model.SearchPatternI18nToAttributeNamesMap;
import de.patty.webgui.ui.workingareas.search.model.validators.NotEmptyValidator;

import java.util.Arrays;
import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/23/13
 * Time: 10:36 AM
 *
 * Layout that displays all input components to create a new {@link de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.model.Filter}.
 * It is displayed in the {@link NewFilterWindow}.
 */
public class FilterInputMask extends FormLayout implements Componentssetable, Internationalizationable {

    private ComboBox columnBox;
    private ComboBox operatorBox;
    private TextField contentField;

    private SearchPatternI18nToAttributeNamesMap map;
    private ResourceBundle messages;

    /**
     * Instantiates a new FilterInputMask.
     *
     * @param ui the ui
     */
    public FilterInputMask(final MainPattyUI ui) {
        messages = ui.getResourceBundle();
        map = new SearchPatternI18nToAttributeNamesMap(ui);
        columnBox = new ComboBox("", map.keySet());
        columnBox.setNullSelectionAllowed(false);
        columnBox.select(map.keySet().iterator().next());
        columnBox.setImmediate(true);
        contentField = new TextField();
        contentField.addValidator(new NotEmptyValidator(ui.getResourceBundle()));
        contentField.setImmediate(true);
        operatorBox = new ComboBox("", Arrays.asList(FilterOperators.values()));
        operatorBox.setImmediate(true);
        operatorBox.select(FilterOperators.EQUALS);
        operatorBox.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                final FilterOperators operator = (FilterOperators) event.getProperty().getValue();
                if (operator.equals(FilterOperators.IS_NOT_NULL) || operator.equals(FilterOperators.IS_NULL)) {
                    contentField.setValue("");
                    contentField.setEnabled(false);
                } else {
                    contentField.setEnabled(true);
                }
            }
        });
        setComponents();
        doInternationalization();
    }

    @Override
    public void setComponents() {
        addComponent(columnBox);
        addComponent(operatorBox);
        addComponent(contentField);
    }

    @Override
    public void doInternationalization() {
        columnBox.setCaption(messages.getString("columnname"));
        operatorBox.setCaption(messages.getString("operator"));
        contentField.setCaption(messages.getString("content"));
    }

    /* Getters and Setters */
    public ComboBox getColumnBox() {
        return columnBox;
    }

    public void setColumnBox(ComboBox columnBox) {
        this.columnBox = columnBox;
    }

    public ComboBox getOperatorBox() {
        return operatorBox;
    }

    public void setOperatorBox(ComboBox operatorBox) {
        this.operatorBox = operatorBox;
    }

    public TextField getContentField() {
        return contentField;
    }

    public void setContentField(TextField contentField) {
        this.contentField = contentField;
    }
}
