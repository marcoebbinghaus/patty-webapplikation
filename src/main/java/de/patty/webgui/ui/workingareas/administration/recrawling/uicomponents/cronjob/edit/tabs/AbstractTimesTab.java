package de.patty.webgui.ui.workingareas.administration.recrawling.uicomponents.cronjob.edit.tabs;

import com.vaadin.data.Property;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;
import de.patty.webgui.MainPattyUI;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/9/13
 * Time: 2:52 PM
 *
 * Abstract tab class with components that every tab must have.
 */
public abstract class AbstractTimesTab extends VerticalLayout implements PatternReturner {

    /**
     * The Info label where the current tab is explained.
     */
    protected Label infoLabel;
    /**
     * The checkbox for selecting if the user wants to use textfields for manual input or option groups.
     */
    protected CheckBox manualCheckbox;
    /**
     * The Ui.
     */
    protected MainPattyUI ui;
    /**
     * The ResourceBundle.
     */
    protected ResourceBundle messages;

    /**
     * The layout for the textfields.
     */
    protected Layout manualLayout;
    /**
     * The layout for the option groups.
     */
    protected Layout optionGroupsLayout;

    /**
     * The Dummy labels (needed for layouting purposes).
     */
    protected List<Label> dummyLabels = new ArrayList<>();

    /**
     * Instantiates a new AbstractTimesTab.
     *
     * @param ui the ui
     */
    public AbstractTimesTab(final MainPattyUI ui) {
        for (int i = 0; i < 5; i++) {
            final Label lbl = new Label("dummy");
            lbl.setVisible(false);
            dummyLabels.add(lbl);
        }
        this.ui = ui;
        setWidth("100%");
        infoLabel = new Label("", ContentMode.HTML);
        infoLabel.setWidth("60%");
        messages = ui.getResourceBundle();
        manualCheckbox = new CheckBox(messages.getString("manual"));
        manualCheckbox.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                final Boolean checked = (Boolean) event.getProperty().getValue();
                manualLayout.setEnabled(checked);
                optionGroupsLayout.setEnabled(!checked);
            }
        });
        addComponent(infoLabel);
        addComponent(manualCheckbox);
    }

    /**
     * The manual layout (with the textFields for manual input) must be configured here (different fields in different
     * tabs)
     */
    public abstract void configureManualLayout();

    /**
     * The OptionGroups (minutes, hours, ...) must be configured here.
     */
    public abstract void configureHelpLayout();

}
