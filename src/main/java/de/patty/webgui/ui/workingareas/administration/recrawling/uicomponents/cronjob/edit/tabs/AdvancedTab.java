package de.patty.webgui.ui.workingareas.administration.recrawling.uicomponents.cronjob.edit.tabs;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import de.patty.rmi.managers.CronjobManager;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.Componentssetable;
import de.patty.webgui.ui.workingareas.Internationalizationable;
import de.patty.webgui.ui.workingareas.administration.recrawling.logic.CronjobSplitter;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/9/13
 * Time: 1:52 PM
 *
 * Tab for creating advanced cronjobs (without option groups, only text fields). NOT a subclass of {@link TimesTab}
 */
public class AdvancedTab extends FormLayout implements PatternReturner, Componentssetable, Internationalizationable {

    private Label infoLabel;
    private Button readCurrentDataIntoFieldsButton;
    private TextField minutesField;
    private TextField hoursField;
    private TextField monthsField;
    private TextField monthDaysField;
    private TextField weekDaysField;
    private MainPattyUI ui;
    private ResourceBundle messages;

    /**
     * Instantiates a new AdvancedTab.
     *
     * @param ui the ui
     */
    public AdvancedTab(final MainPattyUI ui) {
        this.ui = ui;
        messages = ui.getResourceBundle();
        infoLabel = new Label("", ContentMode.HTML);
        minutesField = new TextField();
        minutesField.setWidth("60%");
        hoursField = new TextField();
        hoursField.setWidth("60%");
        monthDaysField = new TextField();
        monthDaysField.setWidth("60%");
        monthsField = new TextField();
        monthsField.setWidth("60%");
        weekDaysField = new TextField();
        weekDaysField.setWidth("60%");
        readCurrentDataIntoFieldsButton = new Button();
        readCurrentDataIntoFieldsButton.setStyleName("link");
        readCurrentDataIntoFieldsButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                final CronjobManager cronjobManager = new CronjobManager();
                final String cronjobLine = cronjobManager.getCronjobLine();
                final CronjobSplitter splitter = new CronjobSplitter(cronjobLine);
                final String cronjobPattern = splitter.getCronjobPattern();
                final String[] patternParts = cronjobPattern.split("\\s");
                minutesField.setValue(patternParts[0]);
                hoursField.setValue(patternParts[1]);
                monthDaysField.setValue(patternParts[2]);
                monthsField.setValue(patternParts[3]);
                weekDaysField.setValue(patternParts[4]);
            }
        });
        setComponents();
        doInternationalization();
    }

    @Override
    public String returnPattern() {
        final String minutes = minutesField.getValue();
        final String hours = hoursField.getValue();
        final String months = monthsField.getValue();
        final String monthDays = monthDaysField.getValue();
        final String weekDays = weekDaysField.getValue();
        final String pattern = minutes + " " + hours + " " + monthDays + " " + months + " " + weekDays;
        return pattern;
    }

    @Override
    public void setComponents() {
        addComponents(infoLabel, readCurrentDataIntoFieldsButton, minutesField, hoursField, monthDaysField, monthsField, weekDaysField);
    }

    @Override
    public void doInternationalization() {
        infoLabel.setValue(messages.getString("infoadvancedtab"));
        readCurrentDataIntoFieldsButton.setCaption(messages.getString("readincurrentdata"));
        minutesField.setCaption(messages.getString("minutes"));
        hoursField.setCaption(messages.getString("hours"));
        monthDaysField.setCaption(messages.getString("monthdays"));
        monthsField.setCaption(messages.getString("months"));
        weekDaysField.setCaption(messages.getString("days"));
    }
}
