package de.patty.webgui.ui.workingareas.administration.recrawling.uicomponents.cronjob.edit.tabs;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/9/13
 * Time: 1:27 PM
 *
 * Interface for all components that return a cronjob pattern (all cronjob tabs).
 */
public interface PatternReturner {
    public String returnPattern();
}
