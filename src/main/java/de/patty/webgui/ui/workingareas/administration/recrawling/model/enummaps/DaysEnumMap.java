package de.patty.webgui.ui.workingareas.administration.recrawling.model.enummaps;

import de.patty.webgui.ui.workingareas.administration.recrawling.model.enums.Days;

import java.util.EnumMap;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/9/13
 * Time: 3:42 PM
 *
 * Map to link the different days from the {@link Days} enum to integer values.
 */
public class DaysEnumMap extends EnumMap {

    /**
     * Instantiates a new DaysEnumMap.
     *
     * @param map the map
     */
    public DaysEnumMap(EnumMap<Days, Integer> map) {
        super(map);
        put(Days.SUNDAY, 0);
        put(Days.MONDAY, 1);
        put(Days.TUESDAY, 2);
        put(Days.WEDNESDAY, 3);
        put(Days.THURSDAY, 4);
        put(Days.FRIDAY, 5);
        put(Days.SATURDAY, 6);
    }
}
