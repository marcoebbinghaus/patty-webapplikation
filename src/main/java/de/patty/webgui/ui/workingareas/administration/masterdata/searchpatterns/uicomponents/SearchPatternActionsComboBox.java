package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.uicomponents;

import com.vaadin.data.Property;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.ComboBox;
import de.patty.etc.Constants;
import de.patty.persistence.entities.SearchPattern;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.logic.SearchPatternActivateOrUnactivateCommand;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.logic.SearchPatternCommand;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.logic.SearchPatternDeleteCommand;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.logic.SearchPatternEditCommand;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.model.SearchPatternActions;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.model.UnActivateEnum;

import java.util.Arrays;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/17/13
 * Time: 1:12 PM
 *
 * ComboBox which contains all available commands for the single selected SearchPattern in the {@link SearchPatternsTable}.
 * It is displayed in a generated column in every row of the table.
 */
public class SearchPatternActionsComboBox extends ComboBox {


    public SearchPatternActionsComboBox(final MainPattyUI ui, final SearchPattern searchPattern) {
        super("", Arrays.asList(SearchPatternActions.values()));
        setNullSelectionItemId(SearchPatternActions.NONE);
        setImmediate(true);
        setItemIcon(SearchPatternActions.EDIT, new ThemeResource(Constants.ICON_EDIT));
        setItemIcon(SearchPatternActions.DELETE, new ThemeResource(Constants.ICON_DELETE));
        setItemIcon(SearchPatternActions.ACTIVATE, new ThemeResource(Constants.ICON_ACTIVATE));
        addValueChangeListener(new ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                final SearchPatternActions value = (SearchPatternActions) event.getProperty().getValue();
                SearchPatternCommand searchPatternCommand = null;
                if (value != null) {
                    switch (value) {
                        case EDIT:
                            searchPatternCommand = new SearchPatternEditCommand(ui);
                            break;
                        case DELETE:
                            searchPatternCommand = new SearchPatternDeleteCommand(ui);
                            break;
                        case ACTIVATE:
                            searchPatternCommand = new SearchPatternActivateOrUnactivateCommand(ui) {
                                @Override
                                public void setCommandType() {
                                    unActivateEnum = UnActivateEnum.ACTIVATE_OR_UNACTIVATE;
                                }
                            };
                            break;
                        default:
                            break;
                    }
                    searchPatternCommand.execute(searchPattern);
                }
                select(getNullSelectionItemId());
            }
        });
    }
}
