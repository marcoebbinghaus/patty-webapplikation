package de.patty.webgui.ui.workingareas.administration.logfiles.logic;

import de.patty.persistence.pojos.LogFile;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles.panels.uicomponents.LogFileDownloadWindow;

import java.util.Set;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/25/13
 * Time: 5:41 PM
 *
 * Concrete command to open a download window.
 */
public class LogFileOpenDownloadWindowCommand extends LogFileCommand {

    /**
     * Instantiates a new LogFileOpenDownloadWindowCommand.
     *
     * @param ui the ui
     */
    public LogFileOpenDownloadWindowCommand(final MainPattyUI ui) {
        super(ui);
    }

    @Override
    public void execute(final LogFile logFile) {
        final LogFileDownloadWindow logFileDownloadWindow = new LogFileDownloadWindow(ui, logFile);
        ui.addWindow(logFileDownloadWindow);
    }

    @Override
    public void execute(final Set<LogFile> logFiles) {
        //only a single logfile can be downloaded at a time.
    }
}
