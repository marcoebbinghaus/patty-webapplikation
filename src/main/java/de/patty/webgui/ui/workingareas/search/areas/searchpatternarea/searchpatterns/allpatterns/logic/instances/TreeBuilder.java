package de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.searchpatterns.allpatterns.logic.instances;

import com.vaadin.data.Property;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.Tree;
import de.patty.persistence.entities.SearchPattern;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.SearchPatternPanel;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.searchpatterns.allpatterns.logic.AllSearchPatternsBuilder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/6/13
 * Time: 10:05 AM
 *
 */
public class TreeBuilder extends AllSearchPatternsBuilder {

    private Tree tree;
    private HierarchicalContainer container;

    public TreeBuilder(final List<SearchPattern> searchPatterns, final SearchPatternPanel searchPatternPanel) {
        super(searchPatterns, searchPatternPanel);
    }

    @Override
    protected void buildSpecificDisplayComponent() {
        container = new HierarchicalContainer();
        buildTreeContainer();
        tree = new Tree("", container);
        tree.setSizeFull();
        tree.setItemCaptionMode(AbstractSelect.ItemCaptionMode.ID);
        tree.setNullSelectionAllowed(true);
        tree.setImmediate(true);
        tree.setMultiSelect(false);
        tree.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                final SearchPattern selectedSearchPattern = (SearchPattern) valueChangeEvent.getProperty().getValue();
                searchPatternPanel.setSelectedSearchPattern(selectedSearchPattern);
                searchPatternPanel.getButtonLeiste().reload(selectedSearchPattern);
            }
        });
        for (final SearchPattern searchPattern : searchPatterns) {
            if (searchPattern.getParent() == null) {
                tree.expandItemsRecursively(searchPattern);
            }
        }
        displayComponent = tree;
    }

    private void buildTreeContainer() {
        Collections.sort(searchPatterns);
        final List<SearchPattern> rootSearchPatterns = new ArrayList<>();
        for (final SearchPattern searchPattern : searchPatterns) {
            if (searchPattern.getParent() == null) {
                rootSearchPatterns.add(searchPattern);
            }
        }
        buildTree(rootSearchPatterns, null);
    }

    private void buildTree(final List<SearchPattern> searchPatterns, final SearchPattern parentPattern) {
        for (final SearchPattern searchPattern : searchPatterns) {
            container.addItem(searchPattern);
            container.setParent(searchPattern, parentPattern);
            if (searchPattern.getChildren() == null || searchPattern.getChildren().isEmpty()) {
            } else {
                buildTree(new ArrayList<>(searchPattern.getChildren()), searchPattern);
            }
        }
    }

}
