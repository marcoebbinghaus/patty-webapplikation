package de.patty.webgui.ui.workingareas.administration.recrawling.uicomponents.cronjob.edit.tabs;

import com.vaadin.ui.TextField;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.administration.recrawling.logic.CronjobOptionGroupValueChangeListener;
import de.patty.webgui.ui.workingareas.administration.recrawling.model.enummaps.MonthsEnumMap;
import de.patty.webgui.ui.workingareas.administration.recrawling.model.enums.Months;
import de.patty.webgui.ui.workingareas.administration.recrawling.uicomponents.cronjob.edit.AdvancedOptionGroup;

import java.util.Arrays;
import java.util.EnumMap;

import static de.patty.etc.Constants.MONTH_DAYS;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/9/13
 * Time: 1:28 PM
 *
 * Tab for creating monthly cronjobs (minutes, hours, months, days of month).
 */
public class MonthlyTimesTab extends TimesTab {

    private TextField monthsField;
    private TextField monthDaysField;
    private AdvancedOptionGroup monthsOptionGroup;
    private AdvancedOptionGroup monthsDaysOptionGroup;
    private MonthsEnumMap monthsEnumMap;

    /**
     * Instantiates a new MonthlyTimesTab.
     *
     * @param ui the ui
     */
    public MonthlyTimesTab(final MainPattyUI ui) {
        super(ui);
        monthsEnumMap = new MonthsEnumMap(new EnumMap<Months, Integer>(Months.class));
        infoLabel.setValue(messages.getString("infomonthlytimestab"));
        monthsField = new TextField(messages.getString("months"));
        monthsField.setWidth("60%");
        monthDaysField = new TextField(messages.getString("monthdays"));
        monthDaysField.setWidth("60%");
        monthsDaysOptionGroup = new AdvancedOptionGroup(ui, "monthdays", Arrays.asList(MONTH_DAYS));
        monthsDaysOptionGroup.addValueChangeListener(new CronjobOptionGroupValueChangeListener<String>(monthDaysField));
        monthsOptionGroup = new AdvancedOptionGroup(ui, "months", Arrays.asList(Months.values()));
        monthsOptionGroup.setStyleName("horizontal");
        monthsOptionGroup.addValueChangeListener(new CronjobOptionGroupValueChangeListener<Months>(monthsField, monthsEnumMap));
        manualLayout.addComponent(monthsField);
        optionGroup3Layout.removeAllComponents();
        optionGroup3Layout.addComponent(monthsOptionGroup);
        optionGroup4Layout.removeAllComponents();
        optionGroup4Layout.addComponent(monthsDaysOptionGroup);
    }

    @Override
    public String returnPattern() {
        final String minutes = minutesField.getValue();
        final String hours = hoursField.getValue();
        final String months = monthsField.getValue();
        final String monthDays = monthDaysField.getValue();
        final String pattern = minutes + " " + hours + " " + monthDays + " " + months + " *";
        return pattern;
    }
}
