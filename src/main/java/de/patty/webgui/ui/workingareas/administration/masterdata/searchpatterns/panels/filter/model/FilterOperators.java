package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.model;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/22/13
 * Time: 4:00 PM
 *
 * Enum which contains all supported filter operators (by now only equals is supported).
 */
public enum FilterOperators {
    EQUALS,
    NOT_EQUALS,
    GREATER,
    LESS,
    IS_NULL,
    IS_NOT_NULL;

    private static ResourceBundle messages;

    public static void setResourceBundle(final ResourceBundle messages) {
        FilterOperators.messages = messages;
    }

    public String toString() {
        return messages.getString(name() + ".i18n");
    }
}
