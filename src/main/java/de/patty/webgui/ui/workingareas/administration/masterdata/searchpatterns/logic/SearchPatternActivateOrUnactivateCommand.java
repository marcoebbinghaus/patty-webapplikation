package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.logic;

import de.patty.persistence.entities.SearchPattern;
import de.patty.persistence.entitymanagers.SearchPatternManager;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.ConfirmDialog;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.model.UnActivateEnum;

import java.util.Iterator;
import java.util.Set;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/17/13
 * Time: 3:58 PM
 *
 * This command is for activating / unactivating / inverting the activation state of a {@link SearchPattern}.
 */
public abstract class SearchPatternActivateOrUnactivateCommand extends SearchPatternCommand {

    /**
     * Contains the command (activate, unactivate or invert)
     */
    protected UnActivateEnum unActivateEnum;

    /**
     * Instantiates a new SearchPatternActivateOrUnactivateCommand.
     *
     * @param ui the ui
     */
    public SearchPatternActivateOrUnactivateCommand(final MainPattyUI ui) {
        super(ui);
    }

    @Override
    public void execute(final SearchPattern searchPattern) {
        String confirmTextKeyword = "";
        setCommandType();
        switch (unActivateEnum) {
            case ACTIVATE:
                confirmTextKeyword = "activateconfirm";
                break;
            case UNACTIVATE:
                confirmTextKeyword = "unactivateconfirm";
                break;
            case ACTIVATE_OR_UNACTIVATE:
                if (searchPattern.getActivated() == true) {
                    confirmTextKeyword = "unactivateconfirm";
                } else {
                    confirmTextKeyword = "activateconfirm";
                }
                break;
            default:
                break;
        }
        final ConfirmDialog confirmDialog = new ConfirmDialog(searchPattern.getTitle() + ": " + ui.getResourceBundle().getString(confirmTextKeyword), ui) {
            @Override
            public void doThisOnOK() {
                final SearchPatternManager searchPatternManager = new SearchPatternManager();
                switch (unActivateEnum) {
                    case ACTIVATE:
                        searchPatternManager.activateSearchPattern(searchPattern);
                        break;
                    case UNACTIVATE:
                        searchPatternManager.unactivateSearchPattern(searchPattern);
                        break;
                    case ACTIVATE_OR_UNACTIVATE:
                        searchPatternManager.activateOrUnactivateSearchPattern(searchPattern);
                        break;
                    default:
                        break;
                }
                ui.getScreen().reload();
            }

            @Override
            public void doThisOnCancel() {
                //nothing
            }

            @Override
            public void setComponents() {
                //nothing
            }

            @Override
            public void doInternationalization() {
                //nothing
            }
        };
        confirmDialog.getButtonBar().activateShortcuts(true);
        confirmDialog.getButtonBar().setLinkStyle(true);
        ui.addWindow(confirmDialog);
    }

    @Override
    public void execute(Set<SearchPattern> searchPatterns) {
        final Iterator<SearchPattern> searchPatternIterator = searchPatterns.iterator();
        if (searchPatterns.size() == 1) {
            while (searchPatternIterator.hasNext()) {
                final SearchPattern searchPattern = searchPatternIterator.next();
                execute(searchPattern);
            }
        } else {
            String confirmTextKeyword = "";
            setCommandType();
            switch (unActivateEnum) {
                case ACTIVATE:
                    confirmTextKeyword = "activateconfirm";
                    break;
                case UNACTIVATE:
                    confirmTextKeyword = "unactivateconfirm";
                    break;
                case ACTIVATE_OR_UNACTIVATE:
                    confirmTextKeyword = "activateorunactivateconfirm";
                    break;
                default:
                    break;
            }
            final StringBuilder confirmMessage = new StringBuilder();
            confirmMessage.append(searchPatterns.size() + " " + ui.getResourceBundle().getString("searchpatterns"));
            confirmMessage.append(": " + ui.getResourceBundle().getString(confirmTextKeyword));
            final ConfirmDialog confirmDialog = new ConfirmDialog(confirmMessage.toString(), ui) {
                @Override
                public void doThisOnOK() {
                    final SearchPatternManager searchPatternManager = new SearchPatternManager();
                    switch (unActivateEnum) {
                        case ACTIVATE:
                            while (searchPatternIterator.hasNext()) {
                                final SearchPattern searchPattern = searchPatternIterator.next();
                                searchPatternManager.activateSearchPattern(searchPattern);
                            }
                            break;
                        case UNACTIVATE:
                            while (searchPatternIterator.hasNext()) {
                                final SearchPattern searchPattern = searchPatternIterator.next();
                                searchPatternManager.unactivateSearchPattern(searchPattern);
                            }
                            break;
                        case ACTIVATE_OR_UNACTIVATE:
                            while (searchPatternIterator.hasNext()) {
                                final SearchPattern searchPattern = searchPatternIterator.next();
                                searchPatternManager.activateOrUnactivateSearchPattern(searchPattern);
                            }
                            break;
                        default:
                            break;
                    }
                    ui.getScreen().reload();
                }

                @Override
                public void doThisOnCancel() {
                    //nothing
                }

                @Override
                public void setComponents() {
                    //nothing
                }

                @Override
                public void doInternationalization() {
                    //nothing
                }
            };
            confirmDialog.getButtonBar().activateShortcuts(true);
            confirmDialog.getButtonBar().setLinkStyle(true);
            ui.addWindow(confirmDialog);
        }
    }

    /**
     * The UnActivateEnum's state is set in here whenever a {@link SearchPatternActivateOrUnactivateCommand} is instantiated.
     */
    public abstract void setCommandType();
}
