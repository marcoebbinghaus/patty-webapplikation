package de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.searchpatterns.allpatterns.logic;

import com.vaadin.ui.Component;
import de.patty.persistence.entities.SearchPattern;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.SearchPatternPanel;

import java.util.List;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/6/13
 * Time: 10:11 AM
 *
 */
public abstract class AllSearchPatternsBuilder {

    protected List<SearchPattern> searchPatterns;
    protected SearchPatternPanel searchPatternPanel;
    protected Component displayComponent;

    public AllSearchPatternsBuilder(final List<SearchPattern> searchPatterns, final SearchPatternPanel searchPatternPanel) {
        this.searchPatterns = searchPatterns;
        this.searchPatternPanel = searchPatternPanel;
    }

    public Component buildComponentToDisplay() {
        buildSpecificDisplayComponent();
        return displayComponent;
    }

    protected abstract void buildSpecificDisplayComponent();

}
