package de.patty.webgui.ui.workingareas.administration.masterdata.urls;

import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.*;
import de.patty.rmi.managers.URLManager;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.RapidWindow;
import de.patty.webgui.ui.SaveCancelButtonBar;
import de.patty.webgui.ui.workingareas.FilterField;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 5/15/13
 * Time: 11:41 AM
 *
 * Window where the administrator can add one or multiple new URLs that should be recognized by the crawler.
 */
public class AddURLsWindow extends RapidWindow {
    private URLManager urlManager;
    private FormLayout fieldLayout;
    private TextArea urlsBox;
    private FilterField filterField;
    private SaveCancelButtonBar buttonLeiste;
    private List<String> existingURLs;

    /**
     * Instantiates a new AddURLsWindow.
     *
     * @param ui the ui
     */
    public AddURLsWindow(final MainPattyUI ui) {
        super(ui);
        setWidth("500px");

        urlManager = new URLManager();
        existingURLs = urlManager.getURLs();
        filterField = new FilterField(ui) {
            @Override
            public void createContainerAndSetItemCaptionMode() {
                container = new IndexedContainer(existingURLs);
            }
        };
        urlsBox = new TextArea();
        urlsBox.setSizeFull();

        buttonLeiste = new SaveCancelButtonBar() {
            @Override
            public void doInternationalization() {
                saveButton.setCaption(messagesBundle.getString("save"));
                cancelButton.setCaption(messagesBundle.getString("cancel"));
            }

            @Override
            public void setSaveButtonListener() {
                saveButton.addClickListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(final Button.ClickEvent clickEvent) {
                        boolean alreadyExistingURLsEntered = false;
                        final String boxValue = urlsBox.getValue();
                        final List<String> enteredURLs = new ArrayList<>();
                        enteredURLs.addAll(Arrays.asList(boxValue.split("\n")));
                        boolean allUrlsCorrect = true;
                        for (final String existingURL : existingURLs) {
                            if (enteredURLs.contains(existingURL)) {
                                enteredURLs.remove(existingURL);
                                alreadyExistingURLsEntered = true;
                            }
                        }
                        if (!enteredURLs.isEmpty()) {
                            for (final String urlString : enteredURLs) {
                                if (urlString.startsWith("http://")) {
                                    if (urlString.endsWith("/")) {
                                        cutEndingSlash(enteredURLs.indexOf(urlString), enteredURLs);
                                    }
                                } else {
                                    allUrlsCorrect = false;
                                }
                            }
                            if (allUrlsCorrect) {
                                final boolean success = urlManager.addURLs(enteredURLs);
                                if (!success) {
                                    //logger.warn("Fehler beim Schreiben der URLs an den Crawler")
                                    Notification.show(messagesBundle.getString("error_addurls"));
                                }
                                AddURLsWindow.this.close();
                                if (alreadyExistingURLsEntered) {
                                    Notification.show(messagesBundle.getString("urlsignored"));
                                }
                                ui.setWorkingArea(new UrlsScreen(ui));
                            } else {
                                Notification.show(messagesBundle.getString("illegalurlsentered"));
                            }
                        } else {
                            AddURLsWindow.this.close();
                            Notification.show(messagesBundle.getString("allurlsignored"));
                        }
                    }

                    private void cutEndingSlash(int index, List<String> urlStrings) {
                        final String oldUrlString = urlStrings.get(index);
                        final int length = oldUrlString.length();
                        final String newUrlString = oldUrlString.substring(0, length - 1);
                        urlStrings.set(index, newUrlString);
                    }
                });
            }

            @Override
            public void setCancelButtonListener() {
                cancelButton.addClickListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(final Button.ClickEvent clickEvent) {
                        AddURLsWindow.this.close();
                    }
                });
            }
        };
        buttonLeiste.activateCancelShortcut();

        fieldLayout = new FormLayout();
        fieldLayout.addComponent(filterField);
        fieldLayout.addComponent(urlsBox);
        fieldLayout.setSizeFull();
        doInternationalization();
        setComponents();
    }

    @Override
    public void doInternationalization() {
        setCaption(messagesBundle.getString("addurlstitle"));
        urlsBox.setCaption(messagesBundle.getString("urls"));
    }

    @Override
    public void setComponents() {
        addComponent(fieldLayout);
        addComponent(buttonLeiste);
    }

    @Override
    protected void setLayout() {
        setContentLayout(new VerticalLayout());
    }
}
