package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.logic;

import de.patty.persistence.entities.SearchPattern;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.searchpatterns.add.AddOrEditSearchPatternWindow;

import java.util.Set;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/17/13
 * Time: 3:50 PM
 *
 * This command is for editing an existing {@link SearchPattern} by opening a new {@link AddOrEditSearchPatternWindow}
 * with the selected {@link SearchPattern}.
 */
public class SearchPatternEditCommand extends SearchPatternCommand {

    /**
     * Instantiates a new SearchPatternEditCommand.
     *
     * @param ui the ui
     */
    public SearchPatternEditCommand(final MainPattyUI ui) {
        super(ui);
    }

    @Override
    public void execute(final SearchPattern searchPattern) {
        ui.addWindow(new AddOrEditSearchPatternWindow(ui, searchPattern));
    }

    @Override
    public void execute(final Set<SearchPattern> searchPatterns) {
        //not (yet) needed
    }
}
