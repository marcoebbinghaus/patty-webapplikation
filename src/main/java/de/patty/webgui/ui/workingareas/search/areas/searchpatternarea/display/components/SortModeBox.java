package de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.display.components;

import com.vaadin.ui.ComboBox;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.display.model.SortModes;

import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/6/13
 * Time: 9:48 AM
 *
 */
public class SortModeBox extends ComboBox {

    private Map<SortModes, String> enumTranslationMap;
    private Map<String, SortModes> translationEnumMap;
    private ResourceBundle messagesBundle;

    public SortModeBox(final MainPattyUI ui) {
        super();
        setImmediate(true);
        messagesBundle = ui.getResourceBundle();
        buildMaps();
        setNullSelectionAllowed(false);
        for (final Map.Entry<SortModes, String> sortModesStringEntry : enumTranslationMap.entrySet()) {
            addItem(sortModesStringEntry.getValue());
        }
        setTheValue(SortModes.ASCENDING);
        setEnabled(false);
    }

    private void buildMaps() {
        enumTranslationMap = new TreeMap<>();
        enumTranslationMap.put(SortModes.ASCENDING, messagesBundle.getString("sortmode_asc"));
        enumTranslationMap.put(SortModes.DESCENDING, messagesBundle.getString("sortmode_desc"));
        translationEnumMap = new TreeMap<>();
        for (final Map.Entry<SortModes, String> entry : enumTranslationMap.entrySet()) {
            translationEnumMap.put(entry.getValue(), entry.getKey());
        }
    }

    public void setTheValue(final SortModes mode) {
        setValue(enumTranslationMap.get(mode));
        select(enumTranslationMap.get(mode));
    }

    public SortModes getTheValue() {
        return translationEnumMap.get(super.getValue().toString());
    }
}
