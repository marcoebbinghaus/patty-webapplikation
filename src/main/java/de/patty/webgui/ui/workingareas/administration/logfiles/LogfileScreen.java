package de.patty.webgui.ui.workingareas.administration.logfiles;

import com.vaadin.ui.*;
import de.patty.rmi.managers.NoConnectionException;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.RapidPanel;
import de.patty.webgui.ui.workingareas.ReloadScreenButton;
import de.patty.webgui.ui.workingareas.Screen;
import de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles.LogFilesPanel;
import de.patty.webgui.ui.workingareas.administration.logfiles.panels.showlogfile.ShowLogFilePanel;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 5/15/13
 * Time: 10:44 AM
 *
 * The {@link Screen} where the user can see the available logfiles and download them.
 */
public class LogfileScreen extends Screen {

    private boolean connectionToCrawlerSystem;
    private HorizontalLayout frameLayout;
    private RapidPanel logFilesPanel;
    private RapidPanel showLogfilePanel;


    /**
     * Instantiates a new LogfileScreen.
     *
     * @param ui the ui
     */
    public LogfileScreen(final MainPattyUI ui) {
        super(ui);
        connectionToCrawlerSystem = true;
        try {
            frameLayout = new HorizontalLayout();
            frameLayout.setSizeFull();
            frameLayout.setSpacing(false);
            logFilesPanel = new LogFilesPanel(ui);
            showLogfilePanel = new ShowLogFilePanel(ui);
        } catch (final NoConnectionException e) { //Verbindung zum CrawlerSystem gescheitert
            connectionToCrawlerSystem = false;
        }
        setComponents();
        doInternationalization();
    }

    @Override
    public void reload() {
        ui.setWorkingArea(new LogfileScreen(ui));
    }


    @Override
    public void setComponents() {
        activeVerticalFullScreenSize(true);
        if (connectionToCrawlerSystem) {
            frameLayout.addComponent(logFilesPanel);
            frameLayout.addComponent(showLogfilePanel);
            addComponent(frameLayout);
        } else {
            addComponent(new Label(messagesBundle.getString("reloadwhencrawlerisbackup")));
            final ReloadScreenButton reloadButton = new ReloadScreenButton(ui);
            addComponent(reloadButton);
            getContentLayout().setExpandRatio(reloadButton, 1.0f);
            Notification.show(messagesBundle.getString("noconnectiontocrawler"));
        }
    }

    @Override
    public void doInternationalization() {

    }

    /**
     * Gets the textArea (which lays in nested panels inside this screen) in which the content of the selected log file is
     * shown.
     *
     * @return the contentTextArea
     */
    public TextArea getShowLogFileTextArea() {
        return ((ShowLogFilePanel) showLogfilePanel).getLogFileArea();
    }

    /**
     * Gets the ListSelect (which lays in nested panels inside this screen) in which the available log files are listed.
     *
     * @return the log files ListSelect
     */
    public ListSelect getLogFilesListSelect() {
        return ((LogFilesPanel) logFilesPanel).getLogFilesListSelect();
    }

}
