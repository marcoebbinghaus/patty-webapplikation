package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.logic;

import de.patty.persistence.entities.SearchPattern;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.selectedpattern.windows.RatingWindow;

import java.util.Set;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/19/13
 * Time: 11:15 AM
 *
 * This command is for rating an existing {@link SearchPattern} by opening a new {@link RatingWindow}.
 */
public class SearchPatternRateCommand extends SearchPatternCommand {

    /**
     * Instantiates a new SearchPatternRateCommand.
     *
     * @param ui the ui
     */
    public SearchPatternRateCommand(final MainPattyUI ui) {
        super(ui);
    }

    @Override
    public void execute(final SearchPattern searchPattern) {
        ui.addWindow(new RatingWindow(ui, searchPattern));
    }

    @Override
    public void execute(Set<SearchPattern> searchPatterns) {
        //not (yet) needed
    }
}
