package de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles.panels.uicomponents;

import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import de.patty.etc.Constants;
import de.patty.etc.utilities.OnDemandFileDownloader;
import de.patty.persistence.pojos.LogFile;
import de.patty.webgui.BasePattyUI;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.RapidWindow;
import de.patty.webgui.ui.workingareas.administration.logfiles.logic.LogFileImportCommand;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/26/13
 * Time: 9:27 AM
 *
 * The window that is displayed when the user wants to download a log file
 */
public class LogFileDownloadWindow extends RapidWindow {

    private Label infoLabel;
    private HorizontalLayout getFileFromCrawlerSystemLayout;
    private HorizontalLayout startDownloadLayout;
    private Button getFileFromCrawlerSystemButton;
    private Button startDownloadButton;
    private ProgressIndicator getFileFromCrawlerSystemProgressIndicator;
    private Image getFileFromCrawlerSystemImage;
    private LogFile logFile;


    public LogFileDownloadWindow(final BasePattyUI ui, final LogFile logFile) {
        super(ui);
        setWidth("500px");
        this.logFile = logFile;
        infoLabel = new Label();
        infoLabel.setContentMode(ContentMode.HTML);
        getFileFromCrawlerSystemImage = new Image();
        getFileFromCrawlerSystemImage.setSource(new ThemeResource(Constants.ICON_NOT_OK));
        createFirstLayout();
        createSecondLayout();
        checkIfFileAlreadyExists();
        setComponents();
        doInternationalization();
    }

    private void checkIfFileAlreadyExists() {
        final File existingFile = new File(Constants.PATH_TO_DOWNLOADED_FILES + "/" + logFile.getFileName());
        if (existingFile.exists()) {
            getFileFromCrawlerSystemButton.setEnabled(false);
            getFileFromCrawlerSystemImage.setSource(new ThemeResource(Constants.ICON_OK));
            startDownloadButton.setEnabled(true);
        }
    }

    private void createFirstLayout() {
        getFileFromCrawlerSystemLayout = new HorizontalLayout();
        getFileFromCrawlerSystemLayout.setSpacing(true);
        getFileFromCrawlerSystemButton = new Button();
        getFileFromCrawlerSystemButton.setWidth("100px");
        getFileFromCrawlerSystemButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                final LogFileImportCommand importCommand = new LogFileImportCommand((MainPattyUI) ui) {
                    @Override
                    public void doSomethingAfterRMI() {
                        getFileFromCrawlerSystemImage.setSource(new ThemeResource(Constants.ICON_OK));
                        getFileFromCrawlerSystemImage.setVisible(true);
                        getFileFromCrawlerSystemProgressIndicator.setVisible(false);
                        startDownloadButton.setEnabled(true);
                    }
                };
                importCommand.execute(logFile);
                getFileFromCrawlerSystemButton.setEnabled(false);
                getFileFromCrawlerSystemImage.setVisible(false);
                getFileFromCrawlerSystemProgressIndicator.setVisible(true);
            }
        });
        getFileFromCrawlerSystemProgressIndicator = new ProgressIndicator();
        getFileFromCrawlerSystemProgressIndicator.setIndeterminate(true);
        getFileFromCrawlerSystemProgressIndicator.setVisible(false);
        getFileFromCrawlerSystemProgressIndicator.setEnabled(true);
        getFileFromCrawlerSystemLayout.addComponent(getFileFromCrawlerSystemButton);
        getFileFromCrawlerSystemLayout.addComponent(getFileFromCrawlerSystemImage);
        getFileFromCrawlerSystemLayout.addComponent(getFileFromCrawlerSystemProgressIndicator);
        getFileFromCrawlerSystemLayout.setComponentAlignment(getFileFromCrawlerSystemButton, Alignment.BOTTOM_LEFT);
        getFileFromCrawlerSystemLayout.setComponentAlignment(getFileFromCrawlerSystemProgressIndicator, Alignment.BOTTOM_LEFT);
    }

    private void createSecondLayout() {
        final OnDemandFileDownloader onDemandFileDownloader = new OnDemandFileDownloader(new OnDemandFileDownloader.OnDemandStreamResource() {
            @Override
            public String getFilename() {
                return logFile.getFileName();
            }

            @Override
            public InputStream getStream() {
                try {
                    return new FileInputStream(Constants.PATH_TO_DOWNLOADED_FILES + "/" + getFilename());
                } catch (FileNotFoundException e) {
                    return null;
                }
            }
        });
        startDownloadLayout = new HorizontalLayout();
        startDownloadButton = new Button();
        startDownloadButton.setWidth("100px");
        onDemandFileDownloader.extend(startDownloadButton);
        startDownloadButton.setEnabled(false);
        startDownloadLayout.addComponent(startDownloadButton);
    }

    @Override
    protected void setLayout() {
        setContentLayout(new FormLayout());
    }

    @Override
    public void setComponents() {
        addComponent(infoLabel);
        addComponent(getFileFromCrawlerSystemLayout);
        addComponent(startDownloadLayout);
    }

    @Override
    public void doInternationalization() {
        infoLabel.setValue(messagesBundle.getString("info_download"));
        getFileFromCrawlerSystemButton.setCaption(messagesBundle.getString("getfilefromcrawler"));
        startDownloadButton.setCaption(messagesBundle.getString("downloadlogfile"));
    }
}
