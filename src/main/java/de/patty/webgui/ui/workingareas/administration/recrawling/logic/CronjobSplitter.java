package de.patty.webgui.ui.workingareas.administration.recrawling.logic;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/9/13
 * Time: 12:21 PM
 *
 * Utility class to split a complete cronjob line into the cronjob pattern (min h dom mon dow) and the rest (command)
 */
public class CronjobSplitter {

    private String cronjobPattern;
    private String cronjobCommand;

    /**
     * Instantiates a new CronjobSplitter.
     *
     * @param cronjobLine the complete cronjob line
     */
    public CronjobSplitter(final String cronjobLine) {
        final String[] cronjobLineParts = cronjobLine.split("\\s");
        final StringBuilder patternBuilder = new StringBuilder();
        final StringBuilder commandBuilder = new StringBuilder();
        for (int i = 0; i < cronjobLineParts.length; i++) {
            if (i <= 4) {
                patternBuilder.append(cronjobLineParts[i] + " ");
            } else {
                commandBuilder.append(cronjobLineParts[i] + " ");
            }
        }
        patternBuilder.deleteCharAt(patternBuilder.length() - 1);
        commandBuilder.deleteCharAt(commandBuilder.length() - 1);
        cronjobPattern = patternBuilder.toString();
        cronjobCommand = commandBuilder.toString();
    }

    /**
     * Gets the cronjob pattern.
     *
     * @return the cronjob pattern
     */
    public String getCronjobPattern() {
        return cronjobPattern;
    }

    /**
     * Gets the cronjob command.
     *
     * @return the cronjob command
     */
    public String getCronjobCommand() {
        return cronjobCommand;
    }
}
