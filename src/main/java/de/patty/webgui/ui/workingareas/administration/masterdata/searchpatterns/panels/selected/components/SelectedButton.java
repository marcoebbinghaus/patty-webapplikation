package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.selected.components;

import com.vaadin.ui.Button;
import de.patty.etc.designpatterns.observer.Observer;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.uicomponents.SearchPatternsTable;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/22/13
 * Time: 12:07 PM
 *
 * Abstract class for all buttons that react on (multiple) selected SearchPatterns in the {@link SearchPatternsTable}.
 */
public abstract class SelectedButton extends Button implements Observer {

    /**
     * The SearchPatternsTable.
     */
    protected SearchPatternsTable table;
    /**
     * The Ui.
     */
    protected MainPattyUI ui;
    /**
     * The ResourceBundle (i18n).
     */
    protected ResourceBundle messages;

    /**
     * Instantiates a new SelectedButton.
     *
     * @param table the SearchPatternsTable
     * @param ui the ui
     */
    public SelectedButton(final SearchPatternsTable table, final MainPattyUI ui) {
        this.table = table;
        this.ui = ui;
        setEnabled(false);
        table.attach(this);
        this.messages = ui.getResourceBundle();
        setStyleName("link");
    }

    @Override
    public void update() {
        for (final Object clickListener : getListeners(Button.ClickEvent.class)) {
            removeClickListener((Button.ClickListener) clickListener);
        }
        if (table.getSelectedSearchPatterns().isEmpty()) {
            setEnabled(false);
        } else {
            setEnabled(true);
        }
        addClickListener();
    }

    /**
     * Here the corresponding command must be executed.
     */
    protected abstract void addClickListener();


}
