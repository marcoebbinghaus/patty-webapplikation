package de.patty.webgui.ui.workingareas.search.model.exceptions;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/7/13
 * Time: 5:13 PM
 *
 */
public class UndeletableSearchPatternException extends Throwable {
}
