package de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles.panels.filterpanel.model;

import de.patty.persistence.pojos.LogFile;

import java.util.List;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/26/13
 * Time: 2:21 PM
 *
 * Interface for the different filters (date, size)
 */
public interface LogFileFilter {

    /**
     * Filters the log files.
     *
     * @param listToFilter the list to filter
     * @return the filtered list
     */
    public List<LogFile> filterLogFiles(final List<LogFile> listToFilter);
}
