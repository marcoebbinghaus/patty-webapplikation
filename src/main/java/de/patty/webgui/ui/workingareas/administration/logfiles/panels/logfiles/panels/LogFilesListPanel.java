package de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles.panels;

import com.vaadin.ui.ListSelect;
import de.patty.rmi.managers.NoConnectionException;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.RapidPanel;
import de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles.panels.uicomponents.LogFilesButtonBar;
import de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles.panels.uicomponents.LogFilesListSelect;
import de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles.panels.uicomponents.LogFilesListSelectValueChangeListener;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/25/13
 * Time: 1:25 PM
 *
 * The panel which contains the {@link ListSelect} with the available (maybe filtered) log files.
 */
public class LogFilesListPanel extends RapidPanel {

    private ListSelect logFilesListSelect;
    private LogFilesButtonBar logFilesButtonBar;
    private MainPattyUI ui;

    /**
     * Instantiates a new LogFilesListPanel.
     *
     * @param ui the ui
     * @throws NoConnectionException thrown if no connection to the crawler system is established.
     */
    public LogFilesListPanel(final MainPattyUI ui) throws NoConnectionException {
        this.ui = ui;
        setCaption(ui.getResourceBundle().getString("listoflogfiles"));
        setSizeFull();
        getContentLayout().setSizeFull();
        logFilesListSelect = new LogFilesListSelect(ui);
        final LogFilesListSelectValueChangeListener valueChangeListener = new LogFilesListSelectValueChangeListener();
        logFilesListSelect.addValueChangeListener(valueChangeListener);
        logFilesButtonBar = new LogFilesButtonBar(ui, valueChangeListener);
        addComponent(logFilesButtonBar);
        addComponent(logFilesListSelect);
        getContentLayout().setExpandRatio(logFilesListSelect, 1.0f);
    }

    /**
     * Gets the {@link LogFilesListSelect} from the panel.
     *
     * @return the LogFilesListSelect
     */
    public ListSelect getLogFilesListSelect() {
        return logFilesListSelect;
    }
}
