package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.selected.components;

import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.logic.SearchPatternActivateOrUnactivateCommand;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.model.UnActivateEnum;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.uicomponents.SearchPatternsTable;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/22/13
 * Time: 12:18 PM
 *
 * Button which executes a command to invert the activation states of the selected {@link de.patty.persistence.entities.SearchPattern}s in the
 * {@link SearchPatternsTable}.
 */
public class ActivateOrUnactivateSelectedButton extends SelectedButton {

    /**
     * Instantiates a new ActivateOrUnactivateSelectedButton.
     *
     * @param table the table
     * @param ui the ui
     */
    public ActivateOrUnactivateSelectedButton(final SearchPatternsTable table, final MainPattyUI ui) {
        super(table, ui);
        setCaption(messages.getString("activateorunactivateselected"));
    }

    @Override
    protected void addClickListener() {
        addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                final SearchPatternActivateOrUnactivateCommand activateOrUnactivateCommand = new SearchPatternActivateOrUnactivateCommand(ui) {
                    @Override
                    public void setCommandType() {
                        unActivateEnum = UnActivateEnum.ACTIVATE_OR_UNACTIVATE;
                    }
                };
                activateOrUnactivateCommand.execute(table.getSelectedSearchPatterns());
            }
        });
    }
}
