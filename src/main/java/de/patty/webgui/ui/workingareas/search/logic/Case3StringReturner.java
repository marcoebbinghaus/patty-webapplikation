package de.patty.webgui.ui.workingareas.search.logic;

import de.patty.etc.Constants;
import de.patty.persistence.entities.SearchPattern;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/30/13
 * Time: 9:30 AM
 *
 * <p/>
 * EIN SUCHMUSTER, EINE SOLR-QUERY
 */
public class Case3StringReturner extends SearchStringReturner {

    public Case3StringReturner(final String searchInput, final SearchPattern searchPattern) {
        if (searchInput.equals("")) {
            finalSearchString = createPatternContentWithoutUserInput(searchPattern);
        } else {
            finalSearchString = linkPatternContentAndUserInput(searchInput, searchPattern);
        }
    }

    private String linkPatternContentAndUserInput(final String searchInput, final SearchPattern searchPattern) {
        final String[] searchPatternParts = searchPattern.getContent().split("\\s");
        boolean inputVariableExists = false;
        for (final String searchPatternPart : searchPatternParts) {
            if (searchPatternPart.contains(Constants.PATTYSEARCH_INPUT_CONSTANT)) {
                inputVariableExists = true;
            }
        }
        String searchPatternContent = searchPattern.getContent();
        if (inputVariableExists) {
            searchPatternContent = createPatternContentWithoutUserInput(searchPattern);
        }
        return appendSolrQueryStringToSearchPatternContentWithoutInputVariables(searchPatternContent, searchInput);
    }


}
