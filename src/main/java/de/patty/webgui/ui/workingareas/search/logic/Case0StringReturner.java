package de.patty.webgui.ui.workingareas.search.logic;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/29/13
 * Time: 12:01 PM
 *
 * <p/>
 * KEIN SUCHMUSTER, KEINE SOLR-QUERY
 */
public class Case0StringReturner extends SearchStringReturner {

    public Case0StringReturner(final String searchInput) {
        if (searchInput.equals("")) {
            finalSearchString = null;
        } else {
            finalSearchString = createStandardSearchForNormalSearchWords(searchInput);
        }
    }

    private String createStandardSearchForNormalSearchWords(final String searchInput) {
        final StringBuilder stringBuilder = new StringBuilder();
        final String[] searchTerms = searchInput.split("\\s");
        stringBuilder.append(CONTENT + "(");
        for (final String searchTerm : searchTerms) {
            stringBuilder.append(searchTerm + AND);
        }
        stringBuilder.delete(stringBuilder.length() - AND.length(), stringBuilder.length());
        stringBuilder.append(")^20" + OR + "(");
        for (final String searchTerm : searchTerms) {
            stringBuilder.append(CONTENT + searchTerm + OR + TITLE + searchTerm + OR);
        }
        stringBuilder.delete(stringBuilder.length() - OR.length(), stringBuilder.length());
        stringBuilder.append(")");
        return stringBuilder.toString();
    }
}
