package de.patty.webgui.ui.workingareas.search.areas.searchpatternarea;

import com.vaadin.ui.Layout;
import de.patty.persistence.entities.SearchPattern;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.RapidPanel;
import de.patty.webgui.ui.workingareas.Componentssetable;
import de.patty.webgui.ui.workingareas.Internationalizationable;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.display.DisplayLayout;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.display.components.DisplayModeValueChangeListener;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.display.model.DisplayModes;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.searchpatterns.AllSearchPatternsPanel;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.selectedpattern.components.SelectedSearchPatternButtonLeiste;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/4/13
 * Time: 12:28 PM
 *
 */
public class SearchPatternPanel extends RapidPanel implements Componentssetable, Internationalizationable {

    //private SelectedSearchPatternPanel selectedSearchPatternPanel;
    private DisplayLayout displayLayout;
    private AllSearchPatternsPanel allSearchPatternsPanel;


    private DisplayModeValueChangeListener displayModeValueChangeListener;


    public SearchPatternPanel(final MainPattyUI ui) {
        getContentLayout().setMargin(false);
        //selectedSearchPatternPanel = new SelectedSearchPatternPanel(ui, null);
        displayLayout = new DisplayLayout(ui);
        allSearchPatternsPanel = new AllSearchPatternsPanel(ui);
        displayModeValueChangeListener = new DisplayModeValueChangeListener(ui, this,
                displayLayout.getDisplayModeBox(), displayLayout.getSortModeBox());
        displayLayout.getDisplayModeBox().addValueChangeListener(displayModeValueChangeListener);
        setStandardValues();
        getContentLayout().setSizeFull();
        setComponents();
        doInternationalization();
    }

    private void setStandardValues() {
        displayLayout.getDisplayModeBox().setTheValue(DisplayModes.ALPHABETICAL_LIST);
    }

    @Override
    public void setComponents() {
        //addComponent(selectedSearchPatternPanel);
        addComponent(displayLayout);
        addComponent(allSearchPatternsPanel);
        getContentLayout().setExpandRatio(allSearchPatternsPanel, 1.0f);
    }

    @Override
    public void doInternationalization() {
        //do nothing
    }

    public SearchPattern getSelectedSearchPattern() {
        return allSearchPatternsPanel.getSelectedSearchPattern();
    }

    public Layout getAllSearchPatternsLayout() {
        return allSearchPatternsPanel.getSearchPatternsListLayout();
    }

    public void setSelectedSearchPattern(final SearchPattern searchPattern) {
        allSearchPatternsPanel.setSelectedSearchPattern(searchPattern);
    }

    public SelectedSearchPatternButtonLeiste getButtonLeiste() {
        return allSearchPatternsPanel.getButtonLeiste();
    }
}
