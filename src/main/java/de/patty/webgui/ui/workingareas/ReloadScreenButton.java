package de.patty.webgui.ui.workingareas;

import com.vaadin.ui.Button;
import de.patty.webgui.MainPattyUI;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/15/13
 * Time: 10:54 AM
 *
 */
public class ReloadScreenButton extends Button {

    public ReloadScreenButton(final MainPattyUI ui) {
        setStyleName("link");
        setCaption(ui.getResourceBundle().getString("reload"));
        addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                ui.getScreen().reload();
            }
        });
    }

}
