package de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.searchpatterns.filter.components;

import com.vaadin.data.util.BeanItemContainer;
import de.patty.persistence.entities.SearchPattern;
import de.patty.persistence.entitymanagers.SearchPatternManager;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.FilterField;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/16/13
 * Time: 11:45 AM
 *
 */
public class SearchPatternsFilterField extends FilterField {

    public SearchPatternsFilterField(final MainPattyUI ui) {
        super(ui);
    }

    @Override
    public void createContainerAndSetItemCaptionMode() {
        final SearchPatternManager searchPatternManager = new SearchPatternManager();
        container = new BeanItemContainer<SearchPattern>(SearchPattern.class, searchPatternManager.getActivatedSearchPatterns());
        setItemCaptionMode(ItemCaptionMode.PROPERTY);
        setItemCaptionPropertyId(SearchPattern.TITLE);
    }
}
