package de.patty.webgui.ui.workingareas;

import com.vaadin.ui.ProgressIndicator;
import com.vaadin.ui.themes.Reindeer;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.RapidPanel;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org) Ebbinghaus
 * Date: 03.08.12
 * Time: 11:07
 * To change this template use File | Settings | File Templates.
 */
public abstract class Screen extends RapidPanel implements Internationalizationable, Componentssetable {

    protected ResourceBundle messagesBundle;
    protected MainPattyUI ui;
    protected ProgressIndicator progressIndicator;

    public Screen(final MainPattyUI ui) {
        this.messagesBundle = ui.getResourceBundle();
        this.ui = ui;
        this.setStyleName(Reindeer.PANEL_LIGHT);
        //this.addStyleName("medsizemargin");
        this.setSizeFull();
    }

    public void activeVerticalFullScreenSize(final boolean b) {
        if (b) {
            getContentLayout().setSizeFull();
        } else {
            getContentLayout().setSizeUndefined();
            getContentLayout().setWidth("100%");
        }

    }

    public ResourceBundle getMessagesBundle() {
        return messagesBundle;
    }

    public MainPattyUI getUi() {
        return ui;
    }

    public abstract void reload();
}
