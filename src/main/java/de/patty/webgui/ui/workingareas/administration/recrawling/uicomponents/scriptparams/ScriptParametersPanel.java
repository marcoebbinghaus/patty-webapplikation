package de.patty.webgui.ui.workingareas.administration.recrawling.uicomponents.scriptparams;

import com.vaadin.data.Validator;
import com.vaadin.data.util.converter.StringToIntegerConverter;
import com.vaadin.data.validator.IntegerRangeValidator;
import com.vaadin.event.FieldEvents;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import de.patty.rmi.DoRMILogicAndReloadScreenRunnable;
import de.patty.rmi.managers.ConfigManager;
import de.patty.rmi.managers.CrawlerManager;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.RapidPanel;

import java.util.ResourceBundle;

import static de.patty.etc.Constants.DELIMITER;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/12/13
 * Time: 11:35 AM
 *
 * The panel where the count of IGFPU-runs and the count of recognized URLs per IGFPU-run can be edited.
 */
public class ScriptParametersPanel extends RapidPanel {

    private Label infoLabel;
    private TextField depthField;
    private TextField topNField;
    private ConfigManager configManager;
    private String parametersLine;
    private ResourceBundle messages;
    private FormLayout fieldsLayout;
    private Button saveButton;
    private MainPattyUI ui;
    private ProgressIndicator progressIndicator;

    /**
     * Instantiates a new ScriptParametersPanel.
     *
     * @param ui the ui
     */
    public ScriptParametersPanel(final MainPattyUI ui) {
        this.ui = ui;
        messages = ui.getResourceBundle();
        setCaption(messages.getString("crawlparameters"));
        depthField = new TextField();
        topNField = new TextField();
        infoLabel = new Label(messages.getString("infocrawlparameters"), ContentMode.HTML);
        progressIndicator = new ProgressIndicator();
        progressIndicator.setIndeterminate(true);
        progressIndicator.setEnabled(true);
        progressIndicator.setVisible(false);
        saveButton = new Button();
        saveButton.setCaption(messages.getString("save"));
        saveButton.setStyleName("link");
        saveButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (depthField.isValid() && topNField.isValid()) {
                    final CrawlerManager crawlerManager = new CrawlerManager();
                    final Boolean crawlerRunning = crawlerManager.checkRunning();
                    if (crawlerRunning) {
                        Notification.show(messages.getString("error_crawlerrunning"));
                    } else {
                        final Thread thread = new Thread(new DoRMILogicAndReloadScreenRunnable(ui, "error_changeconfig") {
                            @Override
                            public boolean doRMI() {
                                return configManager.setDepthAndTopN(depthField.getValue(), topNField.getValue());
                            }
                        });
                        thread.start();
                        saveButton.setEnabled(false);
                        progressIndicator.setVisible(true);
                    }
                } else {
                    Notification.show(messages.getString("invaliddata"));
                }

            }
        });
        configManager = new ConfigManager();
        parametersLine = configManager.getDepthAndTopN();
        String[] depthAndTopN = parametersLine.split(DELIMITER);
        configureField(depthField, "depth", depthAndTopN[0], new IntegerRangeValidator("1-20", 1, 20));
        configureField(topNField, "topN", depthAndTopN[1], new IntegerRangeValidator("min. 1", 1, null));
        fieldsLayout = new FormLayout(depthField, topNField);
        addComponent(progressIndicator);
        addComponent(infoLabel);
        addComponent(fieldsLayout);
        addComponent(saveButton);
    }

    private void configureField(final TextField field, final String captionWord, final String value, final Validator validator) {
        field.setCaption(messages.getString(captionWord));
        field.setLocale(ui.getLocale());
        field.setConverter(new StringToIntegerConverter());
        field.addValidator(validator);
        field.setImmediate(true);
        field.setValue(value);
        field.addFocusListener(new FieldEvents.FocusListener() {
            @Override
            public void focus(FieldEvents.FocusEvent event) {
                field.selectAll();
            }
        });
        field.addBlurListener(new FieldEvents.BlurListener() {
            @Override
            public void blur(FieldEvents.BlurEvent event) {
                if (field.isValid()) {
                    field.removeStyleName("redbg");
                } else {
                    field.addStyleName("redbg");
                }
            }
        });
    }
}
