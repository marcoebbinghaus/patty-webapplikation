package de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles.panels.uicomponents;

import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.ProgressIndicator;
import de.patty.etc.designpatterns.observer.Subject;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.administration.logfiles.logic.LogFileOpenDownloadWindowCommand;
import de.patty.webgui.ui.workingareas.administration.logfiles.logic.LogFileShowCommand;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/25/13
 * Time: 2:06 PM
 *
 * Button bar which contains the "download" and "show" button for the selected log file.
 */
public class LogFilesButtonBar extends HorizontalLayout {

    private Button showContentButton;
    private Button downloadButton;
    private ProgressIndicator progressIndicator;
    private ResourceBundle messages;

    /**
     * Instantiates a new LogFilesButtonBar.
     *
     * @param ui the ui
     * @param logFilesListSelectValueChangeListener the {@link com.vaadin.data.Property.ValueChangeListener} of the
     *                                              {@link com.vaadin.ui.ListSelect} with the log files.
     */
    public LogFilesButtonBar(final MainPattyUI ui, final Subject logFilesListSelectValueChangeListener) {
        messages = ui.getResourceBundle();
        setSpacing(true);
        progressIndicator = new ProgressIndicator();
        progressIndicator.setIndeterminate(true);
        progressIndicator.setEnabled(true);
        progressIndicator.setVisible(false);
        showContentButton = new LogFileDependentButton(logFilesListSelectValueChangeListener, new LogFileShowCommand(ui, this), messages.getString("showlogfile"));
        downloadButton = new LogFileDependentButton(logFilesListSelectValueChangeListener, new LogFileOpenDownloadWindowCommand(ui), messages.getString("downloadlogfile"));
        addComponent(progressIndicator);
        addComponent(showContentButton);
        addComponent(downloadButton);
    }

    /**
     * Gets progress indicator.
     *
     * @return the progress indicator
     */
    public ProgressIndicator getProgressIndicator() {
        return progressIndicator;
    }
}
