package de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles.panels.filterpanel.model;

import com.vaadin.data.validator.DateRangeValidator;
import com.vaadin.ui.DateField;
import com.vaadin.ui.FormLayout;
import de.patty.persistence.pojos.LogFile;
import de.patty.webgui.MainPattyUI;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/26/13
 * Time: 2:55 PM
 *
 * Layout which is displayed when the DateFilter is selected in the
 * {@link de.patty.webgui.ui.workingareas.administration.logfiles.LogfileScreen}
 */
public class LogFileDateFilterLayout extends LogFileFilterLayout {

    private FormLayout formLayout;
    private DateField fromDateField;
    private DateField toDateField;

    /**
     * Instantiates a new LogFileDateFilterLayout.
     *
     * @param ui the ui
     */
    public LogFileDateFilterLayout(final MainPattyUI ui) {
        super(ui);
        formLayout = new FormLayout();
        fromDateField = new DateField(messages.getString("from"));
        fromDateField.addValidator(new DateRangeValidator("gsog", null, null, null));
        toDateField = new DateField(messages.getString("to"));
        toDateField.setValue(Calendar.getInstance().getTime());
        toDateField.addValidator(new DateRangeValidator("gsog", null, null, null));
        formLayout.addComponents(fromDateField, toDateField);
        addComponent(formLayout);
    }

    @Override
    public Boolean isValid() {
        if (toDateField.isValid() && fromDateField.isValid()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public List<LogFile> filterLogFiles(final List<LogFile> listToFilter) {
        final List<LogFile> filteredList = new ArrayList<>();
        for (final LogFile logFile : listToFilter) {
            final long logFileTime = logFile.getCreationDate().getTime();
            if (logFileTime >= fromDateField.getValue().getTime() &&
                    logFileTime <= toDateField.getValue().getTime()) {
                filteredList.add(logFile);
            }
        }
        return filteredList;
    }
}
