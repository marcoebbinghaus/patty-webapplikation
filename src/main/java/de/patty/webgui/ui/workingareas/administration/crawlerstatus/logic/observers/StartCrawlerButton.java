package de.patty.webgui.ui.workingareas.administration.crawlerstatus.logic.observers;

import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;
import de.patty.etc.designpatterns.observer.Observer;
import de.patty.rmi.managers.CrawlerManager;
import de.patty.webgui.ui.workingareas.administration.crawlerstatus.logic.ThreadListener;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 5/28/13
 * Time: 12:52 PM
 *
 * The Button which sends the command to start the crawler to the crawler system. Is an observer of the ThreadListener
 * because its enabled-state must correspond to the ThreadListeners state.
 */
public class StartCrawlerButton extends Button implements Observer {

    private ResourceBundle messages;
    private ThreadListener threadListener;
    private boolean stayDisabledForTwoTicks;
    private int ticks;

    /**
     * Instantiates a new StartCrawlerButton.
     *
     * @param messages the ResourceBundle (i18n)
     * @param threadListener the ThreadListener
     */
    public StartCrawlerButton(final ResourceBundle messages, final ThreadListener threadListener) {
        super();
        ticks = 0;
        stayDisabledForTwoTicks = false;
        threadListener.attach(this);
        this.messages = messages;
        this.threadListener = threadListener;
        setEnabled(false);
        setCaption(this.messages.getString("startcrawler"));
        addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                final CrawlerManager crawlerManager = new CrawlerManager();
                final Boolean crawlerStarted = crawlerManager.startCrawler();
                if (crawlerStarted) {
                    setEnabled(false);
                    stayDisabledForTwoTicks = true;
                } else {
                    Notification.show(StartCrawlerButton.this.messages.getString("error_startcrawler"));
                }
            }
        });
    }

    @Override
    public void update() {
        if (stayDisabledForTwoTicks) {
            if (ticks < 2) {
                ticks++;
            } else {
                ticks = 0;
                stayDisabledForTwoTicks = false;
            }
        } else {
            try {
                if (threadListener.isCrawlerRunning()) {
                    setEnabled(false);
                } else {
                    setEnabled(true);
                }
            } catch (final NullPointerException e) {
                setEnabled(false);
            }
        }

    }
}
