package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.ui;

import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Notification;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.RapidWindow;
import de.patty.webgui.ui.SaveCancelButtonBar;
import de.patty.webgui.ui.workingareas.Componentssetable;
import de.patty.webgui.ui.workingareas.Internationalizationable;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.SearchPatternsScreen;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.logic.FilterCreator;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.model.Filter;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.uicomponents.SearchPatternsTable;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/22/13
 * Time: 3:02 PM
 *
 * Window where the user can add a new {@link Filter} to the {@link SearchPatternsTable}.
 */
public class NewFilterWindow extends RapidWindow implements Componentssetable, Internationalizationable {

    private MainPattyUI ui;
    private SearchPatternsTable table;
    private FilterInputMask filterInputMask;

    private SaveCancelButtonBar buttonLeiste;

    /**
     * Instantiates a new NewFilterWindow.
     *
     * @param ui the ui
     */
    public NewFilterWindow(final MainPattyUI ui) {
        super(ui);
        this.ui = ui;
        this.table = ((SearchPatternsScreen) ui.getScreen()).getSearchPatternsTable();
        filterInputMask = new FilterInputMask(ui);
        buttonLeiste = new SaveCancelButtonBar() {
            @Override
            public void doInternationalization() {
                saveButton.setCaption(messagesBundle.getString("add"));
                cancelButton.setCaption(messagesBundle.getString("cancel"));
            }

            @Override
            public void setSaveButtonListener() {
                saveButton.addClickListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent event) {
                        if (filterInputMask.getContentField().isValid() || !filterInputMask.getContentField().isEnabled()) {
                            final Thread thread = new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    ui.getSession().lock();
                                    try {
                                        final FilterCreator filterCreator = new FilterCreator(ui);
                                        final Filter filter = filterCreator.getFilter(filterInputMask);
                                        table.addFilter(filter);
                                        ((SearchPatternsScreen) ui.getScreen()).getAppliedFiltersLayout().addComponent(new UIFilterRow(filter, ui));
                                        NewFilterWindow.this.close();
                                    } finally {
                                        ui.getSession().unlock();
                                    }

                                }
                            });
                            progressIndicator.setVisible(true);
                            saveButton.setEnabled(false);
                            thread.start();
                        } else {
                            Notification.show(messagesBundle.getString("invaliddata"));
                        }


                    }
                });
            }

            @Override
            public void setCancelButtonListener() {
                cancelButton.addClickListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent event) {
                        NewFilterWindow.this.close();
                    }
                });
            }
        };
        buttonLeiste.setLinkStyle(true);
        buttonLeiste.activateShortcuts(true);
        setComponents();
        doInternationalization();
    }

    /**
     * Gets filter input mask.
     *
     * @return the filter input mask
     */
    public FilterInputMask getFilterInputMask() {
        return filterInputMask;
    }

    @Override
    protected void setLayout() {
        setContentLayout(new FormLayout());
    }

    @Override
    public void setComponents() {
        removeAllComponents();
        addComponent(progressIndicator);
        addComponent(filterInputMask);
        addComponent(buttonLeiste);
    }

    @Override
    public void doInternationalization() {
        setCaption(messagesBundle.getString("add"));
    }


}
