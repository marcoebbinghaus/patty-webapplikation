package de.patty.webgui.ui.workingareas.administration.crawlerstatus.logic.observers;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Image;
import de.patty.etc.Constants;
import de.patty.etc.designpatterns.observer.Observer;
import de.patty.webgui.ui.workingareas.administration.crawlerstatus.logic.ThreadListener;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 5/28/13
 * Time: 12:43 PM
 *
 * The image that shows if a connection to the crawler system exists and (if so) if the crawler is running or not. Is an
 * observer of the ThreadListener because the displayed resource changes if the ThreadListeners state changes.
 */
public class CrawlerRunningImage extends Image implements Observer {

    private ThreadListener threadListener;

    /**
     * Instantiates a new CrawlerRunningImage.
     *
     * @param threadListener the ThreadListener
     */
    public CrawlerRunningImage(final ThreadListener threadListener) {
        super();
        this.threadListener = threadListener;
        threadListener.attach(this);
    }

    @Override
    public void update() {
        if (threadListener.isCrawlerRunning() == null) {
            setSource(new ThemeResource(Constants.CRAWLER_NOT_CONNECTED));
        } else {
            if (threadListener.isCrawlerRunning()) {
                setSource(new ThemeResource(Constants.CRAWLER_RUNNING_IMAGE));
            } else {
                setSource(new ThemeResource(Constants.CRAWLER_NOT_RUNNING_IMAGE));
            }
        }


    }
}
