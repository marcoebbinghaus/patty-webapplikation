package de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.display.components;

import com.vaadin.ui.ComboBox;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.display.model.DisplayModes;

import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/6/13
 * Time: 9:38 AM
 *
 */
public class DisplayModeBox extends ComboBox {

    private Map<DisplayModes, String> enumTranslationMap;
    private Map<String, DisplayModes> translationEnumMap;
    private ResourceBundle messagesBundle;

    public DisplayModeBox(final MainPattyUI ui) {
        super();
        setImmediate(true);
        messagesBundle = ui.getResourceBundle();
        buildMaps();
        setNullSelectionAllowed(false);
        for (final Map.Entry<DisplayModes, String> displayModesStringEntry : enumTranslationMap.entrySet()) {
            addItem(displayModesStringEntry.getValue());
        }
    }

    private void buildMaps() {
        enumTranslationMap = new TreeMap<>();
        enumTranslationMap.put(DisplayModes.TREE, messagesBundle.getString("displaymode_tree"));
        enumTranslationMap.put(DisplayModes.ALPHABETICAL_ACCORDION, messagesBundle.getString("displaymode_alpha_acc"));
        enumTranslationMap.put(DisplayModes.ALPHABETICAL_LIST, messagesBundle.getString("displaymode_alpha_list"));
        enumTranslationMap.put(DisplayModes.MOSTPOPULAR, messagesBundle.getString("displaymode_mostpopular"));
        translationEnumMap = new TreeMap<>();
        for (final Map.Entry<DisplayModes, String> stringDisplayModesEntry : enumTranslationMap.entrySet()) {
            translationEnumMap.put(stringDisplayModesEntry.getValue(), stringDisplayModesEntry.getKey());
        }
    }

    public void setTheValue(final DisplayModes mode) {
        setValue(enumTranslationMap.get(mode));
        select(enumTranslationMap.get(mode));
    }

    public DisplayModes getTheValue() {
        return translationEnumMap.get(super.getValue().toString());
    }
}
