package de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles.panels.filterpanel.model;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/26/13
 * Time: 3:12 PM
 *
 * Enumeration of the different filters.
 */
public enum LogFileFilters {

    NONE,
    DATE,
    SIZE;

    private static ResourceBundle messages;

    /**
     * Sets the resource bundle.
     *
     * @param messages the ResourceBundle
     */
    public static void setResourceBundle(final ResourceBundle messages) {
        LogFileFilters.messages = messages;
    }

    public String toString() {
        return messages.getString(name() + ".i18n");
    }
}
