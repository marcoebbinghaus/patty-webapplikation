package de.patty.webgui.ui.workingareas.search.logic;

import de.patty.persistence.entities.SearchPattern;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.search.model.Result;

import java.util.List;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/4/13
 * Time: 2:43 PM
 *
 */
public abstract class SearchExecutor {

    private ResultsCreator resultsCreator;
    protected List<Result> results;
    protected String searchInput;
    protected SearchPattern searchPattern;
    protected MainPattyUI ui;

    public SearchExecutor(final MainPattyUI ui, final String searchInput, final SearchPattern searchPattern, final Boolean queryLanguage) {
        this.ui = ui;
        this.searchInput = searchInput;
        this.searchPattern = searchPattern;
        resultsCreator = new ResultsCreator(ui, searchInput, searchPattern, queryLanguage);
        results = resultsCreator.getResults();
    }

    public abstract void processResults();

    public List<Result> getResults() {
        return results;
    }

    public ResultsCreator getResultsCreator() {
        return resultsCreator;
    }
}
