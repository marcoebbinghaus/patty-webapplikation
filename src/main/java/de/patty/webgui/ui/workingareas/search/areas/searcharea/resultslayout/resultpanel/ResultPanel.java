package de.patty.webgui.ui.workingareas.search.areas.searcharea.resultslayout.resultpanel;

import de.patty.webgui.ui.RapidPanel;
import de.patty.webgui.ui.workingareas.search.areas.searcharea.resultslayout.resultpanel.components.Content;
import de.patty.webgui.ui.workingareas.search.areas.searcharea.resultslayout.resultpanel.components.Titel;
import de.patty.webgui.ui.workingareas.search.areas.searcharea.resultslayout.resultpanel.components.Url;
import de.patty.webgui.ui.workingareas.search.model.Result;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/4/13
 * Time: 11:41 AM
 *
 */
public class ResultPanel extends RapidPanel {

    private Titel titel;
    private Url url;
    private Content content;

    public ResultPanel(final Result result) {
        titel = new Titel(result);
        url = new Url(result);
        content = new Content(result);
        addComponent(titel);
        addComponent(url);
        addComponent(content);
    }
}
