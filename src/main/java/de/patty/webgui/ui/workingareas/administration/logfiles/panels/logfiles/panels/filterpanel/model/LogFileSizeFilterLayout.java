package de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles.panels.filterpanel.model;

import com.vaadin.data.util.converter.StringToIntegerConverter;
import com.vaadin.data.validator.IntegerRangeValidator;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import de.patty.persistence.pojos.LogFile;
import de.patty.webgui.MainPattyUI;

import java.util.ArrayList;
import java.util.List;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/26/13
 * Time: 3:00 PM
 *
 * Layout which is displayed when the SizeFilter is selected in the
 * {@link de.patty.webgui.ui.workingareas.administration.logfiles.LogfileScreen}
 */
public class LogFileSizeFilterLayout extends LogFileFilterLayout {

    private FormLayout formLayout;
    private TextField minSizeField;
    private TextField maxSizeField;

    public LogFileSizeFilterLayout(final MainPattyUI ui) {
        super(ui);
        formLayout = new FormLayout();
        minSizeField = new TextField(messages.getString("minsize"));
        minSizeField.setLocale(ui.getLocale());
        minSizeField.setConverter(new StringToIntegerConverter());
        minSizeField.setValue("0");
        minSizeField.addValidator(new IntegerRangeValidator(messages.getString("error_onlyintegers"), null, null));
        maxSizeField = new TextField(messages.getString("maxsize"));
        maxSizeField.setLocale(ui.getLocale());
        maxSizeField.setConverter(new StringToIntegerConverter());
        maxSizeField.addValidator(new IntegerRangeValidator(messages.getString("error_onlyintegers"), null, null));
        maxSizeField.setValue("100000");
        formLayout.addComponent(minSizeField);
        formLayout.addComponent(maxSizeField);
        addComponent(formLayout);
    }

    @Override
    public List<LogFile> filterLogFiles(final List<LogFile> listToFilter) {
        final List<LogFile> filteredList = new ArrayList<>();
        for (final LogFile logFile : listToFilter) {
            final long logFileSize = logFile.getSizeInKB();
            if (logFileSize >= (Integer) minSizeField.getConvertedValue() && logFileSize <= (Integer) maxSizeField.getConvertedValue()) {
                filteredList.add(logFile);
            }
        }
        return filteredList;
    }

    @Override
    public Boolean isValid() {
        if (maxSizeField.isValid() && minSizeField.isValid()) {
            return true;
        } else {
            return false;
        }
    }

}
