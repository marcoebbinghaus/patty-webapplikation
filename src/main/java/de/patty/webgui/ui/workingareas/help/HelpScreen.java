package de.patty.webgui.ui.workingareas.help;

import com.vaadin.server.BrowserWindowOpener;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.Screen;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 5/15/13
 * Time: 10:44 AM
 *
 */
public class HelpScreen extends Screen {

    private VerticalLayout borderLayout;

    private Label imprintLabelPart1;
    private Button linkButton;
    private Label imprintLabelPart2;


    public HelpScreen(final MainPattyUI ui) {
        super(ui);
        imprintLabelPart1 = new Label();
        imprintLabelPart1.setWidth("100%");
        imprintLabelPart1.setContentMode(ContentMode.HTML);
        imprintLabelPart2 = new Label();
        imprintLabelPart2.setWidth("100%");
        imprintLabelPart2.setContentMode(ContentMode.HTML);
        linkButton = new Button("http://www.rapidpm.org");
        imprintLabelPart1.setValue("<b>Wie funktioniert die Suche?</b><br>\n " +
                "Die Suche wird intern anhand der SolR-Query-Language durchgefuehrt. Der Benutzer hat prinzipiell zwei Moeglichkeiten:<br>\n" +
                "<br>\n" +
                "<p><b>1. Eine SolR-Abfrage generieren lassen</b><br>\n" +
                "Fuer die meisten Benutzer ist dies die richtige Wahl. Die Syntax der SolR-Query-Language muss nicht bekannt sein und es kann einfach ein (oder mehrere) Suchbegriff(e) eingegeben werden. Optional kann zusaetzlich ein Suchmuster verwendet werden, hinter welchem sich eine vorgefertigte Solr-Abfrage verbirgt in welche der eingegebene Suchbegriff integriert wird. Wichtig hierbei ist jedoch, dass Suchmuster nur verwendet werden koennen, wenn nur ein Suchbegriff eingegeben wurde.<br>\n" +
                "<b>Suche ohne Suchmuster</b><br>\n" +
                "Bei der Suche ohne Suchmuster koennen beliebig viele Suchbegriffe eingegeben werden. Diese werden dann in eine rudimentaere Solr-Abfrage umgewandelt, welche allerdings sehr allgemein gehalten ist. Die Qualitaet der Suchergebnisse wird dementsprechend unter dieser allgemeinen Suche leiden.<br>\n" +
                "<b>Suche mit Suchmuster</b><br>\n" +
                "Bei der Suche mit Suchmuster gilt es zu beruecksichtigen, dass in das Suchfeld nur ein Begriff eingegeben werden darf. Dies liegt in der Natur der Sache: Die Spezifizierung der Suche wird anhand des Themas das Suchmusters erreicht. Die Notwendigkeit der Eingabe mehrerer Suchbegriffe entfaellt somit.<br>\n" +
                "Generell kann man sagen, dass die Verwendung eines (guten) Suchmusters immer der Eingabe mehrerer Suchbegriffe vorgezogen werden sollte.</p>\n" +
                "<p><b>2. Selber eine solche SolR-Abfrage erstellen (fortgeschritten)</b><br>\n" +
                "Hier gibt der Benutzer die komplette, syntaktisch korrekte, SolR-Abfrage in das Suchfeld ein. Hierzu muss natuerlich die Syntax bekannt sein. Der Vorteil ist, dass der Benutzer komplett selbst bestimmen kann, wie die finale Abfrage aussieht.<br></p><br>\n" +
                "<p>------------------------</p><br>\n" +
                "<p><b>Erstellung von Suchmustern</b><br>\n" +
                "Bei Suchmustern handelt es sich um verborgene SolR-Query-Abfragen. Das heisst die Syntax musst 100% korrekt sein. Die Benutzereingabe wird innerhalb der Syntax mit %INPUT% markiert. Ein einfaches, syntaktisch korrektes, Suchmuster könnte beispielsweise folgenden Inhalt haben:<br>\n" +
                "content:%INPUT% AND title:%INPUT%<br>\n" +
                "<b>Man beachte:</b>Die Sucheingabe des Benutzers besteht bei Verwendung von Suchmustern <b>IMMER</b> aus <b>EINEM</b> Suchbegriff (weder aus mehreren Suchbegriffen, noch aus SolR-Query-Language-Bestandteilen)</p>");
        linkButton.setStyleName("link");
        final BrowserWindowOpener browserWindowOpener = new BrowserWindowOpener("http://www.rapidpm.org");
        browserWindowOpener.extend(linkButton);
        borderLayout = new VerticalLayout();

        doInternationalization();
        setComponents();
    }

    @Override
    public void reload() {
        ui.setWorkingArea(new HelpScreen(ui));
    }


    @Override
    public void setComponents() {
        activeVerticalFullScreenSize(true);
        borderLayout.addComponent(imprintLabelPart1);
        //borderLayout.addComponent(linkButton);
        //borderLayout.addComponent(imprintLabelPart2);
        addComponent(borderLayout);
    }

    @Override
    public void doInternationalization() {
        //do nothing
    }
}
