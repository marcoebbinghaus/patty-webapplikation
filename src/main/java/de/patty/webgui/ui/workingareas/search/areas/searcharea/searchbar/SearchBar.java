package de.patty.webgui.ui.workingareas.search.areas.searcharea.searchbar;

import com.vaadin.event.FieldEvents;
import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import de.patty.etc.Constants;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.Componentssetable;
import de.patty.webgui.ui.workingareas.Internationalizationable;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/4/13
 * Time: 1:29 PM
 *
 */
public class SearchBar extends HorizontalLayout implements Componentssetable, Internationalizationable {

    private CheckBox queryLanguageCheckBox;
    private TextField searchField;
    private Button searchButton;
    private ResourceBundle messagesBundle;

    public SearchBar(final MainPattyUI ui) {
        setSizeFull();
        setSpacing(true);
        messagesBundle = ui.getResourceBundle();
        queryLanguageCheckBox = new CheckBox();
        queryLanguageCheckBox.setImmediate(true);
        queryLanguageCheckBox.setValue(false);
        searchField = new TextField();
        searchField.setInputPrompt(messagesBundle.getString("searchfield_inputprompt"));
        searchField.setImmediate(true);
        searchField.setSizeFull();
        searchField.addTextChangeListener(new FieldEvents.TextChangeListener() {
            @Override
            public void textChange(FieldEvents.TextChangeEvent textChangeEvent) {
                final String value = textChangeEvent.getText();
                boolean containsOneOfTheKeywords = false;
                for (final String solrQueryKeyword : Constants.SOLR_QUERY_KEYWORDS) {
                    if (value.contains(solrQueryKeyword)) {
                        containsOneOfTheKeywords = true;
                    }
                }
                if (containsOneOfTheKeywords) {
                    queryLanguageCheckBox.setValue(true);
                } else {
                    queryLanguageCheckBox.setValue(false);
                }
            }
        });
        searchButton = new Button();
        searchButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);
        searchButton.setSizeUndefined();
        setComponents();
        doInternationalization();
        setExpandRatio(searchField, 0.9f);
    }

    @Override
    public void setComponents() {
        addComponents(searchField, searchButton, queryLanguageCheckBox);
    }

    @Override
    public void doInternationalization() {
        searchButton.setCaption(messagesBundle.getString("search_go"));
        queryLanguageCheckBox.setCaption(messagesBundle.getString("search_usequerylanguage"));
    }

    public Button getSearchButton() {
        return searchButton;
    }

    public String getSearchInput() {
        return searchField.getValue();
    }

    public CheckBox getQueryLanguageCheckBox() {
        return queryLanguageCheckBox;
    }

    public TextField getSearchField() {
        return searchField;
    }
}
