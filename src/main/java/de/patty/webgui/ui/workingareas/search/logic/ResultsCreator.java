package de.patty.webgui.ui.workingareas.search.logic;

import com.vaadin.ui.Notification;
import de.patty.etc.utilities.SolrServerSingleton;
import de.patty.persistence.entities.SearchPattern;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.search.model.Result;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;

import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/3/13
 * Time: 4:15 PM
 *
 */
public class ResultsCreator {

    private FinalSearchstringBuilder finalSearchstringBuilder;
    private SolrServer solrServer = SolrServerSingleton.getInstance();
    private List<Result> results;
    private QueryResponse antwort;
    private ResourceBundle messages;
    private String finalSearchstring;

    public ResultsCreator(final MainPattyUI ui, final String searchInput, final SearchPattern searchPattern, final Boolean queryLanguage) {
        messages = ui.getResourceBundle();
        finalSearchstringBuilder = new FinalSearchstringBuilder(searchInput, searchPattern, queryLanguage);
    }

    public List<Result> getResults() {
        try {
            finalSearchstring = finalSearchstringBuilder.getFinalSearchstring();
            if (finalSearchstring == null) {
                throw new NullPointerException();
            }
            final SolrQuery abfrage = new SolrQuery(finalSearchstring);
            buildQuery(abfrage);
            antwort = solrServer.query(abfrage);
            generateResultsFromResponse(antwort);
            return results;
        } catch (SolrServerException e) {
            Notification.show(messages.getString("error_maxclausecount"));
            return null;
        } catch (NullPointerException e) {
            Notification.show(messages.getString("error_entersearchorchoosesearchpattern"));
            return null;
        }
    }

    private void generateResultsFromResponse(final QueryResponse queryResponse) {
        final Iterator<SolrDocument> iterator = queryResponse.getResults().iterator();
        results = queryResponse.getBeans(Result.class);
        //highlight-snippets auslesen falls diese existieren und ggf. die normal formatierten Strings durch diese ersetzen
        while (iterator.hasNext()) {
            final SolrDocument resultDoc = iterator.next();
            final String idOfCurrentDocument = (String) resultDoc.getFieldValue("id"); //idOfCurrentDocument is the uniqueKey field
            if (queryResponse.getHighlighting().get(idOfCurrentDocument) != null) {
                final Result tempResult = new Result();
                tempResult.setId(idOfCurrentDocument);
                final Result result = results.get(results.indexOf(tempResult));
                if (queryResponse.getHighlighting().get(idOfCurrentDocument).get("content") != null) {
                    result.setContent(queryResponse.getHighlighting().get(idOfCurrentDocument).get("content").get(0));
                }
                if (queryResponse.getHighlighting().get(idOfCurrentDocument).get("title") != null) {
                    result.setTitle(queryResponse.getHighlighting().get(idOfCurrentDocument).get("title").get(0));
                }
                if (queryResponse.getHighlighting().get(idOfCurrentDocument).get("url") != null) {
                    result.setUrl(queryResponse.getHighlighting().get(idOfCurrentDocument).get("url").get(0));
                }

            } else {
                System.out.println("fuer dieses result wurde gar nicht erst geprueft");
            }

        }
    }

    private void buildQuery(final SolrQuery query) {
        query.setHighlight(true).setHighlightSnippets(1);
        query.setParam("hl.fl", "content,title,url");
        query.setParam("hl.fragsize", "300");
        query.setParam("hl.simple.pre", "<strong>");
        query.setParam("hl.simple.post", "</strong>");
        query.setRows(300);
    }

    public QueryResponse getAntwort() {
        return antwort;
    }

    public String getFinalSearchstring() {
        return finalSearchstring;
    }
}
