package de.patty.webgui.ui.workingareas.search.logic;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/29/13
 * Time: 12:12 PM
 *
 * <p/>
 * KEIN SUCHMUSTER, ABER SOLR-QUERY
 */
public class Case1StringReturner extends SearchStringReturner {

    public Case1StringReturner(final String searchInput) {
        if (searchInput.equals("")) {
            finalSearchString = null;
        }
        finalSearchString = searchInput;
    }
}
