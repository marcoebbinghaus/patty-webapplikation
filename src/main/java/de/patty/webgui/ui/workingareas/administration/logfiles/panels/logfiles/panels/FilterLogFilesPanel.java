package de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles.panels;

import com.vaadin.data.Property;
import com.vaadin.ui.*;
import de.patty.persistence.pojos.LogFile;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.RapidPanel;
import de.patty.webgui.ui.workingareas.Componentssetable;
import de.patty.webgui.ui.workingareas.administration.logfiles.LogfileScreen;
import de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles.panels.filterpanel.model.LogFileDateFilterLayout;
import de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles.panels.filterpanel.model.LogFileFilterLayout;
import de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles.panels.filterpanel.model.LogFileFilters;
import de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles.panels.filterpanel.model.LogFileSizeFilterLayout;
import de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles.panels.uicomponents.LogFilesListSelect;

import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/25/13
 * Time: 1:24 PM
 *
 * The panel on the {@link LogfileScreen} where a filter can be selected / applied to filter all available log files.
 */
public class FilterLogFilesPanel extends RapidPanel implements Componentssetable {

    private ResourceBundle messages;
    private ComboBox filterComboBox;
    private Layout selectedFilterLayout;
    private HorizontalLayout buttonLeiste;
    private Button filterButton;
    private Button resetFiltersButton;

    private MainPattyUI ui;

    /**
     * Instantiates a new FilterLogFilesPanel.
     *
     * @param ui the ui
     */
    public FilterLogFilesPanel(final MainPattyUI ui) {
        getContentLayout().setSpacing(false);
        getContentLayout().setMargin(false);
        this.ui = ui;
        messages = ui.getResourceBundle();
        setCaption(messages.getString("filter"));
        filterComboBox = new ComboBox("", Arrays.asList(LogFileFilters.values()));
        filterComboBox.setNullSelectionAllowed(false);
        filterComboBox.select(LogFileFilters.NONE);
        filterComboBox.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                final LogFileFilters value = (LogFileFilters) event.getProperty().getValue();
                switch (value) {
                    case SIZE:
                        selectedFilterLayout = new LogFileSizeFilterLayout(ui);
                        setComponents();
                        break;
                    case DATE:
                        selectedFilterLayout = new LogFileDateFilterLayout(ui);
                        setComponents();
                        break;
                    case NONE:
                        selectedFilterLayout = new LogFileFilterLayout(ui) {
                            @Override
                            public List<LogFile> filterLogFiles(List<LogFile> listToFilter) {
                                return listToFilter;
                            }

                            @Override
                            public Boolean isValid() {
                                return null;
                            }
                        };
                        setComponents();
                        break;
                    default:
                        break;
                }
            }
        });
        selectedFilterLayout = new LogFileFilterLayout(ui) {
            @Override
            public List<LogFile> filterLogFiles(List<LogFile> listToFilter) {
                return listToFilter;
            }

            @Override
            public Boolean isValid() {
                return null;
            }
        };
        buttonLeiste = new HorizontalLayout();
        buttonLeiste.setSpacing(true);
        filterButton = new Button(messages.getString("applyfilter"));
        filterButton.setStyleName("link");
        filterButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                if (((LogFileFilterLayout) selectedFilterLayout).isValid() == null || ((LogFileFilterLayout) selectedFilterLayout).isValid()) {
                    final LogfileScreen screen = (LogfileScreen) ui.getScreen();
                    final LogFilesListSelect logFilesListSelect = (LogFilesListSelect) screen.getLogFilesListSelect();
                    final List<LogFile> filteredList = ((LogFileFilterLayout) selectedFilterLayout).filterLogFiles(logFilesListSelect.getLogFileList());
                    logFilesListSelect.setLogFileList(filteredList);
                    if (((LogFileFilterLayout) selectedFilterLayout).isValid() == null) {
                        resetFiltersButton.setEnabled(false);
                    } else {
                        resetFiltersButton.setEnabled(true);
                    }
                } else {
                    Notification.show(messages.getString("invaliddata"));
                }
            }
        });
        resetFiltersButton = new Button(messages.getString("resetfilters"));
        resetFiltersButton.setStyleName("link");
        resetFiltersButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                final LogfileScreen screen = (LogfileScreen) ui.getScreen();
                final LogFilesListSelect logFilesListSelect = (LogFilesListSelect) screen.getLogFilesListSelect();
                logFilesListSelect.resetList();
                resetFiltersButton.setEnabled(false);
            }
        });
        resetFiltersButton.setEnabled(false);
        buttonLeiste.addComponents(filterButton, resetFiltersButton);
        setComponents();
        getContentLayout().setExpandRatio(buttonLeiste, 1.0f);
    }

    @Override
    public void setComponents() {
        removeAllComponents();
        addComponent(filterComboBox);
        addComponent(selectedFilterLayout);
        addComponent(buttonLeiste);
    }
}
