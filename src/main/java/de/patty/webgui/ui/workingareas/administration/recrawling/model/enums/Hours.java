package de.patty.webgui.ui.workingareas.administration.recrawling.model.enums;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/9/13
 * Time: 3:30 PM
 *
 * Enum for hours of a day.
 */
public enum Hours {
    ONE_AM,
    TWO_AM,
    THREE_AM,
    FOUR_AM,
    FIVE_AM,
    SIX_AM,
    SEVEN_AM,
    EIGHT_AM,
    NINE_AM,
    TEN_AM,
    ELEVEN_AM,
    TWELVE_NOON,
    ONE_PM,
    TWO_PM,
    THREE_PM,
    FOUR_PM,
    FIVE_PM,
    SIX_PM,
    SEVEN_PM,
    EIGHT_PM,
    NINE_PM,
    TEN_PM,
    ELEVEN_PM,
    TWELVE_MIDNIGHT;


    private static ResourceBundle messages;

    public static void setResourceBundle(final ResourceBundle messages) {
        Hours.messages = messages;
    }

    public String toString() {
        return messages.getString(name() + ".i18n");
    }

}
