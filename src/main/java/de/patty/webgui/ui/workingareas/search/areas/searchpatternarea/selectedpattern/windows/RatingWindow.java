package de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.selectedpattern.windows;

import com.vaadin.data.Property;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.ProgressIndicator;
import de.patty.persistence.entities.BenutzerSearchpattern;
import de.patty.persistence.entities.SearchPattern;
import de.patty.persistence.entitymanagers.SearchPatternManager;
import de.patty.rmi.DoRMILogicAndReloadScreenRunnable;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.RapidWindow;
import de.patty.webgui.ui.workingareas.search.SearchScreen;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.selectedpattern.components.RatingOptionGroup;

import java.util.List;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/8/13
 * Time: 1:10 PM
 *
 */
public class RatingWindow extends RapidWindow {

    private RatingOptionGroup ratingGroup;
    final ProgressIndicator progressIndicator;
    private SearchPattern searchPatternToRate;

    public RatingWindow(final MainPattyUI ui, final SearchPattern searchPatternToRate) {
        super(ui);
        this.searchPatternToRate = searchPatternToRate;
        setWidth("250px");
        ratingGroup = new RatingOptionGroup(ui);
        setValueIfUserAlreadyVotedForThisSearchpattern();
        progressIndicator = new ProgressIndicator();
        progressIndicator.setIndeterminate(true);
        progressIndicator.setEnabled(false);
        progressIndicator.setVisible(false);
        ratingGroup.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                final Thread thread = new Thread(new DoRMILogicAndReloadScreenRunnable((MainPattyUI) ui, "error_rating") {
                    @Override
                    public boolean doRMI() {
                        final SearchPatternManager searchPatternManager = new SearchPatternManager();
                        RatingWindow.this.close();
                        return searchPatternManager.rateSearchPattern(ui.getCurrentUser(), RatingWindow.this.searchPatternToRate, (Integer) ratingGroup.getValue());
                    }
                });
                thread.start();
                progressIndicator.setEnabled(true);
                progressIndicator.setVisible(true);
                ratingGroup.setEnabled(false);
            }
        });
        setComponents();
        doInternationalization();

    }

    private void setValueIfUserAlreadyVotedForThisSearchpattern() {
        final List<BenutzerSearchpattern> benutzerSearchpatternList = ui.getCurrentUser().getBenutzerSearchpatterns();
        if (benutzerSearchpatternList != null) {
            for (final BenutzerSearchpattern benutzerSearchpattern : benutzerSearchpatternList) {
                if (benutzerSearchpattern.getSearchPattern().equals(searchPatternToRate)) {
                    ratingGroup.setValue(benutzerSearchpattern.getRating());
                }
            }
        }
    }

    @Override
    protected void setLayout() {
        setContentLayout(new FormLayout());
    }

    @Override
    public void setComponents() {
        addComponent(ratingGroup);
        addComponent(progressIndicator);
    }

    @Override
    public void doInternationalization() {
        setCaption(messagesBundle.getString("rating_title"));
    }
}
