package de.patty.webgui.ui.workingareas.administration.recrawling.model.enums;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/9/13
 * Time: 3:45 PM
 *
 * Enum for days of a week.
 */

public enum Days {
    MONDAY,
    TUESDAY,
    WEDNESDAY,
    THURSDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY;

    private static ResourceBundle messages;

    public static void setResourceBundle(final ResourceBundle messages) {
        Days.messages = messages;
    }

    public String toString() {
        return messages.getString(name() + ".i18n");
    }
}