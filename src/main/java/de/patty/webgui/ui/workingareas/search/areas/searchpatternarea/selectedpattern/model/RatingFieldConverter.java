package de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.selectedpattern.model;

import com.vaadin.data.util.converter.Converter;
import de.patty.webgui.MainPattyUI;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/6/13
 * Time: 2:12 PM
 *
 */
public class RatingFieldConverter implements Converter<String, Double> {

    private ResourceBundle messagesBundle;

    public RatingFieldConverter(final MainPattyUI ui) {
        messagesBundle = ui.getResourceBundle();
    }

    @Override
    public Double convertToModel(String s, Locale locale) throws ConversionException {
        if (s.equals(messagesBundle.getString("notenoughvotes"))) {
            return -1.0;
        } else {
            return Double.valueOf(s);
        }
    }

    @Override
    public String convertToPresentation(Double aDouble, Locale locale) throws ConversionException {
        if (aDouble < 0.0) {
            return messagesBundle.getString("notenoughvotes");
        } else {
            return aDouble.toString();
        }
    }

    @Override
    public Class<Double> getModelType() {
        return Double.class;
    }

    @Override
    public Class<String> getPresentationType() {
        return String.class;
    }
}
