package de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.display.model;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/6/13
 * Time: 9:38 AM
 *
 */
public enum SortModes {
    ASCENDING,
    DESCENDING;
}
