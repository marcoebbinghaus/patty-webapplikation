package de.patty.webgui.ui.workingareas.search.model.exceptions;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/7/13
 * Time: 4:35 PM
 *
 */
public class TitleAlreadyExistingException extends Throwable {
}
