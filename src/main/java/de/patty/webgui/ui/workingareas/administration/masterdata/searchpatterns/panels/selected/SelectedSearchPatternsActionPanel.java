package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.selected;

import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.RapidPanel;
import de.patty.webgui.ui.workingareas.Componentssetable;
import de.patty.webgui.ui.workingareas.Internationalizationable;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.selected.components.*;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.uicomponents.SearchPatternsTable;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/22/13
 * Time: 12:31 PM
 *
 * Panel where the buttons for the available {@link de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.logic.SearchPatternCommand}s
 * are displayed.
 */
public class SelectedSearchPatternsActionPanel extends RapidPanel implements Componentssetable, Internationalizationable {

    private SelectedButton deleteSelectedButton;
    private SelectedButton activateSelectedButton;
    private SelectedButton unactivateSelectedButton;
    private SelectedButton activateOrUnactivateSelectedButton;
    private MainPattyUI ui;

    /**
     * Instantiates a new SelectedSearchPatternsActionPanell.
     *
     * @param ui the ui
     * @param searchPatternsTable the search patterns table
     */
    public SelectedSearchPatternsActionPanel(final MainPattyUI ui, final SearchPatternsTable searchPatternsTable) {
        setSizeUndefined();
        this.ui = ui;
        deleteSelectedButton = new DeleteSelectedButton(searchPatternsTable, ui);
        activateSelectedButton = new ActivateSelectedButton(searchPatternsTable, ui);
        unactivateSelectedButton = new UnactivateSelectedButton(searchPatternsTable, ui);
        activateOrUnactivateSelectedButton = new ActivateOrUnactivateSelectedButton(searchPatternsTable, ui);
        setComponents();
        doInternationalization();
    }

    @Override
    public void setComponents() {
        addComponent(deleteSelectedButton);
        addComponent(activateSelectedButton);
        addComponent(unactivateSelectedButton);
        addComponent(activateOrUnactivateSelectedButton);
    }

    @Override
    public void doInternationalization() {
        setCaption(ui.getResourceBundle().getString("selected"));
    }
}
