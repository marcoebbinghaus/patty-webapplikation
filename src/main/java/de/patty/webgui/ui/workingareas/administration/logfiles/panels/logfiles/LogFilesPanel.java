package de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles;

import com.vaadin.ui.ListSelect;
import de.patty.rmi.managers.NoConnectionException;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.RapidPanel;
import de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles.panels.FilterLogFilesPanel;
import de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles.panels.LogFilesListPanel;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/25/13
 * Time: 1:20 PM
 *
 * The panel which contains the {@link FilterLogFilesPanel} and the {@link LogFilesListPanel}
 */
public class LogFilesPanel extends RapidPanel {

    private MainPattyUI ui;
    private RapidPanel filterLogFilesPanel;
    private RapidPanel logFilesListPanel;

    /**
     * Instantiates a LogFilesPanel.
     *
     * @param ui the ui
     * @throws NoConnectionException thrown if no connection to the crawler system is established.
     */
    public LogFilesPanel(final MainPattyUI ui) throws NoConnectionException {
        this.ui = ui;
        setCaption(ui.getResourceBundle().getString("existinglogfiles"));
        setSizeFull();
        getContentLayout().setMargin(false);
        getContentLayout().setSizeFull();
        filterLogFilesPanel = new FilterLogFilesPanel(ui);
        logFilesListPanel = new LogFilesListPanel(ui);
        addComponent(filterLogFilesPanel);
        addComponent(logFilesListPanel);
        getContentLayout().setExpandRatio(logFilesListPanel, 1.0f);
    }

    /**
     * Gets LogFilesListSelect from the containing {@link LogFilesListPanel}
     *
     * @return the LogFilesListSelect
     */
    public ListSelect getLogFilesListSelect() {
        return ((LogFilesListPanel) logFilesListPanel).getLogFilesListSelect();
    }
}
