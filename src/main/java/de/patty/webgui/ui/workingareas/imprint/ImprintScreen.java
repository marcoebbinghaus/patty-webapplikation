package de.patty.webgui.ui.workingareas.imprint;

import com.vaadin.server.BrowserWindowOpener;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.Screen;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 5/15/13
 * Time: 10:44 AM
 *
 */
public class ImprintScreen extends Screen {

    private VerticalLayout borderLayout;

    private Label imprintLabelPart1;
    private Button linkButton;
    private Label imprintLabelPart2;


    public ImprintScreen(final MainPattyUI ui) {
        super(ui);
        imprintLabelPart1 = new Label();
        imprintLabelPart1.setWidth("100%");
        imprintLabelPart1.setContentMode(ContentMode.HTML);
        imprintLabelPart2 = new Label();
        imprintLabelPart2.setWidth("100%");
        imprintLabelPart2.setContentMode(ContentMode.HTML);
        linkButton = new Button("http://www.rapidpm.org");
        imprintLabelPart1.setValue("<b>Verantwortlich für den Inhalt dieser Webseite:</b> \n" +
                "</br>\n" +
                "Marco Ebbinghaus</br> \n" +
                "Kastanienweg 16D</br> \n" +
                "58675 Hemer</br> \n" +
                "marco.ebbinghaus@rapidpm.org</br> \n");
        imprintLabelPart2.setValue("</br>\n" +
                "</br>\n" +
                "Haftungsausschluss</br> \n" +
                "</br>\n" +
                "1. Inhalt des Onlineangebotes</br> \n" +
                "Der Autor übernimmt keinerlei Gewähr für die Aktualität, Korrektheit, Vollständigkeit oder Qualität der bereitgestellten Informationen. Haftungsansprüche gegen den Autor, welche sich auf Schäden materieller oder ideeller Art beziehen, die durch die Nutzung oder Nichtnutzung der dargebotenen Informationen bzw. durch die Nutzung fehlerhafter und unvollständiger Informationen verursacht wurden, sind grundsätzlich ausgeschlossen, sofern seitens des Autors kein nachweislich vorsätzliches oder grob fahrlässiges Verschulden vorliegt.</br> \n" +
                "Alle Angebote sind freibleibend und unverbindlich. Der Autor behält es sich ausdrücklich vor, Teile der Seiten oder das gesamte Angebot ohne gesonderte Ankündigung zu verändern, zu ergänzen, zu löschen oder die Veröffentlichung zeitweise oder endgültig einzustellen.</br> \n" +
                "</br>\n" +
                "2. Verweise und Links</br> \n" +
                "Bei direkten oder indirekten Verweisen auf fremde Webseiten (Hyperlinks), die außerhalb des Verantwortungsbereiches des Autors liegen, würde eine Haftungsverpflichtung ausschließlich in dem Fall in Kraft treten, in dem der Autor von den Inhalten Kenntnis hat und es ihm technisch möglich und zumutbar wäre, die Nutzung im Falle rechtswidriger Inhalte zu verhindern. \n" +
                "Der Autor erklärt hiermit ausdrücklich, dass zum Zeitpunkt der Linksetzung keine illegalen Inhalte auf den zu verlinkenden Seiten erkennbar waren. Auf die aktuelle und zukünftige Gestaltung, die Inhalte oder die Urheberschaft der verlinkten/verknüpften Seiten hat der Autor keinerlei Einfluss. Deshalb distanziert er sich hiermit ausdrücklich von allen Inhalten aller verlinkten /verknüpften Seiten, die nach der Linksetzung verändert wurden. Diese Feststellung gilt für alle innerhalb des eigenen Internetangebotes gesetzten Links und Verweise sowie für Fremdeinträge in vom Autor eingerichteten Gästebüchern, Diskussionsforen, Linkverzeichnissen, Mailinglisten und in allen anderen Formen von Datenbanken, auf deren Inhalt externe Schreibzugriffe möglich sind. Für illegale, fehlerhafte oder unvollständige Inhalte und insbesondere für Schäden, die aus der Nutzung oder Nichtnutzung solcherart dargebotener Informationen entstehen, haftet allein der Anbieter der Seite, auf welche verwiesen wurde, nicht derjenige, der über Links auf die jeweilige Veröffentlichung lediglich verweist.</br> \n" +
                "</br>\n" +
                "3. Urheber- und Kennzeichenrecht</br> \n" +
                "Der Autor ist bestrebt, in allen Publikationen die Urheberrechte der verwendeten Grafiken, Tondokumente, Videosequenzen und Texte zu beachten, von ihm selbst erstellte Grafiken, Tondokumente, Videosequenzen und Texte zu nutzen oder auf lizenzfreie Grafiken, Tondokumente, Videosequenzen und Texte zurückzugreifen.</br> \n" +
                "Alle innerhalb des Internetangebotes genannten und ggf. durch Dritte geschützten Marken- und Warenzeichen unterliegen uneingeschränkt den Bestimmungen des jeweils gültigen Kennzeichenrechts und den Besitzrechten der jeweiligen eingetragenen Eigentümer. Allein aufgrund der bloßen Nennung ist nicht der Schluss zu ziehen, dass Markenzeichen nicht durch Rechte Dritter geschützt sind!</br> \n" +
                "Das Copyright für veröffentlichte, vom Autor selbst erstellte Objekte bleibt allein beim Autor der Seiten. Eine Vervielfältigung oder Verwendung solcher Grafiken, Tondokumente, Videosequenzen und Texte in anderen elektronischen oder gedruckten Publikationen ist ohne ausdrückliche Zustimmung des Autors nicht gestattet.</br> \n" +
                "</br>\n" +
                "4. Datenschutz</br> \n" +
                "Sofern innerhalb des Internetangebotes die Möglichkeit zur Eingabe persönlicher oder geschäftlicher Daten (Emailadressen, Namen, Anschriften) besteht, so erfolgt die Preisgabe dieser Daten seitens des Nutzers auf ausdrücklich freiwilliger Basis. Die Inanspruchnahme und Bezahlung aller angebotenen Dienste ist - soweit technisch möglich und zumutbar - auch ohne Angabe solcher Daten bzw. unter Angabe anonymisierter Daten oder eines Pseudonyms gestattet. Die Nutzung der im Rahmen des Impressums oder vergleichbarer Angaben veröffentlichten Kontaktdaten wie Postanschriften, Telefon- und Faxnummern sowie Emailadressen durch Dritte zur Übersendung von nicht ausdrücklich angeforderten Informationen ist nicht gestattet. Rechtliche Schritte gegen die Versender von sogenannten Spam-Mails bei Verstössen gegen dieses Verbot sind ausdrücklich vorbehalten.</br> \n" +
                "</br>\n" +
                "5. Rechtswirksamkeit dieses Haftungsausschlusses</br> \n" +
                "Dieser Haftungsausschluss ist als Teil des Internetangebotes zu betrachten, von dem aus auf diese Seite verwiesen wurde. Sofern Teile oder einzelne Formulierungen dieses Textes der geltenden Rechtslage nicht, nicht mehr oder nicht vollständig entsprechen sollten, bleiben die übrigen Teile des Dokumentes in ihrem Inhalt und ihrer Gültigkeit davon unberührt.</br> \n" +
                "</br>\n" +
                "Auf der Seite werden u.A. folgende Icons/IconSets verwendet:</br>" +
                "<a href=\"http://www.cssjunction.com/freebies/rating-stars-psd-and-png/\">Rating Stars PSD And PNG</a></br>\n" +
                "<a href=\"http://www.visualpharm.com/must_have_icon_set/\">Visualpharm - Must Have Icon Set</a></br>\n" +
                "<a href=\"http://www.iconfinder.com/iconsets/DarkGlass_Reworked/\">DarkGlass Reworked</a></br>\n" +
                "</br>\n" +
                "Disclaimer</br> \n" +
                "</br>\n" +
                "1. Content</br> \n" +
                "The author reserves the right not to be responsible for the topicality, correctness, completeness or quality of the information provided. Liability claims regarding damage caused by the use of any information provided, including any kind of information which is incomplete or incorrect,will therefore be rejected.</br> \n" +
                "All offers are not-binding and without obligation. Parts of the pages or the complete publication including all offers and information might be extended, changed or partly or completely deleted by the author without separate announcement.</br> \n" +
                "</br>\n" +
                "2. Referrals and links</br> \n" +
                "The author is not responsible for any contents linked or referred to from his pages - unless he has full knowledge of illegal contents and would be able to prevent the visitors of his site fromviewing those pages. If any damage occurs by the use of information presented there, only the author of the respective pages might be liable, not the one who has linked to these pages. Furthermore the author is not liable for any postings or messages published by users of discussion boards, guestbooks or mailinglists provided on his page.</br> \n" +
                "</br>\n" +
                "3. Copyright</br> \n" +
                "The author intended not to use any copyrighted material for the publication or, if not possible, to indicatethe copyright of the respective object.</br> \n" +
                "The copyright for any material created by the author is reserved. Any duplication or use of objects such as diagrams, sounds or texts in other electronic or printed publications is not permitted without the author's agreement.</br> \n" +
                "</br>\n" +
                "4. Privacy policy</br> \n" +
                "If the opportunity for the input of personal or business data (email addresses, name, addresses) is given, the input of these data takes place voluntarily. The use and payment of all offered services are permitted - if and so far technically possible and reasonable - without specification of any personal data or under specification of anonymized data or an alias. The use of published postal addresses, telephone or fax numbers and email addresses for marketing purposes is prohibited, offenders sending unwanted spam messages will be punished.</br> \n" +
                "</br>\n" +
                "5. Legal validity of this disclaimer</br> \n" +
                "This disclaimer is to be regarded as part of the internet publication which you were referred from. If sections or individual terms of this statement are not legal or correct, the content or validity of the other parts remain uninfluenced by this fact. <br>" +
                "</br>The following icons/iconsets are used on this website:</br>" +
                "<a href=\"http://www.cssjunction.com/freebies/rating-stars-psd-and-png/\">Rating Stars PSD And PNG</a></br>\n" +
                "<a href=\"http://www.visualpharm.com/must_have_icon_set/\">Visualpharm - Must Have Icon Set</a></br>\n" +
                "<a href=\"http://www.iconfinder.com/iconsets/DarkGlass_Reworked/\">DarkGlass Reworked</a></br>\n");
        linkButton.setStyleName("link");
        final BrowserWindowOpener browserWindowOpener = new BrowserWindowOpener("http://www.rapidpm.org");
        browserWindowOpener.extend(linkButton);
        borderLayout = new VerticalLayout();

        doInternationalization();
        setComponents();
    }

    @Override
    public void reload() {
        ui.setWorkingArea(new ImprintScreen(ui));
    }


    @Override
    public void setComponents() {
        activeVerticalFullScreenSize(true);
        borderLayout.addComponent(imprintLabelPart1);
        borderLayout.addComponent(linkButton);
        borderLayout.addComponent(imprintLabelPart2);
        addComponent(borderLayout);
    }

    @Override
    public void doInternationalization() {
        //do nothing
    }
}
