package de.patty.webgui.ui.workingareas.administration.logfiles.logic;

import com.vaadin.ui.Notification;
import de.patty.persistence.pojos.LogFile;
import de.patty.rmi.managers.LogfilesManager;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.administration.logfiles.LogfileScreen;
import de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles.panels.uicomponents.LogFilesButtonBar;

import java.util.Set;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/25/13
 * Time: 5:27 PM
 *
 * Concrete command to show the content of a selected log file.
 */
public class LogFileShowCommand extends LogFileCommand {

    private LogFilesButtonBar buttonLeiste;

    /**
     * Instantiates a new LogFileShowCommand.
     *
     * @param ui the ui
     * @param buttonBar the save/cancel button bar
     */
    public LogFileShowCommand(final MainPattyUI ui, final LogFilesButtonBar buttonBar) {
        super(ui);
        this.buttonLeiste = buttonBar;
    }

    @Override
    public void execute(final LogFile logFile) {
        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                ui.getSession().lock();
                String logFileContent;
                if (logFile.getContent() == null) {
                    final LogfilesManager logfilesManager = new LogfilesManager();
                    logFileContent = logfilesManager.getContentOfLogfile(logFile);
                    logFile.setContent(logFileContent);
                } else {
                    logFileContent = logFile.getContent();
                }
                ((LogfileScreen) ui.getScreen()).getShowLogFileTextArea().setValue(logFileContent);
                buttonLeiste.getProgressIndicator().setVisible(false);
                buttonLeiste.setEnabled(true);
                ui.getSession().unlock();
            }
        });
        thread.start();
        buttonLeiste.getProgressIndicator().setVisible(true);
        buttonLeiste.setEnabled(false);
        Notification.show(ui.getResourceBundle().getString("info_mighttakeawhile"));
    }

    @Override
    public void execute(final Set<LogFile> logFiles) {
        //only the content of a single log file can be shown at a time.
    }
}
