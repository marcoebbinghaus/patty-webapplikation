package de.patty.webgui.ui.workingareas.administration.configuration.model;

import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.Table;
import de.patty.webgui.ui.SaveCancelButtonBar;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/8/13
 * Time: 12:13 PM
 *
 * {@link com.vaadin.event.ItemClickEvent.ItemClickListener} for the {@link CrawlerParameter} table. Applys the correct
 * FieldFactory for the table and enables/disables the save/cancel buttons.
 */
public class ParametersTableItemClickListener implements ItemClickEvent.ItemClickListener {

    private Table table;
    private SaveCancelButtonBar buttonBar;

    /**
     * Instantiates a new ParametersTableItemClickListener.
     *
     * @param table the table
     * @param buttonBar the button bar to save the changes
     */
    public ParametersTableItemClickListener(final Table table, final SaveCancelButtonBar buttonBar) {
        this.table = table;
        this.buttonBar = buttonBar;
    }

    @Override
    public void itemClick(ItemClickEvent itemClickEvent) {
        final CrawlerParameter crawlerParameter = (CrawlerParameter) itemClickEvent.getItemId();
        if (crawlerParameter == null) {
            buttonBar.setVisible(false);
            table.setEditable(false);
        } else {
            buttonBar.setVisible(true);
            table.setTableFieldFactory(new EditSelectedParamFieldFactory(crawlerParameter));
            table.setEditable(true);
        }
    }
}
