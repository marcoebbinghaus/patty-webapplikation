package de.patty.webgui.ui.workingareas.administration.recrawling.uicomponents.cronjob.edit.tabs;

import com.vaadin.ui.TextField;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.administration.recrawling.logic.CronjobOptionGroupValueChangeListener;
import de.patty.webgui.ui.workingareas.administration.recrawling.model.enummaps.DaysEnumMap;
import de.patty.webgui.ui.workingareas.administration.recrawling.model.enums.Days;
import de.patty.webgui.ui.workingareas.administration.recrawling.uicomponents.cronjob.edit.AdvancedOptionGroup;

import java.util.Arrays;
import java.util.EnumMap;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/9/13
 * Time: 10:02 AM
 *
 * Tab for creating weekly cronjobs (minutes, hours, days of week).
 */
public class WeeklyTimesTab extends TimesTab {

    private TextField daysField;
    private AdvancedOptionGroup daysOptionGroup;
    private DaysEnumMap daysEnumMap;

    /**
     * Instantiates a new WeeklyTimesTab.
     *
     * @param ui the ui
     */
    public WeeklyTimesTab(final MainPattyUI ui) {
        super(ui);
        daysEnumMap = new DaysEnumMap(new EnumMap<Days, Integer>(Days.class));
        infoLabel.setValue(messages.getString("infoweeklytimestab"));
        daysField = new TextField(messages.getString("days"));
        daysField.setWidth("60%");
        daysOptionGroup = new AdvancedOptionGroup(ui, "days", Arrays.asList(Days.values()));
        daysOptionGroup.setWidth("20%");
        daysOptionGroup.addValueChangeListener(new CronjobOptionGroupValueChangeListener<Days>(daysField, daysEnumMap));
        manualLayout.addComponent(daysField);
        optionGroup3Layout.removeAllComponents();
        optionGroup3Layout.addComponent(daysOptionGroup);
    }

    @Override
    public String returnPattern() {
        final String minutes = minutesField.getValue();
        final String hours = hoursField.getValue();
        final String days = daysField.getValue();
        final String pattern = minutes + " " + hours + " * * " + days;
        return pattern;
    }
}
