package de.patty.webgui.ui.workingareas.administration.recrawling;

import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.ReloadScreenButton;
import de.patty.webgui.ui.workingareas.Screen;
import de.patty.webgui.ui.workingareas.administration.recrawling.uicomponents.cronjob.current.CurrentCronjobPanel;
import de.patty.webgui.ui.workingareas.administration.recrawling.uicomponents.cronjob.edit.EditCronjobPanel;
import de.patty.webgui.ui.workingareas.administration.recrawling.uicomponents.scriptparams.ScriptParametersPanel;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/4/13
 * Time: 10:15 AM
 *
 * The {@link Screen} where the user can control IGFPU-runs and the recrawling cronjob.
 */
public class RecrawlingScreen extends Screen {

    private boolean connectionToCrawlerSystem = true;
    private ScriptParametersPanel scriptParametersPanel;
    private CurrentCronjobPanel currentCronjobPanel;
    private EditCronjobPanel editCronjobPanel;

    /**
     * Instantiates a new RecrawlingScreen.
     *
     * @param ui the ui
     */
    public RecrawlingScreen(final MainPattyUI ui) {
        super(ui);
        activeVerticalFullScreenSize(false);
        try {
            scriptParametersPanel = new ScriptParametersPanel(ui);
            currentCronjobPanel = new CurrentCronjobPanel(ui);
            editCronjobPanel = new EditCronjobPanel(ui);
        } catch (final NullPointerException e) { // Connection to the crawler system not established.
            connectionToCrawlerSystem = false;
            e.printStackTrace();
        }
        setComponents();
        doInternationalization();
    }

    @Override
    public void reload() {
        ui.setWorkingArea(new RecrawlingScreen(ui));
    }

    @Override
    public void setComponents() {
        if (connectionToCrawlerSystem) {
            addComponent(scriptParametersPanel);
            addComponent(currentCronjobPanel);
            addComponent(editCronjobPanel);
            getContentLayout().setExpandRatio(editCronjobPanel, 1F);
        } else {
            addComponent(new Label(messagesBundle.getString("reloadwhencrawlerisbackup")));
            final ReloadScreenButton reloadButton = new ReloadScreenButton(ui);
            addComponent(reloadButton);
            getContentLayout().setExpandRatio(reloadButton, 1.0f);
            Notification.show(messagesBundle.getString("noconnectiontocrawler"));
        }
    }

    @Override
    public void doInternationalization() {
        // no localization needed
    }
}
