package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.model;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/17/13
 * Time: 1:08 PM
 *
 * Enum which contains the different tasks for a {@link de.patty.persistence.entities.SearchPattern}. It is used for the
 * {@link de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.uicomponents.SearchPatternActionsComboBox}
 * in the {@link de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.SearchPatternsScreen}.
 */
public enum SearchPatternActions {
    NONE,
    EDIT,
    DELETE,
    ACTIVATE;

    private static ResourceBundle messages;

    /**
     * Sets the ResourceBundle (i18n).
     *
     * @param messages the messages
     */
    public static void setResourceBundle(final ResourceBundle messages) {
        SearchPatternActions.messages = messages;
    }

    public String toString() {
        return messages.getString(name() + ".i18n");
    }
}
