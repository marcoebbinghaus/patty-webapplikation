package de.patty.webgui.ui.workingareas.administration.crawlerstatus;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.Screen;
import de.patty.webgui.ui.workingareas.administration.crawlerstatus.logic.ThreadListener;
import de.patty.webgui.ui.workingareas.administration.crawlerstatus.logic.observers.CrawlerRunningImage;
import de.patty.webgui.ui.workingareas.administration.crawlerstatus.logic.observers.StartCrawlerButton;
import de.patty.webgui.ui.workingareas.administration.crawlerstatus.logic.observers.StopCrawlerButton;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 5/15/13
 * Time: 10:44 AM
 *
 * The screen where the administrator can observer the crawler's state and control the crawler (start/stop it).
 */
public class CrawlerStatusScreen extends Screen {

    private CrawlerRunningImage crawlerRunningImage;
    private StartCrawlerButton startButton;
    private StopCrawlerButton stopButton;
    private ResourceBundle messages;
    private Label infoLabel;
    private VerticalLayout startStopButtonsLayout;

    /**
     * Instantiates a new CrawlerStatusScreen.
     *
     * @param ui the ui
     */
    public CrawlerStatusScreen(final MainPattyUI ui) {
        super(ui);
        startStopButtonsLayout = new VerticalLayout();
        messages = ui.getResourceBundle();
        final ThreadListener threadListener = ui.getThreadListener();
        threadListener.removeAllObservers();
        crawlerRunningImage = new CrawlerRunningImage(threadListener);
        startButton = new StartCrawlerButton(messages, threadListener);
        stopButton = new StopCrawlerButton(ui, threadListener);
        infoLabel = new Label();
        infoLabel.setSizeUndefined();
        crawlerRunningImage.setSizeUndefined();
        startStopButtonsLayout.setSizeFull();
        startButton.setSizeUndefined();
        stopButton.setSizeUndefined();
        startButton.setWidth("100%");
        stopButton.setWidth("100%");
        startStopButtonsLayout.addComponent(startButton);
        startStopButtonsLayout.addComponent(stopButton);
        startStopButtonsLayout.setComponentAlignment(startButton, Alignment.BOTTOM_CENTER);
        startStopButtonsLayout.setComponentAlignment(stopButton, Alignment.TOP_CENTER);
        setSizeFull();
        doInternationalization();
        setComponents();
    }

    @Override
    public void reload() {
        ui.setWorkingArea(new CrawlerStatusScreen(ui));
    }


    @Override
    public void setComponents() {
        activeVerticalFullScreenSize(true);
        addComponent(infoLabel);
        addComponent(crawlerRunningImage);
        addComponent(startStopButtonsLayout);
        getContentLayout().setComponentAlignment(crawlerRunningImage, Alignment.MIDDLE_CENTER);
        getContentLayout().setComponentAlignment(startStopButtonsLayout, Alignment.MIDDLE_CENTER);
    }

    @Override
    public void doInternationalization() {
        infoLabel.setValue(messages.getString("checkrunninginfo"));
    }
}
