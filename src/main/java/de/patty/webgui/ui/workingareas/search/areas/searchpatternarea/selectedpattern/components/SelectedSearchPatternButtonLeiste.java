package de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.selectedpattern.components;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import de.patty.etc.Constants;
import de.patty.persistence.entities.Benutzer;
import de.patty.persistence.entities.SearchPattern;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.logic.*;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/10/13
 * Time: 10:43 AM
 *
 */
public class SelectedSearchPatternButtonLeiste extends HorizontalLayout {

    private Benutzer currentUser;
    private SearchPattern selectedSearchPattern;
    private ResourceBundle messagesBundle;

    private Button addButton;
    private Button rateButton;
    private Button editButton;
    private Button deleteButton;
    private Button infoButton;

    public SelectedSearchPatternButtonLeiste(final MainPattyUI ui, final SearchPattern selectedSearchPattern) {
        currentUser = ui.getCurrentUser();
        messagesBundle = ui.getResourceBundle();
        addButton = new Button();
        rateButton = new Button();
        editButton = new Button();
        deleteButton = new Button();
        infoButton = new Button();
        configureButton(addButton, new SearchPatternAddCommand(ui), new ThemeResource(Constants.ICON_ADD), "tooltip_add");
        configureButton(rateButton, new SearchPatternRateCommand(ui), new ThemeResource(Constants.ICON_RATE), "tooltip_rate");
        configureButton(editButton, new SearchPatternEditCommand(ui), new ThemeResource(Constants.ICON_EDIT), "tooltip_edit");
        configureButton(deleteButton, new SearchPatternDeleteCommand(ui), new ThemeResource(Constants.ICON_DELETE), "tooltip_delete");
        configureButton(infoButton, new SearchPatternShowInfoCommand(ui), new ThemeResource(Constants.ICON_INFO), "tooltip_info");
        reload(selectedSearchPattern);
        addComponents(addButton, rateButton, editButton, deleteButton, infoButton);
    }

    private void configureButton(final Button button, final SearchPatternCommand command, final ThemeResource icon, final String toolTipI18NWord) {
        button.setIcon(icon);
        button.setDescription(messagesBundle.getString(toolTipI18NWord));
        button.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                command.execute(selectedSearchPattern);
            }
        });
    }

    private void configureButtonsAccess() {
        infoButton.setEnabled(true);
        if (currentUser.getBenutzerGruppe().getId() <= 1) {
            addButton.setEnabled(false);
            rateButton.setEnabled(false);
            editButton.setEnabled(false);
            deleteButton.setEnabled(false);
        } else {
            addButton.setEnabled(true);
            if (currentUser.equals(selectedSearchPattern.getBenutzer())) {
                rateButton.setEnabled(false);
                editButton.setEnabled(true);
                deleteButton.setEnabled(true);
            } else {
                rateButton.setEnabled(true);
                editButton.setEnabled(false);
                deleteButton.setEnabled(false);
            }
        }
    }

    public void reload(final SearchPattern selectedSearchPattern) {
        this.selectedSearchPattern = selectedSearchPattern;
        if (selectedSearchPattern == null) {
            editButton.setEnabled(false);
            rateButton.setEnabled(false);
            deleteButton.setEnabled(false);
            infoButton.setEnabled(false);
        } else {
            configureButtonsAccess();
        }
    }
}
