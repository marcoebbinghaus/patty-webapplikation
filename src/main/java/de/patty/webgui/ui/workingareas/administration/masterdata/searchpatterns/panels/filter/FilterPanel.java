package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter;

import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.RapidPanel;
import de.patty.webgui.ui.workingareas.Componentssetable;
import de.patty.webgui.ui.workingareas.Internationalizationable;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.ui.NewFilterWindow;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/17/13
 * Time: 3:26 PM
 *
 * Layout where new {@link de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.model.Filter}s
 * can be added and applied filters can be removed. Is displayed in the {@link de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.SearchPatternsScreen}.
 */
public class FilterPanel extends RapidPanel implements Componentssetable, Internationalizationable {

    private Button addFilterButton;
    private FormLayout appliedFiltersLayout;
    private MainPattyUI ui;
    private ResourceBundle messages;

    /**
     * Instantiates a new FilterPanel.
     *
     * @param ui the ui
     */
    public FilterPanel(final MainPattyUI ui) {
        this.ui = ui;
        setSizeFull();
        messages = ui.getResourceBundle();
        appliedFiltersLayout = new FormLayout();
        addFilterButton = new Button();
        addFilterButton.setStyleName("link");
        addFilterButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                ui.addWindow(new NewFilterWindow(ui));
            }
        });
        setComponents();
        doInternationalization();
    }


    /**
     * Gets appliedFiltersLayout. Used to add the {@link de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.ui.UIFilterRow}s
     * for the applied {@link de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.model.Filter}s.
     *
     * @return the applied filters layout
     */
    public FormLayout getAppliedFiltersLayout() {
        return appliedFiltersLayout;
    }

    @Override
    public void setComponents() {
        addComponent(addFilterButton);
        addComponent(appliedFiltersLayout);
    }

    @Override
    public void doInternationalization() {
        addFilterButton.setCaption(messages.getString("addfilter"));
        setCaption(messages.getString("filters"));
    }
}
