package de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.searchpatterns.filter;

import com.vaadin.ui.HorizontalLayout;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.FilterField;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.searchpatterns.filter.components.SearchPatternsFilterField;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/4/13
 * Time: 12:32 PM
 *
 */
public class FilterLayout extends HorizontalLayout {

    private FilterField filterField;

    public FilterLayout(final MainPattyUI ui) {
        setWidth("100%");
        filterField = new SearchPatternsFilterField(ui);
        addComponent(filterField);
    }
}
