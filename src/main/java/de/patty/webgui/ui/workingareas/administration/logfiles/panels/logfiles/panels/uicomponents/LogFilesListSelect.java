package de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles.panels.uicomponents;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.ListSelect;
import de.patty.persistence.pojos.LogFile;
import de.patty.rmi.managers.LogfilesManager;
import de.patty.rmi.managers.NoConnectionException;
import de.patty.webgui.MainPattyUI;

import java.util.List;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/25/13
 * Time: 2:02 PM
 *
 * The {@link ListSelect} which contains all log files.
 */
public class LogFilesListSelect extends ListSelect {

    private List<LogFile> logFileList;
    private LogfilesManager logfilesManager;

    /**
     * Instantiates a new LogFilesListSelect.
     *
     * @param ui the ui
     * @throws NoConnectionException thrown if no connection to the crawler system is established.
     */
    public LogFilesListSelect(final MainPattyUI ui) throws NoConnectionException {
        setSizeFull();
        logfilesManager = new LogfilesManager();
        logFileList = logfilesManager.getLogfiles();
        final BeanItemContainer logFilesContainer = new BeanItemContainer<>(LogFile.class, logFileList);
        setContainerDataSource(logFilesContainer);
        setItemCaptionMode(ItemCaptionMode.ID);
    }

    /**
     * Gets the containing log files.
     *
     * @return the log file list
     */
    public List<LogFile> getLogFileList() {
        return logFileList;
    }

    /**
     * Sets the containing log files.
     *
     * @param logFileList the log file list
     */
    public void setLogFileList(List<LogFile> logFileList) {
        this.logFileList = logFileList;
        final BeanItemContainer logFilesContainer = new BeanItemContainer<>(LogFile.class, logFileList);
        setContainerDataSource(logFilesContainer);
    }

    /**
     * resets the containing log files (removes filters so that all existing log files are contained).
     */
    public void resetList() {
        final LogfilesManager logfilesManager = new LogfilesManager();
        try {
            logFileList = logfilesManager.getLogfiles();
        } catch (final NoConnectionException e) {
            //do nothing
        }
        setLogFileList(logFileList);
    }
}
