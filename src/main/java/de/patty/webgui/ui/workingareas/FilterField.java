package de.patty.webgui.ui.workingareas;

import com.vaadin.data.Container;
import com.vaadin.shared.ui.combobox.FilteringMode;
import com.vaadin.ui.ComboBox;
import de.patty.webgui.MainPattyUI;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/4/13
 * Time: 12:35 PM
 *
 */
public abstract class FilterField extends ComboBox implements Internationalizationable {

    private ResourceBundle messagesBundle;
    protected Container container;

    public FilterField(final MainPattyUI ui) {
        setWidth("100%");
        messagesBundle = ui.getResourceBundle();
        createContainerAndSetItemCaptionMode();
        doInternationalization();
        setImmediate(true);
        setContainerDataSource(container);
        setFilteringMode(FilteringMode.CONTAINS);
        setNullSelectionAllowed(true);
        setNewItemsAllowed(true);
    }

    public abstract void createContainerAndSetItemCaptionMode();


    @Override
    public void doInternationalization() {
        setCaption(messagesBundle.getString("filter"));
    }
}
