package de.patty.webgui.ui.workingareas.search.areas.searcharea.resultslayout.infolayout;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import de.patty.persistence.entities.SearchPattern;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.RapidInfoWindow;
import de.patty.webgui.ui.workingareas.Componentssetable;
import de.patty.webgui.ui.workingareas.Internationalizationable;
import de.patty.webgui.ui.workingareas.search.areas.searcharea.resultslayout.resultpanel.ResultPanel;

import java.util.List;
import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/11/13
 * Time: 12:05 PM
 *
 */
public class SearchInfoLayout extends VerticalLayout implements Componentssetable, Internationalizationable {

    private Label zeile1;
    private Label zeile2;
    private Button showSearchQueryButton;
    private ResourceBundle messages;
    private MainPattyUI ui;
    private String searchString;
    private SearchPattern searchPattern;
    private List<ResultPanel> resultList;

    public SearchInfoLayout(final MainPattyUI ui, final String searchString, final SearchPattern searchPattern, final List<ResultPanel> resultList) {
        this.ui = ui;
        this.searchString = searchString;
        this.searchPattern = searchPattern;
        this.resultList = resultList;
        messages = ui.getResourceBundle();
        zeile1 = new Label();
        zeile1.setContentMode(ContentMode.HTML);
        zeile2 = new Label();
        zeile2.setContentMode(ContentMode.HTML);
        showSearchQueryButton = new Button();
        showSearchQueryButton.setStyleName("link");
        build();
        doInternationalization();
        setComponents();
    }

    public void build() {
        buildZeile1();
        buildZeile2();
        buildButton();
    }

    private void buildZeile1() {
        if (searchPattern == null) {
            zeile1.setValue(messages.getString("searchedwith") + " -");
        } else {
            zeile1.setValue(messages.getString("searchedwith") + " " + searchPattern.getTitle());
        }
    }

    private void buildZeile2() {
        if (resultList == null || resultList.isEmpty()) {
            zeile2.setValue(messages.getString("noresults"));
        } else {
            zeile2.setValue(messages.getString("pattyfoundpart1") + " <b>" + resultList.size() + "</b> " + messages.getString("pattyfoundpart2"));
        }
    }

    private void buildButton() {
        showSearchQueryButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                ui.addWindow(new RapidInfoWindow(ui, "searchquery", searchString, false));
            }
        });
    }


    @Override
    public void doInternationalization() {
        showSearchQueryButton.setCaption(messages.getString("querystring"));
    }

    @Override
    public void setComponents() {
        removeAllComponents();
        addComponent(zeile1);
        addComponent(zeile2);
        addComponent(showSearchQueryButton);
    }
}
