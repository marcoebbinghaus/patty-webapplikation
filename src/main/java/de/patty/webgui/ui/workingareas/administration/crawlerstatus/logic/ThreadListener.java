package de.patty.webgui.ui.workingareas.administration.crawlerstatus.logic;

import com.github.wolfie.refresher.Refresher;
import de.patty.etc.designpatterns.observer.Subject;

import java.util.ArrayList;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 5/27/13
 * Time: 06:22 PM
 *
 * Contains the thread which checks in regular intervals if the crawler is running and notifies all observers if the
 * running state changes.
 */
public class ThreadListener extends Subject implements Refresher.RefreshListener {

    private final CheckRunningThread thread;
    private Boolean crawlerRunning;
    private int notRunningTicks = 0;

    /**
     * Instantiates a new ThreadListener.
     *
     * @param thread the CheckRunningThread
     */
    public ThreadListener(final CheckRunningThread thread) {
        this.thread = thread;
        this.observers = new ArrayList<>();
    }

    @Override
    public void refresh(final Refresher refresher) {
        crawlerRunning = thread.isCrawlerRunning();
        if (crawlerRunning != null) {
            if (crawlerRunning) {
                notifyObservers();
                notRunningTicks = 0;
            } else {
                notRunningTicks += 1;
                if (notRunningTicks >= 5) {
                    notifyObservers();
                    notRunningTicks = 0;
                }
            }
        } else {
            notifyObservers();
        }
    }

    /**
     * Returns if the crawler is running
     *
     * @return true if the crawler is running, false otherwise (null if no connection to the crawler system).
     */
    public Boolean isCrawlerRunning() {
        return crawlerRunning;
    }
}
