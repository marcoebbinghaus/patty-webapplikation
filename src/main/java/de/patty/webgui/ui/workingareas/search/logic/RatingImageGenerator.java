package de.patty.webgui.ui.workingareas.search.logic;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Image;
import de.patty.etc.Constants;
import de.patty.persistence.entities.SearchPattern;
import de.patty.webgui.MainPattyUI;
import org.apache.log4j.Logger;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/10/13
 * Time: 9:36 AM
 *
 */
public class RatingImageGenerator {
    private Logger logger = Logger.getLogger(RatingImageGenerator.class);
    private SearchPattern searchPattern;
    private ResourceBundle messagesBundle;

    public RatingImageGenerator(final MainPattyUI ui, final SearchPattern searchPattern) {
        messagesBundle = ui.getResourceBundle();
        this.searchPattern = searchPattern;
    }

    public Image generateRatingImage() {
        final Double rating = searchPattern.getRating();
        Image image = null;
        if (rating < 0.0) {
            //do nothing (leave image as null)
        } else if (rating >= 0.0 && rating <= 0.5) {
            image = new Image(messagesBundle.getString("sp_rating"), new ThemeResource(Constants.RATING_0_STARS));
        } else if (rating > 0.5 && rating <= 1.5) {
            image = new Image(messagesBundle.getString("sp_rating"), new ThemeResource(Constants.RATING_1_STARS));
        } else if (rating > 1.5 && rating <= 2.5) {
            image = new Image(messagesBundle.getString("sp_rating"), new ThemeResource(Constants.RATING_2_STARS));
        } else if (rating > 2.5 && rating <= 3.5) {
            image = new Image(messagesBundle.getString("sp_rating"), new ThemeResource(Constants.RATING_3_STARS));
        } else if (rating > 3.5 && rating <= 4.5) {
            image = new Image(messagesBundle.getString("sp_rating"), new ThemeResource(Constants.RATING_4_STARS));
        } else if (rating > 4.5) {
            image = new Image(messagesBundle.getString("sp_rating"), new ThemeResource(Constants.RATING_5_STARS));
        } else {
            logger.warn("fehlerhaftes Ratingergebnis");
        }
        return image;
    }
}
