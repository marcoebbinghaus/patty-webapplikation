package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.model;

import de.patty.persistence.entities.SearchPattern;
import de.patty.webgui.MainPattyUI;

import java.util.HashMap;
import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/22/13
 * Time: 2:52 PM
 *
 *  Map that contains the translations of the {@link SearchPattern} properties as keys and the SearchPattern properties theirselves as values.
 *  The {@link SearchPatternAttributeNameToI18nMap} is the inverted Map.
 */
public class SearchPatternI18nToAttributeNamesMap extends HashMap<String, String> {

    private MainPattyUI ui;
    private ResourceBundle messages;

    public SearchPatternI18nToAttributeNamesMap(final MainPattyUI ui) {
        this.ui = ui;
        this.messages = ui.getResourceBundle();
        put(messages.getString("sp_id"), SearchPattern.ID);
        put(messages.getString("sp_author"), SearchPattern.AUTHOR);
        put(messages.getString("sp_title"), SearchPattern.TITLE);
        put(messages.getString("sp_content"), SearchPattern.CONTENT);
        put(messages.getString("sp_descr"), SearchPattern.DESCR);
        put(messages.getString("sp_parent"), SearchPattern.PARENT);
        put(messages.getString("sp_activated"), SearchPattern.ACTIVATED);
    }
}
