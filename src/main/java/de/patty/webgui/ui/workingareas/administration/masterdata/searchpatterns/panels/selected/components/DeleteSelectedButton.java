package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.selected.components;

import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.logic.SearchPatternDeleteCommand;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.uicomponents.SearchPatternsTable;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/22/13
 * Time: 10:25 AM
 *
 * Button which executes a {@link SearchPatternDeleteCommand} for all selected SearchPatterns in the {@link SearchPatternsTable}.
 */
public class DeleteSelectedButton extends SelectedButton {


    public DeleteSelectedButton(final SearchPatternsTable table, final MainPattyUI ui) {
        super(table, ui);
        setCaption(messages.getString("deleteselected"));
    }

    @Override
    protected void addClickListener() {
        addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                final SearchPatternDeleteCommand deleteCommand = new SearchPatternDeleteCommand(ui);
                deleteCommand.execute(table.getSelectedSearchPatterns());
            }
        });
    }
}
