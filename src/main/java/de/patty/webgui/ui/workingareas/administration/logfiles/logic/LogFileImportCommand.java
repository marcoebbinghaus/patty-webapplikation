package de.patty.webgui.ui.workingareas.administration.logfiles.logic;

import de.patty.etc.Constants;
import de.patty.persistence.pojos.LogFile;
import de.patty.rmi.DoRMILogicAndDoSomethingAfterRunnable;
import de.patty.rmi.managers.LogfilesManager;
import de.patty.webgui.MainPattyUI;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/26/13
 * Time: 11:30 AM
 *
 * Concrete command class to import a log file from the crawler system to the patty system
 * (which must be done before it can be downloaded).
 */
public abstract class LogFileImportCommand extends LogFileCommand {
    /**
     * Instantiates a new LogFileImportCommand.
     *
     * @param ui the ui
     */
    public LogFileImportCommand(MainPattyUI ui) {
        super(ui);
    }

    @Override
    public void execute(final LogFile logFile) {
        final Thread thread = new Thread(new DoRMILogicAndDoSomethingAfterRunnable(ui) {
            @Override
            public boolean doRMI() {
                try {
                    final LogfilesManager logfilesManager = new LogfilesManager();
                    final FileWriter fileWriter = new FileWriter(new File(Constants.PATH_TO_DOWNLOADED_FILES + "/" + logFile.getFileName()));
                    if (logFile.getContent() == null) {
                        logFile.setContent(logfilesManager.getContentOfLogfile(logFile));
                    }
                    fileWriter.write(logFile.getContent());
                    fileWriter.flush();
                    fileWriter.close();
                } catch (final IOException e) {
                    return false;
                }
                return true;
            }

            @Override
            public void doSomethingAfterRMI() {
                LogFileImportCommand.this.doSomethingAfterRMI();
            }

        });
        thread.start();
    }

    @Override
    public void execute(Set<LogFile> logFiles) {
        // only a single log file can be imported at a time.
    }

    /**
     * Actions that must be done after the communication with the crawler system. Is implemented in the
     * {@link de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles.panels.uicomponents.LogFileDownloadWindow}
     * where the download button is enabled after the log file import is completed.
     */
    public abstract void doSomethingAfterRMI();
}
