package de.patty.webgui.ui.workingareas.search;

import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.Screen;
import de.patty.webgui.ui.workingareas.search.areas.searcharea.SearchPanel;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.SearchPatternPanel;
import de.patty.webgui.ui.workingareas.search.logic.SearchExecutorForVaadinUI;

public class SearchScreen extends Screen {

    private HorizontalLayout frameLayout;
    private SearchPatternPanel searchPatternPanel;
    private SearchPanel searchPanel;

    public SearchScreen(final MainPattyUI ui) {
        super(ui);
        super.getContentLayout().setMargin(false);
        activeVerticalFullScreenSize(true);

        searchPatternPanel = new SearchPatternPanel(ui);
        searchPatternPanel.setSizeFull();

        searchPanel = new SearchPanel(ui);
        searchPanel.setSizeFull();

        frameLayout = new HorizontalLayout(searchPatternPanel, searchPanel);
        frameLayout.setSizeFull();
        frameLayout.setExpandRatio(searchPatternPanel, 0.2f);
        frameLayout.setExpandRatio(searchPanel, 0.8f);
        frameLayout.setSizeFull();
        searchPanel.getSearchBar().getSearchButton().addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                final SearchExecutorForVaadinUI searchExecutorForVaadinUI = new SearchExecutorForVaadinUI(ui,
                        searchPanel.getSearchBar().getSearchInput(),
                        searchPatternPanel.getSelectedSearchPattern(),
                        searchPanel.getSearchBar().getQueryLanguageCheckBox().getValue(),
                        searchPanel.getResultsLayout());
                searchExecutorForVaadinUI.processResults();
                searchPanel.getSearchBar().getSearchField().focus();

            }
        });

        setComponents();
        doInternationalization();
    }

    @Override
    public void reload() {
        ui.setWorkingArea(new SearchScreen(ui));
    }

    @Override
    public void setComponents() {
        addComponent(frameLayout);
    }

    @Override
    public void doInternationalization() {
        searchPatternPanel.setCaption(messagesBundle.getString("searchpattern"));
        searchPanel.setCaption(messagesBundle.getString("thesearch"));
    }
}
