package de.patty.webgui.ui.workingareas.search.logic;

import de.patty.etc.Constants;
import de.patty.persistence.entities.SearchPattern;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/29/13
 * Time: 12:00 PM
 *
 */
public class SearchStringReturner {

    public static final String CONTENT = "content:";
    public static final String TITLE = "title:";
    public static final String OR = " OR ";
    public static final String AND = " AND ";
    public static final String CONCATENATORS[] = {"&&", "||", "AND", "OR"};

    protected String finalSearchString;

    public String getSearchString() {
        return finalSearchString;
    }

    public String createPatternContentWithoutUserInput(final SearchPattern searchPattern) {
        final String[] searchPatternParts = searchPattern.getContent().split("\\s");
        final List<String> searchPatternPartsList = new ArrayList<>(Arrays.asList(searchPatternParts));
        String searchInputPart;
        do {
            searchInputPart = null;
            for (final String part : searchPatternPartsList) {
                if (part.contains(Constants.PATTYSEARCH_INPUT_CONSTANT)) {
                    searchInputPart = part;
                }
            }
            if (searchInputPart != null) {
                try {
                    final int indexOfPartBeforeSearchInputPart = searchPatternPartsList.indexOf(searchInputPart) - 1;
                    for (final String concatenator : CONCATENATORS) {
                        if (searchPatternPartsList.get(indexOfPartBeforeSearchInputPart).equals(concatenator)) {
                            searchPatternPartsList.remove(indexOfPartBeforeSearchInputPart);
                        }
                    }
                } catch (final IndexOutOfBoundsException e) {
                    //do nothing
                }
                try {
                    final int indexOfPartAfterSearchInputPart = searchPatternPartsList.indexOf(searchInputPart) - 1;
                    for (final String concatenator : CONCATENATORS) {
                        if (searchPatternPartsList.get(indexOfPartAfterSearchInputPart).equals(concatenator)) {
                            searchPatternPartsList.remove(indexOfPartAfterSearchInputPart);
                        }
                    }
                } catch (final IndexOutOfBoundsException e) {
                    //do nothing
                }
                searchPatternPartsList.remove(searchInputPart);
            }
        } while (searchInputPart != null);
        final StringBuilder stringBuilder = new StringBuilder();
        for (final String s : searchPatternPartsList) {
            stringBuilder.append(s);
            stringBuilder.append(" ");
        }
        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
        return stringBuilder.toString();
    }

    public String appendSolrQueryStringToSearchPatternContentWithoutInputVariables(final String searchPatternContent, final String solrQueryString) {
        final StringBuilder searchPatternContentWithoutInputVariablePlusSolrQueryInputStringBuilder = new StringBuilder();
        searchPatternContentWithoutInputVariablePlusSolrQueryInputStringBuilder.append("(");
        searchPatternContentWithoutInputVariablePlusSolrQueryInputStringBuilder.append(searchPatternContent);
        searchPatternContentWithoutInputVariablePlusSolrQueryInputStringBuilder.append(")");
        searchPatternContentWithoutInputVariablePlusSolrQueryInputStringBuilder.append(AND);
        searchPatternContentWithoutInputVariablePlusSolrQueryInputStringBuilder.append("(");
        searchPatternContentWithoutInputVariablePlusSolrQueryInputStringBuilder.append(solrQueryString);
        searchPatternContentWithoutInputVariablePlusSolrQueryInputStringBuilder.append(")");
        return searchPatternContentWithoutInputVariablePlusSolrQueryInputStringBuilder.toString();
    }
}
