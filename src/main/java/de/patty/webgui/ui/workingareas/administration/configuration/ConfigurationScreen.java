package de.patty.webgui.ui.workingareas.administration.configuration;

import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.ReloadScreenButton;
import de.patty.webgui.ui.workingareas.Screen;
import de.patty.webgui.ui.workingareas.administration.configuration.uicomponents.ConfigurationLayout;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/4/13
 * Time: 10:15 AM
 *
 * The {@link Screen} in which the administrator can change the parameters from the nutch-site.xml / nutch-default.xml
 *
 */
public class ConfigurationScreen extends Screen {

    private boolean connectionToCrawlerSystem = true;
    private ConfigurationLayout configurationLayout;

    /**
     * Instantiates a new ConfigurationScreen.
     *
     * @param ui the ui
     */
    public ConfigurationScreen(final MainPattyUI ui) {
        super(ui);
        activeVerticalFullScreenSize(true);
        try {
            configurationLayout = new ConfigurationLayout(ui);
        } catch (final NullPointerException e) { //Connection to the crawler system failed
            connectionToCrawlerSystem = false;
        }
        setComponents();
        doInternationalization();
    }

    @Override
    public void reload() {
        ui.setWorkingArea(new ConfigurationScreen(ui));
    }

    @Override
    public void setComponents() {
        if (connectionToCrawlerSystem) {
            addComponent(configurationLayout);
        } else {
            addComponent(new Label(messagesBundle.getString("reloadwhencrawlerisbackup")));
            final ReloadScreenButton reloadButton = new ReloadScreenButton(ui);
            addComponent(reloadButton);
            getContentLayout().setExpandRatio(reloadButton, 1.0f);
            Notification.show(messagesBundle.getString("noconnectiontocrawler"));
        }
    }

    @Override
    public void doInternationalization() {
        //nothing to localize
    }
}
