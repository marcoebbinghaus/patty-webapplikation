package de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.selectedpattern.model;

import com.vaadin.data.Container;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.*;
import de.patty.persistence.dao.BenutzerDAO;
import de.patty.persistence.dao.DaoFactorySingelton;
import de.patty.persistence.entities.Benutzer;
import de.patty.persistence.entities.SearchPattern;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.searchpatterns.filter.components.SearchPatternsFilterField;
import de.patty.webgui.ui.workingareas.search.logic.RatingImageGenerator;
import de.patty.webgui.ui.workingareas.search.model.validators.NotEmptyValidator;
import de.patty.webgui.ui.workingareas.search.model.validators.SearchPatternSyntaxValidator;

import java.util.List;
import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/6/13
 * Time: 1:23 PM
 *
 */
public class SearchPatternFieldGroup extends FieldGroup {

    private Component rating;

    public SearchPatternFieldGroup(final MainPattyUI ui, final SearchPattern searchPattern) {
        final BenutzerDAO benutzerDAO = DaoFactorySingelton.getInstance().getBenutzerDAO();
        final List<Benutzer> benutzerList = benutzerDAO.loadAllEntities();
        final Container authorsContainer = new BeanItemContainer<Benutzer>(Benutzer.class, benutzerList);
        setItemDataSource(new BeanItem<>(searchPattern));
        final ResourceBundle messages = ui.getResourceBundle();
        final TextField titleField = new TextField(messages.getString("sp_title"));
        final TextArea descriptionArea = new TextArea(messages.getString("sp_descr"));
        final TextArea syntaxArea = new TextArea(messages.getString("sp_content"));
        final SearchPatternsFilterField vaterField = new SearchPatternsFilterField(ui);
        final TextField allTimeVotesField = new TextField(messages.getString("sp_alltimevotes"));
        final ComboBox authorBox = new ComboBox(messages.getString("sp_author"));
        final RatingImageGenerator ratingImageGenerator = new RatingImageGenerator(ui, searchPattern);
        rating = ratingImageGenerator.generateRatingImage();
        if (rating == null) {
            final Label notEnoughVotesLabel = new Label(messages.getString("notenoughvotes"));
            notEnoughVotesLabel.setCaption(messages.getString("sp_rating"));
            rating = notEnoughVotesLabel;
        }
        titleField.setWidth("100%");
        titleField.setSizeFull();
        titleField.setRequired(true);
        titleField.setImmediate(true);
        titleField.addValidator(new NotEmptyValidator(ui.getResourceBundle()));
        titleField.setCaption(ui.getResourceBundle().getString("name"));
        titleField.setNullRepresentation("");
        descriptionArea.setWidth("100%");
        descriptionArea.setRequired(true);
        descriptionArea.setNullRepresentation("");
        syntaxArea.setWidth("100%");
        syntaxArea.setSizeFull();
        syntaxArea.setRequired(true);
        syntaxArea.setImmediate(true);
        syntaxArea.setNullRepresentation("");
        syntaxArea.setDescription(ui.getResourceBundle().getString("descriptonforsyntax"));
        syntaxArea.addValidator(new SearchPatternSyntaxValidator(ui.getResourceBundle()));
        syntaxArea.setCaption(ui.getResourceBundle().getString("syntax"));
        vaterField.setSizeFull();
        vaterField.setImmediate(true);
        vaterField.setCaption(ui.getResourceBundle().getString("parentpattern"));
        allTimeVotesField.setWidth("100%");
        authorBox.setWidth("100%");
        authorBox.setNullSelectionAllowed(false);
        authorBox.setContainerDataSource(authorsContainer);
        authorBox.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        authorBox.setItemCaptionPropertyId(Benutzer.LOGIN);
        bind(titleField, SearchPattern.TITLE);
        bind(descriptionArea, SearchPattern.DESCR);
        bind(syntaxArea, SearchPattern.CONTENT);
        bind(authorBox, SearchPattern.AUTHOR);
        bind(vaterField, SearchPattern.PARENT);
        bind(allTimeVotesField, SearchPattern.ALLTIMEVOTES);
        for (final Field<?> field : getFields()) {
            field.setReadOnly(false);
        }
    }

    public Component getRating() {
        return rating;
    }
}
