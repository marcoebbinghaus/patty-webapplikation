package de.patty.webgui.ui.workingareas.search.model.validators;

import com.vaadin.data.Validator;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/7/13
 * Time: 1:54 PM
 *
 */
public abstract class AbstractValidator implements Validator {

    protected ResourceBundle messagesBundle;

    public AbstractValidator(final ResourceBundle messages) {
        messagesBundle = messages;
    }
}
