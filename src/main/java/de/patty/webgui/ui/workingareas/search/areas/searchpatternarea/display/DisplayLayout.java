package de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.display;

import com.vaadin.data.Property;
import com.vaadin.ui.FormLayout;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.Componentssetable;
import de.patty.webgui.ui.workingareas.Internationalizationable;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.display.components.DisplayModeBox;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.display.components.SortModeBox;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.display.model.DisplayModes;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/5/13
 * Time: 5:36 PM
 *
 */
public class DisplayLayout extends FormLayout implements Componentssetable, Internationalizationable {

    private DisplayModeBox displayModeBox;
    private SortModeBox sortModeBox;
    private ResourceBundle messagesBundle;

    public DisplayLayout(final MainPattyUI ui) {
        messagesBundle = ui.getResourceBundle();
        displayModeBox = new DisplayModeBox(ui);
        sortModeBox = new SortModeBox(ui);
        displayModeBox.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                System.out.println("displaybox-value: " + displayModeBox.getTheValue());
                if (displayModeBox.getTheValue() == DisplayModes.ALPHABETICAL_LIST ||
                        displayModeBox.getTheValue() == DisplayModes.MOSTPOPULAR) {
                    sortModeBox.setEnabled(true);
                } else {
                    sortModeBox.setEnabled(false);
                }
            }
        });
        sortModeBox.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                final DisplayModes selectedDisplayMode = displayModeBox.getTheValue();
                displayModeBox.setTheValue(DisplayModes.ALPHABETICAL_ACCORDION);
                displayModeBox.setTheValue(selectedDisplayMode);
            }
        });
        setComponents();
        doInternationalization();
    }

    @Override
    public void setComponents() {
        addComponents(displayModeBox, sortModeBox);
    }

    @Override
    public void doInternationalization() {
        displayModeBox.setCaption(messagesBundle.getString("displaymode"));
        sortModeBox.setCaption(messagesBundle.getString("sortmode"));
    }

    public SortModeBox getSortModeBox() {
        return sortModeBox;
    }

    public DisplayModeBox getDisplayModeBox() {
        return displayModeBox;
    }
}
