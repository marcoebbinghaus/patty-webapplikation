package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.logic;

import de.patty.persistence.entities.SearchPattern;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.searchpatterns.add.AddOrEditSearchPatternWindow;

import java.util.Set;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/19/13
 * Time: 11:13 AM
 *
 * This command is for adding a new {@link SearchPattern} (by opening a new {@link AddOrEditSearchPatternWindow}).
 */
public class SearchPatternAddCommand extends SearchPatternCommand {

    /**
     * Instantiates a SearchPatternAddCommand.
     *
     * @param ui the ui
     */
    public SearchPatternAddCommand(final MainPattyUI ui) {
        super(ui);
    }

    @Override
    public void execute(final SearchPattern searchPattern) {
        ui.addWindow(new AddOrEditSearchPatternWindow(ui));
    }

    @Override
    public void execute(Set<SearchPattern> searchPatterns) {
        // not (yet) needed
    }
}
