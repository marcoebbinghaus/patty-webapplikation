package de.patty.webgui.ui.workingareas.administration.crawlerstatus.logic;

import de.patty.etc.Constants;
import de.patty.rmi.managers.CrawlerManager;
import de.patty.webgui.ui.workingareas.administration.crawlerstatus.CrawlerStatusScreen;
import org.apache.log4j.Logger;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 5/27/13
 * Time: 04:22 PM
 *
 * Thread which sends a request every 500ms (or whatever value is {@link Constants}.CHECKRUNNING_INTERVAL} if the user
 * is on the {@link CrawlerStatusScreen}.
 */
public class CheckRunningThread extends Thread {

    private static final Logger logger = Logger.getLogger(CheckRunningThread.class);

    private CrawlerManager crawlerManager;
    private Boolean crawlerRunning = false;
    private Object workingArea;
    private boolean killMe;


    /**
     * Instantiates a new CheckRunningThread.
     */
    public CheckRunningThread() {
        crawlerManager = new CrawlerManager();
        killMe = false;
    }

    @Override
    public void run() {
        while (!killMe) {
            if (workingArea instanceof CrawlerStatusScreen) {
                try {
                    crawlerRunning = crawlerManager.checkRunning();
                } catch (final NullPointerException e) {
                    crawlerRunning = null;
                }
            }
            try {
                Thread.sleep(Constants.CHECKRUNNING_INTERVAL);
            } catch (final InterruptedException e) {
                logger.warn(e.getLocalizedMessage());
            }
        }
        System.out.println("thread killed");
    }

    /**
     * returns if the crawler is running
     *
     * @return true if the crawler is running, false otherwise.
     */
    public Boolean isCrawlerRunning() {
        return crawlerRunning;
    }

    /**
     * Sets the current screen whenever the user changes it.
     *
     * @param workingArea the new shown screen
     */
    public void setWorkingArea(Object workingArea) {
        this.workingArea = workingArea;
    }

    /**
     * marks the killMe attribute as true so that the Thread stops (happends if a user logs out).
     */
    public void kill() {
        killMe = true;
    }
}