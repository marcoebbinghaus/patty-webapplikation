package de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.searchpatterns.add;

import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.*;
import de.patty.persistence.dao.DaoFactorySingelton;
import de.patty.persistence.entities.SearchPattern;
import de.patty.persistence.entitymanagers.SearchPatternManager;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.RapidWindow;
import de.patty.webgui.ui.SaveCancelButtonBar;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.selectedpattern.model.SearchPatternFieldGroup;
import de.patty.webgui.ui.workingareas.search.model.exceptions.FatherPatternNotExistsException;
import de.patty.webgui.ui.workingareas.search.model.exceptions.TitleAlreadyExistingException;

public class AddOrEditSearchPatternWindow extends RapidWindow {
    private Label infoLabel;
    private FormLayout fieldLayout;
    private SearchPatternFieldGroup fieldGroup;
    private SaveCancelButtonBar buttonLeiste;

    public AddOrEditSearchPatternWindow(final MainPattyUI ui, final SearchPattern searchPatternToEdit) {
        super(ui);
        setWidth("500px");
        infoLabel = new Label(messagesBundle.getString("addinfo"));
        infoLabel.setContentMode(ContentMode.HTML);
        fieldGroup = new SearchPatternFieldGroup(ui, searchPatternToEdit);
        makeFieldsVisible(ui);
        buttonLeiste = new SaveCancelButtonBar() {
            @Override
            public void doInternationalization() {
                saveButton.setCaption(messagesBundle.getString("save"));
                cancelButton.setCaption(messagesBundle.getString("cancel"));
            }

            @Override
            public void setSaveButtonListener() {
                saveButton.addClickListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(final Button.ClickEvent clickEvent) {
                        try {
                            final SearchPattern oldFatherSearchPattern = DaoFactorySingelton.getInstance().getSearchPatternDAO().findByID(searchPatternToEdit.getId()).getParent();
                            fieldGroup.commit();
                            final BeanItem<SearchPattern> beanItem = (BeanItem) fieldGroup.getItemDataSource();
                            final SearchPattern searchPattern = beanItem.getBean();
                            final SearchPattern fatherSearchPattern = searchPattern.getParent();
                            final SearchPatternManager searchPatternManager = new SearchPatternManager();
                            if (oldFatherSearchPattern != fatherSearchPattern) {
                                searchPatternManager.saveEditedSearchPattern(searchPatternToEdit, oldFatherSearchPattern);
                            } else {
                                searchPatternManager.saveEditedSearchPattern(searchPatternToEdit);
                            }
                            AddOrEditSearchPatternWindow.this.close();
                            ui.getScreen().reload();
                        } catch (final FieldGroup.CommitException e) {
                            Notification.show(messagesBundle.getString("invaliddata"));
                        }
                    }
                });
            }

            @Override
            public void setCancelButtonListener() {
                cancelButton.addClickListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(final Button.ClickEvent clickEvent) {
                        AddOrEditSearchPatternWindow.this.close();
                    }
                });
            }
        };
        doInternationalization();
        setComponents();
    }

    public AddOrEditSearchPatternWindow(final MainPattyUI ui) {
        super(ui);
        infoLabel = new Label(messagesBundle.getString("addinfo"));
        infoLabel.setContentMode(ContentMode.HTML);
        setWidth("500px");
        fieldGroup = new SearchPatternFieldGroup(ui, new SearchPattern());
        makeFieldsVisible(ui);
        final ComboBox authorsField = (ComboBox) fieldGroup.getField(SearchPattern.AUTHOR);
        authorsField.setValue(ui.getCurrentUser());
        buttonLeiste = new SaveCancelButtonBar() {
            @Override
            public void doInternationalization() {
                saveButton.setCaption(messagesBundle.getString("save"));
                cancelButton.setCaption(messagesBundle.getString("cancel"));
            }

            @Override
            public void setSaveButtonListener() {
                saveButton.addClickListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(final Button.ClickEvent clickEvent) {
                        try {
                            fieldGroup.commit();
                            final BeanItem<SearchPattern> beanItem = (BeanItem) fieldGroup.getItemDataSource();
                            final SearchPattern searchPattern = beanItem.getBean();
                            final SearchPattern fatherSearchPattern = searchPattern.getParent();
                            final SearchPatternManager searchPatternManager = new SearchPatternManager();
                            if (fatherSearchPattern != null) {
                                searchPatternManager.saveNewSearchPattern(searchPattern, fatherSearchPattern);
                            } else {
                                searchPatternManager.saveNewSearchPattern(searchPattern);
                            }
                            AddOrEditSearchPatternWindow.this.close();
                            ui.getScreen().reload();
                        } catch (final FatherPatternNotExistsException e) {
                            Notification.show(messagesBundle.getString("error_nosuchsearchpattern"));
                        } catch (final TitleAlreadyExistingException e) {
                            Notification.show(messagesBundle.getString("error_titlealreadyexists"));
                        } catch (final FieldGroup.CommitException e) {
                            Notification.show(messagesBundle.getString("invaliddata"));
                        }
                    }
                });
            }

            @Override
            public void setCancelButtonListener() {
                cancelButton.addClickListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(final Button.ClickEvent clickEvent) {
                        AddOrEditSearchPatternWindow.this.close();
                    }
                });
            }
        };
        doInternationalization();
        setComponents();
    }

    private void makeFieldsVisible(final MainPattyUI ui) {
        if (ui.getCurrentUser().getBenutzerGruppe().getId() <= 2) {
            fieldGroup.getField(SearchPattern.AUTHOR).setVisible(false);
        }
    }

    @Override
    public void setComponents() {
        fieldLayout = new FormLayout();
        fieldLayout.addComponent(fieldGroup.getField(SearchPattern.TITLE));
        fieldLayout.addComponent(fieldGroup.getField(SearchPattern.DESCR));
        fieldLayout.addComponent(fieldGroup.getField(SearchPattern.CONTENT));
        fieldLayout.addComponent(fieldGroup.getField(SearchPattern.PARENT));
        fieldLayout.addComponent(fieldGroup.getField(SearchPattern.AUTHOR));
        fieldLayout.setSizeFull();
        addComponent(infoLabel);
        addComponent(fieldLayout);
        addComponent(buttonLeiste);
    }

    @Override
    public void doInternationalization() {
        setCaption(messagesBundle.getString("addsearchpattern"));
    }

    @Override
    protected void setLayout() {
        setContentLayout(new VerticalLayout());
    }
}
