package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.logic;

import de.patty.persistence.entities.SearchPattern;
import de.patty.webgui.MainPattyUI;

import java.util.Set;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/17/13
 * Time: 3:43 PM
 *
 * Abstract class for all kinds of {@link SearchPattern} related commands.
 */
public abstract class SearchPatternCommand {

    /**
     * The Ui.
     */
    protected MainPattyUI ui;

    /**
     * Instantiates a new SearchPatternCommand.
     *
     * @param ui the ui
     */
    public SearchPatternCommand(final MainPattyUI ui) {
        this.ui = ui;
    }

    /**
     * The logic of the command for a single {@link SearchPattern} is done here.
     *
     * @param searchPattern the search pattern
     */
    public abstract void execute(final SearchPattern searchPattern);

    /**
     * The logic of the command for a list of {@link SearchPattern}s is done here.
     *
     * @param searchPatterns the search patterns
     */
    public abstract void execute(final Set<SearchPattern> searchPatterns);
}
