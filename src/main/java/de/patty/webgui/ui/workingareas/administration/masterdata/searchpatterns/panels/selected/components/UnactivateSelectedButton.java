package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.selected.components;

import com.vaadin.ui.Button;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.logic.SearchPatternActivateOrUnactivateCommand;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.model.UnActivateEnum;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.uicomponents.SearchPatternsTable;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/22/13
 * Time: 12:17 PM
 * Button which executes a command to unactivate the selected {@link de.patty.persistence.entities.SearchPattern}s in the
 * {@link SearchPatternsTable}.
 */
public class UnactivateSelectedButton extends SelectedButton {

    public UnactivateSelectedButton(final SearchPatternsTable table, final MainPattyUI ui) {
        super(table, ui);
        setCaption(messages.getString("unactivateselected"));
    }

    @Override
    protected void addClickListener() {
        addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                final SearchPatternActivateOrUnactivateCommand activateOrUnactivateCommand = new SearchPatternActivateOrUnactivateCommand(ui) {
                    @Override
                    public void setCommandType() {
                        unActivateEnum = UnActivateEnum.UNACTIVATE;
                    }
                };
                activateOrUnactivateCommand.execute(table.getSelectedSearchPatterns());
            }
        });
    }
}
