package de.patty.webgui.ui.workingareas.administration.recrawling.uicomponents.cronjob.edit;

import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;
import com.vaadin.ui.ProgressIndicator;
import com.vaadin.ui.TabSheet;
import de.patty.rmi.DoRMILogicAndReloadScreenRunnable;
import de.patty.rmi.managers.CronjobManager;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.RapidPanel;
import de.patty.webgui.ui.workingareas.administration.recrawling.logic.CronjobSplitter;
import de.patty.webgui.ui.workingareas.administration.recrawling.uicomponents.cronjob.edit.tabs.*;
import it.sauronsoftware.cron4j.SchedulingPattern;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/9/13
 * Time: 12:08 PM
 *
 * The panel for editing a cronjob. The different tabs are included in here.
 */
public class EditCronjobPanel extends RapidPanel {

    private MainPattyUI ui;
    private ResourceBundle messages;
    private TabSheet tabSheet;
    private Button saveButton;
    private ProgressIndicator progressIndicator;

    /**
     * Instantiates a new EditCronjobPanel.
     *
     * @param ui the ui
     */
    public EditCronjobPanel(final MainPattyUI ui) {
        this.ui = ui;
        messages = ui.getResourceBundle();
        setCaption(messages.getString("editcronjob"));
        setSizeFull();
        getContentLayout().setWidth("100%");
        tabSheet = new TabSheet();
        tabSheet.setWidth("100%");
        fillTabSheet();
        progressIndicator = new ProgressIndicator();
        progressIndicator.setEnabled(true);
        progressIndicator.setIndeterminate(true);
        progressIndicator.setVisible(false);
        saveButton = new Button(messages.getString("save"));
        saveButton.setStyleName("link");
        saveButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                final String enteredCronjobPattern = ((PatternReturner) tabSheet.getSelectedTab()).returnPattern();
                final boolean cronjobPatternCorrect = SchedulingPattern.validate(enteredCronjobPattern);
                if (cronjobPatternCorrect) {
                    final Thread thread = new Thread(new DoRMILogicAndReloadScreenRunnable(ui, "error_changecronjob") {
                        @Override
                        public boolean doRMI() {
                            final CronjobManager cronjobManager = new CronjobManager();
                            final String oldCronjob = cronjobManager.getCronjobLine();
                            final CronjobSplitter cronjobSplitter = new CronjobSplitter(oldCronjob);
                            final String newCronjobLine = enteredCronjobPattern + " " + cronjobSplitter.getCronjobCommand();
                            final boolean success = cronjobManager.setCronjob(newCronjobLine);
                            return success;
                        }
                    });
                    thread.start();
                    progressIndicator.setVisible(true);
                    tabSheet.setEnabled(false);
                    saveButton.setEnabled(false);
                } else {
                    Notification.show(messages.getString("error_cronjobincorrect"));
                }
            }
        });
        addComponent(progressIndicator);
        addComponent(tabSheet);
        addComponent(saveButton);
        getContentLayout().setExpandRatio(tabSheet, 1.0f);
    }

    private void fillTabSheet() {
        tabSheet.addTab(new TimesTab(ui), messages.getString("daytab"));
        tabSheet.addTab(new WeeklyTimesTab(ui), messages.getString("weektab"));
        tabSheet.addTab(new MonthlyTimesTab(ui), messages.getString("monthtab"));
        tabSheet.addTab(new AdvancedTab(ui), messages.getString("advanced"));
    }
}
