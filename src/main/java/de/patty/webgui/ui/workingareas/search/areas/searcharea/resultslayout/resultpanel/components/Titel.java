package de.patty.webgui.ui.workingareas.search.areas.searcharea.resultslayout.resultpanel.components;

import com.vaadin.server.BrowserWindowOpener;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Label;
import de.patty.webgui.ui.workingareas.search.model.Result;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/4/13
 * Time: 11:56 AM
 *
 */
public class Titel extends Label {

    private BrowserWindowOpener browserWindowOpener;

    public Titel(final Result result) {
        super(result.getTitle());
        System.out.println("title: " + result.getTitle());
        setStyleName("titellabel");
        setContentMode(ContentMode.HTML);
        final String urlWithHighlighting = result.getUrl();
        final String urlWithoutHighlighting = urlWithHighlighting.replaceAll("(<strong>)|(</strong>)", "");
        browserWindowOpener = new BrowserWindowOpener(urlWithoutHighlighting);
        browserWindowOpener.extend(this);
    }
}
