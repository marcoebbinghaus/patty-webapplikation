package de.patty.webgui.ui.workingareas.search.logic;

import com.vaadin.ui.VerticalLayout;
import de.patty.persistence.entities.SearchPattern;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.search.areas.searcharea.resultslayout.ResultsLayout;
import de.patty.webgui.ui.workingareas.search.areas.searcharea.resultslayout.infolayout.SearchInfoLayout;
import de.patty.webgui.ui.workingareas.search.areas.searcharea.resultslayout.resultpanel.ResultPanel;

import java.util.List;
import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/4/13
 * Time: 2:51 PM
 *
 */
public class SearchExecutorForVaadinUI extends SearchExecutor {

    private ResultsLayout resultsLayout;
    private ResultPanelsCreator resultPanelsCreator;
    private ResourceBundle messages;

    public SearchExecutorForVaadinUI(final MainPattyUI ui, final String searchInput, final SearchPattern searchPattern,
                                     final Boolean queryLanguage, final VerticalLayout resultsLayout) {
        super(ui, searchInput, searchPattern, queryLanguage);
        messages = ui.getResourceBundle();
        this.resultsLayout = (ResultsLayout) resultsLayout;
        resultPanelsCreator = new ResultPanelsCreator(results);
    }

    @Override
    public void processResults() {
        final List<ResultPanel> resultpanelList = resultPanelsCreator.getResultPanels();
        // if(!resultpanelList.isEmpty()){
        resultsLayout.removeAllComponents();
        resultsLayout.setInfoLayout(new SearchInfoLayout(ui, getResultsCreator().getFinalSearchstring(), searchPattern, resultpanelList));
        resultsLayout.setResultpanelList(resultpanelList);
        resultsLayout.setResultsArea(new VerticalLayout());
        resultsLayout.buildPagingComponent();
        resultsLayout.setComponents();
        //    }
    }
}
