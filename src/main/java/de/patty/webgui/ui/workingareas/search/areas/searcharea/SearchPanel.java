package de.patty.webgui.ui.workingareas.search.areas.searcharea;

import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.RapidPanel;
import de.patty.webgui.ui.workingareas.Componentssetable;
import de.patty.webgui.ui.workingareas.Internationalizationable;
import de.patty.webgui.ui.workingareas.search.areas.searcharea.resultslayout.ResultsLayout;
import de.patty.webgui.ui.workingareas.search.areas.searcharea.searchbar.SearchBar;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/4/13
 * Time: 12:28 PM
 *
 */
public class SearchPanel extends RapidPanel implements Componentssetable, Internationalizationable {

    private SearchBar searchBar;
    private ResultsLayout resultsLayout;
    private ResourceBundle messagesBundle;

    public SearchPanel(final MainPattyUI ui) {
        messagesBundle = ui.getResourceBundle();
        searchBar = new SearchBar(ui);
        resultsLayout = new ResultsLayout(ui, searchBar);
        setComponents();
        doInternationalization();
    }

    @Override
    public void setComponents() {
        addComponent(searchBar);
        addComponent(resultsLayout);
    }

    @Override
    public void doInternationalization() {
        //do nothing
    }

    public SearchBar getSearchBar() {
        return searchBar;
    }

    public ResultsLayout getResultsLayout() {
        return resultsLayout;
    }
}
