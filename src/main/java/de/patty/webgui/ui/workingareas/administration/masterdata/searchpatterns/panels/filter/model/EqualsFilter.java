package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.model;

import com.vaadin.data.Container;
import com.vaadin.data.util.BeanItemContainer;
import de.patty.persistence.entities.SearchPattern;

import java.util.HashSet;
import java.util.Set;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/22/13
 * Time: 3:25 PM
 *
 */
public class EqualsFilter extends Filter {


    public EqualsFilter(final String searchPatternProperty, final String content) {
        super(searchPatternProperty, content, FilterOperators.EQUALS);
    }

    @Override
    public void applyFilterToContainer(final Container container) {
        final Set<SearchPattern> searchPatternsToRemove = new HashSet<>();
        final BeanItemContainer<SearchPattern> searchPatternBeanItemContainer = (BeanItemContainer<SearchPattern>) container;
        for (final SearchPattern searchPattern : searchPatternBeanItemContainer.getItemIds()) {
            switch (searchPatternProperty) {
                case SearchPattern.ID:
                    if (searchPattern.getId() != Long.valueOf(content)) {
                        searchPatternsToRemove.add(searchPattern);
                    }
                    break;
                case SearchPattern.TITLE:
                    if (!searchPattern.getTitle().equals(content)) {
                        searchPatternsToRemove.add(searchPattern);
                    }
                    break;
                case SearchPattern.DESCR:
                    if (!searchPattern.getDescription().equals(content)) {
                        searchPatternsToRemove.add(searchPattern);
                    }
                    break;
                case SearchPattern.CONTENT:
                    if (!searchPattern.getContent().equals(content)) {
                        searchPatternsToRemove.add(searchPattern);
                    }
                    break;
                case SearchPattern.PARENT:
                    break;
                case SearchPattern.AUTHOR:
                    if (!searchPattern.getBenutzer().getLoginname().equals(content)) {
                        searchPatternsToRemove.add(searchPattern);
                    }
                    break;
                case SearchPattern.ACTIVATED:
                    if (!searchPattern.getActivated().toString().equals(content)) {
                        searchPatternsToRemove.add(searchPattern);
                    }
                    break;
                default:
                    break;
            }
        }
        for (final SearchPattern searchPattern : searchPatternsToRemove) {
            container.removeItem(searchPattern);
        }
    }


}
