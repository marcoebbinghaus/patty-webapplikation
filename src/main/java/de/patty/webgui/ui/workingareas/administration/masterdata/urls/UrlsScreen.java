package de.patty.webgui.ui.workingareas.administration.masterdata.urls;

import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.*;
import de.patty.etc.Constants;
import de.patty.rmi.DoRMILogicAndReloadScreenRunnable;
import de.patty.rmi.managers.URLManager;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.ConfirmDialog;
import de.patty.webgui.ui.workingareas.FilterField;
import de.patty.webgui.ui.workingareas.ReloadScreenButton;
import de.patty.webgui.ui.workingareas.Screen;

import java.util.*;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 5/15/13
 * Time: 10:44 AM
 *
 * Screen where the administrator can see all URLs that are recognized (crawled) by the crawler. The existing URLs may
 * be removed and new URLs may be added.
 */
public class UrlsScreen extends Screen {

    private ResourceBundle messages;
    private boolean connectionToCrawlerSystem;
    private HorizontalLayout crudButtonLeiste;
    private FilterField filterField;
    private ListSelect urlsSelect;
    private Button addURLsButton;
    private Button deleteURLsButton;
    private ProgressIndicator progressIndicator;
    private MainPattyUI ui;


    /**
     * Instantiates a new UrlsScreen.
     *
     * @param ui the ui
     */
    public UrlsScreen(final MainPattyUI ui) {
        super(ui);
        connectionToCrawlerSystem = true;
        activeVerticalFullScreenSize(true);
        try {
            this.ui = ui;
            progressIndicator = new ProgressIndicator();
            progressIndicator.setIndeterminate(true);
            progressIndicator.setEnabled(false);
            progressIndicator.setVisible(false);
            messages = ui.getResourceBundle();
            final URLManager urlManager = new URLManager();
            crudButtonLeiste = new HorizontalLayout();
            addURLsButton = new Button();
            addURLsButton.setIcon(new ThemeResource(Constants.ICON_ADD));
            deleteURLsButton = new Button();
            deleteURLsButton.setIcon(new ThemeResource(Constants.ICON_DELETE));
            deleteURLsButton.setEnabled(false);
            deleteURLsButton.addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent clickEvent) {
                    ui.addWindow(new ConfirmDialog(messages.getString("confirmdeleteurls"), ui) {
                        @Override
                        public void doThisOnOK() {
                            final Thread thread = new Thread(new DoRMILogicAndReloadScreenRunnable(UrlsScreen.this.ui, "error_deleteurls") {
                                @Override
                                public boolean doRMI() {
                                    final List<String> urlsToRemove = new ArrayList<>((Set<String>) urlsSelect.getValue());
                                    final boolean success = urlManager.removeURLs(urlsToRemove);
                                    return success;
                                }
                            });
                            thread.start();
                            progressIndicator.setEnabled(true);
                            progressIndicator.setVisible(true);
                            deleteURLsButton.setEnabled(false);
                            addURLsButton.setEnabled(false);
                        }

                        @Override
                        public void doThisOnCancel() {
                            close();
                        }

                        @Override
                        public void setComponents() {
                            //nothing to do
                        }

                        @Override
                        public void doInternationalization() {
                            //nothing to do
                        }
                    });


                }
            });
            final List<String> urls = urlManager.getURLs();
            Collections.sort(urls, String.CASE_INSENSITIVE_ORDER);
            final Container urlsContainer = new IndexedContainer(urls);
            filterField = new FilterField(ui) {
                @Override
                public void createContainerAndSetItemCaptionMode() {
                    this.container = urlsContainer;
                }
            };
            urlsSelect = new ListSelect("", urlsContainer);
            urlsSelect.setNullSelectionAllowed(true);
            urlsSelect.setSizeFull();
            urlsSelect.setMultiSelect(true);
            urlsSelect.addValueChangeListener(new Property.ValueChangeListener() {
                @Override
                public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                    final Set<String> value = (Set<String>) valueChangeEvent.getProperty().getValue();
                    System.out.println(value);
                    if (value == null || value.isEmpty()) {
                        deleteURLsButton.setEnabled(false);
                    } else {
                        deleteURLsButton.setEnabled(true);
                    }
                }
            });
            addURLsButton.addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent event) {
                    ui.addWindow(new AddURLsWindow(ui));
                }
            });

            crudButtonLeiste.addComponents(addURLsButton, deleteURLsButton);
            crudButtonLeiste.setSizeUndefined();
        } catch (final NullPointerException e) { //No connection to the crawler system was established.
            connectionToCrawlerSystem = false;
            e.printStackTrace();
        }
        setComponents();
        doInternationalization();

    }

    @Override
    public void reload() {
        ui.setWorkingArea(new UrlsScreen(ui));
    }

    @Override
    public void setComponents() {
        if (connectionToCrawlerSystem) {
            addComponent(progressIndicator);
            addComponent(crudButtonLeiste);
            addComponent(filterField);
            addComponent(urlsSelect);
            getContentLayout().setExpandRatio(urlsSelect, 1.0f);
        } else {
            addComponent(new Label(messagesBundle.getString("reloadwhencrawlerisbackup")));
            final ReloadScreenButton reloadButton = new ReloadScreenButton(ui);
            addComponent(reloadButton);
            getContentLayout().setExpandRatio(reloadButton, 1.0f);
            Notification.show(messages.getString("noconnectiontocrawler"));
        }

    }

    @Override
    public void doInternationalization() {
        if (connectionToCrawlerSystem) {
            filterField.setCaption(messages.getString("fastsearch"));
        }
    }
}
