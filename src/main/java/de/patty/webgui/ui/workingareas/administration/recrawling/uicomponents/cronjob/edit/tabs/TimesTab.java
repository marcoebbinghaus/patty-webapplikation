package de.patty.webgui.ui.workingareas.administration.recrawling.uicomponents.cronjob.edit.tabs;

import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.administration.recrawling.logic.CronjobOptionGroupValueChangeListener;
import de.patty.webgui.ui.workingareas.administration.recrawling.model.enummaps.HoursEnumMap;
import de.patty.webgui.ui.workingareas.administration.recrawling.model.enums.Hours;
import de.patty.webgui.ui.workingareas.administration.recrawling.uicomponents.cronjob.edit.AdvancedOptionGroup;

import java.util.Arrays;
import java.util.EnumMap;

import static de.patty.etc.Constants.MINUTES;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/9/13
 * Time: 1:52 PM
 *
 * Tab for time based cronjobs (hours and minutes).
 */
public class TimesTab extends AbstractTimesTab {
    public static final String HELPSUBLAYOUT_HEIGHT = "200px";
    /**
     * The Minutes field.
     */
    protected TextField minutesField;
    /**
     * The Hours field.
     */
    protected TextField hoursField;

    /**
     * The layout for Option group 1.
     */
    protected VerticalLayout optionGroup1Layout;
    /**
     * The layout for Option group 2.
     */
    protected VerticalLayout optionGroup2Layout;
    /**
     * The layout for Option group 3.
     */
    protected VerticalLayout optionGroup3Layout;
    /**
     * The layout for Option group 4.
     */
    protected VerticalLayout optionGroup4Layout;
    /**
     * The layout for Option group 5.
     */
    protected VerticalLayout optionGroup5Layout;

    /**
     * The Minutes option group.
     */
    protected AdvancedOptionGroup minutesGroup;
    /**
     * The Hours option group.
     */
    protected AdvancedOptionGroup hoursGroup;

    private HoursEnumMap hoursEnumMap;

    /**
     * Instantiates a new TimesTab.
     *
     * @param ui the ui
     */
    public TimesTab(final MainPattyUI ui) {
        super(ui);
        setSpacing(true);
        infoLabel.setValue(messages.getString("infotimestab"));
        hoursEnumMap = new HoursEnumMap(new EnumMap<Hours, Integer>(Hours.class));
        minutesField = new TextField(messages.getString("minutes"));
        minutesField.setWidth("60%");
        hoursField = new TextField(messages.getString("hours"));
        hoursField.setWidth("60%");
        minutesGroup = new AdvancedOptionGroup(ui, "minutes", Arrays.asList(MINUTES));
        hoursGroup = new AdvancedOptionGroup(ui, "hours", Arrays.asList(Hours.values()));
        minutesGroup.addValueChangeListener(new CronjobOptionGroupValueChangeListener<String>(minutesField));
        hoursGroup.addValueChangeListener(new CronjobOptionGroupValueChangeListener<Hours>(hoursField, hoursEnumMap));

        configureHelpLayout();
        configureManualLayout();
        addComponent(manualLayout);
        addComponent(optionGroupsLayout);
        super.setExpandRatio(optionGroupsLayout, 1.0f);
    }

    @Override
    public void configureManualLayout() {
        manualLayout = new FormLayout(minutesField, hoursField);
        manualLayout.setEnabled(false);
    }

    @Override
    public void configureHelpLayout() {
        optionGroupsLayout = new HorizontalLayout();
        optionGroup1Layout = new VerticalLayout(hoursGroup);
        optionGroupsLayout.setSizeFull();
        optionGroup1Layout.setStyleName("decentgreenbg");
        optionGroup1Layout.setWidth("100%");
        optionGroup1Layout.setHeight(HELPSUBLAYOUT_HEIGHT);
        optionGroup2Layout = new VerticalLayout(minutesGroup);
        optionGroup2Layout.setStyleName("decentyellowbg");
        optionGroup2Layout.setWidth("100%");
        optionGroup2Layout.setHeight(HELPSUBLAYOUT_HEIGHT);
        optionGroup3Layout = new VerticalLayout(dummyLabels.get(0));
        optionGroup3Layout.setStyleName("decentgreenbg");
        optionGroup3Layout.setWidth("100%");
        optionGroup3Layout.setHeight(HELPSUBLAYOUT_HEIGHT);
        optionGroup4Layout = new VerticalLayout(dummyLabels.get(1));
        optionGroup4Layout.setStyleName("decentyellowbg");
        optionGroup4Layout.setWidth("100%");
        optionGroup4Layout.setHeight(HELPSUBLAYOUT_HEIGHT);
        optionGroup5Layout = new VerticalLayout(dummyLabels.get(2));
        optionGroup5Layout.setStyleName("decentgreenbg");
        optionGroup5Layout.setWidth("100%");
        optionGroup5Layout.setHeight(HELPSUBLAYOUT_HEIGHT);
        optionGroupsLayout.addComponents(optionGroup1Layout, optionGroup2Layout, optionGroup3Layout, optionGroup4Layout, optionGroup5Layout);
    }


    @Override
    public String returnPattern() {
        final String minutes = minutesField.getValue();
        final String hours = hoursField.getValue();
        final String pattern = minutes + " " + hours + " * * *";
        return pattern;
    }
}
