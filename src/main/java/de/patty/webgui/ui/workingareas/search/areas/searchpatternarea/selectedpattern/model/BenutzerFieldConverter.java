package de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.selectedpattern.model;

import com.vaadin.data.util.converter.Converter;
import de.patty.persistence.entities.Benutzer;
import de.patty.webgui.MainPattyUI;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/6/13
 * Time: 2:12 PM
 *
 */
public class BenutzerFieldConverter implements Converter<String, Benutzer> {

    private ResourceBundle messagesBundle;

    public BenutzerFieldConverter(final MainPattyUI ui) {
        messagesBundle = ui.getResourceBundle();
    }

    @Override
    public Benutzer convertToModel(String s, Locale locale) throws ConversionException {
        final Benutzer benutzer = new Benutzer();
        benutzer.setLoginname(s);
        return benutzer;
    }

    @Override
    public String convertToPresentation(Benutzer benutzer, Locale locale) throws ConversionException {
        return benutzer.getLoginname();
    }

    @Override
    public Class<Benutzer> getModelType() {
        return Benutzer.class;
    }

    @Override
    public Class<String> getPresentationType() {
        return String.class;
    }
}
