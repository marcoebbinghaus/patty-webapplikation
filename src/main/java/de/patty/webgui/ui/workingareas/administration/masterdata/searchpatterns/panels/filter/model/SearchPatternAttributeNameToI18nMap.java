package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.model;

import de.patty.persistence.entities.SearchPattern;
import de.patty.webgui.MainPattyUI;

import java.util.HashMap;
import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/23/13
 * Time: 2:16 PM
 *
 * Map that contains all {@link SearchPattern} properties as keys and their translations as values. The {@link SearchPatternI18nToAttributeNamesMap}
 * is the inverted Map.
 */
public class SearchPatternAttributeNameToI18nMap extends HashMap<String, String> {

    private MainPattyUI ui;
    private ResourceBundle messages;

    /**
     * Instantiates a new SearchPatternAttributeNameToI18nMap.
     *
     * @param ui the ui
     */
    public SearchPatternAttributeNameToI18nMap(final MainPattyUI ui) {
        this.ui = ui;
        this.messages = ui.getResourceBundle();
        put(SearchPattern.ID, messages.getString("sp_id"));
        put(SearchPattern.AUTHOR, messages.getString("sp_author"));
        put(SearchPattern.TITLE, messages.getString("sp_title"));
        put(SearchPattern.CONTENT, messages.getString("sp_content"));
        put(SearchPattern.DESCR, messages.getString("sp_descr"));
        put(SearchPattern.PARENT, messages.getString("sp_parent"));
        put(SearchPattern.ACTIVATED, messages.getString("sp_activated"));
    }
}
