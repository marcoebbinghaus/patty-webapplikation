package de.patty.webgui.ui.workingareas.administration.recrawling.model.enums;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/9/13
 * Time: 4:00 PM
 *
 * Enum for months of a year.
 */
public enum Months {
    JANUARY,
    FEBRUARY,
    MARCH,
    APRIL,
    MAY,
    JUNE,
    JULY,
    AUGUST,
    SEPTEMBER,
    OCTOBER,
    NOVEMBER,
    DECEMBER;

    private static ResourceBundle messages;

    public static void setResourceBundle(final ResourceBundle messages) {
        Months.messages = messages;
    }

    public String toString() {
        return messages.getString(name() + ".i18n");
    }
}