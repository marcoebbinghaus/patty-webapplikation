package de.patty.webgui.ui.workingareas.administration.crawlerstatus.logic.observers;

import com.vaadin.ui.Button;
import de.patty.etc.designpatterns.observer.Observer;
import de.patty.etc.utilities.StopCommandSentSingleton;
import de.patty.rmi.managers.CrawlerManager;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.ConfirmDialog;
import de.patty.webgui.ui.workingareas.administration.crawlerstatus.CrawlerStatusScreen;
import de.patty.webgui.ui.workingareas.administration.crawlerstatus.logic.ThreadListener;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 5/28/13
 * Time: 12:52 PM
 *
 * The button which sends the command to stop the crawler on the crawler system. Is an observer of the ThreadListener
 * because its enabled-state must correspond to the ThreadListeners state.
 */
public class StopCrawlerButton extends Button implements Observer {

    private ResourceBundle messages;
    private ThreadListener threadListener;
    private MainPattyUI ui;

    /**
     * Instantiates a new StopCrawlerButton.
     *
     * @param ui the ui
     * @param threadListener the ThreadListener
     */
    public StopCrawlerButton(final MainPattyUI ui, final ThreadListener threadListener) {
        super();
        this.ui = ui;
        this.messages = ui.getResourceBundle();
        this.threadListener = threadListener;
        setEnabled(false);
        if (StopCommandSentSingleton.getStopCommandSent()) {
            setCaption(messages.getString("manualstopped"));
        } else {
            setCaption(messages.getString("stopcrawler"));
        }
        threadListener.attach(this);
        addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                ui.addWindow(new ConfirmDialog(messages.getString("confirm_stopcrawler"), ui) {
                    @Override
                    public void doThisOnOK() {
                        StopCrawlerButton.this.setCaption(messagesBundle.getString("manualstopped"));
                        StopCrawlerButton.this.setEnabled(false);
                        StopCommandSentSingleton.setStopCommandSent(true);
                        final CrawlerManager crawlerManager = new CrawlerManager();
                        crawlerManager.stopCrawler();
                        close();
                        StopCrawlerButton.this.ui.setWorkingArea(new CrawlerStatusScreen(StopCrawlerButton.this.ui));
                    }

                    @Override
                    public void doThisOnCancel() {
                        close();
                    }

                    @Override
                    public void setComponents() {
                        //nothing to do
                    }

                    @Override
                    public void doInternationalization() {
                        //nothing to do
                    }
                });

            }
        });
    }

    @Override
    public void update() {
        try {
            if (threadListener.isCrawlerRunning()) {
                if (StopCommandSentSingleton.getStopCommandSent()) {
                    setEnabled(false);
                } else {
                    setEnabled(true);
                }
            } else {
                setEnabled(false);
                if (StopCommandSentSingleton.getStopCommandSent()) {
                    StopCommandSentSingleton.setStopCommandSent(false);
                    setCaption(messages.getString("stopcrawler"));
                }
            }
        } catch (final NullPointerException e) {
            setEnabled(false);
        }
    }
}
