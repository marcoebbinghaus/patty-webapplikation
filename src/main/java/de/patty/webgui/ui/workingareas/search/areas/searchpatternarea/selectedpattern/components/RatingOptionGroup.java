package de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.selectedpattern.components;

import com.vaadin.data.Container;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.OptionGroup;
import de.patty.webgui.MainPattyUI;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/8/13
 * Time: 1:40 PM
 *
 */
public class RatingOptionGroup extends OptionGroup {

    private ResourceBundle messages;

    public RatingOptionGroup(final MainPattyUI ui) {
        super();
        final List<Integer> ratingList = new ArrayList<>();
        ratingList.add(5);
        ratingList.add(4);
        ratingList.add(3);
        ratingList.add(2);
        ratingList.add(1);
        ratingList.add(0);
        final Container container = new IndexedContainer(ratingList);
        setContainerDataSource(container);
        setItemCaptionMode(ItemCaptionMode.ICON_ONLY);
        setItemIcon(5, new ThemeResource("images/5stars.png"));
        setItemIcon(4, new ThemeResource("images/4stars.png"));
        setItemIcon(3, new ThemeResource("images/3stars.png"));
        setItemIcon(2, new ThemeResource("images/2stars.png"));
        setItemIcon(1, new ThemeResource("images/1stars.png"));
        setItemIcon(0, new ThemeResource("images/0stars.png"));
    }

}
