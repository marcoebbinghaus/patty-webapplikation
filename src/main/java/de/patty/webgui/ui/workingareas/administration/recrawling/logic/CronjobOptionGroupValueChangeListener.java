package de.patty.webgui.ui.workingareas.administration.recrawling.logic;

import com.vaadin.data.Property;
import com.vaadin.ui.TextField;

import java.util.*;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/11/13
 * Time: 11:05 AM
 *
 * {@link com.vaadin.data.Property.ValueChangeListener} which connects the {@link com.vaadin.ui.OptionGroup} with the
 * corresponding textField (e.g. the "minutes" option group (0-59) with the corresponding "minutes" textfield, so that
 * the selected minutes are written into the text field).
 */
public class CronjobOptionGroupValueChangeListener<T> implements Property.ValueChangeListener {

    private TextField matchingField;
    private EnumMap enumMap;

    /**
     * Instantiates a new CronjobOptionGroupValueChangeListener.
     *
     * @param matchingField the corresponding textField (corresponding to the option group)
     */
    public CronjobOptionGroupValueChangeListener(final TextField matchingField) {
        this.matchingField = matchingField;
    }

    /**
     * Instantiates a new CronjobOptionGroupValueChangeListener.
     *
     * @param matchingField the matching field
     * @param enumMap the enum map of the option group
     */
    public CronjobOptionGroupValueChangeListener(final TextField matchingField, final EnumMap enumMap) {
        this.matchingField = matchingField;
        this.enumMap = enumMap;
    }

    @Override
    public void valueChange(Property.ValueChangeEvent event) {
        final Set<T> selectedT = (Set<T>) event.getProperty().getValue();
        final Iterator<T> iterator = selectedT.iterator();
        final List<Integer> integersForField = new ArrayList<>();
        while (iterator.hasNext()) {
            final T next = iterator.next();
            if (enumMap == null) {
                integersForField.add(Integer.valueOf((String) next));
            } else {
                integersForField.add((Integer) enumMap.get(next));
            }
        }
        Collections.sort(integersForField);
        final StringBuilder fieldValueBuilder = new StringBuilder();
        if (integersForField.size() > 0) {
            for (final Integer integer : integersForField) {
                fieldValueBuilder.append(integer + ",");
            }
            fieldValueBuilder.deleteCharAt(fieldValueBuilder.length() - 1);
        }
        matchingField.setValue(fieldValueBuilder.toString());
    }
}
