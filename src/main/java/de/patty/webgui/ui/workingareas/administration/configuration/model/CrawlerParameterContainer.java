package de.patty.webgui.ui.workingareas.administration.configuration.model;

import com.vaadin.data.util.BeanItemContainer;

import java.util.Collections;
import java.util.List;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/5/13
 * Time: 10:14 AM
 *
 * Container for the {@link CrawlerParameter}s which are displayed in the table in the screen.
 */
public class CrawlerParameterContainer extends BeanItemContainer<CrawlerParameter> {

    /**
     * Instantiates a new CrawlerParameterContainer.
     *
     * @param crawlerParameterList the crawler parameters as list
     */
    public CrawlerParameterContainer(final List<CrawlerParameter> crawlerParameterList) {
        super(CrawlerParameter.class);
        Collections.sort(crawlerParameterList);
        addAll(crawlerParameterList);
    }
}
