package de.patty.webgui.ui.workingareas.search.logic;

import de.patty.persistence.entities.SearchPattern;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/3/13
 * Time: 3:51 PM
 *
 */
public class FinalSearchstringBuilder {

    private Boolean queryLanguageAcitvated;
    private String searchInput;
    private SearchPattern searchPattern;
    private Integer theCase;
    private SearchStringReturner searchStringReturner;

    public FinalSearchstringBuilder(final String searchInput, final SearchPattern searchPattern, final Boolean queryLanguageAcitvated) {
        this.searchInput = searchInput;
        this.searchPattern = searchPattern;
        this.queryLanguageAcitvated = queryLanguageAcitvated;
        calculateCase();
    }

    private void calculateCase() {
        theCase = 0;
        if (queryLanguageAcitvated) {
            theCase += 1;
        }
        if (searchPattern != null) {
            theCase += 2;
        }
    }

    public String getFinalSearchstring() {
        switch (theCase) {
            case 0:
                searchStringReturner = new Case0StringReturner(searchInput);
                break;
            case 1:
                searchStringReturner = new Case1StringReturner(searchInput);
                break;
            case 2:
                searchStringReturner = new Case2StringReturner(searchInput, searchPattern);
                break;
            case 3:
                searchStringReturner = new Case3StringReturner(searchInput, searchPattern);
                break;
            default:
                return null;
        }
        return searchStringReturner.getSearchString();
    }
}
