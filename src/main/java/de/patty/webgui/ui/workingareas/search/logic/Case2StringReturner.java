package de.patty.webgui.ui.workingareas.search.logic;

import de.patty.etc.Constants;
import de.patty.persistence.entities.SearchPattern;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/29/13
 * Time: 12:08 PM
 *
 * <p/>
 * EIN SUCHMUSTER / KEIN SOLR-QUERY
 */
public class Case2StringReturner extends SearchStringReturner {

    public Case2StringReturner(final String searchInput, final SearchPattern searchPattern) {
        if (searchInput.equals("")) {
            finalSearchString = createPatternContentWithoutUserInput(searchPattern);
        } else {
            finalSearchString = linkPatternContentAndUserInput(searchInput, searchPattern);
        }
    }

    private String linkPatternContentAndUserInput(final String searchInput, final SearchPattern searchPattern) {
        final String[] searchTerms = searchInput.split("\\s");
        if (searchPattern.getContent().contains(Constants.PATTYSEARCH_INPUT_CONSTANT)) {
            final String solrQueryString = transformEnteredSearchTermsToSolrQuery(searchTerms, false);
            final String newSearchPatternContent = searchPattern.getContent().replaceAll(Constants.PATTYSEARCH_INPUT_CONSTANT, solrQueryString.toString());
            return newSearchPatternContent;
        } else {
            final String solrQueryString = transformEnteredSearchTermsToSolrQuery(searchTerms, true);
            return appendSolrQueryStringToSearchPatternContentWithoutInputVariables(searchPattern.getContent(), solrQueryString);
        }
    }

    private String transformEnteredSearchTermsToSolrQuery(final String[] searchTerms, final boolean withContentTag) {
        final StringBuilder inputStringBuilder = new StringBuilder();
        if (withContentTag) {
            inputStringBuilder.append(CONTENT);
        }
        inputStringBuilder.append("(");
        for (final String searchTerm : searchTerms) {
            inputStringBuilder.append(searchTerm + AND);
        }
        final StringBuilder finalInputStringBuilder = new StringBuilder(inputStringBuilder.substring(0, inputStringBuilder.length() - AND.length()));
        finalInputStringBuilder.append(")");
        return finalInputStringBuilder.toString();
    }


}
