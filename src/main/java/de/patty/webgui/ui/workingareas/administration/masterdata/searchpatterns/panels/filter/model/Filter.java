package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.model;

import com.vaadin.data.Container;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/22/13
 * Time: 3:23 PM
 *
 * Abstract class for the different {@link de.patty.persistence.entities.SearchPattern} filters (e.g. {@link EqualsFilter}).
 * A Filter contains a property from the SearchPattern, a value for that property and the operator (equals, greater than, ...).
 */
public abstract class Filter {

    /**
     * The SearchPattern's property.
     */
    protected String searchPatternProperty;
    /**
     * The value of the SearchPattern's property
     */
    protected String content;
    /**
     * The filter's operator.
     */
    protected FilterOperators filterOperator;

    /**
     * Instantiates a new Filter.
     *
     * @param searchPatternProperty The SearchPattern's property
     * @param content The value of the SearchPattern's property
     * @param filterOperator The filter's operator
     */
    public Filter(final String searchPatternProperty, final String content, final FilterOperators filterOperator) {
        this.searchPatternProperty = searchPatternProperty;
        this.content = content;
        this.filterOperator = filterOperator;
    }

    /**
     * Applies the filter to a container of SearchPatterns (filters out the matching SearchPatterns).
     *
     * @param container the container
     */
    public abstract void applyFilterToContainer(final Container container);

    /* Getters and Setters */

    public String getSearchPatternProperty() {
        return searchPatternProperty;
    }

    public String getContent() {
        return content;
    }

    public FilterOperators getFilterOperator() {
        return filterOperator;
    }
}
