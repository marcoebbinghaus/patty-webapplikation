package de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles.panels.uicomponents;

import com.vaadin.ui.Button;
import de.patty.etc.designpatterns.observer.Observer;
import de.patty.etc.designpatterns.observer.Subject;
import de.patty.persistence.pojos.LogFile;
import de.patty.webgui.ui.workingareas.administration.logfiles.logic.LogFileCommand;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/25/13
 * Time: 2:20 PM
 *
 * Button that is enabled if a log file is selected in the {@link de.patty.webgui.ui.workingareas.administration.logfiles.LogfileScreen}
 * or disabled otherwise.
 */
public class LogFileDependentButton extends Button implements Observer {

    private Subject subject;
    private LogFile selectedLogFile;

    /**
     * Instantiates a new LogFileDependentButton.
     *
     * @param subject the ValueChangeListener of the {@link com.vaadin.ui.ListSelect} for the log files.
     * @param command the command which is executed when the button is clicked
     * @param caption the button's caption
     */
    public LogFileDependentButton(final Subject subject, final LogFileCommand command, final String caption) {
        setEnabled(false);
        setCaption(caption);
        setStyleName("link");
        this.subject = subject;
        subject.attach(this);
        addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                command.execute(selectedLogFile);
            }
        });
    }

    @Override
    public void update() {
        final LogFilesListSelectValueChangeListener listener = (LogFilesListSelectValueChangeListener) subject;
        selectedLogFile = listener.getSelectedLogFile();
        if (selectedLogFile != null) {
            setEnabled(true);
        } else {
            setEnabled(false);
        }
    }
}
