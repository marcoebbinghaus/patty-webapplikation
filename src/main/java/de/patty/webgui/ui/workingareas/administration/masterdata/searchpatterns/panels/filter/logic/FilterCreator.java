package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.logic;

import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.model.EqualsFilter;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.model.Filter;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.model.FilterOperators;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.model.SearchPatternI18nToAttributeNamesMap;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.ui.FilterInputMask;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/23/13
 * Time: 12:41 PM
 *
 * Contains the logic which returns the right filter for the given input from the
 * {@link de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.ui.NewFilterWindow}.
 * By now only {@link EqualsFilter}s are supported.
 */
public class FilterCreator {

    private SearchPatternI18nToAttributeNamesMap map;

    public FilterCreator(final MainPattyUI ui) {
        map = new SearchPatternI18nToAttributeNamesMap(ui);
    }

    public Filter getFilter(final FilterInputMask filterInputMask) {
        Filter filter = null;
        final String columnBoxValue = filterInputMask.getColumnBox().getValue().toString();
        final FilterOperators filterOperator = (FilterOperators) filterInputMask.getOperatorBox().getValue();
        final String filterValue = filterInputMask.getContentField().getValue();
        switch (filterOperator) {
            case EQUALS:
                filter = new EqualsFilter(map.get(columnBoxValue), filterValue);
                break;
            case LESS:
                break;
            case NOT_EQUALS:
                break;
            case GREATER:
                break;
            case IS_NOT_NULL:
                break;
            case IS_NULL:
                break;
            default:
                break;
        }
        return filter;
    }
}
