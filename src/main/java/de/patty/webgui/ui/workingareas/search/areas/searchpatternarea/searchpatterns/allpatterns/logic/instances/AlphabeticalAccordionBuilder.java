package de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.searchpatterns.allpatterns.logic.instances;

import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.Accordion;
import com.vaadin.ui.ListSelect;
import de.patty.persistence.entities.SearchPattern;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.SearchPatternPanel;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.searchpatterns.allpatterns.logic.AllSearchPatternsBuilder;

import java.util.*;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/6/13
 * Time: 10:07 AM
 *
 */
public class AlphabeticalAccordionBuilder extends AllSearchPatternsBuilder {

    private Map<String, List<SearchPattern>> lettersPatternsMap;
    private Accordion accordion;

    public AlphabeticalAccordionBuilder(final List<SearchPattern> searchPatterns, final SearchPatternPanel searchPatternPanel) {
        super(searchPatterns, searchPatternPanel);
    }

    @Override
    protected void buildSpecificDisplayComponent() {
        accordion = new Accordion();
        accordion.setSizeFull();
        Collections.sort(searchPatterns);
        lettersPatternsMap = new TreeMap<>();
        for (final SearchPattern searchPattern : searchPatterns) {
            final String searchPatternTitle = searchPattern.getTitle();
            final String beginningLetter = searchPatternTitle.substring(0, 1).toUpperCase();
            if (!lettersPatternsMap.containsKey(beginningLetter)) {
                lettersPatternsMap.put(beginningLetter, new ArrayList<SearchPattern>());
            }
            lettersPatternsMap.get(beginningLetter).add(searchPattern);
        }
        for (final Map.Entry<String, List<SearchPattern>> stringListEntry : lettersPatternsMap.entrySet()) {
            final BeanItemContainer<SearchPattern> beanItemContainer = new BeanItemContainer<SearchPattern>(SearchPattern.class);
            beanItemContainer.addAll(stringListEntry.getValue());
            final ListSelect listSelect = new ListSelect(stringListEntry.getKey(), beanItemContainer);
            listSelect.setWidth("100%");
            listSelect.setSizeFull();
            listSelect.setNullSelectionAllowed(true);
            listSelect.setImmediate(true);
            listSelect.setMultiSelect(false);
            listSelect.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
            listSelect.setItemCaptionPropertyId(SearchPattern.TITLE);
            listSelect.addValueChangeListener(new Property.ValueChangeListener() {
                @Override
                public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                    final SearchPattern selectedSearchPattern = (SearchPattern) listSelect.getValue();
                    searchPatternPanel.setSelectedSearchPattern(selectedSearchPattern);
                    searchPatternPanel.getButtonLeiste().reload(selectedSearchPattern);
                }
            });
            accordion.addTab(listSelect, stringListEntry.getKey());
        }
        displayComponent = accordion;
    }
}
