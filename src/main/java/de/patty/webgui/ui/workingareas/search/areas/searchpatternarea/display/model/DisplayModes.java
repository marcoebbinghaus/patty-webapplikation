package de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.display.model;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/6/13
 * Time: 9:36 AM
 *
 */
public enum DisplayModes {
    TREE,
    ALPHABETICAL_ACCORDION,
    ALPHABETICAL_LIST,
    MOSTPOPULAR;
}
