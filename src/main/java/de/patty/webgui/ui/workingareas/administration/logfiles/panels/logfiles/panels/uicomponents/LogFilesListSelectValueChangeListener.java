package de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles.panels.uicomponents;

import com.vaadin.data.Property;
import de.patty.etc.designpatterns.observer.Subject;
import de.patty.persistence.pojos.LogFile;

import java.util.ArrayList;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/25/13
 * Time: 2:15 PM
 *
 * {@link com.vaadin.data.Property.ValueChangeListener} which is a subject for the observer buttons
 * ("show" and "download" buttons) for the log files.
 */
public class LogFilesListSelectValueChangeListener extends Subject implements Property.ValueChangeListener {

    private LogFile selectedLogFile;

    /**
     * Instantiates a new LogFilesListSelectValueChangeListener.
     */
    public LogFilesListSelectValueChangeListener() {
        observers = new ArrayList<>();
    }

    @Override
    public void valueChange(Property.ValueChangeEvent event) {
        final LogFile logFile = (LogFile) event.getProperty().getValue();
        selectedLogFile = logFile;
        notifyObservers();
    }

    /**
     * Gets the selected log file.
     *
     * @return the selected log file
     */
    public LogFile getSelectedLogFile() {
        return selectedLogFile;
    }
}
