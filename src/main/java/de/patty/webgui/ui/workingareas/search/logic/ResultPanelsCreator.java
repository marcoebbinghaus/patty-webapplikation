package de.patty.webgui.ui.workingareas.search.logic;

import de.patty.webgui.ui.workingareas.search.areas.searcharea.resultslayout.resultpanel.ResultPanel;
import de.patty.webgui.ui.workingareas.search.model.Result;

import java.util.ArrayList;
import java.util.List;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/4/13
 * Time: 2:22 PM
 *
 */
public class ResultPanelsCreator {

    private List<Result> resultList;
    private List<ResultPanel> resultPanels;

    public ResultPanelsCreator(final List<Result> resultList) {
        this.resultList = resultList;
        resultPanels = new ArrayList<>();
    }

    public List<ResultPanel> getResultPanels() {
        if (resultList != null) {
            for (final Result result : resultList) {
                resultPanels.add(new ResultPanel(result));
            }
        }
        return resultPanels;
    }
}
