package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.ui;

import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.SearchPatternsScreen;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.model.Filter;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.model.SearchPatternAttributeNameToI18nMap;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.uicomponents.SearchPatternsTable;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/22/13
 * Time: 2:35 PM
 *
 * Displays an applied filter and serves a button to remove that filter.
 */
public class UIFilterRow extends HorizontalLayout {

    private Label filterContent;
    private Button removeFilterButton;

    private SearchPatternAttributeNameToI18nMap map;

    /**
     * Instantiates a new UIFilterRow.
     *
     * @param filter the filter
     * @param ui the ui
     */
    public UIFilterRow(final Filter filter, final MainPattyUI ui) {
        setSpacing(true);
        map = new SearchPatternAttributeNameToI18nMap(ui);
        final SearchPatternsTable searchPatternsTable = ((SearchPatternsScreen) ui.getScreen()).getSearchPatternsTable();
        final Layout filtersLayout = ((SearchPatternsScreen) ui.getScreen()).getAppliedFiltersLayout();
        filterContent = new Label(map.get(filter.getSearchPatternProperty()) + " " + filter.getFilterOperator() + " \"" + filter.getContent() + "\"");
        removeFilterButton = new Button();
        removeFilterButton.setStyleName("link");
        removeFilterButton.setCaption(ui.getResourceBundle().getString("removefilter"));
        removeFilterButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                searchPatternsTable.removeFilter(filter);
                filtersLayout.removeComponent(UIFilterRow.this);
            }
        });
        addComponent(filterContent);
        addComponent(removeFilterButton);
    }
}
