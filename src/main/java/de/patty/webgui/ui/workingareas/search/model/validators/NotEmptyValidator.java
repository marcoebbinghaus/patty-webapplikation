package de.patty.webgui.ui.workingareas.search.model.validators;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/7/13
 * Time: 1:49 PM
 *
 */
public class NotEmptyValidator extends AbstractValidator {

    public NotEmptyValidator(final ResourceBundle messages) {
        super(messages);
    }

    @Override
    public void validate(Object o) throws InvalidValueException {
        final String value = o.toString();
        if (value.matches("^\\s*$")) {
            throw new InvalidValueException(messagesBundle.getString("invalid_onlywhitespaces"));
        }
    }
}
