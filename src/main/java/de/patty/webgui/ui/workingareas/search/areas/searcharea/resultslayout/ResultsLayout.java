package de.patty.webgui.ui.workingareas.search.areas.searcharea.resultslayout;

import com.vaadin.ui.*;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.Componentssetable;
import de.patty.webgui.ui.workingareas.Internationalizationable;
import de.patty.webgui.ui.workingareas.search.areas.searcharea.resultslayout.infolayout.SearchInfoLayout;
import de.patty.webgui.ui.workingareas.search.areas.searcharea.resultslayout.resultpanel.ResultPanel;
import de.patty.webgui.ui.workingareas.search.areas.searcharea.searchbar.SearchBar;
import org.vaadin.pagingcomponent.ComponentsManager;
import org.vaadin.pagingcomponent.PagingComponent;
import org.vaadin.pagingcomponent.builder.ElementsBuilder;
import org.vaadin.pagingcomponent.button.ButtonPageNavigator;
import org.vaadin.pagingcomponent.customizer.style.StyleCustomizer;
import org.vaadin.pagingcomponent.listener.impl.SimplePagingComponentListener;

import java.util.ArrayList;
import java.util.List;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/4/13
 * Time: 1:36 PM
 *
 */
public class ResultsLayout extends VerticalLayout implements Componentssetable, Internationalizationable {

    private List<ResultPanel> resultpanelList;
    private PagingComponent<ResultPanel> pagingComponent;
    private VerticalLayout resultsArea;
    private SearchInfoLayout infoLayout;
    private SearchBar searchBar;

    public ResultsLayout(final MainPattyUI ui, final SearchBar searchBar) {
        this.searchBar = searchBar;
        resultsArea = new VerticalLayout();
        resultpanelList = new ArrayList<>();
        buildPagingComponent();
        setComponents();
        doInternationalization();
    }

    @Override
    public void setComponents() {
        if (infoLayout != null) {
            addComponent(infoLayout);
        }
        addComponent(resultsArea);
        addComponent(pagingComponent);
    }

    @Override
    public void doInternationalization() {
        //do nothing
    }

    public List<ResultPanel> getResultpanelList() {
        return resultpanelList;
    }

    public void setResultpanelList(List<ResultPanel> resultpanelList) {
        this.resultpanelList = resultpanelList;
    }

    public PagingComponent<ResultPanel> getPagingComponent() {
        return pagingComponent;
    }

    public void setPagingComponent(PagingComponent<ResultPanel> pagingComponent) {
        this.pagingComponent = pagingComponent;
    }

    public VerticalLayout getResultsArea() {
        return resultsArea;
    }

    public void setResultsArea(VerticalLayout resultsArea) {
        this.resultsArea = resultsArea;
    }


    public void buildPagingComponent() {
        final StyleCustomizer styler = new StyleCustomizer() {

            @Override
            public void styleButtonPageNormal(ButtonPageNavigator button, int pageNumber) {
                button.setPage(pageNumber);
                button.removeStyleName("styleRed");
            }

            @Override
            public void styleButtonPageCurrentPage(ButtonPageNavigator button, int pageNumber) {
                button.setHtmlContentAllowed(true);
                button.setPage(pageNumber, "[ <strong>" + pageNumber + "</strong> ]"); // Set caption of the button with the page number between brackets.
                button.addStyleName("redbg");
                button.focus();
            }

            @Override
            public void styleTheOthersElements(ComponentsManager manager, ElementsBuilder builder) {
                // if the number of pages is less than 2, the other buttons are not created.
                if (manager.getNumberTotalOfPages() < 2) {
                    return;
                }
                // Allow to hide these buttons when the first page is selected
                boolean visible = !manager.isFirstPage();
                builder.getButtonFirst().setVisible(visible);
                builder.getButtonPrevious().setVisible(visible);
                builder.getFirstSeparator().setVisible(visible);
                // Allow to hide these buttons when the last page is selected
                visible = !manager.isLastPage();
                builder.getButtonLast().setVisible(visible);
                builder.getButtonNext().setVisible(visible);
                builder.getLastSeparator().setVisible(visible);
            }

        };
        pagingComponent = new PagingComponent<ResultPanel>(10, 10, resultpanelList, styler, new SimplePagingComponentListener<ResultPanel>(resultsArea) {

            @Override
            protected Component displayItem(int i, ResultPanel resultPanel) {
                return resultPanel;
            }
        }) {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                super.buttonClick(event);
                searchBar.getSearchField().focus();
            }
        };
    }

    public SearchInfoLayout getInfoLayout() {
        return infoLayout;
    }

    public void setInfoLayout(SearchInfoLayout infoLayout) {
        this.infoLayout = infoLayout;
    }


}
