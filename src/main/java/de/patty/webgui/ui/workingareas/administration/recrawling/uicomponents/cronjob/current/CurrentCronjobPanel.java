package de.patty.webgui.ui.workingareas.administration.recrawling.uicomponents.cronjob.current;

import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import de.patty.etc.Constants;
import de.patty.rmi.managers.CronjobManager;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.RapidPanel;
import de.patty.webgui.ui.workingareas.administration.recrawling.logic.CronjobSplitter;
import it.sauronsoftware.cron4j.Predictor;

import java.text.SimpleDateFormat;
import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/9/13
 * Time: 12:07 PM
 *
 * The panel in the {@link de.patty.webgui.ui.workingareas.administration.recrawling.RecrawlingScreen} where the user
 * can see the current cronjob and the next execution time.
 */
public class CurrentCronjobPanel extends RapidPanel {

    private TextField currentCronjobPatternField;
    private TextField nextExecutionTimeField;
    private CronjobManager cronjobManager;
    private String cronjobLine;
    private CronjobSplitter cronjobSplitter;
    private MainPattyUI ui;
    private ResourceBundle messages;
    private FormLayout fieldsLayout;

    /**
     * Instantiates a new CurrentCronjobPanel.
     *
     * @param ui the ui
     */
    public CurrentCronjobPanel(final MainPattyUI ui) {
        this.ui = ui;
        messages = ui.getResourceBundle();
        setCaption(messages.getString("currentconfiguration"));
        cronjobManager = new CronjobManager();
        cronjobLine = cronjobManager.getCronjobLine();
        cronjobSplitter = new CronjobSplitter(cronjobLine);
        fillCurrentCronjobField();
        fillNextExecutionTimeField();
        fieldsLayout = new FormLayout(currentCronjobPatternField, nextExecutionTimeField);
        addComponent(fieldsLayout);
    }

    private void fillCurrentCronjobField() {
        currentCronjobPatternField = new TextField(messages.getString("currentcronjobpattern"));
        currentCronjobPatternField.setValue(cronjobSplitter.getCronjobPattern());
        currentCronjobPatternField.setWidth("100%");
        currentCronjobPatternField.setReadOnly(true);
    }

    private void fillNextExecutionTimeField() {
        final Predictor predictor = new Predictor(cronjobSplitter.getCronjobPattern());
        final SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT);
        nextExecutionTimeField = new TextField(messages.getString("nextexecutiontime"));
        nextExecutionTimeField.setValue(format.format(predictor.nextMatchingDate()));
        nextExecutionTimeField.setWidth("100%");
        nextExecutionTimeField.setReadOnly(true);
    }
}
