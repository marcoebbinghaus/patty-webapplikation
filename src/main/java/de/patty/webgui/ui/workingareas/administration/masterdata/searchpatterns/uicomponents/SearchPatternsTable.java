package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.uicomponents;

import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Table;
import de.patty.etc.designpatterns.observer.Observer;
import de.patty.persistence.entities.SearchPattern;
import de.patty.persistence.entitymanagers.SearchPatternManager;
import de.patty.webgui.MainPattyUI;

import java.util.*;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/17/13
 * Time: 1:01 PM
 *
 * The Table which contains all SearchPatterns (maybe filtered). An additional column is added to execute a
 * {@link de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.logic.SearchPatternCommand}
 * on the selected SearchPattern.
 */
public class SearchPatternsTable extends Table {

    private static final String CHECKBOX_COLUMN = "checkboxColumn";
    private MainPattyUI ui;
    private Container container;
    private ResourceBundle messages;
    private Set<SearchPattern> selectedSearchPatterns;
    private Set<CheckBox> generatedCheckBoxes;
    private List<Observer> observers;
    private List<de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.model.Filter> filters;

    public SearchPatternsTable(final MainPattyUI ui) {
        observers = new ArrayList<>();
        generatedCheckBoxes = new HashSet<>();
        this.ui = ui;
        filters = new ArrayList<>();
        messages = ui.getResourceBundle();
        setSizeFull();
        selectedSearchPatterns = new HashSet<>();
        addActionColumn();
        addCheckBoxColumn();
        rebuildContainer();
        //((BeanItemContainer<SearchPattern>)container).addContainerFilter(new SimpleStringFilter());

        setPageLength(0);
        setSelectable(true);
        setCellStyleGenerator(new CellStyleGenerator() {
            @Override
            public String getStyle(Table source, Object itemId, Object propertyId) {
                if (((SearchPattern) itemId).getActivated() == false) {
                    return "decentyellowbg";
                }
                return null;
            }
        });
    }

    private void rebuildContainer() {
        final SearchPatternManager searchPatternManager = new SearchPatternManager();
        final List<SearchPattern> searchPatternList = searchPatternManager.getAllSearchPatterns();
        Collections.sort(searchPatternList);
        container = new BeanItemContainer<SearchPattern>(SearchPattern.class, searchPatternList);
        setContainerDataSource(container);
        setVisibleColumns(new String[]{SearchPattern.TITLE, SearchPattern.CONTENT, SearchPattern.DESCR, SearchPattern.AUTHOR,
                SearchPattern.PARENT, SearchPattern.ACTIVATED, messages.getString("action"), CHECKBOX_COLUMN});
        setColumnHeader(SearchPattern.TITLE, messages.getString("sp_title"));
        setColumnHeader(SearchPattern.CONTENT, messages.getString("sp_content"));
        setColumnHeader(SearchPattern.DESCR, messages.getString("sp_descr"));
        setColumnHeader(SearchPattern.PARENT, messages.getString("sp_parent"));
        setColumnHeader(SearchPattern.AUTHOR, messages.getString("sp_author"));
        setColumnHeader(SearchPattern.ACTIVATED, messages.getString("sp_activated"));
        setColumnHeader(CHECKBOX_COLUMN, "");
        setColumnExpandRatio(SearchPattern.CONTENT, 1.0f);
    }

    private void addActionColumn() {
        addGeneratedColumn(messages.getString("action"), new ColumnGenerator() {
            @Override
            public Object generateCell(Table source, Object itemId, Object columnId) {
                final ComboBox actionComboBox = new SearchPatternActionsComboBox(ui, (SearchPattern) itemId);
                return actionComboBox;
            }
        });
    }

    private void addCheckBoxColumn() {
        addGeneratedColumn(CHECKBOX_COLUMN, new ColumnGenerator() {
            @Override
            public Object generateCell(Table source, Object itemId, Object columnId) {
                final CheckBox checkBox = new CheckBox();
                checkBox.setData(itemId);
                checkBox.setImmediate(true);
                checkBox.addValueChangeListener(new ValueChangeListener() {
                    @Override
                    public void valueChange(Property.ValueChangeEvent event) {
                        final Boolean checked = (Boolean) event.getProperty().getValue();
                        final SearchPattern searchPattern = (SearchPattern) checkBox.getData();
                        if (checked) {
                            selectedSearchPatterns.add(searchPattern);
                        } else {
                            selectedSearchPatterns.remove(searchPattern);
                        }
                        notifyObservers();
                    }
                });
                generatedCheckBoxes.add(checkBox);
                return checkBox;
            }
        });
    }

    private void applyFilters() {
        rebuildContainer();
        for (final de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.model.Filter filter : filters) {
            filter.applyFilterToContainer(container);
        }
    }

    public void addFilter(final de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.model.Filter filter) {
        filters.add(filter);
        applyFilters();
    }

    public void removeFilter(final de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.model.Filter filter) {
        filters.remove(filter);
        applyFilters();
    }

    public void removeAllFilters(final de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.model.Filter filter) {
        filters.clear();
        applyFilters();
    }

    public Set<CheckBox> getGeneratedCheckBoxes() {
        return generatedCheckBoxes;
    }

    public void attach(final Observer observer) {
        observers.add(observer);
    }

    public void detach(final Observer observer) {
        observers.remove(observer);
    }

    public void notifyObservers() {
        for (final Observer observer : observers) {
            observer.update();
        }
    }

    public List<Observer> getObservers() {
        return observers;
    }

    public void removeAllObservers() {
        final List<Observer> observersCopy = new ArrayList<>(observers);
        for (final Observer observer : observersCopy) {
            detach(observer);
        }
    }

    public Set<SearchPattern> getSelectedSearchPatterns() {
        return selectedSearchPatterns;
    }
}
