package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.model;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/22/13
 * Time: 11:41 AM
 *
 * Simple enum which contains all possible states for an
 * {@link de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.logic.SearchPatternActivateOrUnactivateCommand}
 */
public enum UnActivateEnum {
    ACTIVATE,
    UNACTIVATE,
    ACTIVATE_OR_UNACTIVATE;
}
