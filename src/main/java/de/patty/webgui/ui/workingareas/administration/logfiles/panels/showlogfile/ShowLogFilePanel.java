package de.patty.webgui.ui.workingareas.administration.logfiles.panels.showlogfile;

import com.vaadin.ui.TextArea;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.RapidPanel;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/25/13
 * Time: 1:21 PM
 *
 * The panel which contains the content of the selected log file (if the "show" button was clicked).
 */
public class ShowLogFilePanel extends RapidPanel {

    private final ResourceBundle messages;
    private MainPattyUI ui;
    private TextArea logFileArea;

    /**
     * Instantiates a new ShowLogFilePanel.
     *
     * @param ui the ui
     */
    public ShowLogFilePanel(final MainPattyUI ui) {
        messages = ui.getResourceBundle();
        setCaption(messages.getString("selectedlogfile"));
        this.ui = ui;
        logFileArea = new TextArea();
        logFileArea.setBuffered(true);
        logFileArea.setSizeFull();
        setSizeFull();
        getContentLayout().setSizeFull();
        addComponent(logFileArea);
    }

    /**
     * Gets the textarea which contains the content of the selected log file.
     *
     * @return the textArea
     */
    public TextArea getLogFileArea() {
        return logFileArea;
    }
}
