package de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.searchpatterns.allpatterns.logic;

import com.vaadin.ui.Component;
import de.patty.persistence.entities.SearchPattern;
import de.patty.persistence.entitymanagers.SearchPatternManager;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.RapidInfoWindow;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.SearchPatternPanel;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.display.model.DisplayModes;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.display.model.SortModes;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.searchpatterns.allpatterns.logic.instances.AlphabeticalAccordionBuilder;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.searchpatterns.allpatterns.logic.instances.AlphabeticalListBuilder;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.searchpatterns.allpatterns.logic.instances.MostPopularListBuilder;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.searchpatterns.allpatterns.logic.instances.TreeBuilder;

import java.util.List;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/6/13
 * Time: 9:52 AM
 *
 */
public class AllSearchPatternsDisplayer {

    private MainPattyUI ui;
    private SearchPatternPanel searchPatternPanel;
    private DisplayModes displayMode;
    private SortModes sortMode;
    private AllSearchPatternsBuilder allSearchPatternsBuilder;

    public AllSearchPatternsDisplayer(final MainPattyUI ui, final SearchPatternPanel searchPatternPanel,
                                      final DisplayModes displayMode, final SortModes sortMode) {

        this.ui = ui;
        this.searchPatternPanel = searchPatternPanel;
        this.displayMode = displayMode;
        this.sortMode = sortMode;
    }

    public void display() {
        final SearchPatternManager searchPatternManager = new SearchPatternManager();
        final List<SearchPattern> searchPatterns = searchPatternManager.getActivatedSearchPatterns();
        switch (displayMode) {
            case TREE:
                allSearchPatternsBuilder = new TreeBuilder(searchPatterns, searchPatternPanel);
                break;
            case ALPHABETICAL_ACCORDION:
                allSearchPatternsBuilder = new AlphabeticalAccordionBuilder(searchPatterns, searchPatternPanel);
                break;
            case ALPHABETICAL_LIST:
                allSearchPatternsBuilder = new AlphabeticalListBuilder(searchPatterns, searchPatternPanel, sortMode);
                break;
            case MOSTPOPULAR:
                allSearchPatternsBuilder = new MostPopularListBuilder(searchPatterns, searchPatternPanel, sortMode);
                break;
            default:
                break;
        }
        if (allSearchPatternsBuilder == null) {
            ui.addWindow(new RapidInfoWindow(ui, "notactive_title", "notactive_message", true));
        } else {
            searchPatternPanel.getAllSearchPatternsLayout().removeAllComponents();
            final Component allSearchPatterns = allSearchPatternsBuilder.buildComponentToDisplay();
            searchPatternPanel.getAllSearchPatternsLayout().addComponent(allSearchPatterns);
        }
    }
}
