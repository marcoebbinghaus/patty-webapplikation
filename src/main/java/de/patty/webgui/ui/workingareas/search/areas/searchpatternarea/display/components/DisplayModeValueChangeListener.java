package de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.display.components;

import com.vaadin.data.Property;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.SearchPatternPanel;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.searchpatterns.allpatterns.logic.AllSearchPatternsDisplayer;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/6/13
 * Time: 12:26 PM
 *
 */
public class DisplayModeValueChangeListener implements Property.ValueChangeListener {

    private final MainPattyUI ui;
    private final SearchPatternPanel searchPatternPanel;
    private final DisplayModeBox displayModeBox;
    private final SortModeBox sortModeBox;
    private AllSearchPatternsDisplayer allSearchPatternsDisplayer;

    public DisplayModeValueChangeListener(final MainPattyUI ui, final SearchPatternPanel searchPatternPanel,
                                          final DisplayModeBox displayModeBox, final SortModeBox sortModeBox) {
        this.ui = ui;
        this.searchPatternPanel = searchPatternPanel;
        this.displayModeBox = displayModeBox;
        this.sortModeBox = sortModeBox;
    }

    @Override
    public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
        allSearchPatternsDisplayer = new AllSearchPatternsDisplayer(ui, searchPatternPanel, displayModeBox.getTheValue(),
                sortModeBox.getTheValue());
        allSearchPatternsDisplayer.display();
    }
}
