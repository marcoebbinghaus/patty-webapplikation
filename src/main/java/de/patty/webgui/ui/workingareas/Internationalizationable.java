package de.patty.webgui.ui.workingareas;

public interface Internationalizationable {
    public void doInternationalization();
}
