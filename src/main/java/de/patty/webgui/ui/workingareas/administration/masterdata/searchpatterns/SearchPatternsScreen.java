package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns;

import com.vaadin.data.Property;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.Screen;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.FilterPanel;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.filter.model.SearchPatternI18nToAttributeNamesMap;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.selected.SelectedSearchPatternsActionPanel;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.uicomponents.SearchPatternsTable;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/17/13
 * Time: 10:22 AM
 *
 * Screen where the adminstrator can manage all existing {@link de.patty.persistence.entities.SearchPattern}s.
 */
public class SearchPatternsScreen extends Screen {

    private CheckBox selectAllCheckBox;
    private Layout panelsLayout;
    private FilterPanel filterPanel;
    private SelectedSearchPatternsActionPanel selectedSearchPatternsActionsPanel;
    private SearchPatternsTable searchPatternsTable;
    private SearchPatternI18nToAttributeNamesMap searchPatternMap;


    /**
     * Instantiates a new SearchPatternsScreen.
     *
     * @param ui the ui
     */
    public SearchPatternsScreen(final MainPattyUI ui) {
        super(ui);
        searchPatternMap = new SearchPatternI18nToAttributeNamesMap(ui);
        activeVerticalFullScreenSize(true);
        selectAllCheckBox = new CheckBox();
        selectAllCheckBox.setImmediate(true);
        selectAllCheckBox.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                final Boolean checked = (Boolean) event.getProperty().getValue();
                for (final CheckBox checkBox : searchPatternsTable.getGeneratedCheckBoxes()) {
                    checkBox.setValue(checked);
                }
            }
        });
        searchPatternsTable = new SearchPatternsTable(ui);
        selectedSearchPatternsActionsPanel = new SelectedSearchPatternsActionPanel(ui, searchPatternsTable);
        filterPanel = new FilterPanel(ui);
        panelsLayout = new HorizontalLayout();
        panelsLayout.setWidth("100%");
        panelsLayout.addComponent(selectedSearchPatternsActionsPanel);
        panelsLayout.addComponent(filterPanel);
        ((HorizontalLayout) panelsLayout).setExpandRatio(filterPanel, 1.0f);
        setComponents();
        doInternationalization();
    }

    @Override
    public void reload() {
        ui.setWorkingArea(new SearchPatternsScreen(ui));
    }

    @Override
    public void setComponents() {
        addComponent(selectAllCheckBox);
        addComponent(panelsLayout);
        addComponent(searchPatternsTable);
        getContentLayout().setExpandRatio(searchPatternsTable, 1.0f);
    }

    @Override
    public void doInternationalization() {
        selectAllCheckBox.setCaption(messagesBundle.getString("selectall"));
    }

    /**
     * Gets applied filters layout.
     *
     * @return the applied filters layout
     */
    public FormLayout getAppliedFiltersLayout() {
        return filterPanel.getAppliedFiltersLayout();
    }

    /**
     * Gets search patterns table.
     *
     * @return the search patterns table
     */
    public SearchPatternsTable getSearchPatternsTable() {
        return searchPatternsTable;
    }
}
