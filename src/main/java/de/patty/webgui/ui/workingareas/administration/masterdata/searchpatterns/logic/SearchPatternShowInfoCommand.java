package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.logic;

import com.vaadin.ui.Window;
import de.patty.persistence.entities.SearchPattern;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.selectedpattern.SelectedSearchPatternPanel;

import java.util.Set;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/19/13
 * Time: 11:28 AM
 *
 * This command is for showing infos on the selected {@link SearchPattern} by opening a window with an
 * {@link SelectedSearchPatternPanel} for the selected {@link SearchPattern}.
 */
public class SearchPatternShowInfoCommand extends SearchPatternCommand {

    /**
     * Instantiates a new SearchPatternShowInfoCommand.
     *
     * @param ui the ui
     */
    public SearchPatternShowInfoCommand(MainPattyUI ui) {
        super(ui);
    }

    @Override
    public void execute(SearchPattern searchPattern) {
        final SelectedSearchPatternPanel selectedSearchPatternPanel = new SelectedSearchPatternPanel(ui, searchPattern);
        final Window window = new Window();
        window.setWidth("500px");
        window.setCaption(ui.getResourceBundle().getString("tooltip_info"));
        window.setContent(selectedSearchPatternPanel);
        window.setModal(true);
        ui.addWindow(window);
    }

    @Override
    public void execute(Set<SearchPattern> searchPatterns) {
        //not (yet) needed
    }
}
