package de.patty.webgui.ui.workingareas.search.areas.searcharea.resultslayout.resultpanel.components;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Label;
import de.patty.webgui.ui.workingareas.search.model.Result;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/4/13
 * Time: 11:57 AM
 *
 */
public class Url extends Label {

    public Url(final Result result) {
        super(result.getUrl());
        setStyleName("urllabel");
        setContentMode(ContentMode.HTML);
    }
}
