package de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.selectedpattern;

import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import de.patty.persistence.entities.SearchPattern;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.RapidPanel;
import de.patty.webgui.ui.workingareas.Componentssetable;
import de.patty.webgui.ui.workingareas.Internationalizationable;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.selectedpattern.model.SearchPatternFieldGroup;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/5/13
 * Time: 5:45 PM
 *
 */
public class SelectedSearchPatternPanel extends RapidPanel implements Componentssetable, Internationalizationable {

    private MainPattyUI ui;
    private SearchPattern searchPattern;
    private ResourceBundle messagesBundle;

    private FormLayout searchPatternLayout;

    private SearchPatternFieldGroup searchPatternFieldGroup;

    public SelectedSearchPatternPanel(final MainPattyUI ui, final SearchPattern searchPattern) {
        getContentLayout().setMargin(new MarginInfo(false, true, false, true));
        getContentLayout().setSpacing(false);
        this.ui = ui;
        this.searchPattern = searchPattern;
        messagesBundle = ui.getResourceBundle();
        searchPatternLayout = new FormLayout();
        build();
        setComponents();
        doInternationalization();
    }

    public void build() {
        searchPatternLayout.removeAllComponents();
        if (searchPattern != null) {
            searchPatternFieldGroup = new SearchPatternFieldGroup(ui, searchPattern);
            for (final Field<?> field : searchPatternFieldGroup.getFields()) {
                field.setReadOnly(true);
            }
            searchPatternLayout.addComponent(searchPatternFieldGroup.getField(SearchPattern.TITLE));
            final TextArea area = (TextArea) searchPatternFieldGroup.getField(SearchPattern.DESCR);
            searchPatternLayout.addComponent(area);
            searchPatternLayout.addComponent(searchPatternFieldGroup.getField(SearchPattern.CONTENT));
            searchPatternLayout.addComponent(searchPatternFieldGroup.getRating());
            searchPatternLayout.addComponent(searchPatternFieldGroup.getField(SearchPattern.ALLTIMEVOTES));
            searchPatternLayout.addComponent(searchPatternFieldGroup.getField(SearchPattern.AUTHOR));
        } else {
            searchPatternLayout.addComponent(new Label(messagesBundle.getString("nosearchpatternselected")));
        }

    }

    @Override
    public void setComponents() {
        addComponent(searchPatternLayout);
    }

    @Override
    public void doInternationalization() {
        setCaption(searchPattern.getTitle());
    }

    public void setSearchPattern(final SearchPattern searchPattern) {
        this.searchPattern = searchPattern;
    }

    public SearchPattern getSearchPattern() {
        return searchPattern;
    }
}
