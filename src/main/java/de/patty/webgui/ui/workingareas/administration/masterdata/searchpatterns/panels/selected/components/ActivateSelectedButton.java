package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.panels.selected.components;

import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.logic.SearchPatternActivateOrUnactivateCommand;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.model.UnActivateEnum;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.uicomponents.SearchPatternsTable;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/22/13
 * Time: 12:07 PM
 * Button which executes a command to activate the selected {@link de.patty.persistence.entities.SearchPattern}s in the
 * {@link SearchPatternsTable}.
 */
public class ActivateSelectedButton extends SelectedButton {

    /**
     * Instantiates a new ActivateSelectedButton.
     *
     * @param table the table
     * @param ui the ui
     */
    public ActivateSelectedButton(final SearchPatternsTable table, final MainPattyUI ui) {
        super(table, ui);
        setCaption(messages.getString("activateselected"));
    }

    @Override
    protected void addClickListener() {
        addClickListener(new ClickListener() {
            @Override
            public void buttonClick(ClickEvent event) {
                final SearchPatternActivateOrUnactivateCommand activateOrUnactivateCommand = new SearchPatternActivateOrUnactivateCommand(ui) {
                    @Override
                    public void setCommandType() {
                        unActivateEnum = UnActivateEnum.ACTIVATE;
                    }
                };
                activateOrUnactivateCommand.execute(table.getSelectedSearchPatterns());
            }
        });
    }
}
