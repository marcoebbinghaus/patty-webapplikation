package de.patty.webgui.ui.workingareas.administration.configuration.model;

import com.vaadin.data.Container;
import com.vaadin.ui.*;

import static de.patty.webgui.ui.workingareas.administration.configuration.model.CrawlerParameter.*;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/4/13
 * Time: 5:51 PM
 *
 * FieldFactory for the "edit selected" mode for the table which contains the crawler parameters. Only the "overwritten"-cell
 * of the selected row is editable.
 */
public class EditSelectedParamFieldFactory extends AbstractFieldFactory implements TableFieldFactory {

    private CrawlerParameter selectedCrawlerParameter;

    /**
     * Instantiates a new EditSelectedParamFieldFactory.
     *
     * @param selectedCrawlerParameter the selected CrawlerParameter
     */
    public EditSelectedParamFieldFactory(final CrawlerParameter selectedCrawlerParameter) {
        this.selectedCrawlerParameter = selectedCrawlerParameter;
    }

    @Override
    public Field<?> createField(Container container, Object itemId, Object propertyId, Component uiContext) {
        CrawlerParameter parameter = (CrawlerParameter) itemId;
        switch (propertyId.toString()) {
            case NAME:
                return null;
            case DESCR:
                return null;
            case DEFAULT_VALUE:
                return null;
            case USER_VALUE:
                if (selectedCrawlerParameter != null) {
                    if (selectedCrawlerParameter.equals(parameter)) {
                        if (parameter.getDefaultValue().getClass() == Boolean.class) {
                            return new CheckBox();
                        }
                        if (parameter.getDefaultValue().getClass() == Integer.class) {
                            final TextField integerTextField = new TextField();
                            configureField(integerTextField, Integer.class);
                            return integerTextField;
                        }
                        if (parameter.getDefaultValue().getClass() == Double.class) {
                            final TextField doubleTextField = new TextField();
                            configureField(doubleTextField, Double.class);
                            return doubleTextField;
                        }
                        if (parameter.getDefaultValue().getClass() == String.class) {
                            final TextField textField = new TextField();
                            configureField(textField, String.class);
                            return textField;
                        }
                    }
                } else {
                    return null;
                }
            default:
                return null;
        }
    }
}
