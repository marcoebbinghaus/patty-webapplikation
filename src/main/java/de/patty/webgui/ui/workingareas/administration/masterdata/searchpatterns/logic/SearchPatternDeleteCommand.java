package de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.logic;

import com.vaadin.ui.Notification;
import de.patty.persistence.entities.SearchPattern;
import de.patty.persistence.entitymanagers.SearchPatternManager;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.ConfirmDialog;
import de.patty.webgui.ui.workingareas.search.model.exceptions.UndeletableSearchPatternException;

import java.util.Iterator;
import java.util.Set;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/17/13
 * Time: 3:44 PM
 *
 * This command is for deleting one or multiple {@link SearchPattern}s.
 */
public class SearchPatternDeleteCommand extends SearchPatternCommand {

    /**
     * Instantiates a new SearchPatternDeleteCommand.
     *
     * @param ui the ui
     */
    public SearchPatternDeleteCommand(MainPattyUI ui) {
        super(ui);
    }

    @Override
    public void execute(final SearchPattern searchPattern) {
        final ConfirmDialog confirmDialog = new ConfirmDialog(searchPattern.getTitle() + ": " + ui.getResourceBundle().getString("deleteconfirm"), ui) {
            @Override
            public void doThisOnOK() {
                try {
                    final SearchPatternManager searchPatternManager = new SearchPatternManager();
                    searchPatternManager.deleteSearchPattern(searchPattern);
                    ui.getScreen().reload();
                } catch (final UndeletableSearchPatternException e) {
                    Notification.show("error_undeletablesearchpattern");
                }
            }

            @Override
            public void doThisOnCancel() {
                //nothing
            }

            @Override
            public void setComponents() {
                //nothing
            }

            @Override
            public void doInternationalization() {
                //nothing
            }
        };
        confirmDialog.getButtonBar().activateShortcuts(true);
        confirmDialog.getButtonBar().setLinkStyle(true);
        ui.addWindow(confirmDialog);
    }

    public void execute(final Set<SearchPattern> searchPatterns) {
        final Iterator<SearchPattern> searchPatternIterator = searchPatterns.iterator();
        if (searchPatterns.size() == 1) {
            while (searchPatternIterator.hasNext()) {
                final SearchPattern searchPattern = searchPatternIterator.next();
                execute(searchPattern);
            }
        } else {
            final StringBuilder confirmDialogMessage = new StringBuilder();
            confirmDialogMessage.append(searchPatterns.size() + " " + ui.getResourceBundle().getString("searchpatterns"));
            confirmDialogMessage.append(": " + ui.getResourceBundle().getString("deleteconfirm"));
            final ConfirmDialog confirmDialog = new ConfirmDialog(confirmDialogMessage.toString(), ui) {
                @Override
                public void doThisOnOK() {
                    boolean allDeleted = true;
                    final SearchPatternManager searchPatternManager = new SearchPatternManager();
                    for (final SearchPattern searchPattern : searchPatterns) {
                        try {
                            searchPatternManager.deleteSearchPattern(searchPattern);
                        } catch (final UndeletableSearchPatternException e) {
                            allDeleted = false;
                        }
                    }
                    ui.getScreen().reload();
                    if (!allDeleted) {
                        Notification.show(messagesBundle.getString("error_undeletablesearchpattern"));
                    }

                }

                @Override
                public void doThisOnCancel() {
                    //nothing
                }

                @Override
                public void setComponents() {
                    //nothing
                }

                @Override
                public void doInternationalization() {
                    //nothing
                }
            };
            confirmDialog.getButtonBar().activateShortcuts(true);
            confirmDialog.getButtonBar().setLinkStyle(true);
            ui.addWindow(confirmDialog);
        }
    }
}
