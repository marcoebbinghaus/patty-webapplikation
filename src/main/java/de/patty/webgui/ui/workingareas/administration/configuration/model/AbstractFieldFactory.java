package de.patty.webgui.ui.workingareas.administration.configuration.model;

import com.vaadin.data.util.converter.StringToDoubleConverter;
import com.vaadin.data.util.converter.StringToIntegerConverter;
import com.vaadin.data.validator.DoubleRangeValidator;
import com.vaadin.data.validator.IntegerRangeValidator;
import com.vaadin.event.FieldEvents;
import com.vaadin.ui.TableFieldFactory;
import com.vaadin.ui.TextField;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/5/13
 * Time: 12:59 PM
 *
 * Abstract FieldFactory. Contains a method which configures the textfield for every editable cell. That method
 * is used in both concrete classes {@link EditAllParamsFieldFactory} and {@link EditSelectedParamFieldFactory}
 */
public abstract class AbstractFieldFactory implements TableFieldFactory {

    /**
     * Configures the textfield for the editable cell (applys converter, validator, ...)
     *
     * @param textField the textField which is displayed in the editable cell
     * @param dataClass the class of the containing data
     */
    protected void configureField(final TextField textField, final Class<?> dataClass) {
        textField.addBlurListener(new FieldEvents.BlurListener() {
            @Override
            public void blur(FieldEvents.BlurEvent blurEvent) {
                final TextField field = (TextField) blurEvent.getComponent();
                if (field.isValid()) {
                    field.removeStyleName("redbg");
                } else {
                    field.setStyleName("redbg");
                }
            }
        });
        textField.setImmediate(true);
        textField.setNullRepresentation("");
        textField.setWidth("100%");
        textField.addFocusListener(new FieldEvents.FocusListener() {
            @Override
            public void focus(FieldEvents.FocusEvent event) {
                textField.selectAll();
            }
        });
        if (dataClass == Integer.class) {
            textField.setConverter(new StringToIntegerConverter());
            textField.addValidator(new IntegerRangeValidator("integer!", null, null));
        } else if (dataClass == Double.class) {
            textField.setConverter(new StringToDoubleConverter());
            textField.addValidator(new DoubleRangeValidator("double!", null, null));
        }
    }
}
