package de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.searchpatterns;

import com.vaadin.ui.Layout;
import com.vaadin.ui.VerticalLayout;
import de.patty.persistence.entities.SearchPattern;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.RapidPanel;
import de.patty.webgui.ui.workingareas.Componentssetable;
import de.patty.webgui.ui.workingareas.Internationalizationable;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.searchpatterns.filter.FilterLayout;
import de.patty.webgui.ui.workingareas.search.areas.searchpatternarea.selectedpattern.components.SelectedSearchPatternButtonLeiste;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/4/13
 * Time: 12:32 PM
 *
 */
public class AllSearchPatternsPanel extends RapidPanel implements Componentssetable, Internationalizationable {

    private FilterLayout filterLayout;
    private SelectedSearchPatternButtonLeiste buttonLeiste;
    private Layout searchPatternsListLayout;
    private SearchPattern selectedSearchPattern;
    //private SearchPatternsTabSheet tabSheet;

    public AllSearchPatternsPanel(final MainPattyUI ui) {
        getContentLayout().setSizeFull();
        getContentLayout().setSpacing(false);
        filterLayout = new FilterLayout(ui);
        buttonLeiste = new SelectedSearchPatternButtonLeiste(ui, getSelectedSearchPattern());
        searchPatternsListLayout = new VerticalLayout();
        searchPatternsListLayout.setSizeFull();
        setSizeFull();
        setComponents();
        doInternationalization();
    }

    @Override
    public void setComponents() {
        addComponent(filterLayout);
        addComponent(buttonLeiste);
        addComponent(searchPatternsListLayout);
        getContentLayout().setExpandRatio(searchPatternsListLayout, 1.0f);
    }

    @Override
    public void doInternationalization() {
        //do nothing
    }


    public SearchPattern getSelectedSearchPattern() {
        return selectedSearchPattern;
    }

    public Layout getSearchPatternsListLayout() {
        return searchPatternsListLayout;
    }

    public void setSelectedSearchPattern(SearchPattern selectedSearchPattern) {
        this.selectedSearchPattern = selectedSearchPattern;
    }

    public SelectedSearchPatternButtonLeiste getButtonLeiste() {
        return buttonLeiste;
    }
}
