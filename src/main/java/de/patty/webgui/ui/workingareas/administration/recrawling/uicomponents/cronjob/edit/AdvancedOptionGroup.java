package de.patty.webgui.ui.workingareas.administration.recrawling.uicomponents.cronjob.edit;

import com.vaadin.data.Property;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Label;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.VerticalLayout;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.workingareas.Componentssetable;
import de.patty.webgui.ui.workingareas.Internationalizationable;

import java.util.List;
import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/10/13
 * Time: 9:59 AM
 *
 * Subclass of {@link OptionGroup} with some extra features (e.g. a integrated checkbox to select/unselect all values)
 */
public class AdvancedOptionGroup extends VerticalLayout implements Componentssetable, Internationalizationable {

    private Label captionLabel;
    private CheckBox selectAllCheckBox;
    private MainPattyUI ui;
    private String captionKeyword;
    private ResourceBundle messages;
    private OptionGroup optionGroup;

    /**
     * Instantiates a new Advanced option group.
     *
     * @param ui the ui
     * @param captionKeyword the caption keyword (i18n)
     * @param optionGroupItems the option group items
     */
    public AdvancedOptionGroup(final MainPattyUI ui, final String captionKeyword, final List optionGroupItems) {
        this.ui = ui;
        captionLabel = new Label();
        captionLabel.setContentMode(ContentMode.HTML);
        this.captionKeyword = captionKeyword;
        messages = ui.getResourceBundle();
        selectAllCheckBox = new CheckBox();
        optionGroup = new OptionGroup("", optionGroupItems);
        optionGroup.setMultiSelect(true);
        optionGroup.setImmediate(true);
        optionGroup.addStyleName("horizontal");
        selectAllCheckBox.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent event) {
                final boolean checked = (boolean) event.getProperty().getValue();
                if (checked) {
                    for (final Object itemId : optionGroup.getItemIds()) {
                        optionGroup.select(itemId);
                    }
                } else {
                    for (final Object itemId : optionGroup.getItemIds()) {
                        optionGroup.unselect(itemId);
                    }
                }
            }
        });
        setComponents();
        doInternationalization();
    }

    @Override
    public void setComponents() {
        addComponent(captionLabel);
        addComponent(selectAllCheckBox);
        addComponent(optionGroup);
    }

    @Override
    public void doInternationalization() {
        captionLabel.setValue("<b>" + messages.getString(captionKeyword) + "</b>");
        selectAllCheckBox.setCaption(messages.getString("selectall"));
    }

    /**
     * Add value change listener.
     *
     * @param valueChangeListener the value change listener
     */
    public void addValueChangeListener(final Property.ValueChangeListener valueChangeListener) {
        optionGroup.addValueChangeListener(valueChangeListener);
    }

    /**
     * Gets value.
     *
     * @return the value
     */
    public Object getValue() {
        return optionGroup.getValue();
    }
}
