package de.patty.webgui.ui.workingareas.administration.logfiles.panels.logfiles.panels.filterpanel.model;

import com.vaadin.ui.VerticalLayout;
import de.patty.webgui.MainPattyUI;

import java.util.ResourceBundle;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/26/13
 * Time: 2:21 PM
 *
 * Abstract layout class for the different filter layouts {@link LogFileDateFilterLayout} and {@link LogFileSizeFilterLayout}.
 */
public abstract class LogFileFilterLayout extends VerticalLayout implements LogFileFilter {

    protected final ResourceBundle messages;
    protected MainPattyUI ui;

    public LogFileFilterLayout(final MainPattyUI ui) {
        this.ui = ui;
        messages = ui.getResourceBundle();
    }

    public abstract Boolean isValid();
}
