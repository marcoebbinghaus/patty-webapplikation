package de.patty.webgui.ui.workingareas.administration.configuration.model;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/4/13
 * Time: 10:23 AM
 *
 * Model class to handle the parameters from nutch-site.xml and nutch-default.xml from the crawler system in an object
 * oriented way.
 */
public class CrawlerParameter implements Comparable<CrawlerParameter> {

    public static final String NAME = "name";
    public static final String DESCR = "description";
    public static final String DEFAULT_VALUE = "defaultValue";
    public static final String USER_VALUE = "overwrittenValue";

    private String name;
    private String description;
    private Object defaultValue;
    private Object overwrittenValue;

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets default value.
     *
     * @return the default value
     */
    public Object getDefaultValue() {
        return defaultValue;
    }

    /**
     * Sets default value.
     *
     * @param defaultValue the default value
     */
    public void setDefaultValue(Object defaultValue) {
        this.defaultValue = defaultValue;
    }

    /**
     * Gets overwritten value.
     *
     * @return the overwritten value
     */
    public Object getOverwrittenValue() {
        return overwrittenValue;
    }

    /**
     * Sets overwritten value.
     *
     * @param overwrittenValue the overwritten value
     */
    public void setOverwrittenValue(Object overwrittenValue) {
        this.overwrittenValue = overwrittenValue;
    }

    /* from String.CASE_INSENSITIVE_ORDER */
    @Override
    public int compareTo(CrawlerParameter o) {
        int n1 = this.getName().length();
        int n2 = o.getName().length();
        int min = Math.min(n1, n2);
        for (int i = 0; i < min; i++) {
            char c1 = this.getName().charAt(i);
            char c2 = o.getName().charAt(i);
            if (c1 != c2) {
                c1 = Character.toUpperCase(c1);
                c2 = Character.toUpperCase(c2);
                if (c1 != c2) {
                    c1 = Character.toLowerCase(c1);
                    c2 = Character.toLowerCase(c2);
                    if (c1 != c2) {
                        // No overflow because of numeric promotion
                        return c1 - c2;
                    }
                }
            }
        }
        return n1 - n2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CrawlerParameter that = (CrawlerParameter) o;

        if (!name.equals(that.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return "CrawlerParameter{" +
                "name='" + name + '\'' +
                ", defaultValue='" + defaultValue + '\'' +
                ", overwrittenValue=" + overwrittenValue +
                '}';
    }
}
