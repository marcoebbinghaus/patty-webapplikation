package de.patty.webgui.ui.workingareas.administration.logfiles.logic;

import de.patty.persistence.pojos.LogFile;
import de.patty.webgui.MainPattyUI;

import java.util.Set;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/25/13
 * Time: 5:27 PM
 *
 * Abstract command class for the logfile commands (import, show content, download).
 */
public abstract class LogFileCommand {

    /**
     * The Ui.
     */
    protected MainPattyUI ui;

    /**
     * Instantiates a new LogFileCommand.
     *
     * @param ui the ui
     */
    public LogFileCommand(final MainPattyUI ui) {
        this.ui = ui;
    }

    /**
     * Abstract method for a command which affects a single log file.
     *
     * @param logFile the log file
     */
    public abstract void execute(final LogFile logFile);

    /**
     * Abstract method for a command which affects a list of log files (no implementation yet, for the future).
     *
     * @param logFiles the log files
     */
    public abstract void execute(final Set<LogFile> logFiles);
}
