package de.patty.webgui.ui.workingareas.administration.configuration.uicomponents;

import com.vaadin.data.Validator;
import com.vaadin.ui.Component;
import com.vaadin.ui.Field;
import com.vaadin.ui.Table;
import de.patty.rmi.managers.ConfigManager;
import de.patty.rmi.managers.NoOverwrittenParametersException;
import de.patty.webgui.ui.workingareas.administration.configuration.model.CrawlerParameter;
import de.patty.webgui.ui.workingareas.administration.configuration.model.CrawlerParameterContainer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import static de.patty.webgui.ui.workingareas.administration.configuration.model.CrawlerParameter.*;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/8/13
 * Time: 12:12 PM
 *
 * Table that contains all {@link CrawlerParameter}s.
 */
public class ParametersTable extends Table {

    private List<CrawlerParameter> crawlerParameterList;

    /**
     * Instantiates a new ParametersTable.
     *
     * @param messages the ResourceBundle (i18n)
     */
    public ParametersTable(final ResourceBundle messages) {
        super(messages.getString("configparameters"));
        final ConfigManager configManager = new ConfigManager();
        try {
            crawlerParameterList = configManager.getConfigAsParameterList(true);
        } catch (NoOverwrittenParametersException e) {
            crawlerParameterList = new ArrayList<>();
        }
        final CrawlerParameterContainer parameterContainer = new CrawlerParameterContainer(crawlerParameterList);
        setContainerDataSource(parameterContainer);
        setImmediate(true);
        setColumnCollapsingAllowed(true);
        setColumnReorderingAllowed(true);
        setSelectable(true);
        setMultiSelect(false);
        setSizeFull();
        setPageLength(getItemIds().size());
        setVisibleColumns(new String[]{NAME, DESCR, DEFAULT_VALUE, USER_VALUE});
        setColumnHeader(NAME, messages.getString("column_name"));
        setColumnHeader(DESCR, messages.getString("column_description"));
        setColumnHeader(DEFAULT_VALUE, messages.getString("column_defaultvalue"));
        setColumnHeader(USER_VALUE, messages.getString("column_uservalue"));
        setColumnExpandRatio(NAME, 0.12f);
        setColumnExpandRatio(DESCR, 0.48f);
        setColumnExpandRatio(DEFAULT_VALUE, 0.2f);
        setColumnExpandRatio(USER_VALUE, 0.2f);
        setCacheRate(100.0);
    }

    public void commit() {
        final Iterator<Component> iterator = iterator();
        while (iterator.hasNext()) {
            final Component next = iterator.next();
            if (next instanceof Field<?>) {
                final Field<?> field = (Field<?>) next;
                if (!field.isValid()) {
                    throw new Validator.InvalidValueException("invalid");
                }
            }
        }
    }

    /**
     * Gets crawler parameter list.
     *
     * @return the crawler parameter list
     */
    public List<CrawlerParameter> getCrawlerParameterList() {
        return crawlerParameterList;
    }
}
