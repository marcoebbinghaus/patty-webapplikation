package de.patty.webgui.ui.workingareas.administration.recrawling.model.enummaps;

import de.patty.webgui.ui.workingareas.administration.recrawling.model.enums.Months;

import java.util.EnumMap;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/9/13
 * Time: 3:42 PM
 * Map to link the different months from the {@link Months} enum to integer values.
 */
public class MonthsEnumMap extends EnumMap {

    public MonthsEnumMap(EnumMap<Months, Integer> m) {
        super(m);
        put(Months.JANUARY, 1);
        put(Months.FEBRUARY, 2);
        put(Months.MARCH, 3);
        put(Months.APRIL, 4);
        put(Months.MAY, 5);
        put(Months.JUNE, 6);
        put(Months.JULY, 7);
        put(Months.AUGUST, 8);
        put(Months.SEPTEMBER, 9);
        put(Months.OCTOBER, 10);
        put(Months.NOVEMBER, 11);
        put(Months.DECEMBER, 12);
    }
}
