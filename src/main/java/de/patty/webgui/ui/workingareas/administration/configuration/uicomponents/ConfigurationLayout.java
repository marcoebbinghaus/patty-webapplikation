package de.patty.webgui.ui.workingareas.administration.configuration.uicomponents;

import com.vaadin.data.Property;
import com.vaadin.data.Validator;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.ui.*;
import de.patty.rmi.DoRMILogicAndReloadScreenRunnable;
import de.patty.rmi.managers.ConfigManager;
import de.patty.rmi.managers.NoOverwrittenParametersException;
import de.patty.webgui.MainPattyUI;
import de.patty.webgui.ui.SaveCancelButtonBar;
import de.patty.webgui.ui.workingareas.administration.configuration.ConfigurationScreen;
import de.patty.webgui.ui.workingareas.administration.configuration.model.CrawlerParameter;
import de.patty.webgui.ui.workingareas.administration.configuration.model.CrawlerParameterContainer;
import de.patty.webgui.ui.workingareas.administration.configuration.model.EditAllParamsFieldFactory;
import de.patty.webgui.ui.workingareas.administration.configuration.model.ParametersTableItemClickListener;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.ResourceBundle;

import static de.patty.webgui.ui.workingareas.administration.configuration.model.CrawlerParameter.*;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/8/13
 * Time: 12:13 PM
 *
 * Layout which contains the content that is displayed in the {@link ConfigurationScreen}.
 */
public class ConfigurationLayout extends VerticalLayout {

    private static final Logger logger = Logger.getLogger(ConfigurationLayout.class);

    private ResourceBundle messages;
    private CheckBox showAllParamsMode;
    private CheckBox editAllParamsMode;
    private ParametersTable parametersTable;
    private SaveCancelButtonBar buttonLeiste;
    private ParametersTableItemClickListener itemClickListener;
    private ProgressIndicator progressIndicator;

    /**
     * Instantiates a new ConfigurationLayout.
     *
     * @param ui the ui
     */
    public ConfigurationLayout(final MainPattyUI ui) {
        setSizeFull();
        setSpacing(true);
        progressIndicator = new ProgressIndicator();
        progressIndicator.setIndeterminate(true);
        progressIndicator.setEnabled(true);
        progressIndicator.setVisible(false);
        messages = ui.getResourceBundle();
        parametersTable = new ParametersTable(messages);
        showAllParamsMode = new CheckBox(messages.getString("showallmode"), false);
        showAllParamsMode.setImmediate(true);
        showAllParamsMode.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                try {
                    final Boolean activated = (Boolean) valueChangeEvent.getProperty().getValue();
                    final ConfigManager manager = new ConfigManager();
                    List<CrawlerParameter> parameters = null;
                    if (activated) {
                        parameters = manager.getConfigAsParameterList(false);
                    } else {
                        parameters = manager.getConfigAsParameterList(true);
                    }
                    final CrawlerParameterContainer container = new CrawlerParameterContainer(parameters);
                    parametersTable.setContainerDataSource(container);
                    parametersTable.setVisibleColumns(new String[]{NAME, DESCR, DEFAULT_VALUE, USER_VALUE});
                } catch (final NoOverwrittenParametersException e) {
                    showAllParamsMode.setValue(true);
                    Notification.show(messages.getString("error_nooverwrittenparameters"));
                }
            }
        });
        editAllParamsMode = new CheckBox(messages.getString("editall"), false);
        editAllParamsMode.setImmediate(true);
        editAllParamsMode.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(Property.ValueChangeEvent valueChangeEvent) {
                final Boolean activated = (Boolean) valueChangeEvent.getProperty().getValue();
                if (activated) {
                    buttonLeiste.setVisible(true);
                    parametersTable.setTableFieldFactory(new EditAllParamsFieldFactory());
                    parametersTable.setEditable(true);
                    parametersTable.setValue(null);
                    parametersTable.setSelectable(false);
                    for (final Object listener : parametersTable.getListeners(Component.Event.class)) {
                        if (listener instanceof ItemClickEvent.ItemClickListener) {
                            parametersTable.removeItemClickListener((ItemClickEvent.ItemClickListener) listener);
                        }
                    }
                } else {
                    buttonLeiste.setVisible(false);
                    parametersTable.setSelectable(true);
                    parametersTable.setEditable(false);
                    parametersTable.addItemClickListener(itemClickListener);
                }
            }
        });
        buttonLeiste = new SaveCancelButtonBar() {

            @Override
            public void doInternationalization() {
                saveButton.setCaption(messages.getString("save"));
                cancelButton.setCaption(messages.getString("cancel"));
            }

            @Override
            public void setSaveButtonListener() {
                saveButton.addClickListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {
                        try {
                            final Thread thread = new Thread(new DoRMILogicAndReloadScreenRunnable(ui, "error_saveconfig") {
                                @Override
                                public boolean doRMI() {
                                    parametersTable.commit();
                                    final ConfigManager configManager = new ConfigManager();
                                    final boolean success = configManager.setConfig((List<CrawlerParameter>) parametersTable.getContainerDataSource().getItemIds());
                                    return success;
                                }
                            });
                            thread.start();
                            progressIndicator.setVisible(true);
                            parametersTable.setEnabled(false);
                            showAllParamsMode.setEnabled(false);
                            editAllParamsMode.setEnabled(false);
                        } catch (Validator.InvalidValueException e) {
                            Notification.show(messages.getString("invaliddata"));
                        }
                    }
                });
            }

            @Override
            public void setCancelButtonListener() {
                cancelButton.addClickListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {
                        ui.setWorkingArea(new ConfigurationScreen(ui));
                    }
                });
            }
        };
        if (parametersTable.getCrawlerParameterList().isEmpty()) {
            showAllParamsMode.setValue(true);
        }
        itemClickListener = new ParametersTableItemClickListener(parametersTable, buttonLeiste);
        parametersTable.addItemClickListener(itemClickListener);
        buttonLeiste.activateShortcuts(true);
        buttonLeiste.setLinkStyle(true);
        buttonLeiste.setVisible(false);
        addComponent(showAllParamsMode);
        addComponent(editAllParamsMode);
        addComponent(progressIndicator);
        addComponent(parametersTable);
        addComponent(buttonLeiste);
        setExpandRatio(parametersTable, 1.0f);
    }


}
