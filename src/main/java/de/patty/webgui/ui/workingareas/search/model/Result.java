package de.patty.webgui.ui.workingareas.search.model;

import org.apache.solr.client.solrj.beans.Field;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/3/13
 * Time: 4:18 PM
 *
 */
public class Result {

    @Field("id")
    private String id;

    @Field("title")
    private String title;

    @Field("url")
    private String url;

    @Field("content")
    private String content;

    private String domain;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Result result = (Result) o;

        if (id != null ? !id.equals(result.id) : result.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Result{\n" +
                "id='" + id + '\n' +
                ", title='" + title + '\n' +
                ", url='" + url + '\n' +
                ", content='" + content + '\n' +
                '}';
    }
}
