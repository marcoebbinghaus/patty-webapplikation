package de.patty.webgui.ui;

import com.vaadin.ui.*;
import de.patty.webgui.BasePattyUI;
import de.patty.webgui.ui.workingareas.Componentssetable;
import de.patty.webgui.ui.workingareas.Internationalizationable;

import java.util.ResourceBundle;

public abstract class RapidWindow extends Window implements Internationalizationable, Componentssetable {


    protected ResourceBundle messagesBundle;
    protected BasePattyUI ui;
    protected ProgressIndicator progressIndicator;

    private AbstractOrderedLayout contentLayout;

    public RapidWindow(final BasePattyUI ui) {
        this.messagesBundle = ui.getResourceBundle();
        this.ui = ui;
        progressIndicator = new ProgressIndicator();
        progressIndicator.setIndeterminate(true);
        progressIndicator.setEnabled(true);
        progressIndicator.setVisible(false);
        setLayout();
        contentLayout.setSpacing(true);
        contentLayout.setMargin(true);
        setContent(contentLayout);
        setModal(true);
    }

    protected abstract void setLayout();

    public void addComponent(final Component component) {
        contentLayout.addComponent(component);
    }

    public void removeAllComponents() {
        getContentLayout().removeAllComponents();
    }

    public Layout getContentLayout() {
        return contentLayout;
    }

    public void setContentLayout(AbstractOrderedLayout contentLayout) {
        this.contentLayout = contentLayout;
    }
}
