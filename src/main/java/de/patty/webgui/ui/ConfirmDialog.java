package de.patty.webgui.ui;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import de.patty.webgui.MainPattyUI;

import java.util.ResourceBundle;

public abstract class ConfirmDialog extends RapidWindow {

    private final MainPattyUI ui;
    private final ResourceBundle messages;
    private SaveCancelButtonBar buttonBar;
    private Label textLabel;

    public ConfirmDialog(final String dialogtext, final MainPattyUI ui) {
        super(ui);
        setWidth("500px");
        this.ui = ui;
        this.messages = ui.getResourceBundle();
        setCaption(messages.getString("confirm"));
        textLabel = new Label(dialogtext);
        textLabel.setContentMode(ContentMode.HTML);
        setModal(true);
        buttonBar = new SaveCancelButtonBar() {
            @Override
            public void doInternationalization() {
                saveButton.setCaption(messages.getString("ok"));
                cancelButton.setCaption(messages.getString("cancel"));
            }

            @Override
            public void setSaveButtonListener() {
                saveButton.addClickListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent event) {
                        close();
                        doThisOnOK();
                    }
                });
            }

            @Override
            public void setCancelButtonListener() {
                cancelButton.addClickListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent event) {
                        close();
                        doThisOnCancel();
                    }
                });
            }
        };
        addComponent(textLabel);
        addComponent(buttonBar);
    }

    public void setModal(boolean b) {
        super.setModal(b);
    }

    @Override
    protected void setLayout() {
        setContentLayout(new VerticalLayout());
    }

    public abstract void doThisOnOK();

    public abstract void doThisOnCancel();

    public SaveCancelButtonBar getButtonBar() {
        return buttonBar;
    }
}
