package de.patty.webgui.ui;

import com.vaadin.event.MouseEvents;
import com.vaadin.ui.*;
import de.patty.webgui.MainPattyUI;

import java.util.Iterator;
import java.util.ResourceBundle;

public abstract class EditableLayout extends VerticalLayout {

    protected Button saveButton = new Button();
    protected Button cancelButton = new Button();
    protected ResourceBundle messages;

    protected Layout componentsLayout;
    protected HorizontalLayout buttonLayout = new HorizontalLayout();

    public EditableLayout(final MainPattyUI ui, final Panel screenPanel) {
        setLayout();
        this.setStyleName("abc");
        this.setMargin(false);
        messages = ui.getResourceBundle();
        saveButton.setCaption(messages.getString("save"));
        cancelButton.setCaption(messages.getString("cancel"));
        screenPanel.addClickListener(new MouseEvents.ClickListener() {
            @Override
            public void click(MouseEvents.ClickEvent event) {
                final Iterator<Component> componentIterator = componentsLayout.iterator();
                while (componentIterator.hasNext()) {
                    final Component component = componentIterator.next();

                    if (component instanceof Table) {
                        if (!((Table) component).isEditable()) {
                            ((Table) component).setEditable(true);
                        }
                    } else if (component instanceof AbstractField) {
                        component.setReadOnly(false);
                    }
                }
                buttonLayout.setVisible(true);
            }
        });

        buttonLayout.addComponent(saveButton);
        buttonLayout.addComponent(cancelButton);
        buttonLayout.setVisible(false);
        addComponent(componentsLayout);
        addComponent(buttonLayout);
    }


    protected abstract void buildForm();

    protected abstract void setLayout();


    public Layout getComponentsLayout() {
        return componentsLayout;
    }

    public void setComponentsLayout(Layout componentsLayout) {
        this.componentsLayout = componentsLayout;
    }

    public HorizontalLayout getButtonLayout() {
        return buttonLayout;
    }

    public void setButtonLayout(HorizontalLayout buttonLayout) {
        this.buttonLayout = buttonLayout;
    }
}
