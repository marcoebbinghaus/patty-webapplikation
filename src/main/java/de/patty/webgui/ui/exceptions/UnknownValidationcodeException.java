package de.patty.webgui.ui.exceptions;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/14/13
 * Time: 11:08 AM
 *
 * Thrown if the patty-search URL is called with an appended unknown activation code.
 */
public class UnknownValidationcodeException extends Throwable {
}
