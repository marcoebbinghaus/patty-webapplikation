package de.patty.webgui.ui.exceptions;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/14/13
 * Time: 11:23 AM
 *
 * Thrown if a user that isn't activated yet tries to log in.
 */
public class UserNotValidatedException extends Throwable {
}
