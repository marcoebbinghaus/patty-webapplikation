package de.patty.webgui.ui.exceptions;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/14/13
 * Time: 11:08 AM
 *
 * Thrown when a it is tried to validate a {@link de.patty.persistence.entities.SearchPattern} that is already validated
 * (which happens if the activation link from the activation emails is clicked more than one time).
 */
public class AlreadyValidatedException extends Throwable {
}
