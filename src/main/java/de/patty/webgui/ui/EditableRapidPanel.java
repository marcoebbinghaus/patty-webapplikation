package de.patty.webgui.ui;

import com.vaadin.event.MouseEvents;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import de.patty.webgui.ui.workingareas.Componentssetable;
import de.patty.webgui.ui.workingareas.Internationalizationable;

import java.util.ResourceBundle;

public abstract class EditableRapidPanel extends RapidPanel implements Internationalizationable, Componentssetable {

    protected Button saveButton = new Button();
    protected Button cancelButton = new Button();
    protected HorizontalLayout buttonsLayout = new HorizontalLayout();
    protected ResourceBundle messagesBundle;

    public EditableRapidPanel(final ResourceBundle messagesBundle) {
        this.messagesBundle = messagesBundle;
        turnEditableDesignOn(true);

        addClickListener(new MouseEvents.ClickListener() {
            @Override
            public void click(MouseEvents.ClickEvent event) {
                activate(true);
            }
        });
    }

    public abstract void activate(boolean b);

    @Override
    public abstract void setComponents();

    @Override
    public abstract void doInternationalization();
}
