package de.patty.webgui.ui;

import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import de.patty.webgui.BasePattyUI;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 5/28/13
 * Time: 2:14 PM
 *
 */
public class RapidInfoWindow extends RapidWindow {

    private Label infoLabel;
    private String titleKeyword;
    private String content;
    private boolean contentIsI18nKeyword;

    public RapidInfoWindow(final BasePattyUI ui, final String titleKeyword, final String content, final boolean contentIsI18nKeyword) {
        super(ui);
        this.titleKeyword = titleKeyword;
        this.content = content;
        this.contentIsI18nKeyword = contentIsI18nKeyword;
        infoLabel = new Label();
        setWidth("500px");
        setComponents();
        doInternationalization();
    }


    @Override
    public void setComponents() {
        addComponent(infoLabel);
    }

    @Override
    public void doInternationalization() {
        setCaption(messagesBundle.getString(titleKeyword));
        if (contentIsI18nKeyword) {
            infoLabel.setValue(messagesBundle.getString(content));
        } else {
            infoLabel.setValue(content);
        }
    }

    @Override
    protected void setLayout() {
        setContentLayout(new VerticalLayout());
    }
}