package de.patty.webgui.ui;

import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import de.patty.webgui.ui.workingareas.Internationalizationable;

public abstract class SaveCancelButtonBar extends HorizontalLayout implements Internationalizationable {

    protected Button saveButton = new Button();
    protected Button cancelButton = new Button();

    public SaveCancelButtonBar() {
        addComponents(saveButton, cancelButton);
        setSpacing(true);
        setSizeUndefined();
        setSaveButtonListener();
        setCancelButtonListener();
        doInternationalization();
    }

    public void activateShortcuts(final boolean b) {
        if (b) {
            saveButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);
            cancelButton.setClickShortcut(ShortcutAction.KeyCode.ESCAPE);
        } else {
            saveButton.removeClickShortcut();
            cancelButton.removeClickShortcut();
        }
    }

    public void activateCancelShortcut() {
        cancelButton.setClickShortcut(ShortcutAction.KeyCode.ESCAPE);
    }

    public void activteSaveShortcut() {
        saveButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);
    }

    public void setLinkStyle(final boolean b) {
        if (b) {
            saveButton.setStyleName("link");
            cancelButton.setStyleName("link");
        } else {
            saveButton.setStyleName("v-button");
            cancelButton.setStyleName("v-button");
        }
    }

    @Override
    public abstract void doInternationalization();

    public abstract void setSaveButtonListener();

    public abstract void setCancelButtonListener();
}
