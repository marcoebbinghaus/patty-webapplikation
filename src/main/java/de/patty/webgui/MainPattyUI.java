package de.patty.webgui;

import com.vaadin.ui.MenuBar;
import de.patty.persistence.entities.Benutzer;
import de.patty.webgui.ui.workingareas.administration.configuration.ConfigurationScreen;
import de.patty.webgui.ui.workingareas.administration.crawlerstatus.CrawlerStatusScreen;
import de.patty.webgui.ui.workingareas.administration.logfiles.LogfileScreen;
import de.patty.webgui.ui.workingareas.administration.masterdata.searchpatterns.SearchPatternsScreen;
import de.patty.webgui.ui.workingareas.administration.masterdata.urls.UrlsScreen;
import de.patty.webgui.ui.workingareas.administration.recrawling.RecrawlingScreen;
import de.patty.webgui.ui.workingareas.help.HelpScreen;
import de.patty.webgui.ui.workingareas.imprint.ImprintScreen;
import de.patty.webgui.ui.workingareas.search.SearchScreen;

import java.util.Locale;
import java.util.ResourceBundle;

import static de.patty.etc.Constants.TODO;

public class MainPattyUI extends BasePattyUI {

    @Override
    protected void initMenuBar(final MenuBar menuBar) {

        final Benutzer benutzer = getCurrentUser();
        final Long userGroupId = benutzer.getBenutzerGruppe().getId();

        messages = ResourceBundle.getBundle("MessagesBundle", locale);
        final MenuBar.MenuItem searchMenu = menuBar.addItem(messages.getString("search"), new MenuBar.Command() {
            @Override
            public void menuSelected(final MenuBar.MenuItem menuItem) {
                setWorkingArea(new SearchScreen(MainPattyUI.this));
            }
        });
        if (userGroupId >= 3) {
            final MenuBar.MenuItem administrationMenu = menuBar.addItem(messages.getString("administration"), null);
            administrationMenu.addItem(messages.getString("crawlerstatus"), new MenuBar.Command() {
                @Override
                public void menuSelected(final MenuBar.MenuItem menuItem) {
                    setWorkingArea(new CrawlerStatusScreen(MainPattyUI.this));
                }
            });
            administrationMenu.addItem(messages.getString("configuration"), new MenuBar.Command() {
                @Override
                public void menuSelected(final MenuBar.MenuItem menuItem) {
                    setWorkingArea(new ConfigurationScreen(MainPattyUI.this));
                }
            });
            administrationMenu.addItem(messages.getString("recrawling"), new MenuBar.Command() {
                @Override
                public void menuSelected(final MenuBar.MenuItem menuItem) {
                    setWorkingArea(new RecrawlingScreen(MainPattyUI.this));
                }
            });
            administrationMenu.addItem(messages.getString("logging"), new MenuBar.Command() {
                @Override
                public void menuSelected(final MenuBar.MenuItem menuItem) {
                    setWorkingArea(new LogfileScreen(MainPattyUI.this));
                }
            });
            final MenuBar.MenuItem stammdatenItem = administrationMenu.addItem(messages.getString("masterdata"), null);
            stammdatenItem.addItem(messages.getString("urlmanagement"), new MenuBar.Command() {
                @Override
                public void menuSelected(MenuBar.MenuItem selectedItem) {
                    setWorkingArea(new UrlsScreen(MainPattyUI.this));
                }
            });
            stammdatenItem.addItem(messages.getString("searchpatternmanagement"), new MenuBar.Command() {
                @Override
                public void menuSelected(MenuBar.MenuItem selectedItem) {
                    setWorkingArea(new SearchPatternsScreen(MainPattyUI.this));
                }
            });
        }

        final MenuBar.MenuItem accountMenu = menuBar.addItem(messages.getString("myaccount"), null);

        if (userGroupId >= 2) {
            accountMenu.addItem(messages.getString("details") + TODO, new MenuBar.Command() {
                @Override
                public void menuSelected(MenuBar.MenuItem selectedItem) {
                    //TODO DetailsScreen implementieren
                }
            });
        }

        accountMenu.addItem(messages.getString("logout"), new MenuBar.Command() {
            @Override
            public void menuSelected(MenuBar.MenuItem selectedItem) {
                thread.kill();
                getSession().close();
                getPage().setLocation("/");
            }
        });

        if (userGroupId >= 2) {
            accountMenu.addItem(messages.getString("delete") + TODO, new MenuBar.Command() {
                @Override
                public void menuSelected(MenuBar.MenuItem selectedItem) {
                    //TODO Loeschvorgang implementieren (evtl. mit ConfirmMailSystem)
                }
            });
        }

        final MenuBar.MenuItem imprintMenu = menuBar.addItem(messages.getString("imprint"), new MenuBar.Command() {
            @Override
            public void menuSelected(MenuBar.MenuItem menuItem) {
                setWorkingArea(new ImprintScreen(MainPattyUI.this));
            }
        });

        final MenuBar.MenuItem helpMenu = menuBar.addItem(messages.getString("help"), new MenuBar.Command() {
            @Override
            public void menuSelected(MenuBar.MenuItem menuItem) {
                setWorkingArea(new HelpScreen(MainPattyUI.this));
            }
        });

        setWorkingArea(new SearchScreen(this));

    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(final Locale locale) {
        this.locale = locale;
    }
}
