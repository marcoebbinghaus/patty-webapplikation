package de.patty.persistence.pojos;

import de.patty.etc.Constants;
import de.patty.etc.utilities.DateFromStringBuilder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/25/13
 * Time: 1:28 PM
 *
 * Plain Old Java Object for LogFiles (not a hibernate entity because they are not saved in the database but on the
 * crawler system)
 */
public class LogFile implements Comparable<LogFile> {

    private Date creationDate;
    private String fileName;
    private Long sizeInKB;
    private String content;

    /**
     * Instantiates a new Logfile.
     *
     * @param fileName the file name
     * @param fileSizeInKB the file size in kB
     */
    public LogFile(final String fileName, final Long fileSizeInKB) {
        this.fileName = fileName;
        final String fileNameWithoutEnding = fileName.substring(0, fileName.length() - 4);
        creationDate = DateFromStringBuilder.getDateFromString(fileNameWithoutEnding);
        sizeInKB = fileSizeInKB;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getSizeInKB() {
        return sizeInKB;
    }

    public void setSizeInKB(Long sizeInKB) {
        this.sizeInKB = sizeInKB;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LogFile logFile = (LogFile) o;

        if (!fileName.equals(logFile.fileName)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return fileName.hashCode();
    }

    @Override
    public String toString() {
        final DateFormat dateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
        return dateFormat.format(creationDate) + " (" + sizeInKB + " kB)";
    }

    @Override
    public int compareTo(LogFile o) {
        if (creationDate.getTime() < o.getCreationDate().getTime()) return -1;
        else if (creationDate.getTime() > o.getCreationDate().getTime()) return 1;
        else return 0;
    }
}
