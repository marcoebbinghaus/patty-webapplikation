package de.patty.persistence.dao;

import org.apache.log4j.Logger;

import javax.persistence.*;
import java.util.InputMismatchException;

/**
 * The type Dao factory.
 */
public class DaoFactory {
    private static final Logger logger = Logger.getLogger(DaoFactory.class);

    /**
     * Instantiates a new Dao factory.
     *
     * @param persistenceUnitName the persistence unit name
     */
    public DaoFactory(final String persistenceUnitName) {
        final EntityManagerFactory emf = Persistence.createEntityManagerFactory(persistenceUnitName);
        this.entityManager = emf.createEntityManager();
    }

    private EntityManager entityManager;

    /**
     * Gets entity manager.
     *
     * @return the entity manager
     */
    public EntityManager getEntityManager() {
        return this.entityManager;
    }

    /**
     * The type Transaction.
     */
    public abstract class Transaction {
        private final EntityTransaction transaction = entityManager.getTransaction();

        /**
         * Instantiates a new Transaction.
         */
        protected Transaction() {
        }

        /**
         * Do task.
         */
        public abstract void doTask();

        /**
         * Execute void.
         *
         * @throws InputMismatchException the input mismatch exception
         */
        public void execute() throws InputMismatchException {
            try {
                transaction.begin();
                doTask();
                if (transaction.isActive()) {
                    transaction.commit();
                } else {
                    logger.warn("tx nicht mehr active.. ");
                }

            } catch (PersistenceException e) {
                e.printStackTrace();
                logger.error(e);
                throw new PersistenceException();
            } finally {
//                System.out.println("e = " + e);
                if (transaction != null && transaction.isActive()) {
                    transaction.rollback();
                } else {
                }
            }

        }
    }

    /**
     * Removes an entity.
     *
     * @param entity the entity
     */
    public <T> void remove(final T entity) {
        if (logger.isInfoEnabled()) {
            logger.info("remove entity: " + entity);
        }
        if (entity == null) {
            if (logger.isInfoEnabled()) {
                logger.info("Entity war null....");
            }
        } else {
            final Class<?> aClass = entity.getClass();
            final Entity annotation = aClass.getAnnotation(Entity.class);
            //noinspection VariableNotUsedInsideIf
            if (annotation == null) {
                if (logger.isInfoEnabled()) {
                    logger.info("Obj ist keine Entity..");
                }
            } else {
                final T theentity = this.entityManager.merge(entity);
                this.entityManager.remove(theentity);
            }
        }
    }

    /**
     * Gets benutzer dAO.
     *
     * @return the benutzer dAO
     */
    public BenutzerDAO getBenutzerDAO() {
        return new BenutzerDAO(getEntityManager());
    }

    /**
     * Gets search pattern dAO.
     *
     * @return the search pattern dAO
     */
    public SearchPatternDAO getSearchPatternDAO() {
        return new SearchPatternDAO(getEntityManager());
    }

    /**
     * Gets benutzer gruppe dAO.
     *
     * @return the benutzer gruppe dAO
     */
    public BenutzerGruppeDAO getBenutzerGruppeDAO() {
        return new BenutzerGruppeDAO(getEntityManager());
    }

    /**
     * Gets safety question dAO.
     *
     * @return the safety question dAO
     */
    public SafetyQuestionDAO getSafetyQuestionDAO() {
        return new SafetyQuestionDAO(getEntityManager());
    }

}
