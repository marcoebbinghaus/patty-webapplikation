package de.patty.persistence.dao;

import de.patty.persistence.entities.Benutzer;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/13/13
 * Time: 10:16 AM
 *
 * Data Access Object for Benutzer entities.
 */
public class BenutzerDAO extends DAO<Long, Benutzer> {
    private static final Logger logger = Logger.getLogger(BenutzerDAO.class);

    /**
     * Instantiates a new BenutzerDAO.
     *
     * @param entityManager the entity manager
     */
    public BenutzerDAO(final EntityManager entityManager) {
        super(entityManager, Benutzer.class);
    }

    /**
     * Loads the user for the given loginname.
     *
     * @param loginname the loginname
     * @return the user
     */
    public Benutzer loadBenutzerForLogin(final String loginname) {
        final TypedQuery<Benutzer> typedQuery = entityManager.createQuery("select b from Benutzer" +
                "  b where b.loginname=:loginname ", Benutzer.class).setParameter("loginname", loginname);
        return getSingleResultOrNull(typedQuery);
    }

    /**
     * Loads the user by the given validationcode.
     *
     * @param validationCode the validation code
     * @return the benutzer
     */
    public Benutzer loadBenutzerByValidationcode(final String validationCode) {
        final TypedQuery<Benutzer> typedQuery = entityManager.createQuery(
                "select b from Benutzer" +
                        "  b where b.validationCode=:validationCode ",
                Benutzer.class)
                .setParameter("validationCode", validationCode);
        return getSingleResultOrNull(typedQuery);
    }
}
