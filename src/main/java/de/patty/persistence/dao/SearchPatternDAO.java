package de.patty.persistence.dao;

import de.patty.persistence.entities.SearchPattern;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 5/14/13
 * Time: 6:37 PM
 *
 */
public class SearchPatternDAO extends DAO<Long, SearchPattern> {
    private static final Logger logger = Logger.getLogger(BenutzerDAO.class);

    /**
     * Instantiates a new SearchPatternDAO.
     *
     * @param entityManager the entity manager
     */
    public SearchPatternDAO(final EntityManager entityManager) {
        super(entityManager, SearchPattern.class);
    }

    /**
     * Finds SearchPattern by its title.
     *
     * @param title the title of the SearchPattern
     * @return the searchpattern entity
     */
    public SearchPattern findByTitle(final String title) {
        final TypedQuery<SearchPattern> typedQuery = entityManager.createQuery(
                "select sp from SearchPattern sp where sp.title=:title ", SearchPattern.class).setParameter("title", title);
        return getSingleResultOrNull(typedQuery);
    }

    /**
     * Load activated search patterns.
     *
     * @return all activated search patterns
     */
    public List<SearchPattern> loadActivatedSearchPatterns() {
        final TypedQuery<SearchPattern> typedQuery = entityManager.createQuery("select s from SearchPattern s" +
                " where s.activated=true ", SearchPattern.class);
        return typedQuery.getResultList();
    }

    /**
     * Load not activated search patterns.
     *
     * @return all non-activated search patterns.
     */
    public List<SearchPattern> loadNotActivatedSearchPatterns() {
        final TypedQuery<SearchPattern> typedQuery = entityManager.createQuery("select s from SearchPattern s" +
                " where s.activated=false ", SearchPattern.class);
        return typedQuery.getResultList();
    }
}