package de.patty.persistence.dao;

import de.patty.persistence.entities.BenutzerGruppe;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/13/13
 * Time: 10:16 AM
 *
 * Data Access Object for BenutzerGruppe entities.
 */
public class BenutzerGruppeDAO extends DAO<Long, BenutzerGruppe> {
    private static final Logger logger = Logger.getLogger(BenutzerGruppeDAO.class);

    /**
     * Instantiates a new BenutzergruppeDAO.
     *
     * @param entityManager the entity manager
     */
    public BenutzerGruppeDAO(final EntityManager entityManager) {
        super(entityManager, BenutzerGruppe.class);
    }

    /**
     * Loads a BenutzerGruppe by its name.
     *
     * @param benutzerGruppenName the benutzer gruppen name
     * @return the benutzer gruppe
     */
    public BenutzerGruppe loadBenutzerGruppeByName(final String benutzerGruppenName) {
        final TypedQuery<BenutzerGruppe> typedQuery = entityManager.createQuery("from BenutzerGruppe bg where bg.gruppenname=:benutzerGruppenName", BenutzerGruppe.class).setParameter("benutzerGruppenName", benutzerGruppenName);
        return getSingleResultOrNull(typedQuery);
    }
}