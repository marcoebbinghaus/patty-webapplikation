package de.patty.persistence.dao;

import de.patty.persistence.entities.SafetyQuestion;
import org.apache.log4j.Logger;

import javax.persistence.EntityManager;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/13/13
 * Time: 10:16 AM
 *
 * Data Access Object for SafetyQuestion entities.
 */
public class SafetyQuestionDAO extends DAO<Long, SafetyQuestion> {
    private static final Logger logger = Logger.getLogger(SafetyQuestionDAO.class);

    /**
     * Instantiates a new SafetyquestionDAO.
     *
     * @param entityManager the entity manager
     */
    public SafetyQuestionDAO(final EntityManager entityManager) {
        super(entityManager, SafetyQuestion.class);
    }

}
