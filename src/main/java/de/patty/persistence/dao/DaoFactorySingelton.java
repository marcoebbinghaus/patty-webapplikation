package de.patty.persistence.dao;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/13/13
 * Time: 10:16 AM
 *
 * Singleton which returns the DaoFactory for the given DataSource.
 */
public class DaoFactorySingelton {
    private static DaoFactory daoFactory = new DaoFactory("jpa");

    /**
     * Gets the instance.
     *
     * @return the instance
     */
    public static DaoFactory getInstance() {
        return daoFactory;
    }

    private DaoFactorySingelton() {
    }
}