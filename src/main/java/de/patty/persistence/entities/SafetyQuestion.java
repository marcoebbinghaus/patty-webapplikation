package de.patty.persistence.entities;

import org.apache.log4j.Logger;

import javax.persistence.*;
import java.io.Serializable;
/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/16/13
 * Time: 10:33 AM
 *
 * Entity Class: SafetyQuestion
 * Represents a SafetyQuestion entity.
 */
@Entity
public class SafetyQuestion implements Serializable {

    public static final String ID = "id";
    public static final String ENGLISH_TEXT = "englishtext";
    public static final String GERMAN_TEXT = "germantext";

    private static final Logger logger = Logger.getLogger(SafetyQuestion.class);

    @Id
    @TableGenerator(name = "PKGenSafetyQuestion",
            table = "pk_gen",
            pkColumnName = "gen_key",
            pkColumnValue = "SafetyQuestion_id",
            valueColumnName = "gen_value",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "PKGenSafetyQuestion")
    private Long id;

    @Basic
    private String englishtext;
    @Basic
    private String germantext;

    public String getEnglishtext() {
        return englishtext;
    }

    public void setEnglishtext(String englishtext) {
        this.englishtext = englishtext;
    }

    public String getGermantext() {
        return germantext;
    }

    public void setGermantext(String germantext) {
        this.germantext = germantext;
    }
}
