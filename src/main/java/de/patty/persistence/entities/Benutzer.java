package de.patty.persistence.entities;

import org.apache.log4j.Logger;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/4/13
 * Time: 10:22 AM
 *
 * Entity Class: Benutzer
 * Represents a Benutzer entity.
 */
@Entity
public class Benutzer implements Serializable {

    public static final String ID = "id";
    public static final String HIDDEN = "hidden";
    public static final String LOGIN = "loginname";
    public static final String EMAIL = "email";
    public static final String PASSWD = "password";
    public static final String LASTLOGIN = "lastLogin";
    public static final String EMAIL_VERIFY = "emailVerification";
    public static final String PASSWORD_VERIFY = "passwordVerification";
    public static final String SAFETY_QUESTION = "safetyQuestion";
    public static final String SAFETY_ANSWER = "safetyAnswer";

    private static final Logger logger = Logger.getLogger(Benutzer.class);

    @Id
    @TableGenerator(name = "PKGenBenutzer",
            table = "pk_gen",
            pkColumnName = "gen_key",
            pkColumnValue = "Benutzer_id",
            valueColumnName = "gen_value",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "PKGenBenutzer")
    private Long id;


    @Basic
    private Boolean hidden;

    @Basic
    @NotNull
    @Size(min = 3)
    private String loginname;
    @Basic
    private String email;
    @Basic
    @NotNull //@Size(min = 8)
    private String password;
    @Basic
    private Date lastLogin;
    @Basic
    private Integer failedLogins;
    @Basic
    private Boolean validated;
    @Basic
    private Date validFrom;
    @Basic
    private Date validUntil;
    @Basic
    private String validationCode;
    @OneToOne
    private SafetyQuestion safetyQuestion;
    @Basic
    private String safetyAnswer;

    @ManyToOne(cascade = {CascadeType.REFRESH}, optional = false, fetch = FetchType.EAGER)
    private BenutzerGruppe benutzerGruppe;

    @OneToMany(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY, mappedBy = "pk.benutzer")
    private List<BenutzerSearchpattern> benutzerSearchpatterns;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getLoginname() {
        return loginname;
    }

    public void setLoginname(final String login) {
        this.loginname = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String passwd) {
        this.password = passwd;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(final Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public Integer getFailedLogins() {
        return failedLogins;
    }

    public void setFailedLogins(final Integer failedLogins) {
        this.failedLogins = failedLogins;
    }

    public Boolean getValidated() {
        return validated;
    }

    public void setValidated(Boolean validated) {
        this.validated = validated;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(final Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(final Date validUntil) {
        this.validUntil = validUntil;
    }

    public BenutzerGruppe getBenutzerGruppe() {
        return benutzerGruppe;
    }

    public void setBenutzerGruppe(final BenutzerGruppe benutzerGruppe) {
        this.benutzerGruppe = benutzerGruppe;
    }

    public List<BenutzerSearchpattern> getBenutzerSearchpatterns() {
        return benutzerSearchpatterns;
    }

    public void setBenutzerSearchpatterns(List<BenutzerSearchpattern> benutzerSearchpatterns) {
        this.benutzerSearchpatterns = benutzerSearchpatterns;
    }

    public String getValidationCode() {
        return validationCode;
    }

    public void setValidationCode(String validationCode) {
        this.validationCode = validationCode;
    }

    public SafetyQuestion getSafetyQuestion() {
        return safetyQuestion;
    }

    public void setSafetyQuestion(SafetyQuestion safetyQuestion) {
        this.safetyQuestion = safetyQuestion;
    }

    public String getSafetyAnswer() {
        return safetyAnswer;
    }

    public void setSafetyAnswer(String safetyAnswer) {
        this.safetyAnswer = safetyAnswer;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(final Boolean hidden) {
        this.hidden = hidden;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Benutzer benutzer = (Benutzer) o;

        if (!email.equals(benutzer.email)) return false;
        if (!loginname.equals(benutzer.loginname)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = loginname.hashCode();
        result = 31 * result + email.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return loginname;
    }
}
