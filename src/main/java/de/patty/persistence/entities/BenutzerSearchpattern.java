package de.patty.persistence.entities;

import javax.persistence.*;
/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/21/13
 * Time: 10:16 PM
 *
 * Entity Class: BenutzerSearchpattern
 * Needed for the association table Benutzer-SearchPatterns
 */
@Entity
@AssociationOverrides({
        @AssociationOverride(name = "pk.benutzer", joinColumns = @JoinColumn(name = "benutzer_id")),
        @AssociationOverride(name = "pk.searchpattern", joinColumns = @JoinColumn(name = "searchpattern_id"))})
public class BenutzerSearchpattern {
    @EmbeddedId
    private BenutzerSearchpatternId pk = new BenutzerSearchpatternId();
    private int rating;

    public BenutzerSearchpatternId getPk() {
        return pk;
    }

    public void setPk(BenutzerSearchpatternId pk) {
        this.pk = pk;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int marks) {
        this.rating = marks;
    }

    @Transient
    public Benutzer getBenutzer() {
        return pk.getBenutzer();
    }

    public void setBenutzer(final Benutzer benutzer) {
        pk.setBenutzer(benutzer);
    }

    @Transient
    public SearchPattern getSearchPattern() {
        return pk.getSearchPattern();
    }

    public void setSearchPattern(final SearchPattern searchPattern) {
        pk.setSearchPattern(searchPattern);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || !(obj instanceof BenutzerSearchpattern))
            return false;
        BenutzerSearchpattern that = (BenutzerSearchpattern) obj;
        if (pk != null ? !pk.equals(that.getPk()) : that.getPk() != null)
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        return (pk != null ? pk.hashCode() : 0);
    }
}