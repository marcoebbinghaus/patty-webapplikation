package de.patty.persistence.entities;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.io.Serializable;
/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/18/13
 * Time: 08:16 AM
 *
 * Needed for the association table Benutzer-SearchPatterns
 */
@Embeddable
public class BenutzerSearchpatternId implements Serializable {
    @ManyToOne(fetch = FetchType.LAZY)
    private Benutzer benutzer;
    @ManyToOne(fetch = FetchType.LAZY)
    private SearchPattern searchpattern;

    public Benutzer getBenutzer() {
        return benutzer;
    }

    public void setBenutzer(Benutzer benutzer) {
        this.benutzer = benutzer;
    }

    public SearchPattern getSearchPattern() {
        return searchpattern;
    }

    public void setSearchPattern(SearchPattern searchpattern) {
        this.searchpattern = searchpattern;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || !(obj instanceof BenutzerSearchpatternId))
            return false;
        BenutzerSearchpatternId that = (BenutzerSearchpatternId) obj;
        if (benutzer != null ? !benutzer.equals(that.getBenutzer()) : that.getBenutzer() != null)
            return false;
        if (searchpattern != null ? !searchpattern.equals(that.getSearchPattern()) : that.getSearchPattern() != null)
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result;
        result = (benutzer != null ? benutzer.hashCode() : 0);
        result = 17 * result + (searchpattern != null ? searchpattern.hashCode() : 0);
        return result;
    }
}