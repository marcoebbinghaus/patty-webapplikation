package de.patty.persistence.entities;

import org.apache.log4j.Logger;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;
/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/21/13
 * Time: 5:36 PM
 *
 * Entity Class: SearchPattern
 * Represents a SearchPattern entity.
 */
@Entity
public class SearchPattern implements Serializable, Comparable<SearchPattern> {

    private static final Logger logger = Logger.getLogger(SearchPattern.class);

    public static final String ID = "id";
    public static final String CONTENT = "content";
    public static final String TITLE = "title";
    public static final String RATING = "rating";
    public static final String ALLTIMEVOTES = "allTimeVoteCount";
    public static final String ALLTIMESCORE = "allTimeScore";
    public static final String AUTHOR = "benutzer";
    public static final String ACTIVATED = "activated";
    public static final String DESCR = "description";
    public static final String PARENT = "parent";

    @Id
    @TableGenerator(name = "PKGenSearchPattern", table = "pk_gen", pkColumnName = "gen_key",
            pkColumnValue = "SearchPattern_id", valueColumnName = "gen_value", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "PKGenSearchPattern")
    private Long id;

    @Basic(optional = false)
    private String title;
    @Basic(optional = false)
    private String content;
    @Basic(optional = false)
    private String description;
    @Basic(optional = false)
    private Boolean activated;
    @Basic(optional = false)
    private Integer allTimeVoteCount;
    @Basic(optional = false)
    private Integer allTimeScore;
    @Basic(optional = false)
    private Date creationDate;
    @ManyToOne(cascade = {CascadeType.REFRESH}, optional = false, fetch = FetchType.EAGER)
    private Benutzer benutzer;
    @OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private SearchPattern parent;
    @OneToMany(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private Set<SearchPattern> children;

    @OneToMany(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY, mappedBy = "pk.searchpattern")
    private Set<BenutzerSearchpattern> benutzerSearchpatterns;
    @Transient
    private Double rating;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(final String content) {
        this.content = content;
    }

    public Double getRating() {
        if (allTimeVoteCount == null || allTimeVoteCount < 1) {
            return -1.0;
        } else {
            return (double) allTimeScore / (double) allTimeVoteCount;
        }
    }

    public Integer getAllTimeVoteCount() {
        return allTimeVoteCount;
    }

    public void setAllTimeVoteCount(final Integer allTimeVoteCount) {
        this.allTimeVoteCount = allTimeVoteCount;
    }

    public Integer getAllTimeScore() {
        return allTimeScore;
    }

    public void setAllTimeScore(final Integer allTimeScore) {
        this.allTimeScore = allTimeScore;
    }

    public Benutzer getBenutzer() {
        return benutzer;
    }

    public void setBenutzer(Benutzer benutzer) {
        this.benutzer = benutzer;
    }

    public Set<SearchPattern> getChildren() {
        return children;
    }

    public void setChildren(Set<SearchPattern> childPlanningUnits) {
        this.children = childPlanningUnits;
    }

    public SearchPattern getParent() {
        return parent;
    }

    public void setParent(SearchPattern parent) {
        this.parent = parent;
    }

    public Set<BenutzerSearchpattern> getBenutzerSearchpatterns() {
        return benutzerSearchpatterns;
    }

    public void setBenutzerSearchpatterns(Set<BenutzerSearchpattern> benutzerSearchpatterns) {
        this.benutzerSearchpatterns = benutzerSearchpatterns;
    }

    public Boolean getActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchPattern that = (SearchPattern) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    /* from String.CASE_INSENSITIVE_ORDER */
    @Override
    public int compareTo(SearchPattern o) {
        int n1 = this.getTitle().length();
        int n2 = o.getTitle().length();
        int min = Math.min(n1, n2);
        for (int i = 0; i < min; i++) {
            char c1 = this.getTitle().charAt(i);
            char c2 = o.getTitle().charAt(i);
            if (c1 != c2) {
                c1 = Character.toUpperCase(c1);
                c2 = Character.toUpperCase(c2);
                if (c1 != c2) {
                    c1 = Character.toLowerCase(c1);
                    c2 = Character.toLowerCase(c2);
                    if (c1 != c2) {
                        // No overflow because of numeric promotion
                        return c1 - c2;
                    }
                }
            }
        }
        return n1 - n2;
    }

    @Override
    public String toString() {
        return title;
    }
}
