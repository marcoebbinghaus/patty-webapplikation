package de.patty.persistence.entitymanagers;

import de.patty.persistence.dao.DaoFactory;
import de.patty.persistence.dao.DaoFactorySingelton;
import de.patty.persistence.entities.Benutzer;
import de.patty.persistence.entities.BenutzerSearchpattern;
import de.patty.persistence.entities.SearchPattern;
import de.patty.webgui.ui.workingareas.search.model.exceptions.FatherPatternNotExistsException;
import de.patty.webgui.ui.workingareas.search.model.exceptions.TitleAlreadyExistingException;
import de.patty.webgui.ui.workingareas.search.model.exceptions.UndeletableSearchPatternException;

import javax.persistence.EntityManager;
import java.util.*;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/5/13
 * Time: 10:28 AM
 *
 * Manages SearchPatterns / The Communication with the database (saves new SearchPatterns, gets all existing SearchPatterns,
 * etc.)
 */
public class SearchPatternManager {

    private List<SearchPattern> searchPatterns;

    /**
     * Instantiates a new SearchPatternManager.
     */
    public SearchPatternManager() {
        searchPatterns = new ArrayList<>();
    }

    /**
     * Gets all existing search patterns from the database.
     *
     * @return all search patterns
     */
    public List<SearchPattern> getAllSearchPatterns() {
        searchPatterns = DaoFactorySingelton.getInstance().getSearchPatternDAO().loadAllEntities();
        return searchPatterns;
    }

    /**
     * Gets all activated search patterns from the database.
     *
     * @return the activated search patterns
     */
    public List<SearchPattern> getActivatedSearchPatterns() {
        searchPatterns = DaoFactorySingelton.getInstance().getSearchPatternDAO().loadActivatedSearchPatterns();
        return searchPatterns;
    }

    /**
     * Save a new search pattern into the database.
     *
     * @param searchPattern the search pattern
     * @throws TitleAlreadyExistingException thrown if a search pattern with the given name already exists in the database
     */
    public void saveNewSearchPattern(final SearchPattern searchPattern) throws TitleAlreadyExistingException {
        final DaoFactory daoFactory = DaoFactorySingelton.getInstance();
        if (searchPattern.getId() == null) {
            final SearchPattern alreadyExistingSearchPattern = daoFactory.getSearchPatternDAO().findByTitle(searchPattern.getTitle());
            if (alreadyExistingSearchPattern != null) {
                throw new TitleAlreadyExistingException();
            }
        }
        daoFactory.new Transaction() {
            @Override
            public void doTask() {
                final EntityManager entityManager = daoFactory.getEntityManager();
                if (searchPattern.getAllTimeScore() == null) {
                    searchPattern.setAllTimeScore(0);
                }
                if (searchPattern.getAllTimeVoteCount() == null) {
                    searchPattern.setAllTimeVoteCount(0);
                }
                if (searchPattern.getAllTimeScore() == null) {
                    searchPattern.setAllTimeScore(0);
                }
                if (searchPattern.getAllTimeVoteCount() == null) {
                    searchPattern.setAllTimeVoteCount(0);
                }
                if (searchPattern.getDescription() == null) {
                    searchPattern.setDescription("-");
                }
                if (searchPattern.getActivated() == null) {
                    searchPattern.setActivated(false);
                }
                if (searchPattern.getCreationDate() == null) {
                    searchPattern.setCreationDate(new Date());
                }
                if (searchPattern.getBenutzer().getBenutzerGruppe().getId() >= 3) {
                    searchPattern.setActivated(true);
                }
                entityManager.persist(searchPattern);
                entityManager.flush();
            }
        }.execute();
    }

    /**
     * Updates a given search pattern with the given rating.
     *
     * @param user the user which rates the search pattern
     * @param toRateSearchPattern the search pattern which is rated
     * @param vote the rating (0 to 5 stars)
     * @return the boolean
     */
    public Boolean rateSearchPattern(final Benutzer user, final SearchPattern toRateSearchPattern, final Integer vote) {
        try {
            final DaoFactory daoFactory = DaoFactorySingelton.getInstance();
            daoFactory.new Transaction() {
                @Override
                public void doTask() {
                    final EntityManager entityManager = daoFactory.getEntityManager();
                    final List<BenutzerSearchpattern> benutzerSearchpatterns = user.getBenutzerSearchpatterns();
                    boolean userAlreadyRatedThisPattern = false;
                    BenutzerSearchpattern existingBenutzerSearchpattern = null;
                    if (benutzerSearchpatterns != null) {
                        for (final BenutzerSearchpattern benutzerSearchpattern : benutzerSearchpatterns) {
                            if (benutzerSearchpattern.getSearchPattern().equals(toRateSearchPattern)) {
                                userAlreadyRatedThisPattern = true;
                                existingBenutzerSearchpattern = benutzerSearchpattern;
                            }
                        }
                    }
                    if (userAlreadyRatedThisPattern) {
                        final int oldRating = existingBenutzerSearchpattern.getRating();
                        toRateSearchPattern.setAllTimeScore(toRateSearchPattern.getAllTimeScore() - oldRating + vote);
                        existingBenutzerSearchpattern.setRating(vote);
                        entityManager.flush();
                    } else {
                        toRateSearchPattern.setAllTimeVoteCount(toRateSearchPattern.getAllTimeVoteCount() + 1);
                        toRateSearchPattern.setAllTimeScore(toRateSearchPattern.getAllTimeScore() + vote);
                        final BenutzerSearchpattern benutzerSearchpattern = new BenutzerSearchpattern();
                        benutzerSearchpattern.setBenutzer(user);
                        benutzerSearchpattern.setSearchPattern(toRateSearchPattern);
                        benutzerSearchpattern.setRating(vote);
                        entityManager.persist(benutzerSearchpattern);
                        if (user.getBenutzerSearchpatterns() == null) {
                            user.setBenutzerSearchpatterns(new ArrayList<BenutzerSearchpattern>());
                        }
                        user.getBenutzerSearchpatterns().add(benutzerSearchpattern);
                        if (toRateSearchPattern.getBenutzerSearchpatterns() == null) {
                            toRateSearchPattern.setBenutzerSearchpatterns(new HashSet<BenutzerSearchpattern>());
                        }
                        toRateSearchPattern.getBenutzerSearchpatterns().add(benutzerSearchpattern);
                        entityManager.flush();
                    }
                }
            }.execute();
        } catch (final Exception e) {
            return false;
        }
        return true;
    }

    /**
     * Deletes the search pattern.
     *
     * @param toDeleteSearchPattern the search pattern to delete
     * @throws UndeletableSearchPatternException thrown if a search pattern has children.
     */
    public void deleteSearchPattern(final SearchPattern toDeleteSearchPattern) throws UndeletableSearchPatternException {
        final DaoFactory daoFactory = DaoFactorySingelton.getInstance();
        final SearchPattern parentSearchPattern = toDeleteSearchPattern.getParent();
        final Set<SearchPattern> childrenSearchPatterns = toDeleteSearchPattern.getChildren();
        if (childrenSearchPatterns != null && !childrenSearchPatterns.isEmpty()) {
            throw new UndeletableSearchPatternException();
        }
        daoFactory.new Transaction() {
            @Override
            public void doTask() {
                final EntityManager entityManager = daoFactory.getEntityManager();
                if (parentSearchPattern != null) {
                    parentSearchPattern.getChildren().remove(toDeleteSearchPattern);
                }
                final Set<BenutzerSearchpattern> ratingsOfTheSearchPattern = toDeleteSearchPattern.getBenutzerSearchpatterns();
                if (ratingsOfTheSearchPattern != null && !ratingsOfTheSearchPattern.isEmpty()) {
                    for (final BenutzerSearchpattern benutzerSearchpattern : ratingsOfTheSearchPattern) {
                        entityManager.remove(benutzerSearchpattern);
                    }
                }
                entityManager.remove(toDeleteSearchPattern);
                entityManager.flush();
            }
        }.execute();
    }

    /**
     * Saves a new search pattern as a child.
     *
     * @param newSearchPattern the new search pattern
     * @param parent the parent
     * @throws FatherPatternNotExistsException thrown if the given parent search pattern doesn't exist
     * @throws TitleAlreadyExistingException thrown if a search pattern with the given name (title) already exists
     */
    public void saveNewSearchPattern(final SearchPattern newSearchPattern, final SearchPattern parent) throws FatherPatternNotExistsException, TitleAlreadyExistingException {
        final DaoFactory daoFactory = DaoFactorySingelton.getInstance();
        if (parent == null) {
            throw new FatherPatternNotExistsException();
        }
        if (newSearchPattern.getId() == null) {
            final SearchPattern alreadyExistingSearchPattern = daoFactory.getSearchPatternDAO().findByTitle(newSearchPattern.getTitle());
            if (alreadyExistingSearchPattern != null) {
                throw new TitleAlreadyExistingException();
            }
        }
        daoFactory.new Transaction() {
            @Override
            public void doTask() {
                final EntityManager entityManager = daoFactory.getEntityManager();
                if (newSearchPattern.getAllTimeScore() == null) {
                    newSearchPattern.setAllTimeScore(0);
                }
                if (newSearchPattern.getAllTimeVoteCount() == null) {
                    newSearchPattern.setAllTimeVoteCount(0);
                }
                if (newSearchPattern.getParent() == null) {
                    newSearchPattern.setParent(parent);
                }
                if (newSearchPattern.getDescription() == null) {
                    newSearchPattern.setDescription("-");
                }
                if (newSearchPattern.getActivated() == null) {
                    newSearchPattern.setActivated(false);
                }
                if (parent.getChildren() == null) {
                    parent.setChildren(new HashSet<SearchPattern>());
                }
                if (newSearchPattern.getCreationDate() == null) {
                    newSearchPattern.setCreationDate(new Date());
                }
                if (newSearchPattern.getBenutzer().getBenutzerGruppe().getId() >= 3) {
                    newSearchPattern.setActivated(true);
                }
                parent.getChildren().add(newSearchPattern);
                entityManager.persist(newSearchPattern);
                entityManager.persist(parent);
                entityManager.flush();
            }
        }.execute();
    }


    /**
     * Saves an edited/updated search pattern.
     *
     * @param searchPatternToEdit the search pattern to edit / update
     */
    public void saveEditedSearchPattern(final SearchPattern searchPatternToEdit) {
        final DaoFactory daoFactory = DaoFactorySingelton.getInstance();
        daoFactory.new Transaction() {
            @Override
            public void doTask() {
                final EntityManager entityManager = daoFactory.getEntityManager();
                entityManager.merge(searchPatternToEdit);
                entityManager.flush();
            }
        }.execute();
    }

    /**
     * Saves an edited/updated search pattern which had an other parent before the update.
     *
     * @param searchPatternToEdit the search pattern to edit
     * @param oldFatherSearchPattern the old father search pattern
     */
    public void saveEditedSearchPattern(final SearchPattern searchPatternToEdit, final SearchPattern oldFatherSearchPattern) {
        final DaoFactory daoFactory = DaoFactorySingelton.getInstance();
        final SearchPattern newFatherSearchPattern = searchPatternToEdit.getParent();
        daoFactory.new Transaction() {
            @Override
            public void doTask() {
                final EntityManager entityManager = daoFactory.getEntityManager();
                //Falls der neue Parent null ist, ist der alte Parent definitiv nicht null
                if (newFatherSearchPattern == null) {
                    oldFatherSearchPattern.getChildren().remove(searchPatternToEdit);
                    entityManager.merge(oldFatherSearchPattern);
                } else {
                    if (oldFatherSearchPattern != null) {
                        oldFatherSearchPattern.getChildren().remove(searchPatternToEdit);
                        entityManager.merge(oldFatherSearchPattern);
                    }
                    if (newFatherSearchPattern.getChildren() == null) {
                        newFatherSearchPattern.setChildren(new HashSet<SearchPattern>());
                    }
                    newFatherSearchPattern.getChildren().add(searchPatternToEdit);
                    entityManager.merge(newFatherSearchPattern);
                }
                searchPatternToEdit.setParent(newFatherSearchPattern);
                entityManager.merge(searchPatternToEdit);
                entityManager.flush();
            }
        }.execute();
    }

    /**
     * Gets not yet activated search patterns.
     *
     * @return the not yet activated search patterns
     */
    public List<SearchPattern> getNotYetActivatedSearchPatterns() {
        searchPatterns = DaoFactorySingelton.getInstance().getSearchPatternDAO().loadNotActivatedSearchPatterns();
        return searchPatterns;
    }

    /**
     * Activates or unactivates the search pattern (inverts the activation state).
     *
     * @param searchPattern the search pattern
     */
    public void activateOrUnactivateSearchPattern(final SearchPattern searchPattern) {
        final DaoFactory daoFactory = DaoFactorySingelton.getInstance();
        daoFactory.new Transaction() {
            @Override
            public void doTask() {
                final EntityManager entityManager = daoFactory.getEntityManager();
                final Boolean activated = searchPattern.getActivated();
                searchPattern.setActivated(!activated);
                entityManager.flush();
            }
        }.execute();
    }

    /**
     * Activates the search pattern.
     *
     * @param searchPattern the search pattern
     */
    public void activateSearchPattern(final SearchPattern searchPattern) {
        if (searchPattern.getActivated() == false) {
            final DaoFactory daoFactory = DaoFactorySingelton.getInstance();
            daoFactory.new Transaction() {
                @Override
                public void doTask() {
                    final EntityManager entityManager = daoFactory.getEntityManager();
                    searchPattern.setActivated(true);
                    entityManager.flush();
                }
            }.execute();
        }
    }

    /**
     * Unactivates the search pattern.
     *
     * @param searchPattern the search pattern
     */
    public void unactivateSearchPattern(final SearchPattern searchPattern) {
        if (searchPattern.getActivated() == true) {
            final DaoFactory daoFactory = DaoFactorySingelton.getInstance();
            daoFactory.new Transaction() {
                @Override
                public void doTask() {
                    final EntityManager entityManager = daoFactory.getEntityManager();
                    searchPattern.setActivated(false);
                    entityManager.flush();
                }
            }.execute();
        }
    }
}
