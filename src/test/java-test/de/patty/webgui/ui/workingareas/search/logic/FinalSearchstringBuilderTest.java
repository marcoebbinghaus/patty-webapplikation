package de.patty.webgui.ui.workingareas.search.logic;

import de.patty.persistence.entities.SearchPattern;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 5/16/13
 * Time: 10:55 AM
 *
 */
public class FinalSearchstringBuilderTest {

    @Test
    public void generatorCase0Test() throws MultipleSearchWordsAndSearchPatternSelectedException, QueryLanguageAndSearchPatternSelectedException {
        final String searchInput = "java";
        final SearchPattern searchPattern = null;
        final Boolean queryLanguage = false;
        final FinalSearchstringBuilder builder = new FinalSearchstringBuilder(searchInput, searchPattern, queryLanguage);
        final String resultString = builder.getFinalSearchstring();
        assertTrue(resultString.equals("content:java"));
    }

    @Test
    public void generatorCase1Test() throws MultipleSearchWordsAndSearchPatternSelectedException, QueryLanguageAndSearchPatternSelectedException {
        final String searchInput = "java";
        final SearchPattern searchPattern = new SearchPattern();
        searchPattern.setContent("content:compiler AND content:%INPUT%");
        final Boolean queryLanguage = false;
        final FinalSearchstringBuilder builder = new FinalSearchstringBuilder(searchInput, searchPattern, queryLanguage);
        final String resultString = builder.getFinalSearchstring();
        assertTrue(resultString.equals("content:compiler AND content:java"));
    }

    @Test
    public void generatorCase2Test() throws MultipleSearchWordsAndSearchPatternSelectedException, QueryLanguageAndSearchPatternSelectedException {
        final String searchInput = "title:java";
        final SearchPattern searchPattern = null;
        final Boolean queryLanguage = true;
        final FinalSearchstringBuilder builder = new FinalSearchstringBuilder(searchInput, searchPattern, queryLanguage);
        final String resultString = builder.getFinalSearchstring();
        assertTrue(resultString.equals(searchInput));
    }

    @Test(expected = QueryLanguageAndSearchPatternSelectedException.class)
    public void generatorCase3Test() throws MultipleSearchWordsAndSearchPatternSelectedException, QueryLanguageAndSearchPatternSelectedException {
        final String searchInput = "java";
        final SearchPattern searchPattern = new SearchPattern();
        searchPattern.setContent("content:compiler AND content:%INPUT%");
        final Boolean queryLanguage = true;
        final FinalSearchstringBuilder builder = new FinalSearchstringBuilder(searchInput, searchPattern, queryLanguage);
        builder.getFinalSearchstring();
    }

    @Test
    public void generatorCase4Test() throws MultipleSearchWordsAndSearchPatternSelectedException, QueryLanguageAndSearchPatternSelectedException {
        final String searchInput = "java bytecode";
        final SearchPattern searchPattern = null;
        final Boolean queryLanguage = false;
        final FinalSearchstringBuilder builder = new FinalSearchstringBuilder(searchInput, searchPattern, queryLanguage);
        final String resultString = builder.getFinalSearchstring();
        assertTrue(resultString.equals("content:java AND content:bytecode"));
    }

    @Test(expected = MultipleSearchWordsAndSearchPatternSelectedException.class)
    public void generatorCase5Test() throws MultipleSearchWordsAndSearchPatternSelectedException, QueryLanguageAndSearchPatternSelectedException {
        final String searchInput = "java bytecode";
        final SearchPattern searchPattern = new SearchPattern();
        searchPattern.setContent("content:compiler AND content:%INPUT%");
        final Boolean queryLanguage = false;
        final FinalSearchstringBuilder builder = new FinalSearchstringBuilder(searchInput, searchPattern, queryLanguage);
        builder.getFinalSearchstring();
    }

    @Test
    public void generatorCase6Test() throws MultipleSearchWordsAndSearchPatternSelectedException, QueryLanguageAndSearchPatternSelectedException {
        final String searchInput = "title:java AND content:bytecode";
        final SearchPattern searchPattern = null;
        final Boolean queryLanguage = true;
        final FinalSearchstringBuilder builder = new FinalSearchstringBuilder(searchInput, searchPattern, queryLanguage);
        final String resultString = builder.getFinalSearchstring();
        assertTrue(resultString.equals(searchInput));
    }

    @Test(expected = QueryLanguageAndSearchPatternSelectedException.class)
    public void generatorCase7Test() throws MultipleSearchWordsAndSearchPatternSelectedException, QueryLanguageAndSearchPatternSelectedException {
        final String searchInput = "content:java AND content:bytecode";
        final SearchPattern searchPattern = new SearchPattern();
        searchPattern.setContent("content:compiler AND content:%INPUT%");
        final Boolean queryLanguage = true;
        final FinalSearchstringBuilder builder = new FinalSearchstringBuilder(searchInput, searchPattern, queryLanguage);
        builder.getFinalSearchstring();
    }


}
