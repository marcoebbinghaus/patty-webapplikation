package de.patty.webgui.ui.workingareas.administration.recrawling.logic;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 7/9/13
 * Time: 12:32 PM
 *
 */
public class CronjobSplitterTest {

    @Test
    public void patternTest(){
        final String commandLine = "* 3,4-6,12 * * * machDasHier > test test";
        final CronjobSplitter cronjobSplitter = new CronjobSplitter(commandLine);
        assertTrue(cronjobSplitter.getCronjobPattern().equals("* 3,4-6,12 * * *"));
    }

    @Test
    public void commandTest(){
        final String commandLine = "* 3,4-6,12 * * * machDasHier > test test";
        final CronjobSplitter cronjobSplitter = new CronjobSplitter(commandLine);
        assertTrue(cronjobSplitter.getCronjobCommand().equals("machDasHier > test test"));
    }
}
