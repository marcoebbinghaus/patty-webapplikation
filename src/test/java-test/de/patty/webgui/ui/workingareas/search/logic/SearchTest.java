package de.patty.webgui.ui.workingareas.search.logic;

import de.patty.etc.utilities.SolrServerSingleton;
import de.patty.webgui.ui.workingareas.search.model.Result;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 6/4/13
 * Time: 9:36 AM
 *
 */
public class SearchTest {

    @Test
    public void generatorTest(){
        try {
            final SolrServer solrServer = SolrServerSingleton.getInstance();
            final SolrQuery query = new SolrQuery("content:*");
            query.setHighlight(true).setHighlightSnippets(1);
            query.setParam("hl.fl", "content,title,url");
            query.setParam("hl.fragsize", "300");
            query.setParam("hl.simple.pre", "<strong>");
            query.setParam("hl.simple.post", "</strong>");
            query.setRows(300);
//            query.setHighlight(true).setHighlightSnippets(1); //set other params as needed
//            query.setParam("hl.fl", "content,title,url");
//            query.setParam("hl.fragsize", "300");
//
            QueryResponse queryResponse = solrServer.query(query);
            final Iterator<SolrDocument> iterator = queryResponse.getResults().iterator();
            final List<Result> resultList = queryResponse.getBeans(Result.class);
            System.out.println(resultList.size());
//            Iterator<SolrDocument> iter = queryResponse.getResults().iterator();
//            List<Result> results = queryResponse.getBeans(Result.class);
//            while (iter.hasNext()) {
//                SolrDocument resultDoc = iter.next();
//                //String content = (String) resultDoc.getFieldValue("content");
//                String id = (String) resultDoc.getFieldValue("id"); //id is the uniqueKey field
//                //System.out.println("content: "+content);
//                //System.out.println("id: "+id);
//                if (queryResponse.getHighlighting().get(id) != null) {
//                    final Result tempResult = new Result();
//                    tempResult.setId(id);
//                    Result result = results.get(results.indexOf(tempResult));
//                    if(queryResponse.getHighlighting().get(id).get("content") != null)
//                        result.setContentSnippets(queryResponse.getHighlighting().get(id).get("content"));
//                    if(queryResponse.getHighlighting().get(id).get("title") != null)
//                        result.setTitleSnippets(queryResponse.getHighlighting().get(id).get("title"));
//                    if(queryResponse.getHighlighting().get(id).get("url") != null)
//                        result.setUrlSnippets(queryResponse.getHighlighting().get(id).get("url"));
////                    List<String> highlightSnippets = queryResponse.getHighlighting().get(id).get("content");
////                    for (final String highlightSnippet : highlightSnippets) {
////                        System.out.println("snippet: "+highlightSnippet);
////                    }
////                    List<String> highlightSnippets2 = queryResponse.getHighlighting().get(id).get("title");
////                    for (final String highlightSnippet : highlightSnippets2) {
////                        System.out.println("snippet: "+highlightSnippet);
////                    }
//                }
//
//
//            }
//            for (final Result result : results) {
//                System.out.println(result.toString());
//            }
        } catch (SolrServerException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        final String hallo = "Hallo";
        final String result = hallo.substring(0,1);
        System.out.println("result: "+result);

    }
}
