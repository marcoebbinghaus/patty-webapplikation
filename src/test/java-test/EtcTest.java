import de.patty.etc.utilities.LoginDataSingleton;
import de.patty.etc.utilities.PasswordEncrypter;
import de.patty.rmi.managers.ConfigManager;
import it.sauronsoftware.cron4j.SchedulingPattern;
import org.junit.Test;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.mail.MessagingException;
import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * PattySearch - www.patty-search.de
 * User: Marco Ebbinghaus (marco.ebbinghaus@rapidpm.org)
 * Date: 5/16/13
 * Time: 10:55 AM
 *
 */
public class EtcTest {

//    @Test
//    public void test1(){
//        DaoFactory daoFactory = new DaoFactory("jpa-test");
//        final URL newURL = new URL();
//        newURL.setUrl("http://www.rapidpm.org");
//        final URL urlFromDB = daoFactory.getURLDAO().findURLByURL("http://www.rapidpm.org");
//        assertFalse(urlFromDB.equals(newURL));
//    }

    @Test
    public void test2(){
        String start = "addurls ht";
        String result = start.substring(8);
        assertTrue(result.equals("ht"));
    }

    @Test
    public void stringBuilderTest() {
        StringBuilder sb = new StringBuilder();
        sb.append('c');
        sb.append('a');
        assertTrue(sb.toString().equals("ca"));
    }

    @Test
    public void cutEndingSlashTest() {
        final List<String> strings = new ArrayList<>();
        strings.add("www.google.de/");
        cutEndingSlash(0, strings);
        assertTrue(strings.get(0).equals("www.google.de"));
    }

//    @Test
//    public void ConfigToCrawlerTest() {
//        final String message = writeConfigToCrawler();
//        System.out.println(message);
//    }

    private void cutEndingSlash(int index, List<String> urlStrings) {
        final String oldUrlString = urlStrings.get(index);
        final int length = oldUrlString.length();
        final String newUrlString = oldUrlString.substring(0, length-1);
        urlStrings.set(index, newUrlString);
    }

//    private String writeConfigToCrawler(){
//        StringBuilder sb = new StringBuilder();
//        sb.append(Constants.CRAWLER_COMMAND_SETCONFIG);
//        DaoFactory daoFactory = new DaoFactory("jpa-test");
//        final List<NutchConfParameter> parameterList = daoFactory.getNutchConfParameterDAO().loadAllEntities();
//        for (final NutchConfParameter nutchConfParameter : parameterList) {
//            sb.append(" ");
//            sb.append(nutchConfParameter.getFileName());
//            sb.append(" ");
//            sb.append(nutchConfParameter.getValue());
//        }
//        return sb.toString();
//    }

    @Test
    public void castingTest(){
        Boolean b = true;
        String s = String.valueOf(b);
        byte[] bytes = s.getBytes();


        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            sb.append((char)bytes[i]);
        }
        Boolean finish = Boolean.valueOf(sb.toString());
        //System.out.println("finish: "+finish);
    }

    @Test
    public void classTest() throws NoSuchAlgorithmException, IOException {
        final MessageDigest digest = MessageDigest.getInstance("SHA-256");
        final BASE64Encoder encoder = new BASE64Encoder();
        final BASE64Decoder decoder = new BASE64Decoder();
        final byte[] salt = decoder.decodeBuffer("hg9iF");
        digest.reset();
        digest.update(salt);
        byte[] btPass = digest.digest("geheim".getBytes("UTF-8"));
        for (int i = 0; i < 5; i++) {
            digest.reset();
            btPass = digest.digest(btPass);
        }
        final String encryptedPW = encoder.encode(btPass);
        System.out.println(encryptedPW);
    }

    @Test
    public void BenutzerTest() throws UnsupportedEncodingException, NoSuchAlgorithmException, MessagingException {
//        final ResourceBundle messages = ResourceBundle.getBundle("MessagesBundle", new Locale("de", "DE"));
//        final Properties props = new Properties();
//        final Session session = Session.getDefaultInstance(props);
//        final Message message = new MimeMessage(session);
//        final InternetAddress receiverAddress = new InternetAddress("nightsleeper2001@googlemail.com");
//        final InternetAddress senderAddress = new InternetAddress("noreply@patty-search.de");
//        props.put("mail.smtp.host", "localhost");
//        message.setFrom(senderAddress);
//        message.setRecipient(Message.RecipientType.TO, receiverAddress);
//        message.setSubject(messages.getString("mail_subject"));
//        final String contentString = messages.getString("mail_content1")+"<a href=\"http://www.patty-search.de/?confirm=" +
//                "abcdef324"+"\">" + messages.getString("confirmation")+"</a>"+messages.getString("mail_content2");
//        message.setContent(contentString, "text/html");
//        Transport.send(message);
    }

    @Test
    public void MailTest2() throws UnsupportedEncodingException, NoSuchAlgorithmException, MessagingException {
//        final ResourceBundle messages = ResourceBundle.getBundle("MessagesBundle", new Locale("de", "DE"));
//        final Properties props = new Properties();
//        props.put("mail.smtps.host", Constants.MAILSERVER_URL);
//        props.put("mail.smtps.port", "465");
//        props.put("mail.smtps.user", LoginDataSingleton.getInstance().getString("send_email_account"));
//        props.put("mail.smtp.auth", "true");
//        props.put("mail.smtp.starttls.enable", "true");
//        props.put("mail.smtps.debug", "true");
//        props.put("mail.transport.protocol", "smtps");
////        props.put("mail.smtp.socketFactory.port", "465");
////        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
////        props.put("mail.smtp.socketFactory.fallback", "false");
//
//        final Session session = Session.getDefaultInstance(props, null);
//        session.setDebug(true);
//
//        final Message message = new MimeMessage(session);
//        message.setFrom(new InternetAddress("noreply@patty-search.de"));
//        message.setRecipient(Message.RecipientType.TO, new InternetAddress("nightsleeper2001@googlemail.com"));
//        message.setSubject(messages.getString("mail_subject"));
//        final String contentString = messages.getString("mail_content1")+"<a href=\"http://www.patty-search.de/?confirm=" +
//                "abcdef324"+"\">" + messages.getString("confirmation")+"</a>"+messages.getString("mail_footer");
//        message.setContent(contentString, "text/html");
//        message.saveChanges();
//        Transport transport = session.getTransport("smtp");
//        System.out.println(LoginDataSingleton.getInstance().getString("send_email_account"));
//        System.out.println(LoginDataSingleton.getInstance().getString("send_email_password"));
//        transport.connect(Constants.MAILSERVER_URL, LoginDataSingleton.getInstance().getString("send_email_account"), LoginDataSingleton.getInstance().getString("send_email_password"));
//        transport.sendMessage(message, message.getAllRecipients());
//        transport.close();
    }

    @Test
    public void encrypterTest() throws NoSuchAlgorithmException, IOException {
        final PasswordEncrypter encrypter = new PasswordEncrypter();
        System.out.println(encrypter.encrypt("geheim", LoginDataSingleton.getInstance().getString("encryption_salt")));
    }

    @Test
    public void passwordCreationTest() {
        final List<Byte> byteList = new ArrayList<>();
        for(byte i = 48; i<58; i++){
            byteList.add(i);
        }
        for(byte i = 65; i<90; i++){
            byteList.add(i);
        }
        for(byte i = 97; i<123; i++){
            byteList.add(i);
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            Collections.shuffle(byteList);
            char c = (char)((byte) byteList.get(0));
            sb.append(c);
        }
        System.out.println("Passwort: "+sb.toString());
    }

    @Test
    public void classTest2() {
        ConfigManager manager = new ConfigManager();
//        final String configString = manager.getDefaultConfigAsString();
//        final String[] namesAndValues = configString.split("\\s");
//        for (int i = 0; i < namesAndValues.length; i += 2) {
//            final String paramName = namesAndValues[i];
//            final String paramValue = namesAndValues[i+1];
//            Object paramValueAsClass = null;
//            try{
//                paramValueAsClass = Integer.valueOf(paramValue);
//            } catch( NumberFormatException e){
//                try{
//                    paramValueAsClass = Double.valueOf(paramValue);
//                } catch (NumberFormatException e1){
//                    paramValueAsClass = paramValue;
//                }
//            }
//            System.out.println(paramName+": "+paramValueAsClass.getClass().getSimpleName());
//        }
        final List<Integer> integers = new ArrayList<>();
        System.out.println(integers.getClass().getName());
    }

    @Test
    public void regexTest() throws ParseException {
        String regex = "1 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17 * * *";
        if(SchedulingPattern.validate(regex)){
            System.out.println("jawoll");
        } else {
            System.out.println("noe");
        }
    }

    @Test
    public void baf() throws ParseException, IOException {
        final BufferedReader reader = new BufferedReader(new FileReader(new File("/home/marco/programmiersprachen")));
        final StringBuilder stringBuilder = new StringBuilder();
        String line;
        while((line = reader.readLine()) != null){
            stringBuilder.append("title:"+line+" || content:"+line+" || ");
        }
        final String str = stringBuilder.toString();
        final String finalStr = str.substring(0, str.length()-4);
        System.out.println(finalStr);
        reader.close();
    }

    @Test
    public void replace(){
        final String string = "haha huhu hoho";
        final String result = string.replace("haha", "ooo");
        System.out.println("fertig");
    }


}
